<?php
if (! defined ( 'BASEPATH'  ))
	exit ( 'Não é possivel acessar diretamente.' );
class DevolucaoEmbalagens extends CI_Controller {
	
	public function index() {
	
	if ($this->session->userdata('logado') != true) {
			redirect ( 'login' );
			die();
		}
		
		$id = $this->input->post ( "cod_embalagem" );
		$data = array ();
		// Check if users are already there
		$this->load->model ( 'embalagens_model' );
		
		 $embalagens = $this->embalagens_model->get ( false );
	
		$undmedidas = $this->embalagens_model->getUndMedida ( false );

		
		$data ['embalagens'] = $embalagens;
		$data ['undmedidas'] = $undmedidas;
		
		if ($embalagens) {
			$data ['already_installed'] = true;
		} else {
			$data ['already_installed'] = false;
		}
		
		// Load View
		$data ['page_title'] = "Devolução Embalagens";
		$this->template->show ( 'devolucaoEmbalagens',$data);
	}
	
	
	public function dadosEmbalagem() {
	
		// recebo o id_cliente da view para trazer os dados somente daquele cliente
		$id = $this->input->post ( "id" );
	
		$this->load->model ( 'embalagens_model' );
	
		$embalagens = $this->embalagens_model->get ( $id );

		// como eu vou retornar os dados para a view em formato jSon, aqui eu crio os índices para serem acessados dentro da função $.post()
		$array_embalagens = array (
				"cod_embalagem" => $embalagens ['cod_embalagem'],
				"ds_embalagem" => $embalagens ['ds_embalagem'],
				"cod_undmedida" => $embalagens ['cod_undmedida'],
				"ds_undmedida" => $embalagens ['ds_undmedida'],
				"num_volume" => $embalagens ['num_volume'],
				"ds_observacao" => $embalagens ['ds_observacao'],
				"sn_devolucao" => $embalagens ['sn_devolucao']
		);
		echo json_encode ( $array_embalagens );
	}
	
	
	public function save() {

		$devolucao = 'N';
		if (array_key_exists("chk_devolucao",$this->input->post())){
			$devolucao = 'S';
		}
		
		$this->load->model ( 'embalagens_model' );
		$dados = array (
				
				'ds_embalagem' => $this->input->post ( 'ds_embalagem' ),
				'cod_undmedida' => $this->input->post ( 'cod_undmedida' ),
				'num_volume' => $this->input->post ( 'num_volume' ),
				'ds_observacao' => $this->input->post ( 'ds_observacao' ),
				'sn_devolucao' => $devolucao,
				'cod_empresa' => $this->session->userdata('codempresa')
		);

		if ($this->input->post ( "cod_embalagem" ) == "") {
			$this->embalagens_model->create ( $dados );
		} else {

			$this->embalagens_model->update ( $this->input->post ( "cod_embalagem" ) , $dados );
		}
		// Load View
		$data ['page_title'] = "Embalagens";
	
		$this->template->show ( 'embalagens', $dados );
	
		redirect ( 'embalagens' );
	}
	
	public function saveundmedida() {
	
		$this->load->model ( 'embalagens_model' );
		$dados = array (
				
				'ds_undmedida' => $this->input->post ( 'ds_undmedida' ),
				'cod_empresa' => $this->session->userdata('codempresa')
		);
	
	
		$this->embalagens_model->createundmedida ( $dados );
		
		
		$this->template->show ( 'embalagens', $dados );
	
		redirect ( 'embalagens' );
	}
	
	public function remove() {

		$id = $this->input->post ( "id" );
		// Check if users are already there
		$this->load->model ( 'embalagens_model' );
	
		$this->embalagens_model->remove ( $id );
	
		// Load View
		$data ['page_title'] = "Embalagens";
	
		$this->template->show ( 'embalagens', $data );
	
		redirect ( 'embalagens' );
	}
	
	public function listaUndMedida(){
		
		$this->load->model ( 'embalagens_model' );
		$listaundmedida = $this->embalagens_model->getUndMedidaJson();
		echo json_encode ( $listaundmedida );

	
	}
}
?>
