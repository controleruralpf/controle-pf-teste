<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'Não é possivel acessar diretamente.' );
class Contas extends CI_Controller {
	public function index() {
		if ($this->session->userdata ( 'logado' ) != true) {
			redirect ( 'login' );
			die ();
		}
		
		$id = $this->input->post ( "cod_conta" );
		$data = array ();
		// Check if users are already there
		$this->load->model ( 'contas_model' );
		
		$contas = $this->contas_model->get ( false );
		$grupoContas = $this->contas_model->getGrupoContas ( false );
		
		$data ['contas'] = $contas;
		
		$data ['grupoContas'] = $grupoContas;
		
		if ($contas) {
			$data ['already_installed'] = true;
		} else {
			$data ['already_installed'] = false;
		}
		
		// Load View
		$data ['page_title'] = "Contas";
		
		$this->template->show ( 'contas', $data );
	}
	
	public function dadosContas() {
		
		// recebo o id_cliente da view para trazer os dados somente daquele cliente
		$id = $this->input->post ( "id" );
		
		$this->load->model ( 'contas_model' );
		
		$contas = $this->contas_model->get ( $id );
		
		// como eu vou retornar os dados para a view em formato jSon, aqui eu crio os índices para serem acessados dentro da função $.post()
		$array_contas = array (
				"cod_conta" => $contas ['cod_conta'],
				"ds_conta" => $contas ['ds_conta'],
				"ds_tipo" => $contas ['ds_tipo'],
				"cod_grupoconta" => $contas ['cod_grupoconta'],
				"ds_grupoconta" => $contas ['ds_grupoconta'],
				"ds_observacao" => $contas ['ds_observacao'],
				"cod_empresa" => $contas ['cod_empresa']
		);
		echo json_encode ( $array_contas );
	}
	public function save() {
		$this->load->model ( 'contas_model' );
		
		// Receita
		$tipoConta = 'R';
		
		if (array_key_exists ( "rdb_tipoD", $this->input->post () )) {
			$tipoConta = 'D';
		}
		
		// Despesa
		$dados = array (
				'ds_conta' => $this->input->post ( 'ds_conta' ),
				'cod_grupoconta' => $this->input->post ( 'cod_grupoconta' ),
				'ds_tipo' => $tipoConta,
				'ds_observacao' => $this->input->post ( 'ds_observacao' ),
				'cod_empresa' => $this->session->userdata('codempresa')
		);
		
		if ($this->input->post ( "cod_conta" ) == "") {
			$this->contas_model->create ( $dados );
		} else {
			$this->contas_model->update ( $id, $dados );
		}
		// Load View
		$data ['page_title'] = "Contas";
		
		$this->template->show ( 'contas', $dados );
		
		redirect ( 'contas' );
	}
	public function remove($id) {
		// Check if users are already there
		$id = $this->input->post ( "id" );
		
		$this->load->model ( 'contas_model' );
		
		$this->contas_model->remove ( $id );
		
		// Load View
		$data ['page_title'] = "Contas";
		
		$this->template->show ( 'contas', $data );
		
		redirect ( 'contas' );
	}
	public function listaGrupoContas() {
		$this->load->model ( 'contas_model' );
		$listagrupoconta = $this->contas_model->getGrupoContas ();
		echo json_encode ( $listagrupoconta );
	}
}
?>
