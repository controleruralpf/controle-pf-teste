<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'Não é possivel acessar diretamente.' );
class Area extends CI_Controller {
	
	public function index() {
		
	
	if ($this->session->userdata('logado') != true) {
			redirect ( 'login' );
			die();
		}
		
		$id = $this->input->post ( "cod_area" );
		$data = array ();
		// Check if users are already there
		$this->load->model ( 'Areas_model' );
		
		$areas = $this->Areas_model->get ( false );
			
		$data ['areas'] = $areas;
		
		if ($areas) {
			$data ['already_installed'] = true;
		} else {
			$data ['already_installed'] = false;
		}
		
		// Load View
		$data ['page_title'] = "Areas";
		$this->template->show ( 'area',$data);
	}
	
	public function dadosArea() {
	
		// recebo o id_cliente da view para trazer os dados somente daquele cliente
		$id = $this->input->post ( "id" );
	
		$this->load->model ( 'areas_model' );
	
		$areas = $this->areas_model->get ( $id );

		// como eu vou retornar os dados para a view em formato jSon, aqui eu crio os índices para serem acessados dentro da função $.post()
		$array_areas = array (
				'ds_area' => $areas ['ds_area' ],
				'num_hectotal' => $areas ['num_hectotal'],
				'num_hecplanta' => $areas ['num_hecplanta'],
				'sn_areapropria' => $areas ['sn_areapropria'],
				'sn_areaativa' => $areas ['sn_areaativa'],
				'ds_lugar' => $areas ['ds_lugar'],
				'ds_observacao' => $areas ['ds_observacao'],
		);
		echo json_encode ( $array_areas );
	}
	
	
	public function save() {
	
		//$areaAtiva = 'N';
		//if (array_key_exists("sn_areaativaA",$this->input->post())){
			$areaAtiva = 'S';
		//}

		$areaPropria = 'A';
		if (array_key_exists("sn_areapropriaP",$this->input->post())){
			$areaPropria = 'P';
		}

		$this->load->model ( 'areas_model' );
	
		$dados = array (
				'ds_area' => $this->input->post ( 'ds_area' ),
				'num_hectotal' => $this->input->post ( 'num_hectotal' ),
				'num_hecplanta' => $this->input->post ( 'num_hecplanta' ),
				'sn_areapropria' =>$areaPropria,
				'sn_areaativa' => $areaAtiva,
				'ds_lugar' => $this->input->post ( 'ds_lugar' ),
				'ds_observacao' => $this->input->post ( 'ds_observacao' ),
				'cod_empresa' => $this->session->userdata('codempresa')
		);
	   
		if ($this->input->post ( "cod_area" ) == "") {
			$this->areas_model->create ( $dados );
		} else {
	
			$this->areas_model->update ( $this->input->post ( "cod_area" ) , $dados );
		}
		// Load View
		$data ['page_title'] = "Areas";
	
		$this->template->show ( 'area', $dados );
	
		redirect ( 'area' );
	}
	
	public function remove() {
		// Check if users are already there
		$id = $this->input->post ( "id" );

		$this->load->model ( 'areas_model' );
	
		$this->areas_model->remove ( $id );
	
		// Load View
		$data ['page_title'] = "Areas";
	
		$this->template->show ( 'area', $data );
	
		redirect ( 'area' );
	}
	
	
}
?>