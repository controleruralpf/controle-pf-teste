<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'Não é possivel acessar diretamente.' );
class Defensivos extends CI_Controller {
	public function index() {
		
	if ($this->session->userdata('logado') != true) {
			redirect ( 'login' );
			die();
		}
		
		$id = $this->input->post ( "cod_insumo" );
		$data = array ();
		// Check if users are already there
		$this->load->model ( 'defensivos_model' );
		
		$defensivos = $this->defensivos_model->get ( false );
		$tipoDefensivos = $this->defensivos_model->getTipoDefensivos ( false );
		
		$data ['defensivos'] = $defensivos;
		
		$data ['tipoDefensivos'] = $tipoDefensivos;
		
		if ($defensivos) {
			$data ['already_installed'] = true;
		} else {
			$data ['already_installed'] = false;
		}
		
		// Load View
		$data ['page_title'] = "Defensivos";
		
		$this->template->show ( 'insumos/defensivos', $data );
	}
	public function dadosDefensivos() {
		
		// recebo o id_cliente da view para trazer os dados somente daquele cliente
		$id = $this->input->post ( "id" );
		
		$this->load->model ( 'defensivos_model' );
		
		$defensivos = $this->defensivos_model->get ( $id );
		
		// como eu vou retornar os dados para a view em formato jSon, aqui eu crio os índices para serem acessados dentro da função $.post()
		$array_defensivos = array (
				"cod_insumo" => $defensivos ['cod_insumo'],
				"ds_descricao" => $defensivos ['ds_descricao'],
				"ds_utilizacao" => $defensivos ['ds_utilizacao'],
				"cod_tipodefensivo" => $defensivos ['cod_tipodefensivo'],
				"ds_tipodefensivo" => $defensivos ['ds_tipodefensivo'],
				"ds_observacao" => $defensivos ['ds_observacao'] 
		);
		echo json_encode ( $array_defensivos );
	}
	public function save() {
		
		
		$this->load->model ( 'defensivos_model' );
		
		if ($this->input->post ( "cod_insumo" ) == "") {
			$id = $this->defensivos_model->getMaxCodigo () + 1;
		} else {
			$id = $this->input->post ( "cod_insumo" );
		}
		
		$dados = array (
				'cod_insumo' => $id,
				'cod_categoria' => 6,
				'ds_descricao' => $this->input->post ( 'ds_descricao' ),
				'cod_tipodefensivo' => $this->input->post ( 'cod_tipodefensivo' ),
				'ds_observacao' => $this->input->post ( 'ds_observacao' ),
				'ds_utilizacao' => $this->input->post ( 'ds_utilizacao' )
				
		);
		
		if ($this->input->post ( "cod_insumo" ) == "") {
			$this->defensivos_model->create ( $dados );
		} else {
			$this->defensivos_model->update ( $id, $dados );
		}
		// Load View
		$data ['page_title'] = "Defensivos";
		
		$this->template->show ( 'insumos/defensivos', $dados );
		
		redirect ( 'defensivos' );
	}
	public function remove($id) {
		// Check if users are already there
		$id = $this->input->post ( "id" );

		$this->load->model ( 'defensivos_model' );
		
		$this->defensivos_model->remove ( $id );
		
		// Load View
		$data ['page_title'] = "Defensivos";
		
		$this->template->show ( 'insumos/defensivos', $data );
		
		redirect ( 'defensivos' );
	}
	// public function update($id) {
	
	// $data = array ();
	// // Check if users are already there
	// $this->load->model ( 'defensivos_model' );
	
	// $defensivos = $this->defensivos_model->get ( $id );
	
	// $tipoDefensivos = $this->defensivos_model->getTipoDefensivos ( false );
	
	// $data ['defensivos'] = $defensivos;
	// $data ['tipoDefensivos'] = $tipoDefensivos;
	
	// $this->template->show ( 'defensivos', $data );
	// }
	
	/*
	 * // Load View
	 * $data['page_title'] = "Login";
	 *
	 *
	 * $data['email'] = '';
	 * $data['password'] = '';
	 *
	 * // $this->load->view('login', $data);
	 * $this->template->show('login', $data);
	 * }
	 */
}
?>
