<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'Não é possivel acessar diretamente.' );
class Fornecedor extends CI_Controller {
	
	public function index() {
	
	if ($this->session->userdata('logado') != true) {
			redirect ( 'login' );
			die();
		}
		
		$id = $this->input->post ( "cod_fornecedor" );
		$data = array ();
		// Check if users are already there
		$this->load->model ( 'fornecedor_model' );
		
		$fornecedor = $this->fornecedor_model->get ( false );
			
		$data ['fornecedor'] = $fornecedor;
		
		if ($fornecedor) {
			$data ['already_installed'] = true;
		} else {
			$data ['already_installed'] = false;
		}
		
		// Load View
		$data ['page_title'] = "Fornecedor";
		$this->template->show ( 'fornecedor',$data);
	}
	
	
	public function dadosFornecedor() {
	
		// recebo o id_cliente da view para trazer os dados somente daquele cliente
		$id = $this->input->post ( "id" );
	
		$this->load->model ( 'fornecedor_model' );
	
		$fornecedor = $this->fornecedor_model->get ( $id );

		// como eu vou retornar os dados para a view em formato jSon, aqui eu crio os índices para serem acessados dentro da função $.post()
		$array_fornecedores = array (
				"cod_fornecedor" => $fornecedor ['cod_fornecedor'],
				"ds_fornecedor" => $fornecedor ['ds_fornecedor'],
				"ds_contato" => $fornecedor ['ds_contato'],
				"ds_fone" => $fornecedor ['ds_fone'],
				"ds_fone2" => $fornecedor ['ds_fone2'],
				"ds_endereco" => $fornecedor ['ds_endereco'],
				"ds_observacao" => $fornecedor ['ds_observacao']
		);
		echo json_encode ( $array_fornecedores );
	}
	
	
	public function save() {
	
	
		$this->load->model ( 'fornecedor_model' );
	
		$dados = array (
				'ds_fornecedor' => $this->input->post ( 'ds_fornecedor' ),
				'ds_contato' => $this->input->post ( 'ds_contato' ),
				'ds_fone' => $this->input->post ( 'ds_fone' ),
				'ds_fone2' => $this->input->post ( 'ds_fone2' ),
				'ds_endereco' => $this->input->post ( 'ds_endereco' ),
				'ds_observacao' => $this->input->post ( 'ds_observacao' ),
				'cod_empresa'=> $this->session->userdata('codempresa')
				
		);
	   
		if ($this->input->post ( "cod_fornecedor" ) == "") {
			$this->fornecedor_model->create ( $dados );
		} else {
	
			$this->fornecedor_model->update ( $this->input->post ( "cod_fornecedor" ) , $dados );
		}
		// Load View
		$data ['page_title'] = "Fornecedores";
	
		$this->template->show ( 'fornecedor', $dados );
	
		redirect ( 'fornecedor' );
	}
	
	public function remove() {
		// Check if users are already there
		$id = $this->input->post ( "id" );
		
		$this->load->model ( 'fornecedor_model' );
	
		$this->fornecedor_model->remove ( $id );
	
		// Load View
		$data ['page_title'] = "Fornecedor";
	
		$this->template->show ( 'fornecedor', $data );
	
		redirect ( 'fornecedor' );
	}

	public function popup(){
    
	$data = array ();
	// Check if users are already there
	$this->load->model ( 'fornecedor_model' );
	
	$culturas = $this->fornecedor_model->get ( false );
		
	$data ['fornecedor'] = $culturas;

    $this->load->view('fornecedor', $data);
	}
	
}
?>
