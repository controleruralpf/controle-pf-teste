<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'Não é possivel acessar diretamente.' );

class Cultivar extends CI_Controller {

	public function index() {
		
	if ($this->session->userdata('logado') != true) {
			redirect ( 'login' );
			die();
		}
				
	//	print_r($this->session->userdata('logado'));exit;
	
		$this->load->model ( 'login_model' );
		
		$cultivares = $this->login_model->autenticado ();
		
		$id = $this->input->post ( "cod_cultivar" );
		$data = array ();
		// Check if users are already there
		$this->load->model ( 'cultivar_model' );
		$this->load->model ( 'cultura_model' );
		
		$cultivares = $this->cultivar_model->get ( false );
	
		$culturas = $this->cultura_model->getCultura ( false );

		
		$data ['cultivares'] = $cultivares;
		$data ['culturas'] = $culturas;
		
		if ($cultivares) {
			$data ['already_installed'] = true;
		} else {
			$data ['already_installed'] = false;
		}
		
		// Load View
		$data ['page_title'] = "Cultivares";
		$this->template->show ( 'cultivar',$data);
	}
	
	
	public function dadosCultivar() {
	
		// recebo o id_cliente da view para trazer os dados somente daquele cliente
		$id = $this->input->post ( "id" );
	
		$this->load->model ( 'cultivar_model' );
	
		$cultivares = $this->cultivar_model->get ( $id );

		// como eu vou retornar os dados para a view em formato jSon, aqui eu crio os índices para serem acessados dentro da função $.post()
		$array_cultivares = array (
				"cod_cultivar" => $cultivares ['cod_cultivar'],
				"ds_cultivar" => $cultivares ['ds_cultivar'],
				"cod_cultura" => $cultivares ['cod_cultura'],
				"ds_cultura" => $cultivares ['ds_cultura'],
				"tp_crescimento" => $cultivares ['tp_crescimento'],
				"ds_tipo" => $cultivares ['ds_tipo'],
				"num_diasciclo" => $cultivares ['num_diasciclo'],
				"ds_anotacao" => $cultivares ['ds_anotacao']
		);
		echo json_encode ( $array_cultivares );
	}
	
	
	public function save() {
	
	
		$this->load->model ( 'cultivar_model' );
		$trangenica = 'N';
		//print_r($this->input->post);exit;
		
		if (array_key_exists("chk_tipo",$this->input->post())){
			$trangenica = 'S';
		}
		
		$dados = array (
				'ds_cultivar' => $this->input->post ( 'ds_cultivar' ),
				'cod_cultura' => $this->input->post ( 'cod_cultura' ),
				'tp_crescimento' => $this->input->post ( 'rdb_crescimento' ),
				'num_diasciclo' => $this->input->post ( 'num_diasciclo' ),
				'ds_anotacao' => $this->input->post ( 'ds_anotacao' ),
				'cod_empresa' => $this->session->userdata('codempresa'),
				'ds_tipo' => $trangenica
				
		);
	

		if ($this->input->post ( "cod_cultivar" ) == "") {
			$this->cultivar_model->create ( $dados );
		} else {

			$this->cultivar_model->update ( $this->input->post ( "cod_cultivar" ) , $dados );
		}
		// Load View
		$data ['page_title'] = "Cultivares";
	
		$this->template->show ( 'cultivar', $dados );
	
		redirect ( 'cultivar' );
	}
	
	public function remove() {
		// Check if users are already there
		$id = $this->input->post ( "id" );
		$this->load->model ( 'cultivar_model' );
	
		$this->cultivar_model->remove ( $id );
	
		// Load View
		$data ['page_title'] = "Cultivares";
	
		$this->template->show ( 'cultivar', $data );
	
		redirect ( 'cultivar' );
	}
	
		
	public function listaCultivar(){
		
		$this->load->model ( 'cultivar_model' );
		$cultivar = $this->cultivar_model->getCultura();
			echo json_encode ( $cultivar );

	
	}
	
	public function listaCulturas(){
	
		$this->load->model ( 'cultura_model' );
	
		$culturas = $this->cultura_model->getCultura();
	
		echo json_encode ( $culturas );

	}
	
}
?>
