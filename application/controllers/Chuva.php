<?php

if (! defined ( 'BASEPATH'  ))
	exit ( 'Não é possivel acessar diretamente.' );

class Chuva extends CI_Controller {
	
	
	public function index() {
	
	if ($this->session->userdata('logado') != true) {
			redirect ( 'login' );
			die();
		}
		
		$id = $this->input->post ( "cod_chuva" );
		$data = array ();
		// Check if users are already there
		$this->load->model ( 'chuva_model' );
		$this->load->model ( 'areas_model' );
		
		
		
		$chuvas = $this->chuva_model->get ( false );
		$areas = $this->areas_model->get ( false );
		
	
		$data ['areas'] = $areas;
		$data ['chuvas'] = $chuvas;
		
		
		if ($chuvas) {
			$data ['already_installed'] = true;
		} else {
			$data ['already_installed'] = false;
		}
		
		// Load View
		$data ['page_title'] = "Chuvas";
		$this->template->show ( 'Chuva',$data);
	}
	
	
	public function dadosChuva() {
	
		// recebo o id_cliente da view para trazer os dados somente daquele cliente
	
		$id = $this->input->post ( "id" );
		
		$this->load->model ( 'chuva_model' );
	
		$chuvas = $this->chuva_model->get ( $id );

		// como eu vou retornar os dados para a view em formato jSon, aqui eu crio os índices para serem acessados dentro da função $.post()
		$array_chuvas = array (
				"cod_chuva" => $chuvas ['cod_chuva'],
				"sn_todasarea" => $chuvas ['sn_todasarea'],
				"dt_ini" => $chuvas ['dt_ini'],
				"dt_fim" => $chuvas ['dt_fim'],
				"ds_observacao" => $chuvas ['ds_observacao']
				
		);
		
	
	
		echo json_encode ( $array_chuvas );
	}
	
	
	public function dadosChuvaArea() {
	
		
		// recebo o id_cliente da view para trazer os dados somente daquele cliente
	
		$codchuva = $this->input->post ( "id" );
	
		$this->load->model ( 'chuva_model' );
	
		$chuvasArea = $this->chuva_model->getChuvaArea ( $codchuva,null );
		

		echo json_encode ( $chuvasArea );
	}
	
	
	public function save() {

		$dataChuvaIni = $this->input->post ( 'dt_chuvaini' );
		$dataChuvaFim = $this->input->post ( 'dt_chuvafim' );
		$dataChuvaIni = date ( "Y-m-d", strtotime ( str_replace ( '/', '-', $dataChuvaIni ) ) );
		$dataChuvaFim = date ( "Y-m-d", strtotime ( str_replace ( '/', '-', $dataChuvaFim ) ) );
		
		if($dataChuvaFim != '1970-01-01') {
			$dataChuvaFim = $dataChuvaFim;
		}else{
			$dataChuvaFim = null;
		}
	
		$this->load->model ( 'chuva_model' );
		$todaArea = 'N';
		$return = 0;
	
		if($this->input->post ('chk_todaarea') === 'true'){			
			
			$todaArea = 'S';
			$return = 1;
		}
		

		$dados = array (
				'sn_todasarea' => $todaArea,
				'ds_observacao' => $this->input->post ( 'ds_observacao' ),
				'dt_ini' =>$dataChuvaIni,
				'dt_fim' => $dataChuvaFim
		);


		if ($this->input->post ( "cod_chuva" ) == "") {
			$this->chuva_model->create ( $dados );
		} else {

			$this->chuva_model->update ( $this->input->post ( "cod_chuva" ) , $dados );
		}
		
		echo  json_decode(utf8_encode($return)); EXIT;
		// Load View
		//$data ['page_title'] = "Chuvas";
	
		//$this->template->show ( 'chuva', $dados );
	
		//redirect ( 'chuva' );
	}
	
	
	
	public function saveChuvasAreas() {
	
		
		$dados = array (
				'cod_chuva' => $_POST['codchuva'],
				'cod_area' => $_POST['codarea'],
				'num_mmchuvatotal' => $_POST['mili']
		);
	
		$this->load->model ( 'chuva_model' );

		
		
		$listachuva = $this->chuva_model->getChuvaArea ( $_POST['codchuva'], $_POST['codarea']);

		//caso nao tenha resgitro para a area cria um novo
		if(count($listachuva) <= 0){ 
			if ($_POST['codchuva'] != "") {	
				$this->chuva_model->createChuvaArea ($dados);
			}
		}else{
			$this->chuva_model->updateChuvaArea ( $_POST['codchuva'], $_POST['codarea'],$dados);
		}
		
	}
	
	
	public function remove($id) {
		// Check if users are already there
		$this->load->model ( 'chuva_model' );
	
		$this->chuva_model->remove ( $id );
	
		// Load View
		$data ['page_title'] = "Chuvas";
	
		$this->template->show ( 'chuva', $data );
	
		redirect ( 'chuva' );
	}

}
?>
