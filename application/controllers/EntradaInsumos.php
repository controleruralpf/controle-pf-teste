<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'Não é possivel acessar diretamente.' );
class EntradaInsumos extends CI_Controller {
	public function index() {
		
	if ($this->session->userdata('logado') != true) {
			redirect ( 'login' );
			die();
		}
		
		$id = $this->input->post ( "cod_defensivo" );
		$data = array ();
		// Check if users are already there
		$this->load->model ( 'entradainsumos_model' );
		 $this->load->model ( 'embalagens_model' );
		$this->load->model ( 'fornecedor_model' );
		$this->load->model ( 'areas_model' );
		
		$listaEntradaInsumos = $this->entradainsumos_model->getEntradaInsumosLista ( false );
		
		$insumos = $this->entradainsumos_model->getInsumos ( false );
		
		$embalagens = $this->embalagens_model->getEmbalagens ();
		
		$fornecedores = $this->fornecedor_model->getFornecedores ();
		
		$areas = $this->areas_model->get ( false );
		
		$data ['listaEntradaInsumos'] = $listaEntradaInsumos;
		
		$data ['insumos'] = $insumos;
		
		$data ['embalagens'] = $embalagens;
		
		$data ['fornecedores'] = $fornecedores;
		
		$data ['areas'] = $areas;
		
		// if ($insumos) {
		// $data ['already_installed'] = true;
		// } else {
		// $data ['already_installed'] = false;
		// }
		
		// Load View
		$data ['page_title'] = "Entrada Insumos";
		
		$this->template->show ( 'entradaInsumos', $data );
	}
	public function save() {
		$this->load->model ( 'entradainsumos_model' );
		
		// COnverte para salvar no mysql
		$dataEntrada = $this->input->post ( 'dt_entrada' );
		$dataEntrada = date ( "Y-m-d", strtotime ( str_replace ( '/', '-', $dataEntrada ) ) );

		
		$valorUnitario = str_replace(',','.', str_replace('.','', $this->input->post ( 'vlr_unitario' )));
		$valorTotal = str_replace(',','.', str_replace('.','', $this->input->post ( 'vlr_total' )));
		$dados = array (
				'cod_insumo' => $this->input->post ( 'cod_insumo' ),
				'dt_entrada' => $dataEntrada,
				'qtd_insumo' => $this->input->post ( 'qtd_insumo' ),
				'vlr_unitario' =>$valorUnitario,
				'vlr_total' => $valorTotal,
				'ds_observacao' => $this->input->post ( 'ds_observacao' ),
				'cod_embalagem' => $this->input->post ( 'cod_embalagem' ),
				'desc_num_nota' => $this->input->post ( 'desc_num_nota' ),
				'cod_fornecedor' => $this->input->post ( 'cod_fornecedor' ), 
				'cod_area' => $this->input->post ( 'cod_area' ),
				'ds_motorista' => $this->input->post ( 'ds_motorista' ),
				'ds_placa' => $this->input->post ( 'ds_placa' ),
				 'cod_empresa' => $this->session->userdata('codempresa')
		);
		

		if ($this->input->post ( "cod_entrada" ) == "") {
			$this->entradainsumos_model->create ( $dados );
		} else {
			
			$this->entradainsumos_model->update ( $this->input->post ( "cod_entrada" ), $dados );
		}
		// Load View
		$data ['page_title'] = "Entrada de Insumos";
		
		$this->template->show ( 'entradaInsumos', $dados );
		
		redirect ( 'entradaInsumos' );
	}
	
	
	public function remove($id) {
		// Check if users are already there
		$id = $this->input->post ( "id" );
		$this->load->model ( 'entradainsumos_model' );
	
		$this->entradainsumos_model->remove ( $id );
	
		// Load View
		$data ['page_title'] = "Entrada Insumos";
	
		$this->template->show ( 'entradaInsumos', $data );
	
		redirect ( 'entradaInsumos' );
	}
	
	
	public function dadosEntradaInsumos() {
		$array_insumos = array ();
		// recebo o id_cliente da view para trazer os dados somente daquele cliente
		$id = $this->input->post ( "id" );
		
		$this->load->model ( 'entradainsumos_model' );
		
		$insumo = $this->entradainsumos_model->getInsumos ( $id );
		
		// como eu vou retornar os dados para a view em formato jSon, aqui eu crio os índices para serem acessados dentro da função $.post()
		$array_insumos = array (
				"cod_insumo" => $insumo ['cod_insumo'],
				"cod_categoria" => $insumo ['cod_categoria'],
				"ds_categoria" => $insumo ['ds_categoria'],
				"ds_descricao" => $insumo ['ds_descricao'] 
		);
		echo json_encode ( $array_insumos );
	}
	
	
	public function carregadadosEditarEntradaInsumos() {
		$dadosEntradaInsumos = array ();
		// recebo o id_cliente da view para trazer os dados somente daquele cliente
		$id = $this->input->post ( "id" );
	
		$this->load->model ( 'entradainsumos_model' );
	
		$dadosEntradaInsumos = $this->entradainsumos_model->getEntradaInsumosLista ( $id );

		$strQtdInsumo = explode(".", $dadosEntradaInsumos['qtd_insumo']);
		if($strQtdInsumo[1] == 0){
			$qtdTotalInsumo = number_format($dadosEntradaInsumos['qtd_insumo'],0);//retira as casas decimais
		}else{
			$totalInsumo = number_format($dadosEntradaInsumos['qtd_insumo'],2,',','.');//Formatação de numero
		}

	
		$dadosEntradaInsumos = array (
				
				'cod_entrada' => $dadosEntradaInsumos['cod_entrada'],
				'cod_insumo' => $dadosEntradaInsumos['cod_insumo'],
				'dt_entrada' => $dadosEntradaInsumos['dt_entrada'],
				'qtd_insumo' => $qtdTotalInsumo,
				'vlr_unitario' => number_format($dadosEntradaInsumos['vlr_unitario'],2,',','.'),			
				'vlr_total' => $dadosEntradaInsumos['vlr_total'],
				'total_entrada'=> number_format($dadosEntradaInsumos['totalEntrada'],2,',','.'), 
				'ds_observacao' => $dadosEntradaInsumos['ds_observacao'],
				'cod_embalagem' => $dadosEntradaInsumos['cod_embalagem' ],
				'ds_embalagem' => $dadosEntradaInsumos['ds_embalagem' ],
				'desc_num_nota' => $dadosEntradaInsumos['desc_num_nota'],
				'cod_fornecedor' => $dadosEntradaInsumos['cod_fornecedor'],
				'cod_undmedida' => $dadosEntradaInsumos['cod_undmedida'],
				'ds_undmedida' => $dadosEntradaInsumos['ds_undmedida'],
				'num_volume' => $dadosEntradaInsumos['num_volume'],
				"ds_categoria" => $dadosEntradaInsumos ['ds_categoria'],
				"ds_descricao" => $dadosEntradaInsumos ['ds_descricao'],
				"ds_motorista" => $dadosEntradaInsumos ['ds_motorista'],
				"ds_placa" => $dadosEntradaInsumos ['ds_placa'],
				"cod_area" => $dadosEntradaInsumos ['cod_area'],
				"ds_area" => $dadosEntradaInsumos ['ds_area']
		);

		echo json_encode ( $dadosEntradaInsumos );
	}
	
	public function getTotalInsumos($codInsumo) {
		
		$qtdEntradaInsumos = $this->entradainsumos_model->getTotalInsumos ( $codInsumo );
	
		
		$data ['qtdEntradaInsumos'] = $qtdEntradaInsumos;
	
	  echo json_encode ( $data  );
	}
	
}
?>
