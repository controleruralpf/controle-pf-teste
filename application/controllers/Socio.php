<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'Não é possivel acessar diretamente.' );
class Socio extends CI_Controller {
	
	public function index() {
	
	if ($this->session->userdata('logado') != true) {
			redirect ( 'login' );
			die();
		}
		
		$id = $this->input->post ( "cod_socio" );
		$data = array ();
		// Check if users are already there
		$this->load->model ( 'socio_model' );
		
		$socio = $this->socio_model->get ( false );
			
		$data ['socio'] = $socio;
		
		if ($socio) {
			$data ['already_installed'] = true;
		} else {
			$data ['already_installed'] = false;
		}
		
		// Load View
		$data ['page_title'] = "Sócio";
		$this->template->show ( 'socio',$data);
	}
	
	
	public function dadosSocio() {
	
		// recebo o id_cliente da view para trazer os dados somente daquele cliente
		$id = $this->input->post ( "id" );
	
		$this->load->model ( 'socio_model' );
	
		$socio = $this->fornecedor_model->get ( $id );

		// como eu vou retornar os dados para a view em formato jSon, aqui eu crio os índices para serem acessados dentro da função $.post()
		$array_socios = array (
				"cod_socio" => $socio ['cod_socio'],
				"ds_socio" => $socio ['ds_socio'],
				"ds_fonesocio" => $socio ['ds_fonesocio'],
				"percentual" => $socio ['percentual']
		);
		echo json_encode ( $array_socios );
	}
	
	
	public function save() {
	
	
		$this->load->model ( 'socio_model' );
			
		$dados = array (
				'ds_socio' => $this->input->post ( 'ds_socio' ),
				'ds_fonesocio' => $this->input->post ( 'ds_fonesocio' ),
				'percentual' => $this->input->post ( 'percentual' ),
				'cod_empresa' => $this->session->userdata('codempresa')
							
		);
	   
		if ($this->input->post ( "cod_socio" ) == "") {
			$this->socio_model->create ( $dados );
		} else {
	
			$this->socio_model->update ( $this->input->post ( "cod_socio" ) , $dados );
		}
		// Load View
		$data ['page_title'] = "Socios";
	
		$this->template->show ( 'socio', $dados );
	
		redirect ( 'socio' );
	}
	
	public function remove() {
		// Check if users are already there
		$id = $this->input->post ( "id" );
		
		$this->load->model ( 'socio_model' );
	
		$this->socio_model->remove ( $id );
	
		// Load View
		$data ['page_title'] = "Sócio";
	
		$this->template->show ( 'socio', $data );
	
		redirect ( 'socio' );
	}

/*
	public function popup(){
    
	$data = array ();
	// Check if users are already there
	$this->load->model ( 'fornecedor_model' );
	
	$culturas = $this->fornecedor_model->get ( false );
		
	$data ['fornecedor'] = $culturas;

    $this->load->view('fornecedor', $data);
	}
	*/
}
?>
