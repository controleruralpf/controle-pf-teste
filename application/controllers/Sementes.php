<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'Não é possivel acessar diretamente.' );
class Sementes extends CI_Controller {
	public function index() {
		
	if ($this->session->userdata('logado') != true) {
			redirect ( 'login' );
			die();
		}
		
		$data = array ();
		// Check if users are already there
		$this->load->model ( 'sementes_model' );
		//$this->load->model ( 'tipoDefensivo_model' );
		
		//$categoriaSementes = $this->sementes_model->getCategoriaSementes ( false );																			
		$cultivares = $this->sementes_model->getCultivares ( false );
		$sementes = $this->sementes_model->getSementes ( false );
		//$tipoDefensivos = $this->tipoDefensivo_model->get( false );
		
		
		$data ['categoriaSementes'] = 3; /*$categoriaSementes;*/
		$data ['cultivares'] = $cultivares;
		$data ['sementes'] = $sementes;
		//$data ['tipoDefensivos'] = $tipoDefensivos;
	
		// Load View
		$data ['page_title'] = "Sementes";
		
		$this->template->show ( 'insumos/sementes',$data);
	}
	
	
	

	public function save() {
		$this->load->model ( 'sementes_model' );
		$dados = array (
				'cod_categoria' => 3,
				'ds_descricao' => $this->input->post ( 'ds_insumo' ),
				'cod_cultivar' => $this->input->post ( 'cod_cultivar' ) == '' ? null : $this->input->post ( 'cod_cultivar' ) ,
				'ds_origemsemente' => $this->input->post ( 'ds_origem' ),
				'ds_observacao' => $this->input->post ( 'ds_observacao' ),
				'ds_lote' => $this->input->post ( 'ds_lote' ),
				'ds_germinacao' => $this->input->post ( 'ds_germinacao' ),
				'ds_vigor' => $this->input->post ( 'ds_vigor' ),
				'ds_peneira' => $this->input->post ( 'ds_peneira' ),
				'cod_empresa' => $this->session->userdata('codempresa') 
		);
	
		if ($this->input->post ( "cod_insumo" ) == "") {
			
			$this->sementes_model->create ( $dados );
		} else {
	
			$this->sementes_model->update ( $this->input->post ( "cod_insumo" ) , $dados );
		}
		// Load View
		$data ['page_title'] = "Sementes";
	
		$this->template->show ( 'insumos/sementes', $dados );
	
		redirect ( 'sementes' );
	}
	
	
	public function remove($id) {
		// Check if users are already there
		$id = $this->input->post ( "id" );
		
		$this->load->model ( 'sementes_model' );
	
		$this->sementes_model->remove ( $id );
	
		// Load View
		$data ['page_title'] = "Sementes";
	
		$this->template->show ( 'insumos/sementes', $data );
	
		redirect ( 'sementes' );
	}
	
	public function dadosSementes() {
	
		// recebo o id_cliente da view para trazer os dados somente daquele cliente
		
		$id = $this->input->post ( "id" );
	
		$this->load->model ( 'sementes_model' );
	
		$sementes = $this->sementes_model->getSementes ( $id );
	
		// como eu vou retornar os dados para a view em formato jSon, aqui eu crio os índices para serem acessados dentro da função $.post()
		$array_sementes = array (
				"cod_insumo" => $sementes ['cod_insumo'],
				"ds_insumo" => $sementes ['ds_descricao'],
				"cod_cultivar" => $sementes ['cod_cultivar'],
				"ds_origem" => $sementes ['ds_origemsemente'],
				"ds_observacao" => $sementes ['ds_observacao'],
				"ds_cultivar" => $sementes ['ds_cultivar'],
				"ds_lote" => $sementes ['ds_lote'],
				"ds_vigor" => $sementes ['ds_vigor'],
				"ds_germinacao" => $sementes ['ds_germinacao'],
				"ds_peneira" => $sementes ['ds_peneira']
				
				
		);
		echo json_encode ( $array_sementes );
	}
	
}
?>
