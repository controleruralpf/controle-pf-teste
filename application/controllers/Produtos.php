<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'Não é possivel acessar diretamente.' );
class Produtos extends CI_Controller {
	public function index() {
		if ($this->session->userdata ( 'logado' ) != true) {
			redirect ( 'login' );
			die ();
		}
		
		$id = $this->input->post ( "cod_produto" );
		$data = array ();
		// Check if users are already there
		$this->load->model ( 'produtos_model' );
		
		$produtos = $this->produtos_model->get ( false );
		$categoriaProdutos = $this->produtos_model->getCategoriaProdutos ( false );
		
		$data ['produtos'] = $produtos;
		
		$data ['categoriaProdutos'] = $categoriaProdutos;
		
		if ($produtos) {
			$data ['already_installed'] = true;
		} else {
			$data ['already_installed'] = false;
		}
		
		// Load View
		$data ['page_title'] = "Produtos";
		
		$this->template->show ( 'produtos', $data );
	}
	
	public function dadosProdutos() {
		
		// recebo o id_cliente da view para trazer os dados somente daquele cliente
		$id = $this->input->post ( "id" );
		
		$this->load->model ( 'produtos_model' );
		
		$produtos = $this->produtos_model->get ( $id );
		
		// como eu vou retornar os dados para a view em formato jSon, aqui eu crio os índices para serem acessados dentro da função $.post()
		$array_produtos = array (
				"cod_produto" => $produtos ['cod_produto'],
				"ds_produto" => $produtos ['ds_produto'],
				"cod_categoriaproduto" => $produtos ['cod_categoriaproduto'],
				"ds_observacao" => $produtos ['ds_observacao'],
				"cod_empresa" => $produtos ['cod_empresa']
		);
		echo json_encode ( $array_produtos );
	}
	public function save() {
		$this->load->model ( 'produtos_model' );
		
		$dados = array (
				'ds_produto' => $this->input->post ( 'ds_produto' ),
				'cod_categoriaproduto' => $this->input->post ( 'cod_categoriaproduto' ),
				'ds_observacao' => $this->input->post ( 'ds_observacao' ),
				'cod_empresa' => $this->session->userdata('codempresa')
		);
		
		if ($this->input->post ( "cod_produto" ) == "") {
			$this->produtos_model->create ( $dados );
		} else {
			$this->produtos_model->update ( $this->input->post ( "cod_produto" ), $dados );
		}
		// Load View
		$data ['page_title'] = "Produtos";
		
		$this->template->show ( 'produtos', $dados );
		
		redirect ( 'produtos' );
	}
	public function remove($id) {
		// Check if users are already there
		$id = $this->input->post ( "id" );
		
		$this->load->model ( 'produtos_model' );
		
		$this->produtos_model->remove ( $id );
		
		// Load View
		$data ['page_title'] = "Produtos";
		
		$this->template->show ( 'produtos', $data );
		
		redirect ( 'produtos' );
	}
	public function listaCategoriaProdutos() {
		$this->load->model ( 'produtos_model' );
		$listacategoriaproduto = $this->produtos_model->getCategoriaProdutos ();
		echo json_encode ( $listacategoriaproduto );
	}
}
?>
