<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'Não é possivel acessar diretamente.' );
class Safra extends CI_Controller {
	
	public function index() {
	
	if ($this->session->userdata('logado') != true) {
			redirect ( 'login' );
			die();
		}
		
		$id = $this->input->post ( "cod_safra" );
		$data = array ();
		// Check if users are already there
		$this->load->model ( 'safra_model' );
			
		$safra = $this->safra_model->get ( false );
		
		$data ['safras'] = $safra;
		
		if ($safra) {
			$data ['already_installed'] = true;
		} else {
			$data ['already_installed'] = false;
		}
		
		// Load View
		$data ['page_title'] = "Safra";
		$this->template->show ( 'safras',$data);
	}
	
	
	public function dadosSafra() {
	
		// recebo o id_cliente da view para trazer os dados somente daquele cliente
		$id = $this->input->post ( "id" );
	
		$this->load->model ( 'safra_model' );
	
		$safra = $this->safra_model->get ( $id );

		// como eu vou retornar os dados para a view em formato jSon, aqui eu crio os índices para serem acessados dentro da função $.post()
		$array_safras = array (
				"cod_safra" => $safra ['cod_safra'],
				"ds_safra" => $safra ['ds_safra'],
				"dt_inicio" => $safra ['dt_inicio'],
				"dt_fim" => $safra ['dt_fim']
		);
		echo json_encode ( $array_safras );
	}
	
	
	public function save() {
	
	
		$this->load->model ( 'safra_model' );
	
		$dados = array (
				'cod_safra' => $this->input->post ( 'cod_safra' ),
				'ds_safra' => $this->input->post ( 'ds_safra' ),
				'dt_inicio' => $this->input->post ( 'dt_inicio' ),
				'dt_fim' => $this->input->post ( 'dt_fim' ),
				'cod_empresa'=> $this->session->userdata('codempresa')
				
		);
	   
		if ($this->input->post ( "cod_safra" ) == "") {
			$this->safra_model->create ( $dados );
		} else {
	
			$this->safra_model->update ( $this->input->post ( "cod_safra" ) , $dados );
		}
		// Load View
		$data ['page_title'] = "Safras";
	
		$this->template->show ( 'safra', $dados );
	
		redirect ( 'safra' );
	}
	
	public function remove() {
		// Check if users are already there
		$id = $this->input->post ( "id" );
		
		$this->load->model ( 'safra_model' );
	
		$this->safra_model->remove ( $id );
	
		// Load View
		$data ['page_title'] = "Safras";
	
		$this->template->show ( 'safra', $data );
	
		redirect ( 'safra' );
	}

	public function popup(){
    
	$data = array ();
	// Check if users are already there
	$this->load->model ( 'safra_model' );
	
	$safras = $this->safra_model->get ( false );
		
	$data ['safra'] = $safras;

    $this->load->view('safra', $data);
	}
	
}
?>
