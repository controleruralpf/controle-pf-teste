<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'Não é possivel acessar diretamente.' );
class Adubos extends CI_Controller {
	public function index() {
		
	if ($this->session->userdata('logado') != true) {
			redirect ( 'login' );
			die();
		}
		
		$id = $this->input->post ( "cod_insumo" );
		$data = array ();
		
		// Check if users are already ther
		$this->load->model ( 'adubos_model' );
		
		$adubos = $this->adubos_model->get ( false );
		//$tipoAdubos = $this->adubos_model->getTipoAdubos ( false );
		
		$data ['adubos'] = $adubos;
		
		//$data ['tipoAdubos'] = $tipoAdubos;
		
		if ($adubos) {
			$data ['already_installed'] = true;
		} else {
			$data ['already_installed'] = false;
		}
		
		// Load View
		$data ['page_title'] = "Adubos";
		
		$this->template->show ( 'insumos/adubos', $data );
	}
	public function dadosAdubos() {
		
		// recebo o id_cliente da view para trazer os dados somente daquele cliente
		$id = $this->input->post ( "id" );
		
		$this->load->model ( 'adubos_model' );
		
		$adubos = $this->adubos_model->get ( $id );
		
		// como eu vou retornar os dados para a view em formato jSon, aqui eu crio os índices para serem acessados dentro da função $.post()
		$array_adubos = array (
				"cod_insumo" => $adubos ['cod_insumo'],
				"ds_descricao" => $adubos ['ds_descricao'],
				"ds_tipoadubo" => $adubos ['ds_tipoadubo'],
				"ds_composicao" => $adubos ['ds_composicaoformula'],
				"ds_observacao" => $adubos ['ds_observacao']
		);
		echo json_encode ( $array_adubos );
	}
	public function save() {
		
		
		$this->load->model ( 'adubos_model' );
		
		if ($this->input->post ( "cod_insumo" ) == "") {
			$id = $this->adubos_model->getMaxCodigo () + 1;
		} else {
			$id = $this->input->post ( "cod_insumo" );
		}
		
		$dados = array (
				'cod_insumo' => $id,
				'cod_categoria' => 4,
				'ds_descricao' => $this->input->post ( 'ds_descricao' ),
				'ds_composicaoformula' => $this->input->post ( 'ds_composicao' ),
				'ds_observacao' => $this->input->post ( 'ds_observacao' ),
				"cod_empresa" => $this->session->userdata('codempresa')
		);
		
		if ($this->input->post ( "cod_insumo" ) == "") {
			$this->adubos_model->create ( $dados );
		} else {
			$this->adubos_model->update ( $id, $dados );
		}
		// Load View
		$data ['page_title'] = "Adubos";
		
		$this->template->show ( 'insumos/adubos', $dados );
		
		redirect ( 'adubos' );
	}
	public function remove($id) {

		$id = $this->input->post ( "id" );
		// Check if users are already there
		$this->load->model ( 'adubos_model' );
		
		$this->adubos_model->remove ( $id );
		
		// Load View
		$data ['page_title'] = "Adubos";
		
		$this->template->show ( 'insumos/adubos', $data );
		
		redirect ( 'adubos' );
	}
	// public function update($id) {
	
	// $data = array ();
	// // Check if users are already there
	// $this->load->model ( 'adubos_model' );
	
	// $adubos = $this->adubos_model->get ( $id );
	
	// $tipoAdubos = $this->adubos_model->getTipoAdubos ( false );
	
	// $data ['adubos'] = $adubos;
	// $data ['tipoAdubos'] = $tipoAdubos;
	
	// $this->template->show ( 'adubos', $data );
	// }
	
	/*
	 * // Load View
	 * $data['page_title'] = "Login";
	 *
	 *
	 * $data['email'] = '';
	 * $data['password'] = '';
	 *
	 * // $this->load->view('login', $data);
	 * $this->template->show('login', $data);
	 * }
	 */
}
?>
