<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'Não é possivel acessar diretamente.' );
class EntradaProdutos extends CI_Controller {
	public function index() {
		if ($this->session->userdata ( 'logado' ) != true) {
			redirect ( 'login' );
			die ();
		}
		
		// $id = $this->input->post ( "cod_socio" );
		$data = array ();
		// Check if users are already there
		$this->load->model ( 'socio_model' );
		$this->load->model ( 'safra_model' );
		$this->load->model ( 'fornecedor_model' );
		$this->load->model ( 'produtos_model' );
		$this->load->model ( 'embalagens_model' );
		
		$socio = $this->socio_model->get ( false );
		$safra = $this->safra_model->get ( false );
		$fornecedor = $this->fornecedor_model->get ( false );
		$produto = $this->produtos_model->get ( false );
		$embalagem = $this->embalagens_model->getEmbalagens();
		
		$data ['socio'] = $socio;
		$data ['safra'] = $safra;
		$data ['fornecedor'] = $fornecedor;
		$data ['produto'] = $produto;
		$data ['embalagem'] = $embalagem;
		
		if ($socio) {
			$data ['already_installed'] = true;
		} else {
			$data ['already_installed'] = false;
		}
		
		// Load View
		$data ['page_title'] = "Entrada de Produtos";
		$this->template->show ( 'entradaProdutos', $data );
	}
	
	function insereItemNota(){
		$this->load->model ( 'entradaprodutos_model' );
		//$idEntrada = $this->input->post ( 'cod_entradaproduto' );
		$arrItensEntrada = array (
				'cod_itementradaproduto' => $this->input->post ( 'cod_itementradaproduto' ),
				'cod_entradaproduto' => $this->input->post ( 'cod_entradaproduto' ),
				'cod_produto' => $this->input->post ( 'cod_produto' ),
				'qtd_insumo' => $this->input->post ( 'qtd_insumo' ),
				'cod_embalagem' => $this->input->post ( 'cod_embalagem' ),
				'vlr_unitario' => $this->input->post ( 'vlr_unitario' ),
				//'vlr_total' => $this->input->post ( 'vlr_total' ),
				'cod_empresa' => $this->session->userdata ( 'codempresa' )
		);
		if($this->input->post ( 'cod_itementradaproduto' ) == ""){
			$retorno = $this->entradaprodutos_model->insereItemNota_model ($arrItensEntrada );	
		}else{
			//updateItemNota($arrItensEntrada);
			$retorno = $this->entradaprodutos_model->updateItemNota_model ($this->input->post ( 'cod_itementradaproduto' ),$this->input->post ( 'cod_entradaproduto' ),$arrItensEntrada );
		}
		
		echo json_encode ( $retorno );
	}
	
	public function createNota(){
		$this->load->model ( 'entradaprodutos_model' );
		$arrDadosNota = array (
				'cod_empresa' => $this->session->userdata ( 'codempresa' )
		);
		$idNota = $this->entradaprodutos_model->createNota_model($arrDadosNota);
		echo json_encode ( $idNota );
	}
	
	function updateDadosNota(){
		$this->load->model ( 'entradaprodutos_model' );
		$idEntrada = $this->input->post ( 'cod_entradaproduto' );
		
		$dt_notaCampo = $this->input->post ( 'dt_nota' );
		$dt_nota = date ( "Y-m-d", strtotime ( str_replace ( '/', '-', $dt_notaCampo ) ) );
		
		$dt_entradaCampo = $this->input->post ( 'dt_nota' );
		$dt_entrada = date ( "Y-m-d", strtotime ( str_replace ( '/', '-', $dt_entradaCampo ) ) );
		
		
		$arrDadosNota = array (
				'cod_entradaproduto' => $idEntrada,
				'ds_numeroNota' => $this->input->post ( 'ds_numeroNota' ),
				'dt_nota' => $dt_nota,
				'dt_entrada' => $dt_entrada,
				'cod_fornecedor' => $this->input->post ( 'cod_fornecedor' ),
				'cod_safra' => $this->input->post ( 'cod_safra' ),
				'cod_empresa' => $this->session->userdata ( 'codempresa' ),
				'vlr_totalnota' => $this->input->post ( 'vlr_totalnota' )
		);
		$retorno = $this->entradaprodutos_model->updateDadosNota_model ($idEntrada, $arrDadosNota );
		//echo json_encode ( $arrDadosNota );
	}
	
	function updateItemNota(){
		$this->load->model ( 'entradaprodutos_model' );
		/*
		$arrItensEntrada = array (
				'cod_entradaproduto' => $this->input->post ( 'cod_entradaproduto' ),
				'cod_itementradaproduto' => $this->input->post ( 'cod_itementradaproduto' ),
				'cod_produto' => $this->input->post ( 'cod_produto' ),
				'qtd_insumo' => $this->input->post ( 'qtd_insumo' ),
				'cod_embalagem' => $this->input->post ( 'cod_embalagem' ),
				'vlr_unitario' => $this->input->post ( 'vlr_unitario' ),
				//'vlr_total' => $this->input->post ( 'vlr_total' ),
				'cod_empresa' => $this->session->userdata ( 'codempresa' )
		);*/
		$retorno = $this->entradaprodutos_model->updateItemNota_model ($arrItensEntrada );
		echo json_encode ( $retorno );
	}
	
	public function getItensNota(){
		$this->load->model ( 'entradaProdutos_model' );
		//vou mandar parâmetro get = true quando é para buscar só um
		if($this->input->post ( 'get' ) == 'true'){
			$cod_itementradaproduto = $this->input->post ( 'cod_itementradaproduto' );
		}else{
			$cod_itementradaproduto = false;
		}
		$itens = $this->entradaProdutos_model->getItensNota_model($cod_itementradaproduto);
		echo json_encode ( $itens );
	
	
	}
	
	public function listaProdutos(){
		$this->load->model ( 'entradaProdutos_model' );
		$produtos = $this->entradaProdutos_model->getProdutos_model();
		echo json_encode ( $produtos );
	
	
	}
	
	public function listaEmbalagens(){
		$this->load->model ( 'embalagens_model' );
		$embalagens = $this->embalagens_model->getEmbalagens ();
		echo json_encode ( $embalagens );
	}
	
	
	
	
	
	
	
	
	public function save() {
		$this->load->model ( 'entradaprodutos_model' );
		
		// Receita
		// $tipoConta = 'R';
		/*
		 * if (array_key_exists ( )) {
		 * $tipoConta = 'D';
		 * }
		 */
		
		// Despesa
		$arrDadosNota = array (
				'ds_numeroNota' => $this->input->post ( 'ds_numeroNota' ),
				'dt_nota' => $this->input->post ( 'dt_nota' ),
				'dt_entrada' => $this->input->post ( 'dt_entrada' ),
				'cod_fornecedor' => $this->input->post ( 'cod_fornecedor' ),
				'cod_safra' => $this->input->post ( 'cod_safra' ),
				'cod_empresa' => $this->session->userdata ( 'codempresa' ),
				'vlr_totalnota' => $this->input->post ( 'vlr_totalnota' ) 
		);
		
		if ($this->input->post ( "cod_entradaproduto" ) == "") {
			$entrada_id = $this->entradaprodutos_model->createDadosNota ( $arrDadosNota );
			//echo json_encode($entrada_id);
			print_r($entrada_id);exit;
			//return $entrada_id;
			//return $entrada_id;
		} else {
			// $this->contas_model->updateDadosNota ( $id, $dados );
		}

	}
	
}
?>