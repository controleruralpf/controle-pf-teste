<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'Não é possivel acessar diretamente.' );
class Atividades extends CI_Controller {
	
	public function index() {
		
	if ($this->session->userdata('logado') != true) {
			redirect ( 'login' );
			die();
		}
		
		//$id = $this->input->post ( "cod_area" );
		$data = array ();
		// Check if users are already there
		$this->load->model ( 'Areas_model' );
		
		$areas = $this->Areas_model->get ( false );
			
		$data ['areas'] = $areas;
		
		if ($areas) {
			$data ['already_installed'] = true;
		} else {
			$areas ['already_installed'] = false;
		}
		
		// Load View
		$data ['page_title'] = "Atividades";
		$this->template->show ( 'atividades',$data);
	}
	

public function getPreparacaoSolo() {
	
	$id = $_GET["codArea"];

	$this->load->model ( 'areas_model' );
	
	$areas = $this->areas_model->get ( $id );

	$data ['areas'] = $areas;
	
	$this->load->view('atividades/preparacaoSolo',$data);
	
	}

	/*
	public function dadosCultura() {
	
		// recebo o id_cliente da view para trazer os dados somente daquele cliente
		$id = $this->input->post ( "id" );
	
		$this->load->model ( 'cultura_model' );
	
		$culturas = $this->cultura_model->get ( $id );

		// como eu vou retornar os dados para a view em formato jSon, aqui eu crio os índices para serem acessados dentro da função $.post()
		$array_culturas = array (
				"cod_cultura" => $culturas ['cod_cultura'],
				"ds_cultura" => $culturas ['ds_cultura']
		);
		echo json_encode ( $array_culturas );
	}
	
	
	public function save() {
	
	
		$this->load->model ( 'cultura_model' );
	
		$dados = array (
				'ds_cultura' => $this->input->post ( 'ds_cultura' )
				
		);
	   
		if ($this->input->post ( "cod_cultura" ) == "") {
			$this->cultura_model->create ( $dados );
		} else {
	
			$this->cultura_model->update ( $this->input->post ( "cod_cultura" ) , $dados );
		}
		// Load View
		$data ['page_title'] = "Culturas";
	
		$this->template->show ( 'cultura', $dados );
	
		redirect ( 'cultura' );
	}
	
	public function remove($id) {
		// Check if users are already there
		$this->load->model ( 'cultura_model' );
	
		$this->cultura_model->remove ( $id );
	
		// Load View
		$data ['page_title'] = "Culturas";
	
		$this->template->show ( 'cultura', $data );
	
		redirect ( 'cultura' );
	}
	
	*/
}
?>