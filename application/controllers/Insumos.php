<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'Não é possivel acessar diretamente.' );
class Insumos extends CI_Controller {
	public function index($oq) {
		
	if ($this->session->userdata('logado') != true) {
			redirect ( 'login' );
			die();
		}
		
		$data = array ();
		// Check if users are already there
		$this->load->model ( 'insumos_model' );
		$this->load->model ( 'tipoDefensivo_model' );
		
		$categoriaInsumos = $this->insumos_model->getCategoriaInsumos ( false );
		$cultivares = $this->insumos_model->getCultivares ( false );
		$insumos = $this->insumos_model->getInsumos ( false );
		$tipoDefensivos = $this->tipoDefensivo_model->get( false );
		
		
		$data ['categoriaInsumos'] = $categoriaInsumos;
		$data ['cultivares'] = $cultivares;
		$data ['insumos'] = $insumos;
		$data ['tipoDefensivos'] = $tipoDefensivos;
	
		
		// Load View
		$data ['page_title'] = "Insumos";
		
		$this->template->show ( 'insumos/'.$oq,$data);
	}
	
	
	

	public function save() {
		$this->load->model ( 'insumos_model' );
		$dados = array (
				
				'cod_categoria' => $this->input->post ( 'cod_categoriainsumo' ),
				'ds_descricao' => $this->input->post ( 'ds_insumo' ),
				'cod_cultivar' => $this->input->post ( 'cod_cultivar' ) == '' ? null : $this->input->post ( 'cod_cultivar' ) ,
				'ds_origemsemente' => $this->input->post ( 'ds_origem' ),
				'cod_tipodefensivo' => $this->input->post ( 'cod_tipodefensivo' ) == '' ? null : $this->input->post ( 'cod_tipodefensivo' ),
				'ds_tipoadubo' => $this->input->post ( 'ds_tipoadubo' ),
				'ds_composicaoformula' => $this->input->post ( 'ds_composicao' ),
				'ds_utilizacao' => $this->input->post ( 'ds_utilizacao' ),
				'ds_observacao' => $this->input->post ( 'ds_observacao' ),
				'ds_lote' => $this->input->post ( 'ds_lote' ),
				'ds_germinacao' => $this->input->post ( 'ds_germinacao' ),
				'ds_vigor' => $this->input->post ( 'ds_vigor' ),
				'ds_peneira' => $this->input->post ( 'ds_peneira' )
	
		);
	
		if ($this->input->post ( "cod_insumo" ) == "") {
			
			$this->insumos_model->create ( $dados );
		} else {
	
			$this->insumos_model->update ( $this->input->post ( "cod_insumo" ) , $dados );
		}
		// Load View
		$data ['page_title'] = "Insumos";
	
		$this->template->show ( 'insumos', $dados );
	
		redirect ( 'insumos' );
	}
	
	
	public function remove($id) {
		// Check if users are already there
		$this->load->model ( 'insumos_model' );
	
		$this->insumos_model->remove ( $id );
	
		// Load View
		$data ['page_title'] = "Insumos";
	
		$this->template->show ( 'cultivar', $data );
	
		redirect ( 'insumos' );
	}
	
	public function dadosInsumos() {
	
		// recebo o id_cliente da view para trazer os dados somente daquele cliente
		
		$id = $this->input->post ( "id" );
	
		$this->load->model ( 'insumos_model' );
	
		$insumos = $this->insumos_model->getInsumos ( $id );
	
		// como eu vou retornar os dados para a view em formato jSon, aqui eu crio os índices para serem acessados dentro da função $.post()
		$array_insumos = array (
				"cod_insumo" => $insumos ['cod_insumo'],
				"ds_insumo" => $insumos ['ds_descricao'],
				"cod_cultivar" => $insumos ['cod_cultivar'],
				"ds_categoria" => $insumos ['ds_categoria'],
				"cod_categoria" => $insumos ['cod_categoria'],
				"ds_origem" => $insumos ['ds_origemsemente'],
				"ds_tipoadubo" => $insumos ['ds_tipoadubo'],
				"ds_utilizacao" => $insumos ['ds_utilizacao'],
				"ds_composicao" => $insumos ['ds_composicaoformula'],
				"ds_observacao" => $insumos ['ds_observacao'],
				"ds_faixadefensivo" => $insumos ['ds_faixadefensivo'],
				"tp_defensivo" => $insumos ['tp_defensivo'],
				"cod_tipodefensivo" => $insumos ['cod_tipodefensivo'],
				"ds_tipodefensivo" => $insumos ['ds_tipodefensivo'],
				"num_diasduracaodefensivo" => $insumos ['num_diasduracaodefensivo'],
				"ds_cultivar" => $insumos ['ds_cultivar'],
				"ds_lote" => $insumos ['ds_lote'],
				"ds_vigor" => $insumos ['ds_vigor'],
				"ds_germinacao" => $insumos ['ds_germinacao'],
				"ds_peneira" => $insumos ['ds_peneira']
				
				
		);
		echo json_encode ( $array_insumos );
	}
	
}
?>