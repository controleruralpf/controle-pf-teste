<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Login extends CI_Controller {
	public function index() {
		// Load View
		$data ['page_title'] = "Login";
		
		$data ['email'] = '';
		$data ['password'] = '';
		
		$this->load->view ( 'login', $data );
	}
	
	public function validateInicial() {
		
		
		$this->load->helper('date');

		//timezone America/Sao_Paulo

		$stringdedata = "%Y-%m-%d-%H:%i:%s";
		$data = time();

		$countReg = 0;
		$unike = 0;
		$this->load->model ( 'login_model' );
		
		$result = $this->login_model->validate ( $_POST ['login'], $_POST ['senha'],$_POST['codEmpresa']);
		
		if ($_POST ['codEmpresa'] == null or $_POST ['codEmpresa'] == ''){

		//se for sem empresa e tiver login 	
		if(!isset($result['ds_login']))	{
			
			foreach ( $result as &$value ) {
				$countReg = $countReg + 1;
			}

		}else if (isset($result['ds_login']) and ($result['ds_login'] != '')){
				$countReg = 1;
		}
		}else{
				foreach ( $result as &$value ) {
					$countReg = $countReg + 1;
				}
			}	
			
		if($countReg == 0 ){//Não tem dados sem login ou senha errados
		
			$array_retorno = array (
					'reg' => '0'
			);
			echo json_encode ( $array_retorno );
		}	
			
		// verifica se foi selecionada alguma empresa no login
		if ($_POST ['codEmpresa'] == null or $_POST ['codEmpresa'] == '') {
	
			if ($countReg > 0) {				
				
				if ($countReg == 1) {
					
					$this->session->set_userdata ( array (
							'logado' => true,
							'usuario' => $result['ds_nomeusuario'],
							'login' => $result['ds_login'],
							'razaosocial' => $result['ds_razaosocial'],
							'fantasia' => $result['ds_nomefantasia'],
							'codempresa' => $result['cod_empresa']
					) );
					
					
					$array_retorno = array (
							'reg' => '1' 
					);
					
					echo json_encode ( $array_retorno );
				} else if ($countReg > 1) {
					$this->load->model ( 'login_model' );

					$empresa = $this->login_model->getListaEmpresa ();
					$array_retorno = array (
							'reg' => '2',
							'empresa' => $empresa 
					);
					
					
					foreach ( $result as &$value ) {

						$this->session->set_userdata ( array (
								'logado' => true,
								'usuario' => $value ['ds_nomeusuario'],
								'login' => $value ['ds_login'],
								'razaosocial' => $value ['ds_razaosocial'],
								'fantasia' => $value ['ds_nomefantasia'],
								'codempresa' => $value['cod_empresa']
						) );
					}
					
					
					
					
					echo json_encode ( $array_retorno );
					
					$dadosLog = array (
						'cod_usuario' => $this->session->userdata('login'),
						'ds_usuario' =>  $this->session->userdata('usuario'),
						'ip' => $_SERVER["REMOTE_ADDR"],
						'dthr_login' => mdate($stringdedata, $data)
							
				);
					
				$this->login_model->createLog ( $dadosLog );
					
				} else {
					
					echo json_encode ( array (
							'reg' => '0' 
					) );
				}
			}
		} else {
			
			
			if ($countReg > 0) {
			
					
					$this->session->set_userdata ( array (
							'logado' => true,
							'usuario' => $result['ds_nomeusuario'],
							'login' => $result['ds_login'],
							'razaosocial' => $result['ds_razaosocial'],
							'fantasia' => $result['ds_nomefantasia'],
							'codempresa' => $result['cod_empresa']
					) );
				
				$array_retorno = array (
						'reg' => '1' 
				);
				
				
				echo json_encode ( $array_retorno );
				
				$dadosLog = array (
						'cod_usuario' => $this->session->userdata('login'),
						'ds_usuario' =>  $this->session->userdata('usuario'),
						'ip' => $_SERVER["REMOTE_ADDR"],
						'dthr_login' => mdate($stringdedata, $data)
							
				);
					
				$this->login_model->createLog ( $dadosLog );
				
				
			}
		}
	}
	

	
	public function logout() {
		$this->session->sess_destroy();
		redirect ( 'login' );
	
	}
	
}