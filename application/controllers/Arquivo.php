<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'Não é possivel acessar diretamente.' );
class Arquivo extends CI_Controller {
	
	public function index() {
	
	if ($this->session->userdata('logado') != true) {
			redirect ( 'login' );
			die();
		}
		
		//$id = $this->input->post ( "cod_despesa" );
		$data = array ();
		// Check if users are already there
		$this->load->model ( 'arquivo_model' );
		
		$this->load->model ( 'despesas_model' );
		
		$formapagamentos = $this->arquivo_model->getFormaPagamentos ( );

		$pagamentoParcelaDespesas = $this->despesas_model->getParcelaPagamentoDespesa ( 6 );
				
		$data ['formapagamentos'] = $formapagamentos;
		
		
		$data ['pagamentoParcelas'] = $pagamentoParcelaDespesas;
		
		
		$data ['already_installed'] = false;
		
		
		$this->load->view ( 'arquivo', $data );
	}
	
	
	public function removePagamentoParcelaDespesa() {

		// Check if users are already there
		$codparcelapagamentodespesa = $this->input->post ( "cod_parcelapagamentodespesa" );
		$coddespesa = $this->input->post ( "cod_despesa" );
		$numparcela = $this->input->post ( "num_parcela" );
		$codpagamentodespesa = $this->input->post ( "cod_pagamentodespesa" );

		$data = array ();
		
		$this->load->model ( 'despesas_model' );
	
		$this->despesas_model->removePagamentoParcelaDespesa ($codparcelapagamentodespesa,$coddespesa,$numparcela,$codpagamentodespesa);

		
		
		$this->load->view ( 'arquivo', $data );
		
		// Load View
		//$data ['page_title'] = "Despesa";
	
		//$this->template->show ( 'despesas', $data );
	
		//redirect ( 'despesas' );
	}
	
	/*
	
	public function dadosDespesa() {
	
		// recebo o id_cliente da view para trazer os dados somente daquele cliente
		$id = $this->input->post ( "id" );
	
		$this->load->model ( 'despesas_model' );
	
		// como eu vou retornar os dados para a view em formato jSon, aqui eu crio os índices para serem acessados dentro da função $.post()
		$pagamentoParcelaDespesas = $this->despesas_model->getParcelaPagamentoDespesa ( $id );
		
		$array_despesas = array (
				
				"cod_despesa" => $despesa ['cod_despesa'],
				"ds_depesa" => $despesa ['ds_despesa'],
				"num_nota" => $despesa ['num_nota'],
				"cod_safra" => $despesa ['cod_safra'],
				"cod_fornecedor" => $despesa ['cod_fornecedor'],
				"cod_socio" => $despesa ['cod_socio'],
				"cod_distribuicao" => $despesa ['cod_distribuicao'],
				"dt_pagamento" => $despesa ['dt_pagamento'],
				"dt_nota" => $despesa ['dt_nota'],
				//"dt_uso" => $despesa ['dt_uso'],
				"ds_observacao" => $despesa ['ds_observacao'],
				"cod_conta" => $despesa ['cod_conta'],
				"vlr_total" => $despesa ['vlr_total'],

			
				//Pagamento Despesa
				"cod_formapagamentoDespesa" 	=> $despesa['cod_formapagamentoDespesa'],
				"ds_tppagamentoDespesa"     	=> $despesa['ds_tppagamentoDespesa'],
				"num_parcelas"     				=> $despesa['num_parcelas'],
				"dt_vencimentoDespesa"			=> $despesa['dt_vencimentoDespesa'],
				"dt_PagamentoDespesa"      		=> $despesa['dt_PagamentoDespesa'],
				"vlr_totalpagamentoDespesa"      =>$despesa['vlr_totalpagamentodespesa'],
				"ds_observacaopagamentoDespesa"  => $despesa['ds_observacaopagamentodespesa']
					
		);
		
		echo json_encode ( $array_despesas );
	}
	
	/*
	public function save() {
	
	
		$this->load->model ( 'despesas_model' );
	
		$dataNotaCampo = $this->input->post ( 'dt_nota' );
		$dataNota = date ( "Y-m-d", strtotime ( str_replace ( '/', '-', $dataNotaCampo ) ) );
		
		$dataPagamentoCampo = $this->input->post ( 'dt_pagamento' );
		$dataPagamento = date ( "Y-m-d", strtotime ( str_replace ( '/', '-', $dataPagamentoCampo ) ) );
		
		//$dataUsoCampo = $this->input->post ( 'dt_uso' );
		//$dataUso = date ( "Y-m-d", strtotime ( str_replace ( '/', '-', $dataUsoCampo ) ) );
		
		
		$valorTotal = str_replace(',','.', str_replace('.','', $this->input->post ( 'vlr_total' )));
		
		$valorTotal = str_replace('R$','',$valorTotal);
		$dados = array (
				'ds_despesa' => $this->input->post ( 'ds_despesa' ),
				'cod_safra' =>  $this->input->post ( 'cod_safra' ),
				'cod_fornecedor' => $this->input->post ( 'cod_fornecedor' ),
				'cod_socio' => $this->input->post ( 'cod_socio' ),
				'cod_distribuicao' => $this->input->post ( 'cod_distribuicao' ),
				'ds_observacao' => $this->input->post ( 'ds_observacao' ),
				'num_nota' => $this->input->post ( 'num_nota' ),
				'vlr_total' => $valorTotal,
				'dt_nota' => $dataNota,
				'dt_pagamento' => $dataPagamento,
				'cod_conta' => $this->input->post ( 'cod_conta' ),
				//'dt_uso' => $dataUso,
				'cod_empresa'=> $this->session->userdata('codempresa'),
				'dt_cadastrodespesa' => date("Y-m-d")
				
		);
		
		if ($this->input->post ( "cod_despesa" ) == "") {
			$this->despesas_model->create ( $dados );
		} else {
	
			$this->despesas_model->update ( $this->input->post ( "cod_despesa" ) , $dados );
		}
		// Load View
		$data ['page_title'] = "Despesas";
	
		$this->template->show ( 'despesas', $dados );
	
		redirect ( 'despesas' );
	}
	
	
	public function pagamentoSave() {
	
	
		$this->load->model ( 'despesas_model' );
		
	
		$dataPagamentoCampo = $this->input->post ( 'dt_pagamento' );
		$dataPagamento = date ( "Y-m-d", strtotime ( str_replace ( '/', '-', $dataPagamentoCampo ) ) );
		
		$dataVencimentoCampo = $this->input->post ( 'dt_vencimento' );
		$dataVencimento = date ( "Y-m-d", strtotime ( str_replace ( '/', '-', $dataVencimento ) ) );

		$valorPago =  $this->input->post ( 'vlr_totalpagamento' );
		$dsObservacao =  $this->input->post ( 'ds_observacaopagamento' );
		$dsCodFormapagamento =  $this->input->post ( 'cod_formapagamento' );
		
		//Vista
		$ds_tppagamento = 'V';
		
		if (array_key_exists ( "ds_tppagamentoP", $this->input->post () )) {
			$ds_tppagamento = 'P';
		}
		
		if($ds_tppagamento == 'V'){
			$numParcelas = 0;
		}else if($ds_tppagamento == 'P') {
			$numParcelas = $this->input->post('num_parcela');
		}
	//	$valorTotal = str_replace(',','.', str_replace('.','', $this->input->post ( 'vlr_total' )));
	
		///$valorTotal = str_replace('R$','',$valorTotal);
		$dados = array (
				'cod_pagamentodespesa' => $this->input->post ( 'cod_pagamentodespesa' ),
				'cod_despesa' => $this->input->post ( 'cod_despesaPagamentopai' ),
				'dt_pagamento' => $dataPagamento,
				'dt_vencimento' => $dataVencimento,
				'ds_tppagamento' => $ds_tppagamento,
				'num_parcelas' => $numParcelas,
				'ds_observacao' => $dsObservacao,
				'cod_formapagamento' => $dsCodFormapagamento,
				'vlr_pago' => $valorPago,
				'cod_empresa'=> $this->session->userdata('codempresa')
	
		);
	
		if ($this->input->post ( "cod_pagamentodespesa" ) == "") {
			$this->despesas_model->createPagamento ( $dados );
		} else {
	
			$this->despesas_model->updatePagamento ( $this->input->post ( "cod_pagamentodespesa" ) , $dados );
		}
		
		if($this->input->post('num_parcela') > 0 ){
		for($i =1; $i <= $numParcelas; $i++){
		
			$dadosParcelasPagamentoDespesa = array (
					'cod_despesa' => $this->input->post ( 'cod_despesaPagamentopai' ),
					'dt_pagamento' => $dataPagamento,
					'dt_vencimento' => $dataVencimento,
					'ds_tppagamento' => $ds_tppagamento,
					'num_parcelas' => $numParcelas,
					'ds_observacao' => $dsObservacao,
					'cod_formapagamento' => $dsCodFormapagamento,
					'vlr_pago' => $valorPago,
					'cod_empresa'=> $this->session->userdata('codempresa')
		
			);
			
			if ($this->input->post ( "cod_despesa" ) == "") {
				
				$this->despesas_model->createParcelasPagamentoDespesa ( $dadosParcelasPagamentoDespesa );
			} else {
		
				$this->despesas_model->updateParcelasPagamentoDespesa ( $this->input->post ( "cod_despesa" ) , $dadosParcelasPagamentoDespesa );
			}
		}
			
			
		}
		
		// Load View
		$data ['page_title'] = "Despesas";
	
		$this->template->show ( 'despesas', $dados );
	
		redirect ( 'despesas' );
	}
	
	public function remove() {
		// Check if users are already there
		$id = $this->input->post ( "cod_despesa" );
		
		$this->load->model ( 'despesa_model' );
	
		$this->despesas_model->remove ( $id );
	
		// Load View
		$data ['page_title'] = "Despesa";
	
		$this->template->show ( 'despesas', $data );
	
		redirect ( 'despesas' );
	}

	public function popup(){
    
	$data = array ();
	// Check if users are already there
	$this->load->model ( 'fornecedor_model' );
	
	$culturas = $this->fornecedor_model->get ( false );
		
	$data ['fornecedor'] = $culturas;

    $this->load->view('fornecedor', $data);
	}
	*/
	
}
?>
