<script type="text/javascript">

$(document).ready(function() {


	
	//quando fecha o modal ele exclui todos os campos
	$('#popupDetalhe').on('hidden.bs.modal', function (e) {
		$("#titulo").empty();
		$("#conteudo > #conteudoForm").empty();
	})
	
	$('#btnAdicionarCultura').popover({
         trigger: 'manual',
         html: true,
         title: '<p align="center"><b>Cultura</b></p>',
         content: $('#div-popover').html() // Adiciona o conteúdo da div oculta para dentro do popover.
      }).click(function (e) {
         e.preventDefault();
         // Exibe o popover.
         $(this).popover('show');
      });
	
	//iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_square-green',
      radioClass: 'iradio_square-green'
    });

	$("#cod_cultura").change(function(){
		//revalida somente o campo de seleção quando ele selecionar algun item.
		var validator = $("#frm_cultivar").validate();
		validator.element("#cod_cultura");
	});
	
	$("#frm_cultivar").validate({
        ignore: ':not(select:hidden, input:visible, textarea:visible)',
        errorPlacement: function (error, element) {
            if ($(element).is('select')) {
                element.next().after(error); // Validação especial para os campos select
            } else {
                error.insertAfter(element);  //Validação normal para os outros campos
            }
        },
        highlight: function(element, errorClass, validClass) {
        		$(element).closest('.form-group').addClass(errorClass).removeClass(validClass);
		    	$(element).closest('.form-group').addClass('has-error');
		  },
		unhighlight: function(element, errorClass, validClass) {
			$(element).closest('.form-group').removeClass(errorClass).addClass(validClass);
			$(element).closest('.form-group').removeClass('has-error');
		  }
    });

	
	//função que adiciona plugins a tabela de Cultivar
	$('#tblCultivar').dataTable({
		"iDisplayLength":9,                              
	    "oLanguage": {
	     "sProcessing": "Aguarde enquanto os dados são carregados ...",
	     /*"sLengthMenu": "Mostrar _ hghgghg MENU_ registros por pagina",*/
	     "sLengthMenu": "",
	     "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
	     "sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
	     "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
	     "sInfoFiltered": "",
	     "sSearch": "Procurar",
	     "oPaginate": {
	      "sFirst":    "Primeiro",
	      "sPrevious": "Anterior",
	      "sNext":     "Próximo",
	      "sLast":     "Último"
	    }
	  }                              
	});

	//cria um botão dentro da tabela de Cultivar
	$("#tblCultivar_length").append("<button id='btnCadastrar' class='btn btn-success' onclick='manutencaoCultivar(1)'>Cadastrar</button>");
	
	
});

function insereCultura(){
	var ds_cultura_aux = $('#ds_cultura_aux').val();
	var parametros = "ds_cultura="+ds_cultura_aux;

	if(ds_cultura_aux != ""){
		$.ajax({
			url: "cultura/save",
			type: "post",
			data: parametros,
			success: function(){
			$('#btnAdicionarCultura').popover('hide');
			$('#ds_cultura_aux').val("");
		},
			error:function(){
			alert("Errou");
			}
		}).always(function() {
			//após a inserção
			carregaDadosCulturaJson();
		})
	}else{
		$('#btnAdicionarCultura').popover('show');
	}
}

function carregaDadosCulturaJson(){

		$.post(base_url+'cultivar/listaCulturas', {
			}, function (data){
			$('#cod_cultura').children().remove().end();//.append('<option selected value="">Selecione um item</option>') ;
	    		$(data).each(function(){
	    			$("#cod_cultura").append('<option value="'+this.cod_cultura+'">'+this.ds_cultura+'</option>');
				});
			$('#cod_cultura').selectpicker('refresh');

	  		}, 'json').always(function(data) {
  				//busca o ultimo código cadastrado
  				var sel = 1;
	  			$(data).each(function(){
	    			if(sel < this.cod_cultura){
						sel = this.cod_cultura;
	            	}
				});
				//seleciona o último cadastrado
  			 $('#cod_cultura').selectpicker('val', sel);
	
		});;

}

function cancelaCultura(){
	$('#btnAdicionarCultura').popover('hide');
	 $('#ds_cultura_aux').val("");
}

function manutencaoCultivar(acao){
	if(acao == 1){
		limpaForm();
		$('#manutencaoCultivar').collapse('show');
		$('#tabelaCultivar').collapse('hide');
		 $("#ds_cultivar").focus();

		if ($('#cod_cultivar').val() == ""){
			$("#chk_tipo").attr("checked", true);
		}
		 
		
	}else if(acao == 2){
		limpaForm();	
		$('#manutencaoCultivar').collapse('hide');
		$('#tabelaCultivar').collapse('show');

	}
}

function limpaForm(){
	$("#frm_cultivar input").val("");
	$("#frm_cultivar textarea").val("");
	$('#cod_cultura').selectpicker('val', "");
	$("#chk_tipo").iCheck('uncheck');
	$("#rdb_crescimentoD, #rdb_crescimentoI, #rdb_crescimentoS").iCheck('uncheck');
	$("#frm_cultivar").validate().resetForm();
	$(".form-group").removeClass("has-error");
	$("button.dropdown-toggle").css("border", "1px solid #DDD");
	//button.dropdown-toggle
	//$("#rdb_crescimentoD").iCheck('check');
}

var base_url = "<?= base_url() ?>";

	 function carregaDadosCultivarJson(id){

 	     		$.post(base_url+'cultivar/dadosCultivar', {
 	     			id: id
 	     		}, function (data){
 	     			$('#cod_cultivar').val(data.cod_cultivar);
 	     			$('#ds_cultivar').val(data.ds_cultivar);
 	     			$('#cod_cultura').val(data.cod_cultura);

 	     			if(data.ds_tipo == 'S'){
 	 	     			//jQuery("#chk_tipo").attr('checked', 'checked');
 	     				$("#chk_tipo").iCheck('check');
 	     			}

 	     			$('#num_diasciclo').val(data.num_diasciclo);
 	     			
 	     			$('#ds_anotacao').val(data.ds_anotacao);

 	     			
 	     			if(data.tp_crescimento == 'I'){
 	     				$("#rdb_crescimentoI").iCheck('check');
 	 	 	     	}else if (data.tp_crescimento == 'S'){
 	 	 	     		$("#rdb_crescimentoS").iCheck('check');
 	 	 	 	    }else if (data.tp_crescimento == 'D'){
 	 	 	     		$("#rdb_crescimentoD").iCheck('check');
	 	 	 	     }

 	     			//seleciona o valor	
 	     			 $('#cod_cultura').selectpicker('val', data.cod_cultura);
 	     			
 	         		}   , 'json').always(function() {
					//após a inserção
					//carregaDadosCulturaJson();
					$('#manutencaoCultivar').collapse('show');
 	    			$('#tabelaCultivar').collapse('hide');

 	    		 	$("#ds_cultivar").focus();	
		});
  	
	}
	    
 	function editarCultivar(id){

 		carregaDadosCultivarJson(id);
 	}

 	function carregaDadosPopUp(id){

 		$.post(base_url+'cultivar/dadosCultivar', {
 			id: id
 		}, function (data){

         	$("#titulo").append(data.ds_cultivar);
         	montaPopUp('Cultura',data.ds_cultura);
			montaPopUp('Transgênica',converte('T',data.ds_tipo));
			montaPopUp('Crescimento',converte('C',data.tp_crescimento));
			montaPopUp('Dias Ciclo',data.num_diasciclo);
			montaPopUp('Anotações',data.ds_anotacao);
			
			$('#popupDetalhe').modal('show');
		
     		}, 'json');

		function montaPopUp(valorLabel,valorCampo){
 			$("#conteudo > #conteudoForm").append('<div class="form-group" id="div-campos"><label class="col-sm-2 control-label" id="label-popup">'+valorLabel+'</label><label class="col-sm-8 control-label" id="valor-popup" style="text-align: left;font-weight: 400;">'+valorCampo+'</label></div>')
     	}

     	function converte(a, valor){
     		if(a == "T"){
     			if(valor == "S"){
     				valor = 'Sim';
     			}else{
     				valor = 'Não';
     			}
     		}else if(a == "C"){
	 			if  (valor == 'I') {
 					valor = 'Indeterminado';
 				}else if (valor == 'D'){
 					valor = 'Determinado';
 				}else if (valor == 'S'){
 					valor = 'Semi Determinado';
 				}else{
 					valor = '';
 				}
     		}
     	return valor;
 		}
}

 	function excluirCultivar(id){
 	bootbox.confirm("Confirma a exclusão deste registro?", function(result) {
	 // Example.show("Confirm result: "+result);
	 if(result){
	 $.post(base_url+'cultivar/remove/', {
			id: id
			//alert("ok")
		}).done(function() {
			//refresh na página
	    window.location.reload(true);
	  }).fail(function() {
		    bootbox.alert("Já existem dados vinculados a esta Embalagem. Não é possível fazer a exclusão." );
		})
	}
});
}
</script>

<div class="modal fade" id="popupDetalhe" tabindex="-1" role="dialog"
	aria-labelledby="popupDetalheLabel">
	<div class="modal-content"
		style="height: auto; width: auto; margin: 150px auto; max-width: 70%">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<h4 class="modal-title" id="titulo"
				style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif"></h4>
		</div>
		<div class="modal-body" id="conteudo" style="font-size: 14px">
			<form class="form-horizontal"
				style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif"
				id="conteudoForm"></form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-success pull-left"
				data-dismiss="modal">Fechar</button>
		</div>
	</div>
</div>

<div class="box box-solid">
	<div class="box-default with-border">
		<h4 style="text-align: center">
			<b>Variedades</b>
		</h4>
	</div>
	<div class="box-body">
		<div id="manutencaoCultivar" class="col-md-12 collapse">
			<form class="form-horizontal" id="frm_cultivar" name="frm_cultivar"
				method="post" action="<?php echo base_url('cultivar/save'); ?>">

				<div class="form-group" style="display: block;">
					<label class="col-sm-2 control-label">Código</label>
					<div class="col-sm-10">
						<input type="text" class="form-control-static" id="cod_empresa"
							name="cod_empresa" size="4" readonly="readonly">
					</div>
				</div>

				<!-- Campo de código -->
				<div class="form-group" style="display: none">
					<label class="col-sm-2 control-label">Código</label>
					<div class="col-sm-10">
						<input type="text" class="form-control-static" id="cod_cultivar"
							name="cod_cultivar" size="4" readonly="readonly">
					</div>
				</div>
				<!-- Campo de descrição -->
				<div class="form-group">
					<label for="lbl_dscultivar" class="col-sm-2 control-label">Descrição</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="ds_cultivar"
							name="ds_cultivar" required="required">
					</div>
				</div>
				<div class="form-group" id="idfm">
					<label for="lbl_cod_cultura" class="col-sm-2 control-label">Cultura</label>
					<div class="col-sm-6" style="padding-right: 0px">
						<select id="cod_cultura" name="cod_cultura" required
							data-live-search="true" data-size="8"
							class="selectpicker form-control">
	               <?php foreach ($culturas as $cultura) { ?>
	                <option selected="<?php ?>"
								value="<?php echo $cultura['cod_cultura']?>"><?php echo $cultura['ds_cultura']?> </option>
	                <?php }?>
	              </select>
					</div>
					<div class="col-xs-2" style="padding-left: 0px">
						<a id="btnAdicionarCultura" class="btn btn-success"
							data-toggle="popover"><span class="glyphicon glyphicon-plus"></span></a>
					</div>
				</div>
				<div id="div-popover" class="hide">
					<div class="input-group input-group-sm">
						<input id="ds_cultura_aux" name="ds_cultura_aux" type="text"
							class="form-control" style="width: 120px" placeholder="Descrição">
						<span class="input-group-btn">
							<button id="btnInsereCultura" onclick="insereCultura()"
								class="btn btn-success btn-success" type="button">
								<span class="glyphicon glyphicon-ok"></span>
							</button>
							<button id="btnCancelaCultura" onclick="cancelaCultura()"
								class="btn btn-danger btn-danger" type="button">
								<span class="glyphicon glyphicon-remove"></span>
							</button>
						</span>
					</div>
				</div>
				<!-- Campo de Tipo -->
				<div class="form-group">
					<label for="lbl_tipo" class="col-sm-2 control-label">Transgênica</label>
					<div class="col-sm-3">
						<input type="checkbox" class="minimal" id="chk_tipo"
							name="chk_tipo" value="S" />
					</div>
				</div>
				<!-- Campo de Crescimento -->
				<div class="form-group">
					<label for="lbl_tp_crescimento" class="col-sm-2 control-label">Crescimento</label>
					<div class="col-sm-8">
						<div class="form-group">
							<div class="radio">
								<label> <input type="radio" class="minimal"
									name="rdb_crescimento" id="rdb_crescimentoD" value="D">
									Determinado
								</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <label> <input
									type="radio" class="minimal" name="rdb_crescimento"
									id="rdb_crescimentoI" value="I"> Indeterminado
								</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <label> <input
									type="radio" class="minimal" name="rdb_crescimento"
									id="rdb_crescimentoS" value="S"> Semi Determinado
								</label>
							</div>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="lbl_diasciclo" class="col-sm-2 control-label">Dias
						Ciclo</label>
					<div class="col-sm-8">
						<input type="text" style="width: 50px;" maxlength="3"
							class="form-control" id="num_diasciclo" name="num_diasciclo"
							placeholder="">
					</div>
				</div>
				<div class="form-group">
					<label for="lbl_anotacao" class="col-sm-2 control-label">Anotações</label>
					<div class="col-sm-8">
						<textarea name="ds_anotacao" id="ds_anotacao" maxlength="2000"
							class="form-control" rows="3"></textarea>
					</div>
				</div>


				<div class="box-footer" align="center">
					<button type="submit" class="btn btn-success">Salvar</button>
					<button type="button" class="btn btn-danger"
						onclick="manutencaoCultivar(2)">Cancelar</button>
				</div>
			</form>
		</div>

		<div id="tabelaCultivar" class="col-md-12 collapse in">
			<table class="table table-bordered table-striped" id="tblCultivar">
				<thead>
					<tr>
						<th class="tbl_col_ordenacao"></th>
						<th style="width: 1%">Código</th>
						<th>Descrição</th>
						<th>Cultura</th>
						<th style="width: 10%">Tipo</th>
						<th style="width: 10%">Ciclo(Dias)</th>
					</tr>
				</thead>
				<tbody>
				 <?php foreach ($cultivares as $cultivar) { ?>
					<tr>
						<td class="tbl_col_ordenacao"><a href="javascript:;"
							onclick="editarCultivar(<?=$cultivar['cod_cultivar']?>)"> <span
								class="fa fa-fw fa-edit"></span>
						</a>
											
						<a	href="javascript:;"	onclick="excluirCultivar(<?=$cultivar['cod_cultivar']?>)">
														<span class="fa fa-fw fa-trash-o"></span>
												</a>
						
						
						<a href="javascript:;"
							onclick="carregaDadosPopUp(<?=$cultivar['cod_cultivar']?>)"> <span
								class="fa fa-fw fa-reorder"></span></td>
						<?php
						
if ($cultivar ['tp_crescimento'] == 'I') {
							$crescimento = 'Indeterminado';
						} else if ($cultivar ['tp_crescimento'] == 'D') {
							$crescimento = 'Determinado';
						} else if ($cultivar ['tp_crescimento'] == 'S') {
							$crescimento = 'Semi Determinado';
						}
						?>
						<td><?=$cultivar['cod_cultivar']?></td>
						<td><?=$cultivar['ds_cultivar']?></td>
						<td><?=$cultivar['ds_cultura']?></td>
						<td><?=$cultivar['ds_tipo'] == 'S' ? 'Transgênica' : 'Convencional'?></td>
						<td><?=$cultivar['num_diasciclo']?></td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>

	</div>