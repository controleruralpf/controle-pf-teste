<script type="text/javascript">

$(document).ready(function() {

	$('input').on('ifChecked', function(event){ 
	  
	  if(event.currentTarget.id == "sn_areapropriaP"){
	  	$('#sn_areapropriaP').iCheck('check');
	  	$('#sn_areapropriaA').iCheck('uncheck');
	  }else if(event.currentTarget.id == "sn_areapropriaA"){
	  	$('#sn_areapropriaA').iCheck('check');
	  	$('#sn_areapropriaP').iCheck('uncheck');
	  }


	});

	$('#popupDetalhe').on('hidden.bs.modal', function (e) {
		$("#titulo").empty();
		$("#conteudo > #conteudoForm").empty();
	})
/*
	$("#cod_tipodefensivo").change(function(){
		//revalida somente o campo de seleção quando ele selecionar algun item.
		var validator = $("#frm_defensivos").validate();
		validator.element("#cod_tipodefensivo");
	});*/

	$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
	  checkboxClass: 'icheckbox_square-green',
	  radioClass: 'iradio_square-green'
	});
	
	$("#frm_areas").validate({
        ignore: ':not(select:hidden, input:visible, textarea:visible)',
        errorPlacement: function (error, element) {
            if ($(element).is('select')) {
                element.next().after(error); // Validação especial para os campos select
            } else {
                error.insertAfter(element);  //Validação normal para os outros campos
            }
        },
        highlight: function(element, errorClass, validClass) {
        		$(element).closest('.form-group').addClass(errorClass).removeClass(validClass);
		    	$(element).closest('.form-group').addClass('has-error');
		  },
		unhighlight: function(element, errorClass, validClass) {
			$(element).closest('.form-group').removeClass(errorClass).addClass(validClass);
			$(element).closest('.form-group').removeClass('has-error');
		  }
    });

	//função que adiciona plugins a tabela de Cultura
	$('#tblAreas').dataTable({
	"iDisplayLength":9,                              
	    "oLanguage": {
	     "sProcessing": "Aguarde enquanto os dados são carregados ...",
	     /*"sLengthMenu": "Mostrar _ hghgghg MENU_ registros por pagina",*/
	     "sLengthMenu": "",
	     "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
	     "sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
	     "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
	     "sInfoFiltered": "",
	     "sSearch": "Procurar",
	     "oPaginate": {
	      "sFirst":    "Primeiro",
	      "sPrevious": "Anterior",
	      "sNext":     "Próximo",
	      "sLast":     "Último"
	    }
	  }                              
	});

	//cria um botão dentro da tabela de cultura
	$("#tblAreas_length").append("<button id='btnCadastrar' class='btn btn-success' onclick='manutencaoAreas(1)'>Cadastrar</button>");
	  
});


function manutencaoAreas(acao){
	if(acao == 1){
		$("#frm_areas input").val("");
		$("#frm_areas textarea").val("");
		//$("#cod_categoriainsumo").val("3");//valor do input 
		$('#manutencaoAreas').collapse('show');
		$('#tabelaAreas').collapse('hide');
		$("#ds_descricao").focus();
		//$("#sn_areapropriaP").iCheck('check');
		$("#sn_areaativa").iCheck('check');
		
	}else if(acao == 2){
		//reseta as validações do form, e tira a classe de erros do formulário
		$("#frm_areas input").val("");
		$("#frm_areas textarea").val("");
		//$("#cod_categoriainsumo").val("3");//valor do input 
		$("#frm_areas").validate().resetForm();
		$(".form-group").removeClass("has-error");

		$('#manutencaoAreas').collapse('hide');
		$('#tabelaAreas').collapse('show');
		//$('#cod_tipodefensivo').selectpicker('val', '');
	}

	   $("#num_hectotal,#num_hecplanta").priceFormat({
	    	  	prefix: '',
	    	   centsSeparator: '',
	    	   thousandsSeparator: '.',
	      limit: 17,
	      centsLimit: 0
		})
}

var base_url = "<?= base_url() ?>";
function carregaDadosAreaJson(id){

		$.post(base_url+'area/dadosArea', {
			id: id
		}, function (data){
         	$('#cod_area').val(data.cod_area);
         	$('#ds_area').val(data.ds_area);
			$('#num_hectotal').val(data.num_hectotal);
			$('#num_hecplanta').val(data.num_hecplanta);
			
			if(data.sn_areapropria == "P"){
				$('#sn_areapropriaP').iCheck('check');
				$('#sn_areapropriaA').iCheck('uncheck');
			}else if(data.sn_areapropria == "A"){
				$('#sn_areapropriaA').iCheck('check');
				$('#sn_areapropriaP').iCheck('uncheck');
			}
			
			

			//$('#sn_areaativa').val(data.sn_areaativa);
			$('#ds_lugar').val(data.ds_lugar);
			$('#ds_observacao').val(data.ds_observacao);

			   $("#num_hectotal,#num_hecplanta").priceFormat({
			    	  	prefix: '',
			    	   centsSeparator: '',
			    	   thousandsSeparator: '.',
			      limit: 17,
			      centsLimit: 0
				})


 		}   , 'json').always(function() {
		 		$('#manutencaoAreas').collapse('show');
		 		$('#tabelaAreas').collapse('hide');

				$("#ds_area").focus();
				});   	
	

}

function editarArea(id){
	carregaDadosAreaJson(id);
}

function excluirArea(id){
  bootbox.confirm("Confirma a exclusão deste registro?", function(result) {
   // Example.show("Confirm result: "+result);
   if(result){
     $.post(base_url+'area/remove', {
      id: id
      //alert("ok")
    }).done(function() {
      //refresh na página
      window.location.reload(true);
    }).fail(function() {
      bootbox.alert("Já existem dados vinculados a esta Área. Não é possível fazer a exclusão." );
    })
  }
});
}

 	function carregaDadosPopUp(id){

 		$.post(base_url+'area/dadosArea', {
 			id: id
 		}, function (data){

         	$("#titulo").append(data.ds_area);
			montaPopUp('Hectares Total',data.num_hectotal);
			montaPopUp('Hectares de Planta',data.num_hecplanta);
			montaPopUp('Área',convertePA(data.sn_areapropria));
			//montaPopUp('Área Ativa?',converteSN(data.sn_areaativa));
			montaPopUp('Local',data.ds_lugar);
			montaPopUp('Observação',data.ds_observacao);

			$('#popupDetalhe').modal('show');
		
     		}, 'json');

		function montaPopUp(valorLabel,valorCampo){
 			$("#conteudo > #conteudoForm").append('<div class="form-group" id="div-campos"><label class="col-sm-2 control-label" id="label-popup">'+valorLabel+'</label><label class="col-sm-8 control-label" id="valor-popup" style="text-align: left;font-weight: 400;">'+valorCampo+'</label></div>')
     	}

    	function convertePA(valor){
    			if(valor == "P"){
    				valor = 'Própria';
    			}else{
    				valor = 'Arrendada';
    			}
    	return valor;
		}
}



  </script>

  <div class="modal fade" id="popupDetalhe" tabindex="-1" role="dialog" aria-labelledby="popupDetalheLabel">
  	<div class="modal-content" style="height: auto; width: auto; margin: 150px auto; max-width: 70%">
  		<div class="modal-header">
  			<button type="button" class="close" data-dismiss="modal"
  				aria-label="Close">
  				<span aria-hidden="true">×</span>
  			</button>
  			<h4 class="modal-title" id="titulo" style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif"></h4>
  		</div>
  		<div class="modal-body" id="conteudo" style="font-size: 14px">
  			<form class="form-horizontal" style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif" id="conteudoForm">
  			</form>
  		</div>
  		<div class="modal-footer">
  			<button type="button" class="btn btn-success pull-left"	data-dismiss="modal">Fechar</button>
  		</div>
  	</div>
  </div>

<div class="box box-solid">
	<div class="box-default with-border">
		<h4 style="text-align: center">
			<b>Areas</b>
		</h4>
	</div>

	<div class="box-body">
		<div id="manutencaoAreas" class="col-md-12 collapse">
			<form class="form-horizontal" id="frm_areas"
				name="frm_areas" method="post"
				action="<?php echo base_url('area/save/'.'area'.""); ?>">
				
				<div class="form-group" style="display: none;">
					<label class="col-sm-2 control-label">Código</label>
					<div class="col-sm-10">
						<input type="text" class="form-control-static" id="cod_area"
							name="cod_area" size="4" readonly="readonly">
					</div>
				</div>

				<div class="form-group">
					<label for="lbl_area" class="col-sm-2 control-label">Descrição</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="ds_area"
							name="ds_area" placeholder="" required="required">
					</div>
				</div>
				
				<div class="form-group">
							<label for="lbl_num_hectotal" class="col-sm-2 control-label">Total de Hectares</label>
							<div class="col-sm-1">
								<input name="num_hectotal" id="num_hectotal"
									 class="form-control"></input>
							</div>
							<label for="lbl_num_hecplanta" class="col-sm-2 control-label">Total de Planta</label>
							<div class="col-sm-1">
								<input name="num_hecplanta" id="num_hecplanta"
									 class="form-control"></input>
							</div>
				</div>

					<!-- Campo de Crescimento -->
					<div class="form-group">
						<label for="lbl_sn_areapropria" class="col-sm-2 control-label"></label>
						<div class="col-sm-8">
							 <div class="form-group">
	                      <div class="radio">
	                         <label>
	                          <input type="radio" class="minimal" name="sn_areapropriaP" id="sn_areapropriaP" value="P">
	                          Própria
	                        </label>
	                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	                        <label>
	                          <input type="radio" class="minimal" name="sn_areapropriaA" id="sn_areapropriaA" value="A">
	                         Arrendada
	                        </label>
	                      </div>
	                    </div>
						</div>
					</div>

				<!-- Campo de Tipo 
				<div class="form-group">
					<label for="lbl_tipo" class="col-sm-2 control-label">Esta Área esta Ativa?</label>
					<div class="col-sm-3">
						<input type="checkbox" class="minimal" id="sn_areaativa"  name="sn_areaativa" value="S" />
					</div>
				</div>
-->
				<div class="form-group">
					<label for="lbl_ds_lugar" class="col-sm-2 control-label">Local</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="ds_lugar"
							name="ds_lugar" placeholder="" required="required">
					</div>
				</div>
				
				<div class="form-group">
					<label for="lbl_observacao" class="col-sm-2 control-label">Observação</label>
					<div class="col-sm-8">
						<textarea name="ds_observacao" id="ds_observacao" maxlength="2000"
							class="form-control" rows="3"></textarea>
					</div>
				</div>

				<div class="box-footer" align="center">
					<button type="submit" class="btn btn-success">Salvar</button>
					<button type="button" class="btn btn-danger"
						onclick="manutencaoAreas(2)">Cancelar</button>
				</div>

			</form>
		</div>

		<div id="tabelaAreas" class="col-md-12 collapse in">
		
			<table class="table table-bordered table-striped" id="tblAreas">
				<thead>
					<tr>
						<th class="tbl_col_ordenacao"></th>
						<th style="width: 1%">Código</th>
						<th>Descrição</th>
						<th style="width: 10%">Hec. Total</th>
						<th style="width: 10%">Hec. Planta</th>
						
						<th>Local</th>
					</tr>
				</thead>
				<tbody>
						
					 <?php foreach ($areas as $area) { ?>
						<tr>
						<td class="tbl_col_ordenacao"><a href="javascript:;"
						<a href="javascript:;" onclick="editarArea(<?=$area['cod_area']?>)"> 
						  <span class="fa fa-fw fa-edit"></span>
						</a>						
						<a  href="javascript:;" onclick="excluirArea(<?=$area['cod_area']?>)">
						                <span class="fa fa-fw fa-trash-o"></span>
						            </a>
						<a href="javascript:;"
						  onclick="carregaDadosPopUp(<?=$area['cod_area']?>)">
						    <span class="fa fa-fw fa-reorder"></span></td>
						<td><?=$area['cod_area']?></td>
						<td><?=$area['ds_area']?></td>
						<td><?=$area['num_hectotal']?></td>
						<td><?=$area['num_hecplanta']?></td>
						<td><?=$area['ds_lugar']?></td>
					</tr>
						<?php }?> 
						
					</tbody>
			</table>
			
		</div>

	</div>