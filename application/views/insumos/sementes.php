<script type="text/javascript">

$(document).ready(function() {

	$('#popupDetalhe').on('hidden.bs.modal', function (e) {
		$("#titulo").empty();
		$("#conteudo > #conteudoForm").empty();
	})

	$('#btnAdicionarCultivar').popover({
        trigger: 'manual',
        html: true,
        title: '<p align="center"><b>Variedade</b></p>',
        content: $('#div-popover').html() // Adiciona o conteúdo da div oculta para dentro do popover.
     }).click(function (e) {
        e.preventDefault();
        // Exibe o popover.
        $(this).popover('show');
     });
	
	$("#cod_cultivar").change(function(){
		//revalida somente o campo de seleção quando ele selecionar algun item.
		var validator = $("#frm_sementes").validate();
		validator.element("#cod_cultivar");
	});
	
	$("#frm_sementes").validate({
        ignore: ':not(select:hidden, input:visible, textarea:visible)',
        errorPlacement: function (error, element) {
            if ($(element).is('select')) {
                element.next().after(error); // Validação especial para os campos select
            } else {
                error.insertAfter(element);  //Validação normal para os outros campos
            }
        },
        highlight: function(element, errorClass, validClass) {
        		$(element).closest('.form-group').addClass(errorClass).removeClass(validClass);
		    	$(element).closest('.form-group').addClass('has-error');
		  },
		unhighlight: function(element, errorClass, validClass) {
			$(element).closest('.form-group').removeClass(errorClass).addClass(validClass);
			$(element).closest('.form-group').removeClass('has-error');
		  }
    });
	
	//função que adiciona plugins a tabela de Cultura
	$('#tblSementes').dataTable({
		"iDisplayLength":9,                              
	    "oLanguage": {
	     "sProcessing": "Aguarde enquanto os dados são carregados ...",
	     /*"sLengthMenu": "Mostrar _ hghgghg MENU_ registros por pagina",*/
	     "sLengthMenu": "",
	     "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
	     "sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
	     "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
	     "sInfoFiltered": "",
	     "sSearch": "Procurar",
	     "oPaginate": {
	      "sFirst":    "Primeiro",
	      "sPrevious": "Anterior",
	      "sNext":     "Próximo",
	      "sLast":     "Último"
	    }
	  }                              
	});

	//cria um botão dentro da tabela de cultura
	$("#tblSementes_length").append("<button id='btnCadastrar' class='btn btn-success' onclick='manutencaoSementes(1)'>Cadastrar</button>");
	  
});


function manutencaoSementes(acao){
	if(acao == 1){
		$("#frm_sementes input").val("");
		$("#frm_sementes textarea").val("");
		//$("#cod_categoriainsumo").val("3");//valor do input 
		$('#manutencaoSementes').collapse('show');
		$('#tabelaSementes').collapse('hide');
		$("#ds_sementes").focus();
		$('#cod_cultivar').selectpicker('val', '');
		
	}else if(acao == 2){
		//reseta as validações do form, e tira a classe de erros do formulário
		$("#frm_sementes input").val("");
		$("#frm_sementes textarea").val("");
		//$("#cod_categoriainsumo").val("3");//valor do input 
		$("#frm_sementes").validate().resetForm();
		$(".form-group").removeClass("has-error");

		$('#manutencaoSementes').collapse('hide');
		$('#tabelaSementes').collapse('show');
		$('#cod_cultivar').selectpicker('val', '');
	}
}

function carregaDadosCultivarJson(){

	$.post(base_url+'cultivar/listaCultivar', {
		}, function (data){
		$('#cod_cultivar').children().remove().end();//.append('<option selected value="">Selecione um item</option>') ;
    		$(data).each(function(){
    			$("#cod_cultivar").append('<option value="'+this.cod_cultivar+'">'+this.ds_cultivar+'</option>');
			});
		$('#cod_cultivar').selectpicker('refresh');

  		}, 'json').always(function(data) {
				//busca o ultimo código cadastrado
				var sel = 1;
  			$(data).each(function(){
    			if(sel < this.cod_cultivar){
					sel = this.cod_cultivar;
            	}
			});
			//seleciona o último cadastrado
			 $('#cod_cultivar').selectpicker('val', sel);

	});;

}

function insereCultivar(){
	var ds_cultivar_aux = $('#ds_cultivar_aux').val();
	var parametros = {ds_cultivar:ds_cultivar_aux,cod_cultura:'3'};

	if(ds_cultivar_aux != ""){
		$.ajax({
			url: "cultivar/save",
			type: "post",
			data: parametros,
			success: function(){
			$('#btnAdicionarCultivar').popover('hide');
			$('#ds_cultivar_aux').val("");
		},
			error:function(){
			alert("Errou");
			}
		}).always(function() {
			//após a inserção
			carregaDadosCultivarJson();
		})
	}else{
		$('#btnAdicionarCultivar').popover('show');
	}
}

function cancelaCultivar(){
$('#btnAdicionarCultivar').popover('hide');
 $('#ds_cultivar_aux').val("");
}

var base_url = "<?= base_url() ?>";
function carregaDadosSementeJson(id){

		$.post(base_url+'sementes/dadosSementes', {
			id: id
		}, function (data){

			//$('#cod_categoriainsumo').val(data.cod_categoria);
			$('#cod_insumo').val(data.cod_insumo);
			$('#ds_insumo').val(data.ds_insumo);
			$('#ds_origem').val(data.ds_origem);
			$('#ds_observacao').val(data.ds_observacao);
			$('#cod_cultivar').selectpicker('val', data.cod_cultivar);
			$('#ds_lote').val(data.ds_lote);
			$('#ds_vigor').val(data.ds_vigor);
			$('#ds_germinacao').val(data.ds_germinacao);
			$('#ds_peneira').val(data.ds_peneira);
			
			

 		
 		}   , 'json').always(function() {
		 		$('#manutencaoSementes').collapse('show');
		 		$('#tabelaSementes').collapse('hide');

				$("#ds_insumo").focus();
				});  

}

function editarSemente(id){
	carregaDadosSementeJson(id);
}

function excluirSemente(id){
  bootbox.confirm("Confirma a exclusão deste registro?", function(result) {
   // Example.show("Confirm result: "+result);
   if(result){
     $.post(base_url+'sementes/remove', {
      id: id
      //alert("ok")
    }).done(function() {
      //refresh na página
      window.location.reload(true);
    }).fail(function() {
      bootbox.alert("Já existem dados vinculados a esta Semente. Não é possível fazer a exclusão." );
    })
  }
});
}

 	function carregaDadosPopUp(id){

 		$.post(base_url+'sementes/dadosSementes', {
 			id: id
 		}, function (data){

         	$("#titulo").append(data.ds_insumo);
         	montaPopUp('Variedade',data.ds_cultivar);
			montaPopUp('Origem da Semente',data.ds_origem);
			montaPopUp('Lote',data.ds_lote);
			montaPopUp('Germinação',data.ds_germinacao);
			montaPopUp('Vigor',data.ds_vigor);
			montaPopUp('Peneira',data.ds_peneira);
			montaPopUp('Observação',data.ds_observacao);
			
			$('#popupDetalhe').modal('show');
		
     		}, 'json');

		function montaPopUp(valorLabel,valorCampo){
 			$("#conteudo > #conteudoForm").append('<div class="form-group" id="div-campos"><label class="col-sm-2 control-label" id="label-popup">'+valorLabel+'</label><label class="col-sm-8 control-label" id="valor-popup" style="text-align: left;font-weight: 400;">'+valorCampo+'</label></div>')
     	}
 	}

  </script>

  <div class="modal fade" id="popupDetalhe" tabindex="-1" role="dialog" aria-labelledby="popupDetalheLabel">
  	<div class="modal-content" style="height: auto; width: auto; margin: 150px auto; max-width: 70%">
  		<div class="modal-header">
  			<button type="button" class="close" data-dismiss="modal"
  				aria-label="Close">
  				<span aria-hidden="true">×</span>
  			</button>
  			<h4 class="modal-title" id="titulo" style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif"></h4>
  		</div>
  		<div class="modal-body" id="conteudo" style="font-size: 14px">
  			<form class="form-horizontal" style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif" id="conteudoForm">
  			</form>
  		</div>
  		<div class="modal-footer">
  			<button type="button" class="btn btn-success pull-left"	data-dismiss="modal">Fechar</button>
  		</div>
  	</div>
  </div>

<div class="box box-solid">
	<div class="box-default with-border">
		<h4 style="text-align: center">
			<b>Sementes</b>
		</h4>
	</div>

	<div class="box-body">
		<div id="manutencaoSementes" class="col-md-12 collapse">
			<form class="form-horizontal" id="frm_sementes" name="frm_sementes"
				method="post" action="<?php echo base_url('sementes/save/'.'sementes'.""); ?>">
				<div class="form-group" style="display: none;">
					<label class="col-sm-2 control-label">Código</label>
					<div class="col-sm-10">
						<input type="text" class="form-control-static" id="cod_insumo"
							name="cod_insumo" size="4" readonly="readonly">
					</div>
				</div>
				<!-- Tipo de Defensivo Sementes código fixo banco -> 3 
				<div class="form-group" style="display: none">
					<label class="col-sm-2 control-label">Código</label>
					<div class="col-sm-10">
						<input type="text" id="cod_categoriainsumo"
							name="cod_categoriainsumo" size="4" value="3">
					</div>
				</div>-->
				
				<div class="form-group">
					<label for="lbl_insumo" class="col-sm-2 control-label">Descrição</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="ds_insumo"
							name="ds_insumo" placeholder=""  required="required">
					</div>
				</div>
				
				<!-- Campo de Variedade -->
				<div class="form-group">
					<label for="lbl_cod_cultivar" class="col-sm-2 control-label">Variedade</label>
					<div class="col-sm-4" style="padding-right: 0px">
						<select id="cod_cultivar" name="cod_cultivar"
							data-live-search="false" data-size="auto"
							class="selectpicker form-control" required="required">
							<option value="">Selecione uma variedade</option>
									               <?php foreach ($cultivares as $cultivar) { ?>
									                <option
								value="<?php echo $cultivar['cod_cultivar']?>"><?php echo $cultivar['ds_cultivar']?> </option>
									                <?php }?>
	              </select>
					</div>
				<!--  	<div class="col-xs-2" style="padding-left: 0px">
						<a id="btnAdicionarCultivar" class="btn btn-success"
							data-toggle="popover"><span class="glyphicon glyphicon-plus"></span></a>
					</div>-->
				</div>
 <!--
				<div id="div-popover" class="hide">
					<div class="input-group input-group-sm">
						<input id="ds_cultivar_aux" name="ds_cultivar_aux" type="text"
							class="form-control" style="width: 120px" placeholder="Descrição">
						<span class="input-group-btn">
							<button id="btnInsereCultivar" onclick="insereCultivar()"
								class="btn btn-success btn-success" type="button">
								<span class="glyphicon glyphicon-ok"></span>
							</button>
							<button id="btnCancelaCultivar" onclick="cancelaCultivar()"
								class="btn btn-danger btn-danger" type="button">
								<span class="glyphicon glyphicon-remove"></span>
							</button>
						</span>
					</div>
				</div> -->
				
				<!--Fim do  Campo de Variedade -->

				<div class="form-group">
					<label for="lbl_origem" class="col-sm-2 control-label">Origem da
						Semente</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="ds_origem"
							name="ds_origem">
					</div>
					<label for="lbl_lote" class="col-sm-2 control-label">Lote</label>
					<div class="col-sm-2">
						<input type="text" class="form-control" id="ds_lote"
							name="ds_lote" placeholder="">
					</div>
				</div>

				<div class="form-group">
					<label for="lbl_germinacao" class="col-sm-2 control-label">Germinação</label>
					<div class="col-sm-2">
						<input type="text" class="form-control" id="ds_germinacao"
							name="ds_germinacao" placeholder="">
					</div>
					<label for="lbl_vigor" class="col-sm-2 control-label">Vigor</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="ds_vigor"
							name="ds_vigor" placeholder="">
					</div>
				</div>

				<div class="form-group">
					<label for="lbl_peneira" class="col-sm-2 control-label">Peneira</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="ds_peneira"
							name="ds_peneira" placeholder="">
					</div>
				</div>
				<div class="form-group">
					<label for="lbl_observacao" class="col-sm-2 control-label">Observação</label>
					<div class="col-sm-8">
						<textarea name="ds_observacao" id="ds_observacao" maxlength="2000"
							class="form-control" rows="3"></textarea>
					</div>
				</div>

				<div class="box-footer" align="center">
					<button type="submit" class="btn btn-success">Salvar</button>
					<button type="button" class="btn btn-danger"
						onclick="manutencaoSementes(2)">Cancelar</button>
				</div>
			</form>
		</div>

		<div id="tabelaSementes" class="col-md-12 collapse in">
			<table class="table table-bordered table-striped" id="tblSementes">
				<thead>
					<tr>
						<th class="tbl_col_ordenacao"></th>
						<th style="width: 1%">Código</th>
						<th>Descrição</th>
						<th>Variedade</th>
					</tr>
				</thead>
				<tbody>
					 <?php foreach ($sementes as $semente) { ?>
						<tr>
						<td class="tbl_col_ordenacao">
						<a href="javascript:;"
						<a href="javascript:;" onclick="editarSemente(<?=$semente['cod_insumo']?>)"> 
						  <span class="fa fa-fw fa-edit"></span>
						</a>						
						<a  href="javascript:;" onclick="excluirSemente(<?=$semente['cod_insumo']?>)">
						                <span class="fa fa-fw fa-trash-o"></span>
						            </a>
						<a href="javascript:;"
						  onclick="carregaDadosPopUp(<?=$semente['cod_insumo']?>)">
						    <span class="fa fa-fw fa-reorder"></span></td>
						<td><?=$semente['cod_insumo']?></td>
						<td><?=$semente['ds_descricao']?></td>
						<td><?=$semente['ds_cultivar']?></td>
					</tr>
						<?php }?> 
					</tbody>
			</table>
		</div>

	</div>