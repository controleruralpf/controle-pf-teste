<script type="text/javascript">

$(document).ready(function() {

	$('#popupDetalhe').on('hidden.bs.modal', function (e) {
		$("#titulo").empty();
		$("#conteudo > #conteudoForm").empty();
	})

	$("#cod_tipodefensivo").change(function(){
		//revalida somente o campo de seleção quando ele selecionar algun item.
		var validator = $("#frm_defensivos").validate();
		validator.element("#cod_tipodefensivo");
	});
	
	$("#frm_defensivos").validate({
        ignore: ':not(select:hidden, input:visible, textarea:visible)',
        errorPlacement: function (error, element) {
            if ($(element).is('select')) {
                element.next().after(error); // Validação especial para os campos select
            } else {
                error.insertAfter(element);  //Validação normal para os outros campos
            }
        },
        highlight: function(element, errorClass, validClass) {
        		$(element).closest('.form-group').addClass(errorClass).removeClass(validClass);
		    	$(element).closest('.form-group').addClass('has-error');
		  },
		unhighlight: function(element, errorClass, validClass) {
			$(element).closest('.form-group').removeClass(errorClass).addClass(validClass);
			$(element).closest('.form-group').removeClass('has-error');
		  }
    });
	
	//função que adiciona plugins a tabela de Cultura
	$('#tblDefensivos').dataTable({                              
	    "oLanguage": {
	     "sProcessing": "Aguarde enquanto os dados são carregados ...",
	     /*"sLengthMenu": "Mostrar _ hghgghg MENU_ registros por pagina",*/
	     "sLengthMenu": "",
	     "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
	     "sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
	     "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
	     "sInfoFiltered": "",
	     "sSearch": "Procurar",
	     "oPaginate": {
	      "sFirst":    "Primeiro",
	      "sPrevious": "Anterior",
	      "sNext":     "Próximo",
	      "sLast":     "Último"
	    }
	  }                              
	});

	//cria um botão dentro da tabela de cultura
	$("#tblDefensivos_length").append("<button id='btnCadastrar' class='btn btn-success' onclick='manutencaoDefensivos(1)'>Cadastrar</button>");
	  
});


function manutencaoDefensivos(acao){
	if(acao == 1){
		$("#frm_defensivos input").val("");
		$("#frm_defensivos textarea").val("");
		//$("#cod_categoriainsumo").val("3");//valor do input 
		$('#manutencaoDefensivos').collapse('show');
		$('#tabelaDefensivos').collapse('hide');
		$("#ds_descricao").focus();
		$('#cod_tipodefensivo').selectpicker('val', '');
		
	}else if(acao == 2){
		//reseta as validações do form, e tira a classe de erros do formulário
		$("#frm_defensivos input").val("");
		$("#frm_defensivos textarea").val("");
		//$("#cod_categoriainsumo").val("3");//valor do input 
		$("#frm_defensivos").validate().resetForm();
		$(".form-group").removeClass("has-error");

		$('#manutencaoDefensivos').collapse('hide');
		$('#tabelaDefensivos').collapse('show');
		$('#cod_tipodefensivo').selectpicker('val', '');
	}
}

var base_url = "<?= base_url() ?>";
function carregaDadosDefensivosJson(id){

		$.post(base_url+'defensivos/dadosDefensivos', {
			id: id
		}, function (data){

			$('#cod_insumo').val(data.cod_insumo);
			$('#cod_categoria').val(data.cod_categoria);
			$('#ds_descricao').val(data.ds_descricao);
			$('#ds_observacao').val(data.ds_observacao);
			$('#cod_tipodefensivo').selectpicker('val', data.cod_tipodefensivo);
			$('#ds_utilizacao').val(data.ds_utilizacao);
			
 		}   , 'json').always(function() {
		 		$('#manutencaoDefensivos').collapse('show');
		 		$('#tabelaDefensivos').collapse('hide');

				$("#ds_insumo").focus();
				});   	
	

}

function editarDefensivo(id){
	carregaDadosDefensivosJson(id);
}

function excluirDefensivo(id){
  bootbox.confirm("Confirma a exclusão deste registro?", function(result) {
   // Example.show("Confirm result: "+result);
   if(result){
     $.post(base_url+'defensivos/remove', {
      id: id
      //alert("ok")
    }).done(function() {
      //refresh na página
      window.location.reload(true);
    }).fail(function() {
      bootbox.alert("Já existem dados vinculados a este Defensivo. Não é possível fazer a exclusão." );
    })
  }
});
}

 	function carregaDadosPopUp(id){

 		$.post(base_url+'defensivos/dadosDefensivos', {
 			id: id
 		}, function (data){

         	$("#titulo").append(data.ds_descricao);
			montaPopUp('Tipo de Defensivo',data.ds_tipodefensivo);
			montaPopUp('Recomendações de Uso',data.ds_utilizacao);
			montaPopUp('Observação',data.ds_observacao);
			
			
			$('#popupDetalhe').modal('show');
		
     		}, 'json');

		function montaPopUp(valorLabel,valorCampo){
 			$("#conteudo > #conteudoForm").append('<div class="form-group" id="div-campos"><label class="col-sm-2 control-label" id="label-popup">'+valorLabel+'</label><label class="col-sm-8 control-label" id="valor-popup" style="text-align: left;font-weight: 400;">'+valorCampo+'</label></div>')
     	}
 	}

  </script>

  <div class="modal fade" id="popupDetalhe" tabindex="-1" role="dialog" aria-labelledby="popupDetalheLabel">
  	<div class="modal-content" style="height: auto; width: auto; margin: 150px auto; max-width: 70%">
  		<div class="modal-header">
  			<button type="button" class="close" data-dismiss="modal"
  				aria-label="Close">
  				<span aria-hidden="true">×</span>
  			</button>
  			<h4 class="modal-title" id="titulo" style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif"></h4>
  		</div>
  		<div class="modal-body" id="conteudo" style="font-size: 14px">
  			<form class="form-horizontal" style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif" id="conteudoForm">
  			</form>
  		</div>
  		<div class="modal-footer">
  			<button type="button" class="btn btn-success pull-left"	data-dismiss="modal">Fechar</button>
  		</div>
  	</div>
  </div>

<div class="box box-solid">
	<div class="box-default with-border">
		<h4 style="text-align: center">
			<b>Defensivos</b>
		</h4>
	</div>

	<div class="box-body">
		<div id="manutencaoDefensivos" class="col-md-12 collapse">
			<form class="form-horizontal" id="frm_defensivos"
				name="frm_defensivos" method="post"
				action="<?php echo base_url('defensivos/save/'.'defensivos'.""); ?>">
				<div class="form-group" style="display: none;">
					<label class="col-sm-2 control-label">Código</label>
					<div class="col-sm-10">
						<input type="text" class="form-control-static" id="cod_insumo"
							name="cod_insumo" size="4" readonly="readonly">
					</div>
				</div>

				<div class="form-group">
					<label for="lbl_insumo" class="col-sm-2 control-label">Descrição</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id=ds_descricao
							name="ds_descricao" placeholder="" required="required">
					</div>
				</div>

				<!-- Campo de Variedade -->
				<div class="form-group">
					<label for="lbl_cod_tipodefensivo" class="col-sm-2 control-label">Tipo
						de Defensivo</label>
					<div class="col-sm-6" style="padding-right: 0px">
						<select id="cod_tipodefensivo" name="cod_tipodefensivo"
							data-live-search="false" class="selectpicker form-control"
							required="required">
							<option value="">Selecione um Tipo</option>
               <?php foreach ($tipoDefensivos as $tipoDefensivo) { ?>
                <option
								value="<?php echo $tipoDefensivo['cod_tipodefensivo']?>"><?php echo $tipoDefensivo['ds_tipodefensivo']?> </option>
                <?php }?>
              </select>
					</div>
				</div>
				
				<div class="form-group" id="divinfoutilizacao">
							<label for="lbl_Utilizacao" class="col-sm-2 control-label">Recomendações de Uso</label>
							<div class="col-sm-8">
								<textarea name="ds_utilizacao" id="ds_utilizacao"
									maxlength="2000" class="form-control" rows="5"></textarea>
							</div>
						</div>
				
				<div class="form-group">
					<label for="lbl_observacao" class="col-sm-2 control-label">Observação</label>
					<div class="col-sm-8">
						<textarea name="ds_observacao" id="ds_observacao" maxlength="2000"
							class="form-control" rows="3"></textarea>
					</div>
				</div>

				<div class="box-footer" align="center">
					<button type="submit" class="btn btn-success">Salvar</button>
					<button type="button" class="btn btn-danger"
						onclick="manutencaoDefensivos(2)">Cancelar</button>
				</div>

			</form>
		</div>

		<div id="tabelaDefensivos" class="col-md-12 collapse in">
			<table class="table table-bordered table-striped" id="tblDefensivos">
				<thead>
					<tr>
						<th class="tbl_col_ordenacao"></th>
						<th style="width: 1%">Código</th>
						<th>Descrição</th>
						<th>Tipo</th>
					</tr>
				</thead>
				<tbody>
					 <?php foreach ($defensivos as $defensivo) { ?>
						<tr>
						<td class="tbl_col_ordenacao"><a href="javascript:;"
						<a href="javascript:;" onclick="editarDefensivo(<?=$defensivo['cod_insumo']?>)"> 
						  <span class="fa fa-fw fa-edit"></span>
						</a>						
						<a  href="javascript:;" onclick="excluirDefensivo(<?=$defensivo['cod_insumo']?>)">
						                <span class="fa fa-fw fa-trash-o"></span>
						            </a>
						<a href="javascript:;"
						  onclick="carregaDadosPopUp(<?=$defensivo['cod_insumo']?>)">
						    <span class="fa fa-fw fa-reorder"></span></td>
						<td><?=$defensivo['cod_insumo']?></td>
						<td><?=$defensivo['ds_descricao']?></td>
						<td><?=$defensivo['ds_tipodefensivo']?></td>
					</tr>
						<?php }?> 
					</tbody>
			</table>
		</div>

	</div>