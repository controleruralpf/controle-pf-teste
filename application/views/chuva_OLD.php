<script type="text/javascript">

 $(document).ready(function() {


	 $("input").blur(function(){
		 
	     if($(this).val() != "")
	    	 
	           if ( $(this).attr('name').substring(0, 10) == 'num_mmarea'){
	        	   jQuery.ajax({
	        		    url : base_url + 'Chuva' + '/saveChuvasAreas',
	        		    type : 'POST', 
	        		    data : {  'codchuva': $('#cod_chuva').val(),
		        		    	  'codarea': $(this).attr('name').substring(11,20),
		        		    	  'mili':$(this).val()},
	        		    dataType: 'json',
	        		    success : function(data){ 
	        		                    alert(data);
	        		                }
	        		});
		          
	           }
         });

	
	 
	 $('input').iCheck({
		    checkboxClass: 'icheckbox_square-blue',
		    radioClass: 'iradio_square-blue',
		    increaseArea: '100%' // optional
		  });
	 
	 $("#dt_chuvaini").datepicker({language: 'pt-BR', format: 'dd/mm/yyyy'});
	 $("#dt_chuvafim").datepicker({language: 'pt-BR', format: 'dd/mm/yyyy'});

	 
	//iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_square-green',
      radioClass: 'iradio_square-green'
    });
    
	//CHECK BOX DO LADO
	 $('input').iCheck({
	    checkboxClass: 'icheckbox_square-blue',
	    radioClass: 'iradio_square-blue',
	    increaseArea: '100%' // optional
	  });


	
	 $('#chk_todaarea').on('ifChecked', function(event){
		 $('#manutencaoChuvaAreas').collapse('hide');
		 $('#mmchuva').collapse('show');
		});

	 
	 $('#chk_todaarea').on('ifUnchecked', function(event){
		
		// $('#manutencaoChuvaAreas').collapse('show');
		 $('#mmchuva').collapse('hide');
		});

	 
	$("#cod_chuva").change(function(){
		//revalida somente o campo de seleção quando ele selecionar algun item.
		var validator = $("#frm_Chuva").validate();
		validator.element("#cod_chuva");
	});


	
	$("#frm_embalagem").validate({
        ignore: ':not(select:hidden, input:visible, textarea:visible)',
        errorPlacement: function (error, element) {
            if ($(element).is('select')) {
                element.next().after(error); // Validação especial para os campos select
            } else {
                error.insertAfter(element);  //Validação normal para os outros campos
            }
        },
        highlight: function(element, errorClass, validClass) {
        		$(element).closest('.form-group').addClass(errorClass).removeClass(validClass);
		    	$(element).closest('.form-group').addClass('has-error');
		  },
		unhighlight: function(element, errorClass, validClass) {
			$(element).closest('.form-group').removeClass(errorClass).addClass(validClass);
			$(element).closest('.form-group').removeClass('has-error');
		  }
    });

	
	//função que adiciona plugins a tabela de Cultivar
	$('#tblChuva').dataTable({                              
	    "oLanguage": {
	     "sProcessing": "Aguarde enquanto os dados são carregados ...",
	     /*"sLengthMenu": "Mostrar _ hghgghg MENU_ registros por pagina",*/
	     "sLengthMenu": "",
	     "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
	     "sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
	     "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
	     "sInfoFiltered": "",
	     "sSearch": "Procurar",
	     "oPaginate": {
	      "sFirst":    "Primeiro",
	      "sPrevious": "Anterior",
	      "sNext":     "Próximo",
	      "sLast":     "Último"
	    }
	  }                              
	});

	//cria um botão dentro da tabela de Cultivar
	$("#tblChuva_length").append("<button id='btnCadastrar' class='btn btn-success' onclick='manutencaoChuva(1)'>Cadastrar</button>");
	
	
});


function manutencaoChuva(acao){

	if(acao == 1){
		limpaForm();
		$('#manutencaoChuva').collapse('show');
		
		$('#tabelaChuva').collapse('hide');
	
			if ($('#cod_chuva').val() == ""){
				$("#chk_todaarea").iCheck('check');
				$('#manutencaoChuvaAreas').collapse('hide');
			}
		 
		
	}else if(acao == 2){
		limpaForm();
		$('#manutencaoChuva').collapse('hide');
		$('#manutencaoChuvaAreas').collapse('hide');
		$('#tabelaChuva').collapse('show');

	}
}

function limpaForm(){
	$("#frm_chuva input").val("");
	$("#frm_chuva textarea").val("");
	$("#dt_chuvaini").val("");
	$("#dt_chuvafim").val("");
	$("#chk_todaarea").iCheck('uncheck');
}

function editarChuva(id){
		carregaDadosChuvaJson(id);
		
	}

function saveChuvaArea(){
	alert(this.val);
}

var base_url = "<?= base_url() ?>";

	 function carregaDadosChuvaJson(id){
		limpaForm();
	
			
 		$.post(base_url+'Chuva/dadosChuva', {
 	     			id: id
 	     		}, function (data){
 	     		     
 	     			$('#cod_chuva').val(data.cod_chuva);
 	     			$('#dt_chuvaini').val(data.dt_ini);
 	     			$('#dt_chuvafim').val(data.dt_fim);
 	     			$('#ds_observacao').val(data.ds_observacao);

						if(data.sn_todasarea === 'N'){
 	     					$("#chk_todaarea").iCheck('uncheck');
 	     					 $('#manutencaoChuvaAreas').collapse('show');
						}else{
 	     					$("#chk_todaarea").iCheck('check');
 	     					 $('#manutencaoChuvaAreas').collapse('hide');
 	 	 	     		}

							
 	     			carregaDadoChuvasAreasJson(data.cod_chuva);
 	     			
 	         		}   , 'json');



						
 		//if(data.sn_todasarea ==='N'){
			// $('#mmchuva').collapse('hide');
			//}else{
			//	 $('#mmchuva').collapse('hide');
			//	} 
 		
 	     		$('#manutencaoChuva').collapse('show');
 	    		$('#tabelaChuva').collapse('hide');
 	    		

	     	}

	 	function carregaDadoChuvasAreasJson(codChuva){
	     		$.post(base_url+'Chuva/dadosChuvaArea', {
	     	     			id: codChuva
	     	     		}, function (data){

	     	     			for (var i in data){
	     	     				$('#cod_chuvaarea').val(data[i].cod_chuvaarea);
		     	     			$('#num_mmarea-'+data[i].cod_area).val(data[i].num_mmchuvatotal);
	     	     			}
	     	     			
	     	         		}   , 'json');
	      	
	    	     	}


		 function saveChuva(){

				//alert($('#chk_todaarea').checked);
		 		 jQuery.ajax({
	        		    url : base_url + 'Chuva' + '/save',
	        		    type : 'POST', 
	        		    data : {  cod_chuva: $('#cod_chuva').val(),
		 					dt_chuvaini: $('#dt_chuvaini').val(),
		 					dt_chuvafim: $('#dt_chuvafim').val(),
		 					chk_todaarea: document.getElementById("chk_todaarea").checked,
		 					num_milimetro: $('#num_milimetro').val(),
		 					ds_observacao: $('#ds_observacao').val()},
	        		    dataType: 'json',
	        		    success : function(data){ 
							//se chuva em toda area
							if(data == '1'){
								 	$('#manutencaoChuvaAreas').collapse('hide');
									manutencaoChuva(2);
									location.reload();
									}else{
										 $('#manutencaoChuvaAreas').collapse('show');
									}
	        		                },
	        		            error:  function(data){ 

	    	        		    	alert(data);

		        		            }   
	        		});

			     	}
     	
</script>

<div class="box box-solid">
	<div class="box-default with-border">
		<h4 style="text-align: center">
			<b>Manutenção de Chuvas</b>
		</h4>
	</div>

	<div class="box-body">

		<div class="row">

			<div id="manutencaoChuva" class="col-md-6 collapse">
				<form class="form-horizontal" id="frm_chuva" name="frm_chuva"
					method="post" action="<?php echo base_url('chuva/save'); ?>">

					<!-- Campo de código -->
					<div class="form-group" style="display: none">
						<label class="col-sm-2 control-label">Código</label>
						<div class="col-sm-10">
							<input type="text" class="form-control-static" id="cod_chuva"
								name="cod_chuva" size="4" readonly="readonly">
						</div>
					</div>


					<div class="form-group">
						<label class="col-sm-2 control-label">Inicio Chuva</label>
						<div class="col-sm-3">
							<div class="input-group">
								<input class="form-control pull-right active" id="dt_chuvaini"
									name="dt_chuvaini" type="text">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">Fim Chuva</label>
						<div class="col-sm-3">
							<div class="input-group">
								<input class="form-control pull-right active" id="dt_chuvafim"
									name="dt_chuvafim" type="text">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
							</div>
						</div>
					</div>

					<!-- Campo de Tipo -->
					<div class="form-group" style="visibility: block;">
						<label for="lbl_todaarea" class="col-sm-2 control-label">Todas
							Áreas</label>
						<div class="col-sm-3">
							<input type="checkbox" class="minimal" id="chk_todaarea"
								name="chk_todaarea" />
						</div>
					</div>

					<div id="mmchuva" class="form-group">
						<label for="lbl_mili" class="col-sm-2 control-label">Milímetros</label>
						<div class="col-sm-8">
							<input type="text" style="width: 80px;" maxlength="3"
								class="form-control" id="num_milimetro" name="num_milimetro"
								placeholder="">
						</div>
					</div>

					<div class="form-group">
						<label for="lbl_observacao" class="col-sm-2 control-label">Observações</label>
						<div class="col-sm-8">
							<textarea name="ds_observacao" id="ds_observacao"
								maxlength="2000" class="form-control" rows="5"></textarea>
						</div>
					</div>

					<!-- Campo de Tipo -->


					<div class="box-footer" align="center">
						<button type="button" class="btn btn-success"
							onclick="saveChuva()">Gravar Chuva</button>
						<button type="button" class="btn btn-danger"
							onclick="manutencaoChuva(2)">Cancelar</button>
					</div>

				</form>
			</div>

			<div id="manutencaoChuvaAreas" class="box-body collapse">

			
				<div class="row">
					 <?php foreach ($areas as $area) { ?>
						<div class="col-xs-4" style="width: 15%;">
						<!--  <input class="form-control" style="visibility: hidden;" type="checkbox" id="chkarea-"/>  -->
						  <?=$area['ds_area']?>
						</div>
					<div class="col-xs-1" style="width: 35%;">
						<input class="form-control" type="text" style="width: 80px;"
							maxlength="3" id="num_mmarea-<?=$area['cod_area']?>"
							name="num_mmarea-<?=$area['cod_area']?>" placeholder=""
							data-inputmask='"mask": "(999) 999-9999"' data-mask />
					</div>	
						<?php } ?>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-success"  onclick="manutencaoChuva(1)">Gravar Áreas</button>
													
			</div>
			</div>

			<div id="tabelaChuva" class="col-md-12 collapse in">
				<table class="table table-bordered table-striped" id="tblChuva">
					<thead>
						<tr>
							<th style="width: 25px"></th>

							<th>Data Inicio</th>
							<th>Data Fim</th>
							<th>Milimetros</th>
							<th>Toda Área</th>

						</tr>
					</thead>
					<tbody>
				 <?php foreach ($chuvas as $chuva) { ?>
					<tr>
							<td align=center style="width: 40px"><a href="javascript:;"
								onclick="editarChuva(<?=$chuva['cod_chuva']?>)"> <span
									class="fa fa-fw fa-edit"></span>
							</a></span> <a
								href="<?php echo base_url('chuva/remove/'.$chuva['cod_chuva']."")?>"
								onclick="return confirm('Confirma a exclusão deste registro?')">
									<span class="fa fa-fw fa-trash-o"></span>
							</a></span></td>
							<td><?=$chuva['dt_ini']?></td>
							<td><?=$chuva['dt_fim']?></td>
							<td>TOTALCHUVAAREAS</td>
							<td><?=$chuva['sn_todasarea'] == 'S' ? 'Sim' : 'Não'?></td>
						</tr>
					<?php } ?>
				</tbody>
				</table>
			</div>

		</div>

		<script type="text/javascript">
      $(function () {


    	  $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});

        /* initialize the external events
         -----------------------------------------------------------------*/
        function ini_events(ele) {
          ele.each(function () {

            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            // it doesn't need to have a start or end
            var eventObject = {
              title: $.trim($(this).text()) // use the element's text as the event title
            };

            // store the Event Object in the DOM element so we can get to it later
            $(this).data('eventObject', eventObject);

            // make the event draggable using jQuery UI
            $(this).draggable({
              zIndex: 1070,
              revert: true, // will cause the event to go back to its
              revertDuration: 0  //  original position after the drag
            });

          });
        }
        ini_events($('#external-events div.external-event'));

        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
        var date = new Date();
        var d = date.getDate(),
                m = date.getMonth(),
                y = date.getFullYear();
        
        $('#calendar').fullCalendar({
     
        	 eventClick:  function(event, jsEvent, view) {
                 $('#modalTitle').html(event.title);
                 $('#modalBody').html(event.description);
                 $('#eventUrl').attr('href',event.url);
                 $('#fullCalModal').modal();
             },
            
          header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
          },

          selectable: true,
		  selectHelper: true,

select: function(start, end) {

	 start = $.fullCalendar.formatRange(start, start, 'DD-MM-YYYY');

	alert(  alert('Valor da data: ' + start));
		$('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true

	$('#calendar').fullCalendar('unselect');
},

/*
			 eventClick:  function(event, jsEvent, view) {
		            $('#modalTitle').html(event.title);
		            $('#modalBody').html(event.description);
		            $('#eventUrl').attr('href',event.url);
		            $('#fullCalModal').modal();
		        },
*/

			editable: true,
			eventLimit: true,
		  

          buttonText: {
            today: 'Hoje',
            month: 'Mês',
            week: 'Semana',
            day: 'Dia'
          },
          
          //Random default events
          events: [
            {
              title: 'All Day Event',
              start: new Date(y, m, 1),
              backgroundColor: "#f56954", //red
              borderColor: "#f56954" //red
            },
            {
              title: 'Long Event',
              start: new Date(y, m, d - 5),
              end: new Date(y, m, d - 2),
              backgroundColor: "#f39c12", //yellow
              borderColor: "#f39c12" //yellow
            },
            {
              title: 'Meeting',
              start: new Date(y, m, d, 10, 30),
              allDay: false,
              backgroundColor: "#0073b7", //Blue
              borderColor: "#0073b7" //Blue
            },
            {
              title: 'Lunch',
              start: new Date(y, m, d, 12, 0),
              end: new Date(y, m, d, 14, 0),
              allDay: false,
              backgroundColor: "#00c0ef", //Info (aqua)
              borderColor: "#00c0ef" //Info (aqua)
            },
            {
              title: 'Birthday Party',
              start: new Date(y, m, d + 1, 19, 0),
              end: new Date(y, m, d + 1, 22, 30),
              allDay: false,
              backgroundColor: "#00a65a", //Success (green)
              borderColor: "#00a65a" //Success (green)
            },
            {
              title: 'Click for Google',
              start: new Date(y, m, 28),
              end: new Date(y, m, 29),
              url: 'http://google.com/',
              backgroundColor: "#3c8dbc", //Primary (light-blue)
              borderColor: "#3c8dbc" //Primary (light-blue)
            }

         

            
          ],
         // editable: true,
          droppable: true, // this allows things to be dropped onto the calendar !!!
          drop: function (date, allDay) { // this function is called when something is dropped

            // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject');

            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);

            // assign it the date that was reported
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;
            copiedEventObject.backgroundColor = $(this).css("background-color");
            copiedEventObject.borderColor = $(this).css("border-color");

            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
           $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
              // if so, remove the element from the "Draggable Events" list
              $(this).remove();
            }

          }
        });

        /* ADDING EVENTS */
        var currColor = "#3c8dbc"; //Red by default
        //Color chooser button
        var colorChooser = $("#color-chooser-btn");
        $("#color-chooser > li > a").click(function (e) {
          e.preventDefault();
          //Save color
          currColor = $(this).css("color");
          //Add color effect to button
          $('#add-new-event').css({"background-color": currColor, "border-color": currColor});
        });
        $("#add-new-event").click(function (e) {
          e.preventDefault();
          //Get value and make sure it is not null
          var val = $("#new-event").val();
          if (val.length == 0) {
            return;
          }

          //Create events
          var event = $("<div />");
          event.css({"background-color": currColor, "border-color": currColor, "color": "#fff"}).addClass("external-event");
          event.html(val);
          $('#external-events').prepend(event);

          //Add draggable funtionality
          ini_events(event);

          //Remove event from text input
          $("#new-event").val("");
        });
      });
    </script>