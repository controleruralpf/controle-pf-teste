
 </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
       
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2015 <a href="http://controleruralbr.com.br/" target="_blank">Controle Rural BR</a>.</strong> Todos os direitos reservados.
      </footer>
      
      <!-- Control Sidebar -->      
      <aside class="control-sidebar control-sidebar-dark">                
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
          <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <!-- Home tab content -->
          <div class="tab-pane active" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class='control-sidebar-menu'>
              <li>
                <a href='javascript::;'>
                  <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>
                    <p>Will be 23 on April 24th</p>
                  </div>
                </a>
              </li>              
            </ul><!-- /.control-sidebar-menu -->

            <h3 class="control-sidebar-heading">Tasks Progress</h3> 
            <ul class='control-sidebar-menu'>
              <li>
                <a href='javascript::;'>               
                  <h4 class="control-sidebar-subheading">
                    Custom Template Design
                    <span class="label label-danger pull-right">70%</span>
                  </h4>
                  <div class="progress progress-xxs">
                    <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                  </div>                                    
                </a>
              </li>                         
            </ul><!-- /.control-sidebar-menu -->         

          </div><!-- /.tab-pane -->
          <!-- Stats tab content -->
          <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div><!-- /.tab-pane -->
          <!-- Settings tab content -->
          <div class="tab-pane" id="control-sidebar-settings-tab">            
            <form method="post">
              <h3 class="control-sidebar-heading">General Settings</h3>
              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Report panel usage
                  <input type="checkbox" class="pull-right" checked />
                </label>
                <p>
                  Some information about this general settings option
                </p>
              </div><!-- /.form-group -->
            </form>
          </div><!-- /.tab-pane -->
        </div>
      </aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class='control-sidebar-bg'></div>
    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->
	<!-- jQuery 2.1.4 -->
	<script	src="<?php echo base_url() ?>application/views/adminLTE/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="<?php echo base_url() ?>application/views/adminLTE/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() ?>application/views/adminLTE/dist/js/app.min.js" type="text/javascript"></script>
	<!-- DATA TABES SCRIPT -->
    <script src="<?php echo base_url() ?>application/views/adminLTE/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>application/views/adminLTE/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
   	<!-- SELECT -->
    <script src="<?php echo base_url() ?>application/views/adminLTE/plugins/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>application/views/adminLTE/plugins/bootstrap-select/dist/js/i18n/defaults-pt_BR.js" type="text/javascript"></script>
  	<!-- Jquery Validate -->
  	<script src="<?php echo base_url() ?>application/views/adminLTE/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
   	<script src="<?php echo base_url() ?>application/views/adminLTE/plugins/jquery-validation/additional-methods.min.js" type="text/javascript"></script>
   	<script src="<?php echo base_url() ?>application/views/adminLTE/plugins/jquery-validation/messages_pt_BR.js" type="text/javascript"></script>
   	<!-- iCheck -->
    <script src="<?php echo base_url() ?>application/views/adminLTE/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
   	<!-- InputMask -->
    <script src="<?php echo base_url() ?>application/views/adminLTE/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>application/views/adminLTE/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>application/views/adminLTE/plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>
   	<!-- datepicker -->
    <script src="<?php echo base_url() ?>application/views/adminLTE/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
   	<script src="<?php echo base_url() ?>application/views/adminLTE/plugins/datepicker/locales/bootstrap-datepicker.pt-BR.js" type="text/javascript"></script>
	   	<script src="<?php echo base_url() ?>application/views/adminLTE/plugins/datepicker/locales/bootstrap-datepicker.pt-BR.js" type="text/javascript"></script>
   	<!-- price -->
 	<script src="<?php echo base_url() ?>application/views/adminLTE/plugins/priceFormat/jquery.price_format.2.0.js" type="text/javascript"></script>
 
 	<script src="<?php echo base_url() ?>application/views/adminLTE/plugins/bootbox/bootbox.js" type="text/javascript"></script>
 	<script src="<?php echo base_url() ?>application/views/adminLTE/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
 	
	<script src="<?php echo base_url() ?>application/views/adminLTE/plugins/fullcalendar/fullcalendar.min.js"></script>
	
    <!-- CALENDAR jQuery UI 1.11.1
    <script src="https://code.jquery.com/ui/1.11.1/jquery-ui.min.js" type="text/javascript"></script>
    
      <script src="<?php echo base_url() ?>application/views/adminLTE/plugins/fullcalendar/calendar.jquery-ui.min.js" type="text/javascript"></script>
       -->
    <!-- Slimscroll -->
    <script src="<?php echo base_url() ?>application/views/adminLTE/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='<?php echo base_url() ?>application/views/adminLTE/plugins/fastclick/fastclick.min.js'></script>
    <script src="<?php echo base_url() ?>application/views/adminLTE/plugins/knob/jquery.knob.js" type="text/javascript"></script>
   
    
    
    <!-- fullCalendar 2.2.5
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.7.0/moment.min.js" type="text/javascript"></script>
    
    <script src="<?php echo base_url() ?>application/views/adminLTE/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
    <!-- Page specific script -->
   
   	<script src="<?php echo base_url() ?>application/views/adminLTE/plugins/baixados/bootstrap-notify/bootstrap-notify.min.js"></script>
	
   	
   	<!-- Optionally, you can add Slimscroll and FastClick plugins.
          Both of these plugins are recommended to enhance the
          user experience. Slimscroll is required when using the
          fixed layout. -->
  </body>
 
</html>
