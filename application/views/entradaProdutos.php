<script type="text/javascript">
var base_url = "<?= base_url() ?>";

$(document).ready(function() {
	createEntrada();

	$("#qtd_insumo_insert,#vlr_unitario_insert").keyup(function(){
		somaValorTotal();
	});

	$('#cod_produtos_insert').change(function(){
		obejeto = this;
	});
	

	$("#cod_safra,#cod_fornecedor").css("padding","4px 12px");
	
	$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
      });

	$("#dt_nota,#dt_entrada").datepicker({language: 'pt-BR', format: 'dd/mm/yyyy',});

	//função que adiciona plugins a tabela de Fornecedor
	$('#tblEntradaProdutos').dataTable({       
		"iDisplayLength":9,                       
	    "oLanguage": {
	     "sProcessing": "Aguarde enquanto os dados são carregados ...",
	     /*"sLengthMenu": "Mostrar _ hghgghg MENU_ registros por pagina",*/
	     "sLengthMenu": "",
	    "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
	     "sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
	     "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
	     "sInfoFiltered": "",
	     "sSearch": "Procurar",
	     "oPaginate": {
	      "sFirst":    "Primeiro",
	      "sPrevious": "Anterior",
	      "sNext":     "Próximo",
	      "sLast":     "Último"
	    }
	  }
	});

	//função que adiciona plugins a tabela de Fornecedor
	$('#tblEntradaItensProdutos').dataTable({
		"scrollY": "200px",
        "scrollCollapse":false,
		"bPaginate": false,
		"bFilter": false,
		"ordering":  false,
		"bInfo": false,
		"iDisplayLength":9,                       
	    "oLanguage": {
	    // "sProcessing": "Aguarde enquanto os dados são carregados ...",
	     /*"sLengthMenu": "Mostrar _ hghgghg MENU_ registros por pagina",*/
	     "sLengthMenu": "",
	     "sZeroRecords": " ",
	     "sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
	     "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
	     "sInfoFiltered": "",
	     "sSearch": "Procurar",
	     "oPaginate": {
	      "sFirst":    "Primeiro",
	      "sPrevious": "Anterior",
	      "sNext":     "Próximo",
	      "sLast":     "Último"
	    }
	  }
	});

	//cria um botão dentro da tabela de fornecedor
	$("#tblEntradaProdutos_length").append("<button id='btnCadastrar' class='btn btn-success' onclick='manutencaoEntradaProdutos(1)'>Cadastrar</button>");
	
	
});

function manutencaoEntradaProdutos(acao){
	if(acao == 1){
		//limpaForm();
		$('#manutencaoEntradaProdutos').collapse('show');
		$('#tabelaEntradaProdutos').collapse('hide');

		//$("#ds_fornecedor").focus();
		
	}else if(acao == 2){
		//limpaForm();
		$('#manutencaoEntradaProdutos').collapse('hide');
		$('#tabelaEntradaProdutos').collapse('show');

	}
}
 

//var seq_itens = 0;

function formataQtd(valor){
	
	//$("#qtd_insumo_insert").val(valor.replace(/\d,/, ''));//.replace(/[^\d]+/g, ""));
}


//var cod_entrada = 1;

function addtrItensProdutos(){
	

	var cod_itementradaproduto = $("#cod_itementradaproduto_insert").val();
	var cod_produto = $("select[name=cod_produtos_insert] option[value="+$("#cod_produtos_insert").selectpicker('val')+"]").text();
	var cod_produto_value = $("#cod_produtos_insert").selectpicker('val');
	var qtd_insumo = $("#qtd_insumo_insert").val();
	var cod_embalagem = $("select[name=cod_embalagem_insert] option[value="+$("#cod_embalagem_insert").selectpicker('val')+"]").text();
	var cod_embalagem_value = $("#cod_embalagem_insert").selectpicker('val');
	var vlr_unitario = $("#vlr_unitario_insert").val();
	var vlr_unitario_value = $("#vlr_unitario_insert").val().replace(/,/g,".").replace("R$ ","");
	var vlr_total = $("#vlr_total_insert").val();
	var vlr_total_value = $("#vlr_total_insert").val().replace(/,/g,".").replace("R$ ","");
	var cod_entradaproduto = $("#cod_entradaproduto").val();
	
	if((cod_entradaproduto != "") && (cod_produto != "") && (qtd_insumo != "") && (cod_embalagem != "") && (vlr_unitario != "") && (vlr_total != "")){
	
		$.post(base_url+'EntradaProdutos/insereItemNota', {
			cod_itementradaproduto: cod_itementradaproduto,
			cod_entradaproduto: cod_entradaproduto,
			cod_produto: cod_produto_value,
			qtd_insumo: qtd_insumo,
			cod_embalagem: cod_embalagem_value,
			vlr_unitario: vlr_unitario_value,
			vlr_total: vlr_total_value
				//alert("ok")
			}).done(function(data) {
				var cod_itementradaproduto = $("#cod_itementradaproduto_insert").val();
				var cod_produto = $("select[name=cod_produtos_insert] option[value="+$("#cod_produtos_insert").selectpicker('val')+"]").text();
				var cod_produto_value = $("#cod_produtos_insert").selectpicker('val');
				var qtd_insumo = $("#qtd_insumo_insert").val();
				var cod_embalagem = $("select[name=cod_embalagem_insert] option[value="+$("#cod_embalagem_insert").selectpicker('val')+"]").text();
				var cod_embalagem_value = $("#cod_embalagem_insert").selectpicker('val');
				var vlr_unitario = $("#vlr_unitario_insert").val();
				var vlr_unitario_value = $("#vlr_unitario_insert").val().replace(/,/g,".").replace("R$ ","");
				var vlr_total = $("#vlr_total_insert").val();
				var vlr_total_value = $("#vlr_total_insert").val().replace(/,/g,".").replace("R$ ","");
				var cod_entradaproduto = $("#cod_entradaproduto").val();
				if($("#cod_itementradaproduto_insert").val() != ""){
					
					$("[name='tr_itensProdutos_"+$("#cod_itementradaproduto_insert").val()+"']").empty();
					$("[name='tr_itensProdutos_"+$("#cod_itementradaproduto_insert").val()+"']").append('<td class="tbl_col_ordenacao"><a href="javascript:;" onclick="carregaItemEditar('+cod_itementradaproduto+')"><span class="fa fa-fw fa-edit"></span></a> <a href="javascript:;" onclick="deleteItensProdutos(this)"><span class="fa fa-fw fa-trash-o"></span></a></td><td style="display: none;"><input type="text" id="cod_itementradaproduto" name="cod_itementradaproduto" value="'+cod_itementradaproduto+'"></td><td name="td_produto"><input type="hidden" id="cod_produto" name="cod_produto" value="'+cod_produto_value+'"><p>'+cod_produto+'</p></input></td><td><input type="hidden" id="qtd_insumo" name="qtd_insumo" value="'+qtd_insumo+'"><p>'+qtd_insumo+'</p></input></td><td><input type="hidden" id="cod_embalagem" name="cod_embalagem" value="'+cod_embalagem_value+'"><p>'+cod_embalagem+'</p></input></td><td><input type="hidden" id="vlr_unitario" name="vlr_unitario" value="'+vlr_unitario_value+'"><p>'+vlr_unitario+'</p></input></td><td><input type="hidden" id="vlr_total" name="vlr_total" value="'+vlr_total_value+'"><p>'+vlr_total+'</p></input></td>')
					$.notify('Item alterado com sucesso!', {
						offset: {
							x: 50,
							y: 100
						}
					});
					
					}else{
					//adiciona linha na tabela em tela;
					var cod_itementradaproduto = data;
					var html_linha_itens = '<tr name="tr_itensProdutos_'+cod_itementradaproduto+'"><td class="tbl_col_ordenacao"><a href="javascript:;" onclick="carregaItemEditar('+cod_itementradaproduto+')"><span class="fa fa-fw fa-edit"></span></a> <a href="javascript:;" onclick="deleteItensProdutos(this)"><span class="fa fa-fw fa-trash-o"></span></a></td><td style="display: none;"><input type="text" id="cod_itementradaproduto" name="cod_itementradaproduto" value="'+cod_itementradaproduto+'"></td><td name="td_produto"><input type="hidden" id="cod_produto" name="cod_produto" value="'+cod_produto_value+'"><p>'+cod_produto+'</p></input></td><td><input type="hidden" id="qtd_insumo" name="qtd_insumo" value="'+qtd_insumo+'"><p>'+qtd_insumo+'</p></input></td><td><input type="hidden" id="cod_embalagem" name="cod_embalagem" value="'+cod_embalagem_value+'"><p>'+cod_embalagem+'</p></input></td><td><input type="hidden" id="vlr_unitario" name="vlr_unitario" value="'+vlr_unitario_value+'"><p>'+vlr_unitario+'</p></input></td><td><input type="hidden" id="vlr_total" name="vlr_total" value="'+vlr_total_value+'"><p>'+vlr_total+'</p></input></td></tr>';
					$(html_linha_itens).appendTo("#tblEntradaItensProdutos > tbody");
	
					$.notify('Item inserido com sucesso!', {
						offset: {
							x: 50,
							y: 100
						}
					});
				}
				limpaCampos();
				somaTotalNota();
				
			  }).fail(function() {
			    bootbox.alert("Já existem produto inserido. Não é possível fazer a ixclusão." );
			});
	}

function limpaCampos(){
	$("#cod_itementradaproduto_insert").val('');
	$("#cod_produtos_insert").selectpicker('val','');
	$("#qtd_insumo_insert").val('');
	$("#cod_embalagem_insert").selectpicker('val','');
	$("#vlr_unitario_insert").val('');
	$("#vlr_total_insert").val('');
}
	
}

function somaTotalNota(){
	var total = 0;
	$("[name='vlr_total']").each(function(){
		total = parseFloat(total) + parseFloat($(this).val().replace(/\./g,""));
	});
	$("#vlr_totalnota").val(total).priceFormat({
		clearPrefix: false,
	  	prefix: 'R$ ',
		   centsSeparator: ',',
		   thousandsSeparator: '.',
		   limit: 17,
	      centsLimit: 2
		});
}

var objj = null;

function editItensProdutos(obj){
	//objj = obj;
	//alert($(obj).eq())
	alert($(this).index(this))
	
}



function deleteItensProdutos(obj){
	var num;

	$("[name='tr_itensProdutos']").each(function(){
		num = $(this).index();
	});
	
	if(num == 0){
		$(obj).closest('tr').hide();
	}else{
		$(obj).closest('tr').remove();
	}
	somaTotalNota();
}


function somaValorTotal() {
	var qtd = $("#qtd_insumo_insert").val().replace("R$ ", '').replace(/\./g, '').replace(/\,/g, '');
	var vlrUni = $("#vlr_unitario_insert").val().replace("R$ ", '').replace(/\./g, '').replace(/\,/g, '');

	var total = qtd * vlrUni;
	
	$("#vlr_total_insert").val(total);
	/*$("[name='vlr_total']").each(function(){
		total = (total + $(this).val().replace("R$ ", '').replace(/\./g, '').replace(/\,/g, ''));
	});*/

	$("#vlr_unitario_insert").priceFormat({
		clearPrefix: false,
	  	prefix: 'R$ ',
		   centsSeparator: ',',
		   thousandsSeparator: '.',
		   limit: 17,
	      centsLimit: 2
		});
	
	$("#vlr_total_insert").priceFormat({
		clearPrefix: false,
	  	prefix: 'R$ ',
		   centsSeparator: ',',
		   thousandsSeparator: '.',
		   limit: 17,
	      centsLimit: 2
	});
	
}

/**********************FUNÇÕES DE BANCO*********************************/

function createEntrada(){
		$.ajax({
			url: "EntradaProdutos/createNota",
			type: "post",
			success: function(){
		},
			error:function(data){
			alert("Errou: \n"+data);
			}
		}).always(function(data) {
			$("#cod_entradaproduto").val(data);
		})
}

function updateDadosEntrada(){
	///var parametros = "cod_entradaproduto="+$("#cod_entradaproduto").val()+"ds_numeroNota="+$("#ds_numeroNota").val();

	$.post(base_url+'EntradaProdutos/updateDadosNota', {
		cod_entradaproduto: $("#cod_entradaproduto").val(),
		ds_numeroNota: $("#ds_numeroNota").val(),
		dt_nota: $("#dt_nota").val(),
		dt_entrada: $("#dt_entrada").val(),
		cod_fornecedor: $("#cod_fornecedor").val(),
		cod_safra: $("#cod_safra").val(),
		vlr_totalnota: $("#vlr_totalnota").val()
			//alert("ok")
		}).done(function() {
			//refresh na página
		   // window.location.reload(true);
		  }).fail(function() {
		   // bootbox.alert("Já existem dados vinculados a esta Embalagem. Não é possível fazer a exclusão." );
		});
	/*$.ajax({
		url: "EntradaProdutos/updateDadosNota",
		type: "post",
		data: parametros,
		success: function(){
	},
		error:function(){
		alert("Errou");
		}
	}).always(function(data) {
		//$("#cod_entradaproduto").val(data);
	})*/
}

function carregaItemEditar(id){
	
	$.post(base_url+'EntradaProdutos/getItensNota', {
		get: true,
		cod_itementradaproduto: id
	}, function (data){
		//alert(data[0].cod_produto);
		}, 'json').always(function(data) {
			$("#cod_itementradaproduto_insert").val(data[0].cod_itementradaproduto);
			$("#cod_produtos_insert").selectpicker('val',data[0].cod_produto);
			$("#qtd_insumo_insert").val(data[0].qtd_insumo);
			$("#cod_embalagem_insert").selectpicker('val',data[0].cod_embalagem);
			$("#vlr_unitario_insert").val(data[0].vlr_unitario).keyup();
			$("#btnAdicionarAlterar").text("Alterar");
			//$("#btnAdicionarAlterar").attr('onclick','edit()');

		});
	
}


/***********************************************************************/

</script>

<style type="text/css">
.tab-content {
	height: 80%;
}
</style>
<div class="box box-solid">
	<div class="box-default with-border">
		<h4 style="text-align: center">
			<b>Entrada de Produtos</b>
		</h4>
	</div>

	<div class="box-body" style="height: 72%;">
		<div id="manutencaoEntradaProdutos" class="col-md-12 collapse in">
			<form class="form-horizontal" id="frm_EntradaProdutos"
				name="frm_EntradaProdutos" method="post"
				action="<?php echo base_url('EntradaProdutos/save'); ?>">
				<div class="col-md-12">
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#dadosNotaFiscal" data-toggle="tab">Dados
									da Nota Fiscal</a></li>
							<li><a href="#financeiro" data-toggle="tab">Financeiro</a></li>
							<li><a href="#transporte" data-toggle="tab">Transporte /
									Armazenamento</a></li>
							<li class="pull-right"><a href="#" class="text-muted"><i
									class="fa fa-gear"></i></a></li>
						</ul>

						<div class="tab-content">
							<div class="tab-pane active" id="dadosNotaFiscal">

								<!-- Campo de código -->
								<div class="form-group" style="display: none">
									<label class="col-sm-1 control-label">Código</label>
									<div class="col-sm-1">
										<input type="text" class="form-control-static"
											id="cod_entradaproduto" name="cod_entradaproduto" size="4"
											readonly="readonly">
									</div>
								</div>
								<!-- Número da Nota -->
								<div class="form-group">
									<label class="col-sm-2 control-label">Número da Nota</label>
									<div class="col-sm-2">
										<input type="text" class="form-control" id="ds_numeroNota"
											name="ds_numeroNota">
									</div>
									<!-- Data da nota -->
									<label class="col-sm-2 control-label">Data da Nota</label>
									<div class="col-sm-2">
										<div class="input-group">
											<input class="form-control pull-right active" id="dt_nota"
												name="dt_nota" type="text">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
										</div>
									</div>
									<!-- Data da nota -->
									<label class="col-sm-2 control-label">Data da Entrada</label>
									<div class="col-sm-2">
										<div class="input-group">
											<input class="form-control pull-right active" id="dt_entrada"
												name="dt_entrada" type="text">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
										</div>
									</div>

								</div>

								<div class="form-group">
									<!-- Inicio Fornecedor -->
									<label for="lbl_cod_cultura" class="col-sm-2 control-label">Ano
										Agícola</label>
									<div class="col-sm-2" style="padding-right: 0px">
										<select id="cod_safra" name="cod_safra"
											data-live-search="false" data-size="8"
											class="selectpicker form-control">
											<option value="" selected="selected">Selecione</option>
											<?php foreach ($safra as $safra) { ?>
													<option value="<?=$safra['cod_safra']?>"><?=$safra['ds_safra']?></option>
												<?php }?>
										</select>
									</div>

									<!-- Inicio Fornecedor -->
									<label for="lbl_cod_cultura" class="col-sm-2 control-label">Fornecedor</label>
									<div class="col-sm-5" style="padding-right: 0px">
										<select id="cod_fornecedor" name="cod_fornecedor"
											data-live-search="true" data-size="8"
											class="selectpicker form-control">
											<option value="" selected="selected">Selecione</option>
											<?php foreach ($fornecedor as $fornecedor) { ?>
													<option value="<?=$fornecedor['cod_fornecedor']?>"><?=$fornecedor['ds_fornecedor']?></option>
												<?php }?>
										</select>
									</div>
								</div>



								<div class="well">
									<div>
										<input type="hidden" class="form-control" id="cod_itementradaproduto_insert"
											name="cod_itementradaproduto_insert">
									</div>
									<div class="col-sm-4">
										<select class="selectpicker form-control" id="cod_produtos_insert"
											name="cod_produtos_insert" data-live-search="true" data-size="8"><option
												value="" selected="selected">Selecione o produto</option>
											<?php foreach ($produto as $produto) { ?>
													<option title="<?=$produto['ds_produto']?>" value="<?=$produto['cod_produto']?>"><?=$produto['ds_produto']?></option>
												<?php }?>
										</select>
									</div>
									<div class="col-sm-1">
										<input type="text" class="form-control" id="qtd_insumo_insert" placeholder="qtd"
											name="qtd_insumo_insert" onkeyup="formataQtd(this.value)">
									</div>
									<div class="col-sm-4">
										<select class="selectpicker form-control" id="cod_embalagem_insert"
											name="cod_embalagem_insert" data-live-search="false" data-size="8"><option
												value="" selected="selected">Selecione a embalagem</option>
											<?php foreach ($embalagem as $embalagem) { ?>
													<option value="<?=$embalagem['cod_embalagem']?>"><?=$embalagem['ds_embalagem']?> de  <?=$embalagem['num_volume']?> <?=$embalagem['ds_undmedida']?></option>
												<?php }?>
										</select>
									</div>
									<div class="col-sm-1">
										<input type="text" class="form-control" id="vlr_unitario_insert" placeholder="vlr un."
											name="vlr_unitario_insert"  style="width: 100px">
									</div>
									<div class="col-sm-1">
										<input type="text" readonly="readonly" class="form-control" placeholder="vlr tot."
											id="vlr_total_insert" name="vlr_total_insert"  style="width: 100px">
									</div>
									<div>
										<button id="btnAdicionarAlterar" type="button" class="btn btn-success"
											onclick="addtrItensProdutos()">Adicionar</button>
									</div>
								</div>
								<!-- Itens da Entrada -->
								<table class="table table-striped table-bordered"
									id="tblEntradaItensProdutos">
									<thead>
										<tr>
											<th class="tbl_col_ordenacao"></th>
											<th style="display: none;">Código</th>
											<th style="width: 40%; text-align: center;">Produto</th>
											<th style="width: 10%; text-align: center;">Quantidade</th>
											<th style="width: 20%; text-align: center;">Unidade</th>
											<th style="width: 15%; text-align: center;">Valor Unitario</th>
											<th style="width: 10%; text-align: center;">Valor Total</th>
										</tr>
									</thead>
									<tbody>
										<!-- Inserido as linhas vis js -->
									</tbody>
								</table>

							</div>
							<!-- /.tab-pane -->

							<div class="tab-pane" id="financeiro">
								<div class="form-group">
									<label for="lbl_tp_crescimento" class="col-sm-2 control-label">Pagamento
										Efetuado</label>
									<div class="col-sm-8">
										<div class="form-group">
											<div class="radio">
												<label> <input type="radio" class="minimal" name="ds_pago"
													id="ds_pagoS" value="S"> Sim
												</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <label> <input
													type="radio" class="minimal" name="ds_pago" id="ds_pagoN"
													value="N"> Não
												</label>
											</div>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-2 control-label">Data do Pagamento</label>
									<div class="col-sm-2">
										<div class="input-group">
											<input class="form-control pull-right active"
												id="dt_pagamento" name="dt_pagamento" type="text">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-2 control-label">Pagantes</label>
								</div>

								<!-- Itens da Entrada -->

								<table class="table table-bordered table-striped"
									style="width: 50%" id="tblPagantes">
									<thead>
										<tr>
											<th class="tbl_col_ordenacao"><button type="button"
													onclick="addtrItensPagantes()" class='btn btn-success'
													style="width: 50px; text-align: center;">+</button></th>
											<th style="display: none;">Código</th>
											<th>Nome</th>
											<th>Valor</th>
										</tr>
									</thead>
									<tbody>

										<tr name="tr_itensPagantes">
											<td><a href="javascript:;"> <span class="fa fa-fw fa-edit"></span>
											</a> <a href="javascript:;"
												onclick="deleteItensPagantes(this)"> <span
													class="fa fa-fw fa-trash-o"></span>
											</a></td>
											<td style="display: none;"><input type="text"
												class="form-control" id="cod_pagamentoentrada"
												name="cod_pagamentoentrada"></td>
											<td><select id="cod_socio" name="cod_socio"
												data-live-search="false" data-size="6"
												class="selectpicker form-control" style="width: 70%">
												<?php foreach ($socio as $socio) { ?>
													<option value="<?=$socio['cod_socio']?>"><?=$socio['ds_socio']?></option>
												<?php }?>
											</select></td>
											<td style="width: 30%"><input type="text"
												class="form-control" id="vlr_pago" name="vlr_pago"></td>
										</tr>

									</tbody>
								</table>

								<!-- Fim Itens da Entrada -->


							</div>
							<!-- /.tab-pane -->
							<div class="tab-pane" id="transporte"></div>
							<!-- /.tab-pane -->
						</div>
						<!-- /.tab-content -->
					</div>
					<!-- nav-tabs-custom -->
				</div>
				<!-- /.col -->
				<div class="box-footer" align="right">
					<div class="row">
						<div class="col-sm-6">
							<button type="button" class="btn btn-success" onclick="updateDadosEntrada()">Salvar</button>
							<button type="button" class="btn btn-danger"
								onclick="manutencaoCultivar(2)">Cancelar</button>
						</div>
						<div class="col-sm-6" align="right">
							<h3 style="margin: 2px;">
								<b>VALOR TOTAL</b><input id="vlr_totalnota" name="vlr_totalnota"
									type="text" style="border: 0px none;">
							</h3>
						</div>
					</div>
				</div>
			</form>
		</div>



		<div id="tabelaEntradaProdutos" class="col-md-12 collapse">
			<table class="table table-bordered table-striped"
				id="tblEntradaProdutos">
				<thead>
					<tr>
						<th class="tbl_col_ordenacao"></th>
						<th style="width: 1%">Código</th>
					</tr>
				</thead>
				<tbody>

					<tr id="">
						<td></td>
						<td></td>
					</tr>

				</tbody>
			</table>
		</div>

	</div>
</div>