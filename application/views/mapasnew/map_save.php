<?php require_once dirname(__FILE__) . "/includes/functions.php"; ?>

<?php
    $error_msg = '';
    $map_id = isset($_POST['map_id']) ? $_POST['map_id'] : 0;
    $map_title = isset($_POST['map_title']) ? $_POST['map_title'] : '';
    $map_description = isset($_POST['map_description']) ? $_POST['map_description'] : '';
    $map_center_lat = isset($_POST['map_center_lat']) ? $_POST['map_center_lat'] : 0;
    $map_center_lng = isset($_POST['map_center_lng']) ? $_POST['map_center_lng'] : 0;
    $map_zoom = isset($_POST['map_zoom']) ? $_POST['map_zoom'] : '';
    $map_typeid = isset($_POST['map_typeid']) ? $_POST['map_typeid'] : '';
    //$marker_title = isset($_POST['marker_title']) ? $_POST['marker_title'] : '';
    //$marker_coords = isset($_POST['marker_coords']) ? $_POST['marker_coords'] : '';
    //$marker_icon = isset($_POST['marker_icon']) ? $_POST['marker_icon'] : '';
    //$line_title = isset($_POST['line_title']) ? $_POST['line_title'] : '';
    //$line_coords = isset($_POST['line_coords']) ? $_POST['line_coords'] : '';
    $poly_title = isset($_POST['poly_title']) ? $_POST['poly_title'] : '';
    $poly_coords = isset($_POST['poly_coords']) ? $_POST['poly_coords'] : '';
    //$rect_title = isset($_POST['rect_title']) ? $_POST['rect_title'] : '';
    //$rect_coords = isset($_POST['rect_coords']) ? $_POST['rect_coords'] : '';
    //$circle_title = isset($_POST['circle_title']) ? $_POST['circle_title'] : '';
    //$circle_coords = isset($_POST['circle_coords']) ? $_POST['circle_coords'] : '';
    
    $get_map_entity = updateMap($map_title, $map_description, $map_center_lat, $map_center_lng, $map_zoom, $map_typeid, $map_id);
    if ($get_map_entity[0])	{	// map record created or updated
        // if is existing map, delete map objects existed
        if ($map_id>0)	
            $delete_map_objects = deleteMapObjects($map_id);

        $map_entity_id = intval($get_map_entity[1]);
/*
        if ($marker_title) {
            foreach( $marker_title as $key => $n ) {	// new map marker record created
                $get_object_id = updateMapObject($n, $marker_coords[$key], 1, $map_entity_id, null, $marker_icon[$key]);

                if (!$get_object_id[0])
                        $error_msg .= '<p class="text-danger">'.$get_object_id[1].'</p>';
            }
        }

        if ($line_title) {
            foreach( $line_title as $key => $n ) {	// new map polyline record created
                $get_object_id = updateMapObject($n, $line_coords[$key], 2, $map_entity_id);

                if (!$get_object_id[0])
                    $error_msg .= '<p class="text-danger">'.$get_object_id[1].'</p>';
            }
        }
*/
        if ($poly_title) {
            foreach( $poly_title as $key => $n ) {	// new map polygon record created
                    $get_object_id = updateMapObject($n, $poly_coords[$key], 3, $map_entity_id);
                if (!$get_object_id[0])
                    $error_msg .= '<p class="text-danger">'.$get_object_id[1].'</p>';
            }
        }
/*
        if ($rect_title) {
            foreach( $rect_title as $key => $n ) {	// new map rectagle record created
                $get_object_id = updateMapObject($n, $rect_coords[$key], 4, $map_entity_id);

                if (!$get_object_id[0])
                    $error_msg .= '<p class="text-danger">'.$get_object_id[1].'</p>';
            }
        }

        if ($circle_title) {
            foreach( $circle_title as $key => $n ) {	// new map circle record created
                $get_object_id = updateMapObject($n, $circle_coords[$key], 5, $map_entity_id);

                if (!$get_object_id[0])
                    $error_msg .= '<p class="text-danger">'.$get_object_id[1].'</p>';
            }
        }*/

    }else{
        $error_msg .= '<p class="text-danger">'.$get_map_entity[1].'</p>';
    }
    
    header("location:../index.php");

?>