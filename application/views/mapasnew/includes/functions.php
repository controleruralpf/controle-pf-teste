<?php
require_once dirname(__FILE__) . "/db_connect.php";

function updateMap($title, $description, $map_center_lat, $map_center_lng, $map_zoom, $map_typeid, $id=null) {
    $id = intval($id);
    $title = trim($title);
    $description = trim($description);
    $map_zoom = intval($map_zoom);
    $map_typeid = trim($map_typeid);

    if ($title=='') {
        return array(false, 'Title of cannot be empty');
    }	

    $db = DbConnect::getConnection();
    if ($id>0)	{
        $sql = "update maps set "
            . "title='".mysqli_real_escape_string($db,$title)."', "
            . "description='".mysqli_real_escape_string($db,$description)."', "
            . "center_lat=$map_center_lat, "
            . "center_lng=$map_center_lng, "
            . "zoom=$map_zoom, "
            . "typeid='".mysqli_real_escape_string($db,$map_typeid)."' "
            . "where id=".$id;
    }
    else
    {
        $sql = "insert into maps (title, description, center_lat, center_lng, zoom, typeid) values ("
            . "'".mysqli_real_escape_string($db,$title)."', "
            . "'".mysqli_real_escape_string($db,$description)."', "
            . " ".$map_center_lat.", "
            . " ".$map_center_lng.", "
            . " ".$map_zoom.", "
            . "'".mysqli_real_escape_string($db,$map_typeid)."')";
    }

    $rs = mysqli_query($db, $sql);
    if ($rs === FALSE) {
        return array(false, 'Insert map failed: '.  mysqli_error($db));
    }


    if ($id>0)	
        return array(true, $id); 
    else
        return array(true, $db->insert_id); 

}	

function updateMapObject($title, $coords, $object_id, $map_id, $id=null, $marker_icon='') {
    $id = intval($id);
    $coords = trim($coords);
    $object_id = intval($object_id);
    $map_id = intval($map_id);
    $marker_icon = trim($marker_icon);

    if ($object_id < 1) {
        return array(false, 'Invalid map object on updateMapObject');
    }	

    if ($map_id < 1) {
        return array(false, 'Invalid map id on updateMapObject');
    }	

    $db = DbConnect::getConnection();
    if ($id>0)	{
        $sql = "update map_objects set title='".mysqli_real_escape_string($db,$title)."', coords='".mysqli_real_escape_string($db,$coords)."', object_id=".$object_id.", map_id=".$map_id.", marker_icon='".mysqli_real_escape_string($db,$marker_icon)."' where id=".$id;
    }
    else
    {
        $sql = "insert into map_objects (title, coords, object_id, map_id, marker_icon) values ('".mysqli_real_escape_string($db,$title)."', '".mysqli_real_escape_string($db,$coords)."', ".$object_id.", ".$map_id.", '".mysqli_real_escape_string($db,$marker_icon)."')";
    }

    $rs = mysqli_query($db, $sql);
    if ($rs === FALSE) {
        return array(false, 'Insert map failed: '.  mysqli_error($db));
    }

    return array(true, $db->insert_id); 
}	

function getMap($map_id) {
	$db = DbConnect::getConnection();
	$sql = "SELECT * FROM maps WHERE id=$map_id";
	$result = mysqli_query($db, $sql) or die(mysqli_error($db));
	$rows = array();
	while ($row = mysqli_fetch_assoc($result)) {
            $rows[] = $row;
	}
	
	return $rows;
}

function getMapObjects($map_id) {
	$db = DbConnect::getConnection();
	$sql = "SELECT * FROM map_objects WHERE map_id=$map_id";
	$result = mysqli_query($db, $sql) or die(mysqli_error($db));
	$rows = array();
	while ($row = mysqli_fetch_assoc($result)) {
		$rows[] = $row;
	}
	
	return $rows;
}

function getMaps() {
	$db = DbConnect::getConnection();
	$sql = "SELECT * FROM maps ORDER BY id DESC";
	$result = mysqli_query($db, $sql) or die(mysqli_error($db));
	$rows = array();
	while ($row = mysqli_fetch_assoc($result)) {
		$rows[] = $row;
	}
	
	return $rows;
}

function deleteMap($map_id) {
	$map_id = intval($map_id);
	if ($map_id > 0) {
		$db = DbConnect::getConnection();
		
		$sql = "delete from maps where id=$map_id";
        $rs = mysqli_query($db, $sql);
		if ($rs == FALSE) {
			return array(false, 'The deletion of map failed: '.  mysqli_error($db));
		}
		else {
			$sql = "delete from map_objects where map_id=$map_id";
			$rs = mysqli_query($db, $sql);
			if ($rs == FALSE) {
				return array(false, 'The deletion of map objects failed: '.  mysqli_error($db));
			}			
		}
		return array(true,'');
		
	} else {
		return array(false, 'Invalid map id on deleteMap');
	}
	
}   

function deleteMapObjects($map_id) {
	$map_id = intval($map_id);

	if ($map_id > 0) {
		$db = DbConnect::getConnection();
		
		$sql = "delete from map_objects where map_id=$map_id";
        $rs = mysqli_query($db, $sql);
		if ($rs == FALSE) {
			return array(false, 'The deletion of map objects failed: '.  mysqli_error($db));
		}
		return array(true,'');
		
	} else {
		return array(false, 'Invalid map id on deleteMapObjects');
	}
	
}   
	
