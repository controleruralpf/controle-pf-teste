<?php
	// retrieve markers from marker directory
	$markers_list = '';
	$dir = 'graphics/markers';
	if (is_dir($dir)) {
		if ($dh = opendir($dir)) {
			$allowedExts = array("png", "PNG");
			
			while (($file = readdir($dh)) !== false) {
				$extension = end(explode('.', $file));
				if (in_array($extension, $allowedExts))	{
					$markers_list .= '<img src="'.$dir.'/'.$file.'" id="'.$file.'" class="marker_list"  onClick="setIcon($(this).attr(\'id\'));" />&nbsp;';
				}
			}
			closedir($dh);
		}
	}	
	
	// set default marker
	$default_marker = $dir.'/default.png';
	if (!file_exists($default_marker))
		$default_marker = "http://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi.png";	
?>
	<!-- temporary store location of current marker -->
	<span id="current_marker_path"><?php echo $default_marker; ?></span>
	
	<!-- div below is used for displaying markers -->
	<div id="markers_list_content" style="display:none;">
		<span id="markers_list"><?php echo $markers_list; ?></span>
		<span id="what_todo" style="display:none;"></span>
		<script>
			function setIcon(myvalue) {
				var what = $('#what_todo').text();
				
				if (what) {
					map.markers[what].setIcon('graphics/markers/'+myvalue);
					$('#marker_icon_'+what).val('graphics/markers/'+myvalue);
					$('#marker_'+what+'_chm').popover('hide');
				}
				else {
					$('#current_marker').html('<img src="graphics/markers/'+myvalue+'" alt="Current marker" />');
					$('#current_marker_path').html('graphics/markers/'+myvalue);
					$('#change_icon_btn').popover('hide');
				}
			}
		</script>
	</div>

<?php
	echo '
		<div class="row">
			<div class="span65">
				<div id="basic_options">
					<div class="radio">
						<label for="drawwhat_markers"><input type="radio" name="drawwhat" id="drawwhat_markers" value="markers" checked>Markers</label>
						<label for="drawwhat_line"><input type="radio" name="drawwhat" id="drawwhat_line" value="line" >Line</label>
						<label for="drawwhat_polygon"><input type="radio" name="drawwhat" id="drawwhat_polygon" value="polygon">Polygon</label>     	
						<label for="drawwhat_rectangle"><input type="radio" name="drawwhat" id="drawwhat_rectangle" value="rectangle">Rectangle</label>    
						<label for="drawwhat_circle"><input type="radio" name="drawwhat" id="drawwhat_circle" value="circle">Circle</label>    
						<div id="helpbox" class="text-info"></div>	  
					</div>
				</div>
				<div id="basic_buttons">
					<button type="button" id="drawwhat_clearall" class="btn btn-danger">Clear All</button>
					<button type="button" disabled="disabled" id="add_btn" class="btn btn-primary">Add</button> 
					<button type="button" disabled="disabled" id="save_btn" class="btn btn-success">Save</button> 
					<button type="button" disabled="disabled" id="cancel_editing" class="btn btn-warning">Cancel</button>
					
					<div id="markers_cpanel">
						<button type="button" id="add_current_loc_btn" class="btn btn-primary">Add current location</button>
						<button type="button" id="change_icon_btn" class="btn btn-primary" data-toggle="popover" data-placement="bottom" title="Select Icon" data-content="" >Change icon</button>
						<span id="current_marker"><img src="'.$default_marker.'" alt="Current marker" /></span>
						<span id="current_loc_status"></span>
					</div>
			
				</div>
			</div>			
			<div class="span70 searchbox" >
				<form method="post" id="geocoding_form" class="form-inline" role="form">
					<div class="form-group input">
						<input type="text" class="form-control" id="address" name="address" placeholder="Enter city or address">
						<input type="submit" class="btn btn-primary" value="Search" />
					</div>					
				</form>
			</div>
		</div>
	';
		
?> 

