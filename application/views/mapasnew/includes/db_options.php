<?php
//ini_set('display_errors', 1);
//error_reporting(E_ALL);

function dcb_callback($script, $line, $message) {
    echo "<h1>Condition failed!</h1><br />
        Script: <strong>$script</strong><br />
        Line: <strong>$line</strong><br />
        Condition: <br /><pre>$message</pre>";
}

class Options {
    public static $DBHOST = 'localhost';
    public static $DBUSER = 'root';
    public static $DBPASSWORD = '';
    public static $DBNAME = 'controle_new_bd_controle_rural';
    public static $SESSION_MAX_LIFETIME = 57600;
}
