<script type="text/javascript">

 $(document).ready(function() {

	

 	//quando fecha o modal ele exclui todos os campos
 	$('#popupDetalhe').on('hidden.bs.modal', function (e) {
 		$("#titulo").empty();
 		$("#conteudo > #conteudoForm").empty();
 	})

 	$('input').iCheck({
 	  checkboxClass: 'icheckbox_square-green',
 	  radioClass: 'iradio_square-green',
 	  
 	});

	
	$('#btnAdicionarUndMedida').popover({
         trigger: 'manual',
         html: true,
         title: '<p align="center"><b>Undidade de Medida</b></p>',
         content: $('#div-popover').html() // Adiciona o conteúdo da div oculta para dentro do popover.
      }).click(function (e) {
         e.preventDefault();
         // Exibe o popover.
         $(this).popover('show');
      });

	$("#cod_embalagem").change(function(){
		//revalida somente o campo de seleção quando ele selecionar algun item.
		var validator = $("#frm_embalagem").validate();
		validator.element("#cod_embalagem");
	});

	$('input[name="rdb_tipoR"]').on('ifChecked', function(event){
		  $('input[name="rdb_tipoD"]').iCheck('uncheck');
		});

	$('input[name="rdb_tipoD"]').on('ifChecked', function(event){
		 $('input[name="rdb_tipoR"]').iCheck('uncheck');
		});
	
	$("#frm_conta").validate({
        ignore: ':not(select:hidden, input:visible, textarea:visible)',
        errorPlacement: function (error, element) {
            if ($(element).is('select')) {
                element.next().after(error); // Validação especial para os campos select
            } else {
                error.insertAfter(element);  //Validação normal para os outros campos
            }
        },
        highlight: function(element, errorClass, validClass) {
        		$(element).closest('.form-group').addClass(errorClass).removeClass(validClass);
		    	$(element).closest('.form-group').addClass('has-error');
		  },
		unhighlight: function(element, errorClass, validClass) {
			$(element).closest('.form-group').removeClass(errorClass).addClass(validClass);
			$(element).closest('.form-group').removeClass('has-error');
		  }
    });

	
	//função que adiciona plugins a tabela de Cultivar
	$('#tblConta').dataTable({
		"iDisplayLength":9,            
	    "oLanguage": {
	     "sProcessing": "Aguarde enquanto os dados são carregados ...",
	     /*"sLengthMenu": "Mostrar _ hghgghg MENU_ registros por pagina",*/
	     "sLengthMenu": "",
	     "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
	     "sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
	     "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
	     "sInfoFiltered": "",
	     "sSearch": "Procurar",
	     "oPaginate": {
	      "sFirst":    "Primeiro",
	      "sPrevious": "Anterior",
	      "sNext":     "Próximo",
	      "sLast":     "Último"
	    }
	  }                              
	});

	//cria um botão dentro da tabela de Cultivar
	$("#tblConta_length").append("<button id='btnCadastrar' class='btn btn-success' onclick='manutencaoConta(1)'>Cadastrar</button>");
	
	
});

function insereUndmedida(){
	
	var ds_undmedida_aux = $('#ds_undmedida_aux').val();
	var parametros = "ds_undmedida="+ds_undmedida_aux;

	if(ds_undmedida_aux != ""){
		$.ajax({
			url: "embalagens/saveundmedida",
			type: "post",
			data: parametros,
			success: function(){
			$('#btnAdicionarUndMedida').popover('hide');
			$('#ds_undmedida_aux').val("");
		},
			error:function(){
			alert("Erro ao inserir uma nova Unidade de Medida");
			}
		}).always(function() {
			//após a inserção
			carregaDadosUndMedidaJson();
		})
	}else{
		$('#btnAdicionarUndMedida').popover('show');
	}
}

function carregaDadosGrupoContaJson(){

		$.post(base_url+'contas/listaGrupoContas',{
			}, function (data){
			$('#cod_grupoconta').children().remove().end();//.append('<option selected value="">Selecione um item</option>') ;
	    		$(data).each(function(){
	    			$("#cod_grupoconta").append('<option value="'+this.cod_grupoconta+'">'+this.ds_grupoconta+'</option>');
				});
			$('#cod_grupoconta').selectpicker('refresh');

	  		}, 'json').always(function(data) {
  				//busca o ultimo código cadastrado
  				var sel = 1;
	  			$(data).each(function(){

	    			if(sel < this.cod_grupoconta){
						sel = this.cod_grupoconta;
	            	}
	  	
				});
				//seleciona o último cadastrado

  			 $('#cod_grupoconta').selectpicker('val', sel);
	
		});;

}

function cancelaUndmedida(){
	$('#btnAdicionarUndMedida').popover('hide');
	 $('#ds_undmedida_aux').val("");
}

function manutencaoConta(acao){
	if(acao == 1){
		limpaForm();
		carregaDadosGrupoContaJson();
		$('#manutencaoConta').collapse('show');
		$('#tabelaConta').collapse('hide');
		$("#ds_conta").focus();
		
	}else if(acao == 2){
		limpaForm();	
		$('#manutencaoConta').collapse('hide');
		$('#tabelaConta').collapse('show');

	}
}

function limpaForm(){
	$("#frm_conta input").val("");
	$("#frm_conta textarea").val("");	
	$('input[name="rdb_tipoR"]').iCheck('uncheck');
	$('input[name="rdb_tipoD"]').iCheck('uncheck');
	$("#frm_conta").validate().resetForm();
	$(".form-group").removeClass("has-error");
	$("button.dropdown-toggle").css("border", "1px solid #DDD");
}

function editarConta(id){
		carregaDadosContaJson(id);
	}


var base_url = "<?= base_url() ?>";

	 function carregaDadosContaJson(id){
		 
		 limpaForm();

 	     		$.post(base_url+'contas/dadosContas', {
 	     			id: id
 	     		}, function (data){
 	     			$('#cod_conta').val(data.cod_conta);
 	     			$('#ds_conta').val(data.ds_conta);
 	     			
 	     			if(data.ds_tipo == 'R'){
 	     				 $('input[name="rdb_tipoR"]').iCheck('check');
  	 	 	     		$('input[name="rdb_tipoD"]').iCheck('uncheck');
 	 	 	     	}else if (data.ds_tipo == 'D'){
 	 	 	     	 	$('input[name="rdb_tipoD"]').iCheck('check');
 	 	 	     		$('input[name="rdb_tipoR"]').iCheck('uncheck');
 	 	 	 	   }
     			 	     			
 	     			$('#ds_observacao').val(data.ds_observacao);

 	     			//seleciona o valor	
 	     			 $('#cod_grupoconta').selectpicker('val', data.cod_grupoconta);
 	     			
 	         		}   , 'json');

 	     		$('#manutencaoConta').collapse('show');
 	    		$('#tabelaConta').collapse('hide');

 	    		 $("#ds_conta").focus();	  
  	
	     	}

	     	     	function excluirConta(id){
	     	     		bootbox.confirm("Confirma a exclusão deste registro?", function(result) {
	     	     			 // Example.show("Confirm result: "+result);
	     	     			 if(result){
	     		    			 $.post(base_url+'contas/remove', {
	     		     				id: id
	     		     				//alert("ok")
	     		     			}).done(function() {
	     		     				//refresh na página
	     						    window.location.reload(true);
	     						  }).fail(function() {
	     		     			    bootbox.alert("Já existem dados vinculados a esta Embalagem. Não é possível fazer a exclusão." );
	     		     			})
	     		     		}
	     	     		});
	     	        }

 	function carregaDadosPopUp(id){

 		$.post(base_url+'embalagens/dadosEmbalagem', {
 			id: id
 		}, function (data){

         	$("#titulo").append(data.ds_embalagem);
         	montaPopUp('Embalagem / Volume',data.ds_embalagem +" de "+data.num_volume+" "+data.ds_undmedida);
			montaPopUp('Devolução?',converte(data.ds_tipo));
			montaPopUp('Observações',data.ds_observacao);
			
			$('#popupDetalhe').modal('show');
		
     		}, 'json');

		function montaPopUp(valorLabel,valorCampo){
 			$("#conteudo > #conteudoForm").append('<div class="form-group" id="div-campos"><label class="col-sm-2 control-label" id="label-popup">'+valorLabel+'</label><label class="col-sm-8 control-label" id="valor-popup" style="text-align: left;font-weight: 400;">'+valorCampo+'</label></div>')
     	}

		

     	     	function converte(valor){
     	     			if(valor == "S"){
     	     				valor = 'Sim';
     	     			}else{
     	     				valor = 'Não';
     	     			}
     	     	return valor;
     	 		}
 	}

	    
</script>

<div class="modal fade" id="popupDetalhe" tabindex="-1" role="dialog"
	aria-labelledby="popupDetalheLabel">
	<div class="modal-content"
		style="height: auto; width: auto; margin: 150px auto; max-width: 70%">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<h4 class="modal-title" id="titulo"
				style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif"></h4>
		</div>
		<div class="modal-body" id="conteudo" style="font-size: 14px">
			<form class="form-horizontal"
				style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif"
				id="conteudoForm"></form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-success pull-left"
				data-dismiss="modal">Fechar</button>
		</div>
	</div>
</div>

<div class="box box-solid">
	<div class="box-default with-border">
		<h4 style="text-align: center">
			<b>Contas</b>
		</h4>
	</div>

	<div class="box-body">
		<div id="manutencaoConta" class="col-md-12 collapse">
			<form class="form-horizontal" id="frm_conta" name="frm_conta"
				method="post" action="<?php echo base_url('contas/save'); ?>">


				<!-- Campo de código -->
				<div class="form-group" style="display: none">
					<label class="col-sm-2 control-label">Código</label>
					<div class="col-sm-10">
						<input type="text" class="form-control-static" id="cod_conta"
							name="cod_conta" size="4" readonly="readonly">
					</div>
				</div>

				<!-- Campo de descrição -->
				<div class="form-group">
					<label for="lbl_dsconta" class="col-sm-2 control-label">Conta</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="ds_conta"
							name="ds_conta" required="required">
					</div>
				</div>

				<div class="form-group" id="idfm">
					<label for="lbl_codgrupoconta" class="col-sm-2 control-label">Grupo
						Conta </label>
					<div class="col-sm-6" style="padding-right: 0px">
						<select id="cod_grupoconta" name="cod_grupoconta"
							required="required" data-live-search="true" data-size="8"
							class="selectpicker form-control">
							
	               <?php foreach ($grupoContas as $grupoconta) { ?>
	                <option selected="<?php ?>"
								value="<?php echo $grupoconta['cod_grupoconta']?>"><?php echo $grupoconta['ds_grupoconta']?> </option>
	                <?php }?>
	              </select>
					</div>
					<div class="col-xs-2" style="padding-left: 0px">
						<a id="btnAdicionarGrupoConta" class="btn btn-success"
							data-toggle="popover"><span class="glyphicon glyphicon-plus"></span></a>
					</div>
				</div>

				<!-- 
				<div id="div-popover" class="hide">
					<div class="input-group input-group-sm">
                    	<input id="ds_undmedida_aux" name="ds_undmedida_aux" type="text" class="form-control" style="width:120px" placeholder="Und.Medida">
                    	<span class="input-group-btn">
                      		<button id="btnInsereUndmedida" onclick="insereUndmedida()" class="btn btn-success btn-success" type="button"><span class="glyphicon glyphicon-ok"></span></button>
                    		<button id="btnCancelUndmedida" onclick="cancelaUndmedida()" class="btn btn-danger btn-danger" type="button"><span class="glyphicon glyphicon-remove"></span></button>
                    	</span>
                  	</div>
				</div>
			-->
			

				<!-- Receita / Despesa (Tipo) -->
				<div class="form-group">
					<label for="lbl_dstipo" class="col-sm-2 control-label">Tipo</label>
					<div class="col-sm-8">
						<div class="form-group">
							<div class="radio">
								<label> <input type="radio" class="minimal" name="rdb_tipoR"
									id="rdb_tipo" value="R"> Receita
								</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <label> <input
									type="radio" class="minimal" name="rdb_tipoD" id="rdb_tipo"
									value="D"> Despesa
								</label>
							</div>
						</div>
					</div>
				</div>
				
				
				<div class="form-group">
					<label for="lbl_observacao" class="col-sm-2 control-label">Observações</label>
					<div class="col-sm-8">
						<textarea name="ds_observacao" id="ds_observacao" maxlength="2000"
							class="form-control" rows="3"></textarea>
					</div>
				</div>


				<div class="box-footer" align="center">
					<button type="submit" class="btn btn-success">Salvar</button>
					<button type="button" class="btn btn-danger"
						onclick="manutencaoConta(2)">Cancelar</button>
				</div>
			</form>
		</div>

		<div id="tabelaConta" class="col-md-12 collapse in">
			<table class="table table-bordered table-striped" id="tblConta">
				<thead>
					<tr>
						<th class="tbl_col_ordenacao"></th>
						<th style="width: 1%">Código</th>
						<th>Conta</th>
						<th>Tipo</th>
						<th>Grupo Conta</th>
					</tr>
				</thead>
				<tbody>
				 <?php foreach ($contas as $conta) { ?>
					<tr>
						<td class="tbl_col_ordenacao"><a href="javascript:;"
							onclick="editarConta(<?=$conta['cod_conta']?>)"> <span
								class="fa fa-fw fa-edit"></span>
						</a></span> <a href="javascript:;"
							onclick="excluirConta(<?=$conta['cod_conta']?>)"> <span
								class="fa fa-fw fa-trash-o"></span>
						</a> </a></span>
						<td><?=$conta['cod_conta']?></td>
						<td><?=$conta['ds_conta']?></td>
						<td><?=$conta['ds_tipo']  == 'D' ? 'Despesa' : 'Receita'?> </td>
						<td><?=$conta['ds_grupoconta']?></td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>

	</div>