<script
	src="<?php echo base_url() ?>application/views/adminLTE/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script
	src="<?php echo base_url() ?>application/views/adminLTE/plugins/bootbox/bootbox.js"
	type="text/javascript"></script>
<script
	src="<?php echo base_url() ?>application/views/adminLTE/bootstrap/js/bootstrap.min.js"
	type="text/javascript"></script>

<script type="text/javascript">

 $(document).ready(function() {
	 var base_url = "<?= base_url() ?>";
     var i;
	 document.getElementById("selectEmpresa").style.display = "none";
	 $("#btnLogin").click(function(){

if($('#login').val() == ''){
	 bootbox.alert(" <font size='4' color='red'> <b>Login não efetuado!</b><\BR> Preencha o campo de Login. </font>" );exit;
}

if($('#senha').val() == ''){
	 bootbox.alert("<font size='4' color='red'> <b>Login não efetuado!</b><\BR> Preencha o campo de Senha.</font>" );exit;
}

		 
		 $.post(base_url+'Login/validateInicial', {
  			login: $('#login').val(),
  			senha: $('#senha').val(),
  			codEmpresa: $('#cod_empresa').val()

  	  }, function (data){

			//usuário logado
  			if(data.reg == 1){
 			  				window.location= base_url+'Cultivar/index';
  	  		}else if(data.reg == 2){
  	  			document.getElementById("selectEmpresa").style.display = "block";
	  	  		var options = '';
  	  			for (i = 0; i < data.empresa.length; i++) {
  	  		  		options += '<option value="' + data.empresa[i].cod_empresa + '">' + data.empresa[i].ds_razaosocial + '</option>';
  	  	    	  	}

  	  			$("#cod_empresa").html(options).show();
		
  	  	  	}

  	  		else if(data.reg == 0){ 
				//alert('Login não efetuado/n Verifique seu usuário e senha!');
  	  		 bootbox.alert("<font size='4' color='red'> <b>Login não efetuado!</b><\BR> Verifique Seu Usuário e Senha. </font>" );
  	  	  	}

      		}, 'json').always(function() {
					//após a inserção
		 		
				});
		 
		});
	 
	
});
     	
</script>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Controle Rural BR</title>
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport'>
<!-- Bootstrap 3.3.4 -->
<link
	href="<?php echo base_url() ?>application/views/adminLTE/bootstrap/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<!-- Font Awesome Icons -->
<link
	href="<?php echo base_url() ?>application/views/adminLTE/plugins/baixados/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link
	href="<?php echo base_url() ?>application/views/adminLTE/dist/css/AdminLTE.min.css"
	rel="stylesheet" type="text/css" />
<!-- iCheck -->
<link
	href="<?php echo base_url() ?>application/views/adminLTE/plugins/iCheck/square/blue.css"
	rel="stylesheet" type="text/css" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="login-page">
	<div class="login-box">
		<div class="login-logo">
			<a href=""><b>Controle Rural</b> BR</a>
		</div>
		<!-- /.login-logo -->
		<div class="login-box-body">
			<!--  <p class="login-box-msg">Login</p>-->
			<form id="frm_login" name="frm_login" method="post"
				action="<?php echo base_url('login/validate'); ?>">
				<div class="form-group has-feedback">
					<input type="text" id="login" name="login" class="form-control"
						placeholder="Seu Login" /> <span
						class="glyphicon glyphicon-envelope form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input type="password" id="senha" name="senha" class="form-control"
						placeholder="Sua Senha" /> <span
						class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>

				<div id="selectEmpresa" name="selectEmpresa"
					class="form-group has-feedback collapse">
					<select id="cod_empresa" name="cod_empresa" required
						data-live-search="true" data-size="8"
						class="selectpicker form-control" placeholder="Empresa">

					</select>
				</div>

				<div class="row">
					<!--  
            <div class="col-xs-8">    
              <div class="checkbox icheck">
                <label>
                  <input type="checkbox"> Lembre-me
                </label>
              </div>                        
            </div>
            -->

					<div class="col-xs-4">
						<button id="btnLogin" name="btnLogin" type="button"
							class="btn btn-primary btn-block btn-flat">Entrar</button>
					</div>
					<!-- /.col -->
				</div>




			</form>
			<!--  
        <a href="#">Esqueci minha senha</a><br>
        <a href="register.html" class="text-center">Registrar</a>
		-->
		</div>
		<!-- /.login-box-body -->
	</div>
	<!-- /.login-box -->



</body>
</html>