<script type="text/javascript">

 $(document).ready(function() {

	

 	//quando fecha o modal ele exclui todos os campos
 	$('#popupDetalhe').on('hidden.bs.modal', function (e) {
 		$("#titulo").empty();
 		$("#conteudo > #conteudoForm").empty();
 	})

 	$('input').iCheck({
 	  checkboxClass: 'icheckbox_square-green',
 	  radioClass: 'iradio_square-green',
 	  
 	});

	
	$("#cod_embalagem").change(function(){
		//revalida somente o campo de seleção quando ele selecionar algun item.
		var validator = $("#frm_embalagem").validate();
		validator.element("#cod_embalagem");
	});

	
	$("#frm_produto").validate({
        ignore: ':not(select:hidden, input:visible, textarea:visible)',
        errorPlacement: function (error, element) {
            if ($(element).is('select')) {
                element.next().after(error); // Validação especial para os campos select
            } else {
                error.insertAfter(element);  //Validação normal para os outros campos
            }
        },
        highlight: function(element, errorClass, validClass) {
        		$(element).closest('.form-group').addClass(errorClass).removeClass(validClass);
		    	$(element).closest('.form-group').addClass('has-error');
		  },
		unhighlight: function(element, errorClass, validClass) {
			$(element).closest('.form-group').removeClass(errorClass).addClass(validClass);
			$(element).closest('.form-group').removeClass('has-error');
		  }
    });

	
	//função que adiciona plugins a tabela de Cultivar
	$('#tblProduto').dataTable({
		"iDisplayLength":9,            
	    "oLanguage": {
	     "sProcessing": "Aguarde enquanto os dados são carregados ...",
	     /*"sLengthMenu": "Mostrar _ hghgghg MENU_ registros por pagina",*/
	     "sLengthMenu": "",
	     "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
	     "sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
	     "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
	     "sInfoFiltered": "",
	     "sSearch": "Procurar",
	     "oPaginate": {
	      "sFirst":    "Primeiro",
	      "sPrevious": "Anterior",
	      "sNext":     "Próximo",
	      "sLast":     "Último"
	    }
	  }                              
	});

	//cria um botão dentro da tabela de Cultivar
	$("#tblProduto_length").append("<button id='btnCadastrar' class='btn btn-success' onclick='manutencaoProduto(1)'>Cadastrar</button>");
	
	
});

function carregaDadosCategoriaProdutoJson(){

		$.post(base_url+'produtos/listaCategoriaProdutos',{
			}, function (data){
			$('#cod_').children().remove().end();//.append('<option selected value="">Selecione um item</option>') ;
	    		$(data).each(function(){
	    			$("#cod_categoriaproduto").append('<option value="'+this.cod_categoriaproduto+'">'+this.ds_categoriaproduto+'</option>');
				});
			$('#cod_categoriaproduto').selectpicker('refresh');

	  		}, 'json').always(function(data) {
  				//busca o ultimo código cadastrado
  				var sel = 1;
	  			$(data).each(function(){

	    			if(sel < this.cod_categoriaproduto){
						sel = this.cod_categoriaproduto;
	            	}
	  	
				});
				//seleciona o último cadastrado

  			 $('#cod_categoriaproduto').selectpicker('val', sel);
		});;

}


function manutencaoProduto(acao){
	if(acao == 1){
		limpaForm();
		carregaDadosCategoriaProdutoJson();
		$('#manutencaoProduto').collapse('show');
		$('#tabelaProduto').collapse('hide');
		$("#ds_produto").focus();
		
	}else if(acao == 2){
		limpaForm();	
		$('#manutencaoProduto').collapse('hide');
		$('#tabelaProduto').collapse('show');

	}
}

function limpaForm(){
	$("#frm_produto input").val("");
	$("#frm_produto textarea").val("");	
	$("#frm_produto").validate().resetForm();
	$(".form-group").removeClass("has-error");
	$("button.dropdown-toggle").css("border", "1px solid #DDD");
}

function editarProduto(id){
	carregaDadosProdutoJson(id);
		
	}


var base_url = "<?= base_url() ?>";

	 function carregaDadosProdutoJson(id){
		 
		 limpaForm();

 	     		$.post(base_url+'produtos/dadosProdutos', {
 	     			id: id
 	     		}, function (data){
 	     			$('#cod_produto').val(data.cod_produto);
 	     			$('#ds_produto').val(data.ds_produto);		
 	     			$('#cod_categoriaproduto').selectpicker('val', data.cod_categoriaproduto);
 	     			$('#ds_observacao').val(data.ds_observacao);
 	         		}   , 'json');

 	     		$('#manutencaoProduto').collapse('show');
 	    		$('#tabelaProduto').collapse('hide');

 	    		 $("#ds_produto").focus();	  
  	
	     	}

	     	     	function excluirProduto(id){
	     	     		bootbox.confirm("Confirma a exclusão deste registro?", function(result) {
	     	     			 // Example.show("Confirm result: "+result);
	     	     			 if(result){
	     		    			 $.post(base_url+'produtos/remove', {
	     		     				id: id
	     		     				//alert("ok")
	     		     			}).done(function() {
	     		     				//refresh na página
	     						    window.location.reload(true);
	     						  }).fail(function() {
	     		     			    bootbox.alert("Já existem dados vinculados a esta Embalagem. Não é possível fazer a exclusão." );
	     		     			})
	     		     		}
	     	     		});
	     	        }

 	    
</script>

<div class="modal fade" id="popupDetalhe" tabindex="-1" role="dialog"
	aria-labelledby="popupDetalheLabel">
	<div class="modal-content"
		style="height: auto; width: auto; margin: 150px auto; max-width: 70%">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<h4 class="modal-title" id="titulo"
				style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif"></h4>
		</div>
		<div class="modal-body" id="conteudo" style="font-size: 14px">
			<form class="form-horizontal"
				style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif"
				id="conteudoForm"></form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-success pull-left"
				data-dismiss="modal">Fechar</button>
		</div>
	</div>
</div>

<div class="box box-solid">
	<div class="box-default with-border">
		<h4 style="text-align: center">
			<b>Produtos</b>
		</h4>
	</div>

	<div class="box-body">
		<div id="manutencaoProduto" class="col-md-12 collapse">
			<form class="form-horizontal" id="frm_produto" name="frm_produto"
				method="post" action="<?php echo base_url('produtos/save'); ?>">


				<!-- Campo de código -->
				<div class="form-group" style="display: none">
					<label class="col-sm-2 control-label">Código</label>
					<div class="col-sm-10">
						<input type="text" class="form-control-static" id="cod_produto"
							name="cod_produto" size="4" readonly="readonly">
					</div>
				</div>

				<!-- Campo de descrição -->
				<div class="form-group">
					<label for="lbl_dsproduto" class="col-sm-2 control-label">Produto</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="ds_produto"
							name="ds_produto" required="required">
					</div>
				</div>

				<div class="form-group" id="idfm">
					<label for="lbl_codcategoriaproduto" class="col-sm-2 control-label">Categoria</label>
					<div class="col-sm-6" style="padding-right: 0px">
						<select id="cod_categoriaproduto" name="cod_categoriaproduto"
							required="required" data-live-search="false" data-size="8"
							class="selectpicker form-control">
							 <option value="">Selecione</option>
	               <?php foreach ($categoriaProdutos as $categoriaProduto) { ?>
	                <option value="<?php $categoriaProduto['cod_categoriaproduto']?>"><?php $categoriaProduto['ds_categoriaproduto']?> </option>
	                <?php }?>
	              </select>
					</div>
				</div>

				
				<div class="form-group">
					<label for="lbl_observacao" class="col-sm-2 control-label">Observação</label>
					<div class="col-sm-8">
						<textarea name="ds_observacao" id="ds_observacao" maxlength="2000"
							class="form-control" rows="3"></textarea>
					</div>
				</div>


				<div class="box-footer" align="center">
					<button type="submit" class="btn btn-success">Salvar</button>
					<button type="button" class="btn btn-danger"
						onclick="manutencaoProduto(2)">Cancelar</button>
				</div>
			</form>
		</div>

		<div id="tabelaProduto" class="col-md-12 collapse in">
			<table class="table table-bordered table-striped" id="tblProduto">
				<thead>
					<tr>
						<th class="tbl_col_ordenacao"></th>
						<th style="width: 1%">Código</th>
						<th>Produto</th>
						<th>Categoria</th>
						<th>Insumo</th>
					</tr>
				</thead>
				<tbody>
				 <?php foreach ($produtos as $produto) { ?>
					<tr>
						<td class="tbl_col_ordenacao"><a href="javascript:;"
							onclick="editarProduto(<?=$produto['cod_produto']?>)"> <span
								class="fa fa-fw fa-edit"></span>
						</a></span> <a href="javascript:;"
							onclick="excluirProduto(<?=$produto['cod_produto']?>)"> <span
								class="fa fa-fw fa-trash-o"></span>
						</a> </a></span>
						<td><?=$produto['cod_produto']?></td>
						<td><?=$produto['ds_produto']?></td>
						<td><?=$produto['ds_categoriaproduto']?></td>
						<td><?=$produto['sn_insumo']  == 'S' ? 'Sim' : 'Não'?> </td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>

	</div>