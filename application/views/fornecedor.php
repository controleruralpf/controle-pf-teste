
<script type="text/javascript">

$(document).ready(function() {


	$("#ds_fone").inputmask("(99) 9999-9999");
	$("#ds_fone2").inputmask("(99) 9999-9999");
	
	$("#frm_fornecedor").validate({
	  	highlight: function(element, errorClass, validClass) {
	  		$(element).closest('.form-group').addClass(errorClass).removeClass(validClass);
	    	$(element).closest('.form-group').addClass('has-error');
		  },
		unhighlight: function(element, errorClass, validClass) {
			$(element).closest('.form-group').removeClass(errorClass).addClass(validClass);
			$(element).closest('.form-group').removeClass('has-error');
		  }
	});
	
	//função que adiciona plugins a tabela de Fornecedor
	$('#tblFornecedor').dataTable({       
		"iDisplayLength":9,                       
	    "oLanguage": {
	     "sProcessing": "Aguarde enquanto os dados são carregados ...",
	     /*"sLengthMenu": "Mostrar _ hghgghg MENU_ registros por pagina",*/
	     "sLengthMenu": "",
	     "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
	     "sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
	     "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
	     "sInfoFiltered": "",
	     "sSearch": "Procurar",
	     "oPaginate": {
	      "sFirst":    "Primeiro",
	      "sPrevious": "Anterior",
	      "sNext":     "Próximo",
	      "sLast":     "Último"
	    }
	  }
	});

	//cria um botão dentro da tabela de fornecedor
	$("#tblFornecedor_length").append("<button id='btnCadastrar' class='btn btn-success' onclick='manutencaoFornecedor(1)'>Cadastrar</button>");

	//quando fecha o modal ele exclui todos os campos
	$('#popupDetalhe').on('hidden.bs.modal', function (e) {
		$("#titulo").empty();
		$("#conteudo > #conteudoForm").empty();
	});
	  
});


function manutencaoFornecedor(acao){
	if(acao == 1){
		limpaForm();
		$('#manutencaoFornecedor').collapse('show');
		$('#tabelaFornecedor').collapse('hide');

		 $("#ds_fornecedor").focus();
		
	}else if(acao == 2){
		limpaForm();
		$('#manutencaoFornecedor').collapse('hide');
		$('#tabelaFornecedor').collapse('show');

	}
}

var base_url = "<?= base_url() ?>";

function carregaDadosFornecedorJson(id){

     		$.post(base_url+'fornecedor/dadosFornecedor', {
     			id: id
     		}, function (data){
     			$('#cod_fornecedor').val(data.cod_fornecedor);
     			$('#ds_fornecedor').val(data.ds_fornecedor);
     			$('#ds_contato').val(data.ds_contato);
     			$('#ds_fone').val(data.ds_fone);
     			$('#ds_fone2').val(data.ds_fone2);
     			$('#ds_endereco').val(data.ds_endereco);
     			$('#ds_observacao').val(data.ds_observacao);

         		}, 'json').always(function() {
					//após a inserção
		 		$('#manutencaoFornecedor').collapse('show');
				$('#tabelaFornecedor').collapse('hide');

				 $("#ds_fornecedor").focus();
				});   		
}
    
     	function editarFornecedor(id){
     		carregaDadosFornecedorJson(id);
     	}
     	
     	function excluirFornecedor(id){
     		bootbox.confirm("Confirma a exclusão deste registro?", function(result) {
     			 // Example.show("Confirm result: "+result);
     			 if(result){
	    			 $.post(base_url+'fornecedor/remove', {
	     				id: id
	     				//alert("ok")
	     			}).done(function() {
	     				//refresh na página
					    window.location.reload(true);
					  }).fail(function() {
	     			    bootbox.alert("Já existem dados vinculados a este Fornecedor. Não é possível fazer a exclusão." );
	     			})
	     		}
     		});
        }

     	function carregaDadosPopUp(id){

     		$.post(base_url+'fornecedor/dadosFornecedor', {
     			id: id
     		}, function (data){

             	$("#titulo").append(data.ds_fornecedor);
             	montaPopUp('Contato',data.ds_contato);
				montaPopUp('Telefone',data.ds_fone);
				montaPopUp('Telefone',data.ds_fone2);
				montaPopUp('Endereço',data.ds_endereco);
				montaPopUp('Observação',data.ds_observacao);
				
				$('#popupDetalhe').modal('show');
			
         		}, 'json');

			function montaPopUp(valorLabel,valorCampo){
     			$("#conteudo > #conteudoForm").append('<div class="form-group" id="div-campos"><label class="col-sm-2 control-label" id="label-popup">'+valorLabel+'</label><label class="col-sm-8 control-label" id="valor-popup" style="text-align: left;font-weight: 400;">'+valorCampo+'</label></div>')
         	}
     	}

     	function limpaForm(){
     		$("#frm_fornecedor input").val("");
     		$("#frm_fornecedor textarea").val("");
     		$("#frm_fornecedor").validate().resetForm();
     		$(".form-group").removeClass("has-error");
     	}
     	
  </script>

<div class="modal fade" id="popupDetalhe" tabindex="-1" role="dialog" aria-labelledby="popupDetalheLabel">
	<div class="modal-content" style="height: auto; width: auto; margin: 150px auto; max-width: 70%">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<h4 class="modal-title" id="titulo" style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif"></h4>
		</div>
		<div class="modal-body" id="conteudo" style="font-size: 14px">
			<form class="form-horizontal" style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif" id="conteudoForm">
			</form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-success pull-left"	data-dismiss="modal">Fechar</button>
		</div>
	</div>
</div>

<div class="box box-solid">
	<div class="box-default with-border">
		<h4 style="text-align: center">
			<b>Fornecedores</b>
		</h4>
	</div>

	<div class="box-body">
		<div id="manutencaoFornecedor" class="col-md-12 collapse">
			<form class="form-horizontal" id="frm_fornecedor"
				name="frm_fornecedor" method="post"
				action="<?php echo base_url('fornecedor/save'); ?>">

				<!-- Campo de código -->
				<div class="form-group" style="display: none">
					<label class="col-sm-2 control-label">Código</label>
					<div class="col-sm-10">
						<input type="text" class="form-control-static" id="cod_fornecedor"
							name="cod_fornecedor" size="4" readonly="readonly">
					</div>
				</div>

				<!-- Campo de descrição -->
				<div class="form-group">
					<label for="lbl_fornecedor" class="col-sm-2 control-label">Descrição</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="ds_fornecedor"
							name="ds_fornecedor" required placeholder="">
					</div>
				</div>

				<div class="form-group">
					<label for="lbl_contato" class="col-sm-2 control-label">Contato</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="ds_contato"
							name="ds_contato" placeholder="">
					</div>
				</div>

				<!-- Campo de descrição -->
				<div class="form-group">
					<label for="lbl_fone" class="col-sm-2 control-label">Telefone</label>
					<div class="col-sm-2">
						<input type="text" class="form-control" id="ds_fone"
							name="ds_fone" placeholder="">
					</div>
				</div>

				<!-- Campo de descrição -->
				<div class="form-group">
					<label for="lbl_fone2" class="col-sm-2 control-label">Telefone</label>
					<div class="col-sm-2">
						<input type="text" class="form-control" id="ds_fone2"
							name="ds_fone2" placeholder="">
					</div>
				</div>

				<!-- Campo de descrição -->
				<div class="form-group">
					<label for="lbl_endereco" class="col-sm-2 control-label">Endereço</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="ds_endereco"
							name="ds_endereco" placeholder="">
					</div>
				</div>

				<div class="form-group">
					<label for="lbl_observacao" class="col-sm-2 control-label">Observação</label>
					<div class="col-sm-8">
						<textarea rows="3" type="text" class="form-control"
							id="ds_observacao" name="ds_observacao"></textarea>
					</div>
				</div>

				<div class="box-footer" align="center">
					<button type="submit" class="btn btn-success">Salvar</button>
					<button type="button" class="btn btn-danger"
						onclick="manutencaoFornecedor(2)">Cancelar</button>
				</div>
			</form>
		</div>

		<div id="tabelaFornecedor" class="col-md-12 collapse in">
			<table class="table table-bordered table-striped" id="tblFornecedor">
				<thead>
					<tr>
						<th class="tbl_col_ordenacao"></th>
						<th style="width: 1%">Código</th>
						<th>Descrição</th>
						<th>Contato</th>
						<th style="width: 10%">Telefone</th>
						<th style="width: 10%">Telefone</th>
						<th>Endereço</th>
					</tr>
				</thead>
				<tbody>
				 <?php foreach ($fornecedor as $fornecedor) { ?>
					<tr id="<?=$fornecedor['cod_fornecedor']?>">
						<td class="tbl_col_ordenacao"><a href="javascript:;"
							onclick="editarFornecedor(<?=$fornecedor['cod_fornecedor']?>)"> <span
								class="fa fa-fw fa-edit"></span>
						</a></span> <a
							href="javascript:;"
							onclick="excluirFornecedor(<?=$fornecedor['cod_fornecedor']?>)">
								<span class="fa fa-fw fa-trash-o"></span>
						</a></span><a href="javascript:;"
							onclick="carregaDadosPopUp(<?=$fornecedor['cod_fornecedor']?>)">
								<span class="fa fa-fw fa-reorder"></span>
						</a></td>
						<td><?=$fornecedor['cod_fornecedor']?></td>
						<td><?=$fornecedor['ds_fornecedor']?></td>
						<td><?=$fornecedor['ds_contato']?></td>
						<td><?=$fornecedor['ds_fone']?></td>
						<td><?=$fornecedor['ds_fone2']?></td>
						<td><?=$fornecedor['ds_endereco']?></td>
					</tr>
					<?php }?>
				</tbody>
			</table>
		</div>

	</div>