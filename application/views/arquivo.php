<script
	src="http://localhost/ControleRuralCode/application/views/adminLTE/plugins/bootstrap-select/dist/js/bootstrap-select.js"
	type="text/javascript"></script>

<script type="text/javascript">


 $(document).ready(function() {
<?php for($i =1; $i <= $_POST['qtdParcelas']; $i++){?>

$("#vlr_pago<?=$i?>").priceFormat({
  	prefix: 'R$ ',
    centsSeparator: ',',
    thousandsSeparator: '.',
    limit: 17,
    centsLimit: 2
});



 $("#vlr_parcela<?=$i?>").priceFormat({
	  	prefix: 'R$ ',
	    centsSeparator: ',',
	    thousandsSeparator: '.',
	    limit: 17,
        centsLimit: 2
	});

 $("#vlr_parcela<?=$i?>").val(<?=$pagamentoParcelas[$i-1]['vlr_parcelaDespesa']?>);
 $("#vlr_pago<?=$i?>").val(<?=$pagamentoParcelas[$i-1]['vlrPagoParcelaDespesa']?>);

 
 

 $("#cod_parcelapagamentodespesa<?=$i?>").val(<?=$pagamentoParcelas[$i-1]['cod_parcelapagamentodespesa']?>);
 $("#cod_despesa<?=$i?>").val(<?=$pagamentoParcelas[$i-1]['cod_despesa']?>);
 $("#num_parcela<?=$i?>").val(<?=$pagamentoParcelas[$i-1]['num_parcela']?>);
 $("#cod_pagamentodespesa<?=$i?>").val(<?=$pagamentoParcelas[$i-1]['cod_pagamentodespesa']?>);
 
 $("#dt_vencimentoparcela<?=$i?>").datepicker({language: 'pt-BR', format: 'dd/mm/yyyy'});
 $("#dt_pagamentoparcela<?=$i?>").datepicker({language: 'pt-BR', format: 'dd/mm/yyyy'});

 
 //treta para formatar a data
 $("#vlr_parcela<?=$i?>").focus();
 $("#vlr_pago<?=$i?>").focus();
 $("#cod_formapagamento<?=$i?>").focus();

 $("#vlr_totalpagamentoDespesa").focus();
 $("#vlr_total").focus();
 $("#ds_observacaopagamentoDespesa").focus();

 
<?php }?>
	
});

 var base_url = "<?= base_url() ?>";

 function excluirPagamentoPacelaDespesa(cod_parcelapagamentodespesa,cod_despesa,num_parcela,cod_pagamentodespesa){
	 
		bootbox.confirm("Confirma a exclusão deste registro?", function(result) {
			 // Example.show("Confirm result: "+result);
			 if(result){
  			 $.post(base_url+'arquivo/removePagamentoParcelaDespesa', {
  				cod_parcelapagamentodespesa: cod_parcelapagamentodespesa,
  				cod_despesa: cod_despesa,
  				num_parcela: num_parcela,
  				cod_pagamentodespesa: cod_pagamentodespesa
   			}).done(function() {
   				//refresh na página
				    window.location.reload(true);
				  }).fail(function() {
   			    bootbox.alert("Já existem dados vinculados a esta Despesa. Não é possível fazer a exclusão." );
   			})
   		}
		});
  }
 
 
</script>


<html>
<div class="form-group" id="id_parcelas1">
	<h4>
		<label for="lbl_dtvencimento" class="col-sm-1 control-label"
			style="padding-left: 0%; width: 50%;">Parcelas</label>
	</h4>
</div>
<table class="table table-bordered table-striped" style="width: 50%"
	id="tblPagantes">
	<tbody>
	
	
	<thead>

		<div class="form-group" id="id_parcelas1">
			<div class="col-sm-1">
				<label for="lbl_1" class="col-sm-1 control-label">&nbsp;&nbsp;&nbsp;</label>
			</div>
			<div class="col-sm-2">
				<div class="input-group">
					<label for="lbl_dtvencimento" class="ontrol-label">Data Vencimento</label>
				</div>
			</div>
			<div class="col-sm-2">
				<div class="input-group">
					<label for="lbl_dtvencimento" class="ontrol-label">Valor Parcela</label>
				</div>
			</div>
			<div class="col-sm-2">
				<div class="input-group">
					<label for="lbl_dtvencimento" class="ontrol-label">Data Pagamento</label>
				</div>
			</div>
			<div class="col-sm-2">
				<div class="input-group">
					<label for="lbl_dtvencimento" class="ontrol-label">Valor Pago</label>
				</div>
			</div>
			<div class="col-sm-2">
				<div class="input-group">
					<label for="lbl_dtvencimento" class="ontrol-label">Forma Pagamento</label>
				</div>
			</div>


		</div>
		</div>
	</thead>						
<?php for($i =1; $i <= $_POST['qtdParcelas']; $i++){?>
	
<div class="form-group" id="id_parcelas">

		<div class="input-group" style="display: none;">
			<input class="form-control pull-right active"
				id="cod_parcelapagamentodespesa<?=$i?>"
				name="cod_parcelapagamentodespesa<?=$i?>" type="text"> <input
				class="form-control pull-right active" id="cod_despesa<?=$i?>"
				name="cod_despesa<?=$i?>" type="text"> <input
				class="form-control pull-right active" id="num_parcela<?=$i?>"
				name="num_parcela<?=$i?>" type="text"> <input
				class="form-control pull-right active"
				id="cod_pagamentodespesa<?=$i?>" name="cod_pagamentodespesa<?=$i?>"
				type="text">
		
		</div>

		<div class="col-sm-1" style="vertical-align: middle;width: 3%">
			<div class="input-group">
				<a href="javascript:;"
							onclick="excluirPagamentoPacelaDespesa(<?=$pagamentoParcelas[$i-1]['cod_parcelapagamentodespesa']?>,<?=$pagamentoParcelas[$i-1]['cod_despesa']?>,<?=$pagamentoParcelas[$i-1]['num_parcela']?>,<?=$pagamentoParcelas[$i-1]['cod_pagamentodespesa']?>)"> <span
								class="fa fa-fw fa-trash-o"></span>
						</a>
			</div>
		</div>
		
		
		
		<div class="col-sm-2">
			<div class="input-group">
				<input class="form-control pull-right active"
					id="dt_vencimentoparcela<?=$i?>" name="dt_vencimentoparcela<?=$i?>"
					value="<?=$pagamentoParcelas[$i-1]['dtVencimentoParcelaDespesa']?>"
					type="text">
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
			</div>
		</div>

		<div class="col-sm-2">
			<div class="input-group">
				<input class="form-control pull-right active"
					id="vlr_parcela<?=$i?>" name="vlr_parcela<?=$i?>" type="text">
			</div>
		</div>
		<div class="col-sm-2">
			<div class="input-group">
				<input class="form-control pull-right active"
					id="dt_pagamentoparcela<?=$i?>" name="dt_pagamentoparcela<?=$i?>"
					value="<?=$pagamentoParcelas[$i-1]['dt_pagamentoParcelaDespesa']?>"
					type="text">
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
			</div>
		</div>

		<div class="col-sm-2">
			<div class="input-group">
				<input class="form-control pull-right active" id="vlr_pago<?=$i?>"
					name="vlr_pago<?=$i?>" type="text">
			</div>
		</div>
		<div class="col-sm-2" style="">
			<select id="cod_formapagamento<?=$i?>"
				name="cod_formapagamento<?=$i?>" data-live-search="true"
				class="selectpicker form-control">
								               <?php foreach ($formapagamentos as $formapagamento) { ?>
								                <option
					<?php if  ($formapagamento['cod_formapagamento'] == $pagamentoParcelas[$i-1]['cod_formapagamentoParcelaDespesa'] ?  print("selected") : print("") );?>
					value="<?php echo $formapagamento['cod_formapagamento']?>"><?php echo $formapagamento['ds_formapagamento']?> </option>
								                <?php }?>
								              </select>
		</div>
	</div>
	</div>			
<?php } ?>		
									</tbody>
</table>


</html>



