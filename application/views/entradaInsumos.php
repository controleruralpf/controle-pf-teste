
<script type="text/javascript">
$(document).ready(function() {

	//quando fecha o modal ele exclui todos os campos
	$('#popupDetalhe').on('hidden.bs.modal', function (e) {
		$("#titulo").empty();
		$("#conteudo > #conteudoForm").empty();
	});


	var table = $('#tblEntradaInsumos').dataTable({
		"iDisplayLength":12, 
 	    "oLanguage": {
 	 	 "sProcessing": "Aguarde enquanto os dados são carregados ...",
 	    // "sLengthMenu": "Mostrar _ hghgghg MENU_ registros por pagina",
 	     "sLengthMenu": "",
 	     "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
 	    "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
 	    "sInfoFiltered": "",
 	    "sSearch": "Procurar",
 	   "sEmptyTable": '',
 	    "sInfoEmpty": '',
 	     "oPaginate": {
 	      "sFirst":    "Primeiro",
 	      "sPrevious": "Anterior",
 	      "sNext":     "Próximo",
 	     "sLast":     "Último"
 	    }
 	  },
 	      initComplete: function () {
          this.api().columns().every( function () {
              var column = this;
              var select = $('<select id="selCategoriaEntradaInsumo" style="width: 0px;"></select>')
                  .appendTo( $(column.footer()).empty() )
                  .on( 'focus', function () {
                      var val = $.fn.dataTable.util.escapeRegex(
                          $(this).val()
                      );

                      column
                          .search( val ? '^'+val+'$' : '', true, false )
                          .draw();
                  } )
                  .on( 'change', function () {
                      var val = $.fn.dataTable.util.escapeRegex(
                          $(this).val()
                      );

                      column
                          .search( val ? '^'+val+'$' : '', true, false )
		                          .draw();

                     
                  		$('#selCategoriaInsumo').val(val);
                  		
                  } )       ;

              column.data().unique().sort().each( function ( d, j ) {
                  select.append( '<option value="'+d+'">'+d+'</option>' )
              } 

              );
          } );
      }                              
 	});
	
 	 var table1 =  $('#tblInsumos').dataTable({
 	 "iDisplayLength":9,                               
  	    "oLanguage": {
  	     "sProcessing": "Aguarde enquanto os dados são carregados ...",
  	     /*"sLengthMenu": "Mostrar _ hghgghg MENU_ registros por pagina",*/
  	     "pageLength": "",
  	     "sLengthMenu": "",
  	     "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
  	     "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
  	     "sInfoFiltered": "",
  	     "sSearch": "Procurar",
  	   "sEmptyTable": '',
	    "sInfoEmpty": '',
  	     "oPaginate": {
  	      "sFirst":    "Primeiro",
  	      "sPrevious": "Anterior",
  	      "sNext":     "Próximo",
  	      "sLast":     "Último"
  	    }
  	  },
       initComplete: function () {
           this.api().columns().every( function () {
               var column = this

               var select = $('<select class="selectpicker" data-width="130px" id="selCategoriaInsumo"></select>')
                   .appendTo( $(column.footer()).empty() )
                   .on( 'focus', function () {
                       var val = $.fn.dataTable.util.escapeRegex(
                           $(this).val()
                       );

                       column
                           .search( val ? '^'+val+'$' : '', true, false )
                           .draw();
                   } )
                   .on( 'change', function () {
                       var val = $.fn.dataTable.util.escapeRegex(
                           $(this).val()
                       );
                       $('#selCategoriaEntradaInsumo').val(val);
                       $('#selCategoriaEntradaInsumo').focus();
                       column
                           .search( val ? '^'+val+'$' : '', true, false )
                           .draw();
                   	
                   } )       ;

        
               
               column.data().unique().sort().each( function ( d, j ) {
                   select.append( '<option value="'+d+'">'+d+'</option>' )
                  
               } );
           } );
       }                              
  	});

 	$('#selCategoriaEntradaInsumo').focus();
	$('#selCategoriaInsumo').focus();

	$("#vlr_unitario,#vlr_total").priceFormat({
	  	prefix: 'R$ ',
	   centsSeparator: ',',
	   thousandsSeparator: '.',
     limit: 17,
     centsLimit: 2
	});

	$("#dt_entrada").datepicker({language: 'pt-BR', format: 'dd/mm/yyyy',});

	$("#frm_entradaInsumos").validate({
        ignore: ':not(select:hidden, input:visible, textarea:visible)',
        errorPlacement: function (error, element) {
            if ($(element).is('select')) {
                //element.next().after(error); // Validação especial para os campos select
            } else {
              //  error.insertAfter(element);  //Validação normal para os outros campos
            }
        },
        highlight: function(element, errorClass, validClass) {
        		$(element).closest('.form-group').addClass(errorClass).removeClass(validClass);
		    	$(element).closest('.form-group').addClass('has-error');
		  },
		unhighlight: function(element, errorClass, validClass) {
			$(element).closest('.form-group').removeClass(errorClass).addClass(validClass);
			$(element).closest('.form-group').removeClass('has-error');
		  }
    });
	
});

function chamaManutencaoEntradaInsumos(cod_insumo,cod_entradaInsumo){

	//se tiver cod_insumo é  chamada para cadastrar uma entrada, se já tiver o codigo de entrada ae é edição
	//adiciona a função para mostra a tela de edição. Depois temos que mudar como é chamada a função
//	  > a > i
	  //$('#tblInsumos > tbody > tr > td').click(function() {
		  
/*			var $lefty =  $('#slidebottom button').next();
			$lefty.animate({
				  left: parseInt($lefty.css('left'),10) == 0 ? -$lefty.outerWidth() : 0
				  }, {
				    complete: function() {*/
				    	manutencaoEntradaInsumos(cod_insumo,cod_entradaInsumo,'cadastrar')
				/*    }
				  });*/
}

function abrirEditarIncluir(){
	
	var $lefty =  $('#slidebottom button').next();
	$lefty.animate({
		  left: parseInt($lefty.css('left'),10) == 0 ? -$lefty.outerWidth() : 0
		  });

}

function manutencaoEntradaInsumos(cod_insumo,cod_entradaInsumo,acao){
	if(acao == 'cadastrar'){
		if(cod_insumo != null){
		limpaForm();	
		$('#cod_insumo').val(cod_insumo);
		cadastrarEntradaInsumos(cod_insumo);
		}else{
			$('#cod_entrada').val(cod_entradaInsumo);
			carregadadosEditarEntradaInsumosJson(cod_entradaInsumo);
		}
		
		$("#bloqueioFundo").show();
		}else if(acao == 'cancelar'){
		var $lefty =  $('#slidebottom button').next();
		$lefty.animate({
			  left: parseInt($lefty.css('left'),10) == 0 ? -$lefty.outerWidth() : 0
			  }, {
			    complete: function() {
			    	$("#bloqueioFundo").hide();
			    }
			  });
		}
}


function cadastrarEntradaInsumos(id){
	
	carregaDadosInsumosJson(id);
}

var base_url = "<?= base_url() ?>"; 

function carregaDadosInsumosJson(id){
	
     		$.post(base_url+'entradaInsumos/dadosEntradaInsumos', {
     			id: id
     		}, function (data){
     			$('#lblcategoria').text("")
     			$('#lblcategoria').text(data.ds_categoria);
     			$('#lblinsumo').text(data.ds_descricao);
     			$('#lblstatus').text('[Inserindo]');
     			verificaValor(1,false);
     			$("#dt_entrada").datepicker("setDate", new Date());
    	} , 'json').always(function() {
			//após a inserção
			abrirEditarIncluir();
		})
}



function carregadadosEditarEntradaInsumosJson(id){
	
		$.post(base_url+'entradaInsumos/carregadadosEditarEntradaInsumos', {
			id: id
		}, function (data){

			$('#cod_entrada').val(data.cod_entrada);
			$('#cod_insumo').val(data.cod_insumo);
			$('#cod_fornecedor').select(data.cod_fornecedor);
			$('#cod_fornecedor').selectpicker('val', data.cod_fornecedor);
			$('#dt_entrada').val(data.dt_entrada);
			$('#qtd_insumo').val(data.qtd_insumo);
			$('#cod_embalagem').val(data.cod_embalagem);
			$('#cod_embalagem').selectpicker('val', data.cod_embalagem);
			$('#vlr_unitario').val(data.vlr_unitario);
			$('#vlr_total').val(data.total_entrada);
			$('#cod_area').select(data.cod_area);
			$('#cod_area').selectpicker('val', data.cod_area);
			$('#ds_placa').val(data.ds_placa);
			$('#ds_motorista').val(data.ds_motorista);
			$('#desc_num_nota').val(data.desc_num_nota);
			$('#ds_observacao').val(data.ds_observacao);
			$('#lblcategoria').text(data.ds_categoria);
			$('#lblstatus').text('[Alteração]');
			$('#lblinsumo').text(data.ds_descricao);
			verificaValor(1,false);
			
} , 'json').always(function() {
			//após a inserção
			abrirEditarIncluir();
		});
} 
   
       function calculaValores(campoid){

    	   
       	var qtd_insumo =0;
        var vlr_unitario =0;
        var vlr_total = 0;

    	 qtd_insumo = $('#qtd_insumo').val().replace(/[^\d]+/g,'');
         vlr_unitario = $('#vlr_unitario').val().replace(/[^\d]+/g,'');
         vlr_total = $('#vlr_total').val().replace(/[^\d]+/g,'');

       // alert(vlr_unitario+"\m"+vlr_total)

       	//if(qtd_insumo != ''){
       		//retira a formatação do campo para efetuar o calculo
       //		if(campoid == 'vlr_unitario'){

//       				$('#vlr_total').val(vlr_unitario * qtd_insumo);
   
       				
  //     		}else if(campoid == 'vlr_total'){
    //   				$('#vlr_unitario').val(vlr_total / qtd_insumo);
       				
      // 		}else{
       	//		$('#vlr_unitario').val(vlr_total / qtd_insumo);
       		//	$('#vlr_total').val(vlr_unitario * qtd_insumo);
       			
       	//	}

   			//	formataValor('vlr_total');
       	//	}
       	//	}
           
      // 	}
        if (document.getElementById("qtd_insumo").value != ""){
            //retira a formatação do campo para efetuar o calculo
            if(campoid == 'qtd_insumo'){
					if(document.getElementById("vlr_unitario").value != "" && document.getElementById("vlr_total").value != "" ){
						document.getElementById("vlr_total").value = document.getElementById("vlr_unitario").value.replace(/[^\d]+/g,'') * document.getElementById("qtd_insumo").value; 
						
						formataValor('vlr_total')
						   }  
						
					}
            }

                
	        if(campoid == 'vlr_unitario'){
	        	document.getElementById("vlr_total").value = document.getElementById("vlr_unitario").value.replace(/[^\d]+/g,'') * document.getElementById("qtd_insumo").value; 
	        	formataValor('vlr_total');	
	        					
	 	       }else if(campoid == 'vlr_total'){
		 	      
	        	document.getElementById("vlr_unitario").value = document.getElementById("vlr_total").value.replace(/[^\d]+/g,'') / document.getElementById("qtd_insumo").value;
	        	// alert( document.getElementById("vlr_total").value.replace(/[^\d]+/g,''));
	        	formataValor('vlr_unitario');		
	        		
				   }
				     
        }

       function formataValor(campoid){
    		   $("#vlr_unitario,#vlr_total").priceFormat({
	        	  	prefix: 'R$ ',
	        	   centsSeparator: ',',
	        	   thousandsSeparator: '.',
               limit: 17,
               centsLimit: 2
	        	})
   	}
       
       function limpaForm(){
    	    $('#cod_fornecedor').selectpicker('val', "");
    	    $('#cod_embalagem').selectpicker('val', "");
    	    $('#cod_area').selectpicker('val', "");
    		$("#dt_entrada").val("");
    		$("#qtd_insumo").val("");
    		$("#vlr_unitario").val("");
    		$("#vlr_total").val("");
    		$("#desc_num_nota").val("");
    		$("#ds_placa").val("");
    		$("#ds_motorista").val("");
    		$("#ds_observacao").val("");
    	}

    	     	function excluirEntradaInsumo(id){
    	     		bootbox.confirm("Confirma a exclusão deste registro?<p>Não será possível reverter esta ação!", function(result) {
    	     			 // Example.show("Confirm result: "+result);
    	     			 if(result){
    		    			 $.post(base_url+'entradaInsumos/remove', {
    		     				id: id
    		     				//alert("ok")
    		     			}).done(function() {
    		     				//refresh na página
    						    window.location.reload(true);
    						  }).fail(function() {
    		     			    bootbox.alert("Já existem dados vinculados a esta Entrada de Insumo. Não é possível fazer a exclusão." );
    		     			})
    		     		}
    	     		});
    	        }

    	        function verificaValor(flag,click){
    	        	if (flag == 1){
    	        		$("#radio_unitario").prop('checked', true);
    	        		$("#radio_total").prop('checked', false);
    	        		$("#vlr_unitario").prop("readonly",false)
						$("#vlr_total").prop("readonly",true)
						if (click) {$("#vlr_unitario").focus()}
						
    	        	}else{
    	        		$("#radio_total").prop('checked', true);
    	        		$("#radio_unitario").prop('checked', false);
    	        		$("#vlr_unitario").prop("readonly",true)
						$("#vlr_total").prop("readonly",false)
						if (click) {$("#vlr_total").focus()}
							
    	        	}
    	        }


         	function carregaDadosPopUp(id){

         		$.post(base_url+'entradaInsumos/carregadadosEditarEntradaInsumos', {
         			id: id
         		}, function (data){

                 	$("#titulo").append(data.ds_descricao);
                 	montaPopUp('Quantidade',data.qtd_insumo+" "+data.ds_embalagem+" de "+data.num_volume+" "+data.ds_undmedida);
    				montaPopUp('Valor Unitário',"R$ "+data.vlr_unitario);
    				montaPopUp('Valor Total',"R$ "+data.total_entrada);
    				montaPopUp('Data da Entrada',data.dt_entrada);
    				montaPopUp('Nº Nota',data.desc_num_nota);
    				montaPopUp('Área Descarga',data.ds_area);
    				montaPopUp('Placa',data.ds_placa);
    				montaPopUp('Motorista',data.ds_motorista);
    				montaPopUp('Observações',data.ds_observacao);
    				
    				$('#popupDetalhe').modal('show');
    			
             		}, 'json');
       
    			function montaPopUp(valorLabel,valorCampo){
         			$("#conteudo > #conteudoForm").append('<div class="form-group" id="div-campos"><label class="col-sm-2 control-label" id="label-popup">'+valorLabel+'</label><label class="col-sm-8 control-label" id="valor-popup" style="text-align: left;font-weight: 400;">'+valorCampo+'</label></div>')
             	}
         	}
      	
</script>

<style type="text/css">

.slide {
	position: relative;
	overflow: hidden;
	height: 500px;
	/*width: 350px;*/
	/*margin: 1em 0;*
	/*background-color: #ffffff;*/
	/*border: 1px solid #999;*/
	z-index: 1;
}

.slide .inner {
	position: absolute;
	left: -1000px;
	bottom: 0;
	/*width: 338px;*/
	height: 100%;
	padding: 6px;
	background-color: #ffffff;
	/*color: #333; */
	z-index: 2;
}

th {
	text-align: center;
}

.estoque {
	background-color: #7EDEDE !important;
	text-align: center
}

#campoEdicaoo {
	position: absolute;
	/*  posição absoluta ao elemento pai, neste caso o BODY */
	/* Posiciona no meio, tanto em relação a esquerda como ao topo */
	/*left: 480px;*/
	top: 65px;
	/*width: 50%;  Largura da DIV */
	height: 100%; /* Altura da DIV */
	background-color: #ffffff;
	/*opacity: 1;*/
	/* A margem a esquerda deve ser menos a metade da largura */
	/* A margem ao topo deve ser menos a metade da altura */
	/* Fazendo isso, centralizará a DIV */
	z-index: 3;
	/* Faz com que fique sobre todos os elementos da página */
}

#bloqueioFundo {
	position: absolute;
	/*  posição absoluta ao elemento pai, neste caso o BODY */
	/* Posiciona no meio, tanto em relação a esquerda como ao topo */
	left: 0px;
	top: 0px;
	width: 100%; /* Largura da DIV */
	height: 100%; /* Altura da DIV */
	background-color: #808080;
	/*background-color: #ECF0F5;*/
	opacity: 0.2;
	z-index: 0;
	/* A margem a esquerda deve ser menos a metade da largura */
	/* A margem ao topo deve ser menos a metade da altura */
	/* Fazendo isso, centralizará a DIV */
	/*z-index: 10003;*/
	/* Faz com que fique sobre todos os elementos da página */
}
</style>

<div class="modal fade" id="popupDetalhe" tabindex="-1" role="dialog" aria-labelledby="popupDetalheLabel">
	<div class="modal-content" style="height: auto; width: auto; margin: 100px auto; max-width: 70%">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<h4 class="modal-title" id="titulo" style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif"></h4>
		</div>
		<div class="modal-body" id="conteudo" style="font-size: 14px">
			<form class="form-horizontal" style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif" id="conteudoForm">
			</form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-success pull-left"	data-dismiss="modal">Fechar</button>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-4">
		<div class="table">
			<table id="tblInsumos" class="table table-striped">
				<label style="position: absolute; width: 40%; top: 1%; left: 1.5%;">Insumo:
				</label>
				<tfoot
					style="position: absolute; width: 40%; top: -40px; left: 13%;">
					<th>Categoria</th>
				</tfoot>
				<thead>
					<th style="display: none">Categoria</th>
					<th style="width: 50%">Descrição</th>
					<th>Estoque</th>
					<th></th>
				</thead>
				<tbody>
			
				 <?php foreach ($insumos as $insumo) { ?>
						<tr>
						<td style="display: none"><?php echo $insumo['ds_categoria']?></td>
						<?php   $listaqtdInsumos = $this->entradainsumos_model->getTotalInsumos($insumo['cod_insumo']);?>
						
						<td style="width: 70%"><b><?php echo  $insumo['ds_descricao']?><b></td>
						<td style="width: 30%">
						<?php foreach ($listaqtdInsumos as $qtd) {?>
	
						<span style="color: blue; font-family:sans-serif;font-size: 12px;"><b><?=number_format($qtd['qtdTotalVolumeInsumo'],2,',','.'). ' ' .$qtd['ds_undmedida']?></b></span>
						<BR>
						<?php  } ?> 
						</td>
						<td style="width: 10%"><a class="btn btn-success btn-sm"
							onclick="chamaManutencaoEntradaInsumos(<?=$insumo['cod_insumo']?>,null)"><i
								class="glyphicon glyphicon-log-in"></i></a></td>
					</tr>
					<?php }?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-md-8" id="divEntradas">
		<div id="slidebottom" class="col-md-12 slide">
			<!-- -----------------------------------------------------------------Início da Tabela--------------------------------------------------------------------- -->

			<div class="table">

				<table id="tblEntradaInsumos" class="table table-striped table-bordered">
					<label
						style="position: absolute; width: 40%; top: 1%; left: 1.5%; display: none;">Tipo
						de Insumo:</label>
					<tfoot style="position: absolute; width: 0%; top: 0px; left: -2%;">
						<th>Categoria</th>
					</tfoot>

					<thead>
						<th style="display: none">Categoria</th>
						<th style="display: none">Código</th>
						<th>Data</th>
						<th>Produto</th>
						<th>Quantidade</th>
						<th>R$ Unitário</th>
						<th>R$ Total</th>
						
					</thead>

					<tbody>
					
					 <?php 
				
					 foreach ($listaEntradaInsumos as $entradaInsumos) { 
					 	?>
					 <tr>
						
							<td style="display: none"><?php echo $entradaInsumos['ds_categoria'];?></td>
							
							<td><?php echo $entradaInsumos['dt_entrada'];?></td>
							<td><?php echo $entradaInsumos['ds_descricao'] ?></td>
							<?php $explodeEntrada =  explode(".",$entradaInsumos['qtd_insumo']); ?>
							<?php $stringEntrada =  $entradaInsumos['ds_embalagem'].' '.$entradaInsumos['num_volume'].$entradaInsumos['ds_undmedida'];?>
							<td><?php echo  $explodeEntrada[1] == 0 ? number_format($entradaInsumos['qtd_insumo'],0).' '.$stringEntrada : number_format($entradaInsumos['qtd_insumo'],2,',','.').' '.$stringEntrada ?></td>
							<td width="120px">R$ <?php echo number_format($entradaInsumos['vlr_unitario'],2,',','.') ?></td>
							<td width="120px">R$ <?php echo number_format($entradaInsumos['totalEntrada'],2,',','.') ?></td>
							<td style="width: 65px; min-width: 40px;"><a href="javascript:;"
								onclick="chamaManutencaoEntradaInsumos(null,<?=$entradaInsumos['cod_entrada']?>)">
									<span class="fa fa-fw fa-edit"></span>
							</a>
							<a	href="javascript:;"	onclick="excluirEntradaInsumo(<?=$entradaInsumos['cod_entrada']?>)">
															<span class="fa fa-fw fa-trash-o"></span>
													</a><a href="javascript:;"
							onclick="carregaDadosPopUp(<?=$entradaInsumos['cod_entrada']?>)">
								<span class="fa fa-fw fa-reorder"></span>
						</a> </td>
							</tr>

						
						<?php } ?>
						
					</tbody>
				</table>
			</div>
			<button style="display: none"></button>
			<!-- -----------------------------------------------------------------Fim da Tabela--------------------------------------------------------------------- -->
			<div class="col-md-12 inner" id="inner">
				<!-- -----------------------------------------------------------------Início Efeito--------------------------------------------------------------------- -->

				<div class="info-box" style="min-height: 50px;">
 
				<span class="info-box-icon bg-green" style="height: 50px; font-size: 25px;width: 60px;line-height: 60px;margin-right: 10px;"><i class="fa fa-leaf" style="height: 50px;"></i></span>
				 <!--
					<div class="info-box-content" style="height: 25px;">
				--> <h4 class="box-title"> <label id="lblcategoria"></label>
							&nbsp;<label	id="lblstatus" style="font-size: 11px; color: red;"></label><BR>
							<label id="lblinsumo" style="font-size: 14px"></label>
						</h4>
<!-- 
					
					 -->
				</div>

				<form class="form-horizontal" id="frm_entradaInsumos"
					name="frm_entradaInsumos" method="post"
					action="<?php echo base_url('entradaInsumos/save')?>";>
					<!-- Campo de código entrada -->
					<div class="form-group" style="display: none">
						<label class="col-sm-2 control-label">Código da Entrada</label>
						<div class="col-sm-2">
							<input type="text" class="form-control" id="cod_entrada"
								name="cod_entrada">
						</div>
					</div>

					<!-- Campo de codigo insumo -->
					<div class="form-group" style="display: none">
						<label class="col-sm-2 control-label">Código do Insumo</label>
						<div class="col-sm-2">
							<input type="text" class="form-control" id="cod_insumo"
								name="cod_insumo">
						</div>
					</div>

					<div class="form-group" style="margin-bottom: 7px;">
						<label class="col-sm-2 control-label">Fornecedor</label>
						<div class="col-sm-6">
							<select type="text" data-live-search="false"
								class="selectpicker form-control" id="cod_fornecedor"
								name="cod_fornecedor" required="required">
								 <?php foreach ($fornecedores as $fornecedor) { ?>
                		<option
									value="<?php echo $fornecedor['cod_fornecedor']?>"><?php echo $fornecedor['ds_fornecedor']?> </option>
                				<?php }?>
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 7px;">
						<label class="col-sm-2 control-label">Data da Entrada</label>
						<div class="col-sm-4">
							<div class="input-group">
								<input class="form-control pull-right active" id="dt_entrada"
									name="dt_entrada" type="text"   required="required">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
							</div>
						</div>
					</div>

					<!-- Campo de quantidade -->
					<div class="form-group" style="margin-bottom: 7px;">
						<label class="col-sm-2 control-label">Quantidade</label>
						<div class="col-sm-3" style="padding-right: 0px">
							<input type="text" class="form-control" id="qtd_insumo"
								name="qtd_insumo" maxlength="9" onblur="calculaValores(this.id)" required="required">
						</div>
						<div class="col-sm-3" style="padding-left: 0px">
							<select type="text" data-live-search="false"
								class="selectpicker form-control" id="cod_embalagem"
								name="cod_embalagem">
								<!-- descrição / volume /unidade medida -->
								<!-- 
								<optgroup label="Sacos">
									<option>Sacos 50 Kg</option>
									<option>Sacos 30 Kg</option>
								</optgroup>
								<ouptgroup label="Bag">
								<option>Bag 1000 Kg</option>
								<option>Bag 500 Kg</option>
								</ouptgroup>
								<ouptgroup label="Litros">
								<option>Galão 20 Litros</option>
								</ouptgroup>
								 -->
								 <?php foreach ($embalagens as $embalagem) { ?>
                					<option
									value="<?php echo $embalagem['cod_embalagem']?>"><?php echo $embalagem['ds_embalagem'].' '.$embalagem['num_volume'] .$embalagem['ds_undmedida']?> </option>
                				<?php }?>
								 
							</select>
						</div>
					</div>

					<div class="form-group" style="margin-bottom: 7px;">
				
						<label class="col-sm-2 control-label">Valor Unit.</label>
						<div class="input-group col-sm-3" style="float: left;padding-left: 15px;">
	                        <span class="input-group-addon">
	                          <input type="radio" id="radio_unitario" onclick="verificaValor(1,true)">
	                        </span>
	                    	<input type="text" class="form-control" id="vlr_unitario" name="vlr_unitario" onblur="calculaValores(this.id)" required="required">
	                  	</div>

	                  	<label class="col-sm-2 control-label">Valor Total</label>
  						<div class="input-group col-sm-3" style="float:left;padding-left: 15px;">
  	                        <span class="input-group-addon">
  	                          <input type="radio" id="radio_total" onclick="verificaValor(2,true)">
  	                        </span>
  	                    	<input type="text" class="form-control" id="vlr_total" name="vlr_total" onblur="calculaValores(this.id)" required="required">
  	                  	</div>

						<!-- 
						<label class="col-sm-2 control-label">Valor Unitário</label>
						<div class="col-sm-2">
							<input type="text" class="form-control" id="vlr_unitario"
								name="vlr_unitario" onblur="calculaValores(this.id)" required="required">
						</div>

						<label class="col-sm-2 control-label">Valor Total</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="vlr_total"
								name="vlr_total" onblur="calculaValores(this.id)" required="required">
						</div>
						-->
					</div>

					<div class="form-group" style="margin-bottom: 7px;">
						<label class="col-sm-2 control-label">Nº Nota</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" id="desc_num_nota"
								name="desc_num_nota">
						</div>
					</div>
					
					<div class="form-group" style="margin-bottom: 7px;">
						<label class="col-sm-2 control-label">Área Descarga</label>
						<div class="col-sm-5">
							<select type="text" data-live-search="false"
								class="selectpicker form-control" data-size="5" id="cod_area"
								name="cod_area">
								<option value="">Selecione uma Área</option>
								 <?php foreach ($areas as $area) { ?>
                		<option
									value="<?php echo $area['cod_area']?>"><?php echo $area['ds_area']?> </option>
                				<?php }?>
							</select>
						</div>
					</div>
					
					<div class="form-group" style="margin-bottom: 7px;">
						<label class="col-sm-2 control-label">Placa</label>
						<div class="col-sm-2">
							<input type="text" class="form-control" id="ds_placa"
								name="ds_placa">
						</div>
					
						<label class="col-sm-2 control-label">Motorista</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="ds_motorista"
								name="ds_motorista">
						</div>
					</div>

					<div class="form-group" style="margin-bottom: 7px;">
						<label for="lbl_anotacao" class="col-sm-2 control-label">Observações</label>
						<div class="col-sm-8">
							<textarea name="ds_observacao" id="ds_observacao"
								maxlength="2000" class="form-control" rows="2"></textarea>
						</div>
					</div>

					<div class="box-footer" align="center">
						<button type="submit" class="btn btn-success">Salvar</button>
						<button type="button" class="btn btn-danger"
							onclick="manutencaoEntradaInsumos(null,null,'cancelar')">Cancelar</button>
					</div>
				</form>
				<!-- -----------------------------------------------------------------Fim Efeito--------------------------------------------------------------------- -->
			</div>
		</div>
	</div>
</div>
<div class="modal" id="bloqueioFundo"></div>
<!-- ----------------------------------------------------------------------------------------------------------------------------------------------------- -->
<!-- Modal -->
