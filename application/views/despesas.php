<script type="text/javascript">

 $(document).ready(function() {

	 var base_urlArquivo = "<?= base_url('arquivo') ?>";

	 $("#div_parcela").css("display", "none");
	 $("#div_vecimentoDespesa").css("display", "block");	 

	 $('input[name="ds_tppagamentoV"]').iCheck('check');	
	 $('input[name="ds_tppagamentoP"]').iCheck('uncheck');	
		

 	//quando fecha o modal ele exclui todos os campos
 	$('#popupDetalhe').on('hidden.bs.modal', function (e) {
 		$("#titulo").empty();
 		$("#conteudo > #conteudoForm").empty();
 	})

 	$('input').iCheck({
 	  checkboxClass: 'icheckbox_square-green',
 	  radioClass: 'iradio_square-green',
 	  
 	});

	
	$('#btnAdicionarUndMedida').popover({
         trigger: 'manual',
         html: true,
         title: '<p align="center"><b>Undidade de Medida</b></p>',
         content: $('#div-popover').html() // Adiciona o conteúdo da div oculta para dentro do popover.
      }).click(function (e) {
         e.preventDefault();
         // Exibe o popover.
         $(this).popover('show');
      });

	$("#cod_embalagem").change(function(){
		//revalida somente o campo de seleção quando ele selecionar algun item.
		var validator = $("#frm_embalagem").validate();
		validator.element("#cod_embalagem");
	});


	$("#btn_salvar").click(function(){

		$("#vlr_totalpagamento").value($("#vlr_total").value());
	
	});

	$("#dt_nota").datepicker({language: 'pt-BR', format: 'dd/mm/yyyy'});
	$("#dt_pagamento").datepicker({language: 'pt-BR', format: 'dd/mm/yyyy'});
	$("#dt_vencimento").datepicker({language: 'pt-BR', format: 'dd/mm/yyyy'});
	$("#dt_uso").datepicker({language: 'pt-BR', format: 'dd/mm/yyyy'});

	$("#dt_pagamentoDespesa").datepicker({language: 'pt-BR', format: 'dd/mm/yyyy'});
	$("#dt_vencimentoDespesa").datepicker({language: 'pt-BR', format: 'dd/mm/yyyy'});

	$("#vlr_total").priceFormat({
	  	prefix: 'R$ ',
	   centsSeparator: ',',
	   thousandsSeparator: '.',
	   limit: 17,
      centsLimit: 2
	});

	$("#vlr_totalpagamentoDespesa").priceFormat({
	  	prefix: 'R$ ',
	   centsSeparator: ',',
	   thousandsSeparator: '.',
       limit: 17,
       centsLimit: 2
	});

		$("#num_parcela").blur(function(){
			$.ajax({
  			//url: '../application/views/arquivo.php',
  			url : base_urlArquivo,
  			data: 'qtdParcelas='+$("#num_parcela").val(),
   		    type: "POST",
  			success: function(data) {
     		$('#testDIV').html(data);
  			}
	});		
			
	});

	$('input[name="ds_tppagamentoV"]').on('ifChecked', function(event){
		
		 $("#div_parcela").css("display", "none");
		 $("#div_vecimento").css("display", "block");
		 $('#testDIV').css("display", "none");
		 
		 $('input[name="ds_tppagamentoP"]').iCheck('uncheck');

	});

	$('input[name="ds_tppagamentoP"]').on('ifChecked', function(event){

		 $("#div_parcela").css("display", "block");
		 $("#div_vecimento").css("display", "none");
		 $('#testDIV').css("display", "block");

		 $('input[name="ds_tppagamentoV"]').iCheck('uncheck');

			});

	$("#frm_conta").validate({
        ignore: ':not(select:hidden, input:visible, textarea:visible)',
        errorPlacement: function (error, element) {
            if ($(element).is('select')) {
                element.next().after(error); // Validação especial para os campos select
            } else {
                error.insertAfter(element);  //Validação normal para os outros campos
            }
        },
        highlight: function(element, errorClass, validClass) {
        		$(element).closest('.form-group').addClass(errorClass).removeClass(validClass);
		    	$(element).closest('.form-group').addClass('has-error');
		  },
		unhighlight: function(element, errorClass, validClass) {
			$(element).closest('.form-group').removeClass(errorClass).addClass(validClass);
			$(element).closest('.form-group').removeClass('has-error');
		  }
    });

	
	//função que adiciona plugins a tabela de Cultivar
	$('#tblDespesa').dataTable({
		"iDisplayLength":9,            
	    "oLanguage": {
	     "sProcessing": "Aguarde enquanto os dados são carregados ...",
	     /*"sLengthMenu": "Mostrar _ hghgghg MENU_ registros por pagina",*/
	     "sLengthMenu": "",
	     "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
	     "sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
	     "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
	     "sInfoFiltered": "",
	     "sSearch": "Procurar",
	     "oPaginate": {
	      "sFirst":    "Primeiro",
	      "sPrevious": "Anterior",
	      "sNext":     "Próximo",
	      "sLast":     "Último"
	    }
	  }                              
	});

	//cria um botão dentro da tabela de Cultivar
	$("#tblDespesa_length").append("<button id='btnCadastrar' class='btn btn-success' onclick='manutencaoDespesa(1)'>Cadastrar</button>");
	
	
});

function insereUndmedida(){
	
	var ds_undmedida_aux = $('#ds_undmedida_aux').val();
	var parametros = "ds_undmedida="+ds_undmedida_aux;

	if(ds_undmedida_aux != ""){
		$.ajax({
			url: "embalagens/saveundmedida",
			type: "post",
			data: parametros,
			success: function(){
			$('#btnAdicionarUndMedida').popover('hide');
			$('#ds_undmedida_aux').val("");
		},
			error:function(){
			alert("Erro ao inserir uma nova Unidade de Medida");
			}
		}).always(function() {
			//após a inserção
			carregaDadosUndMedidaJson();
		})
	}else{
		$('#btnAdicionarUndMedida').popover('show');
	}
}

function carregaDadosFornecedorJson(){
		$.post(base_url+'despesas/listaFornecedores',{
			}, function (data){
			$('#cod_fornecedor').children().remove().end();//.append('<option selected value="">Selecione um item</option>') ;
	    		$(data).each(function(){
	    			$("#cod_fornecedor").append('<option value="'+this.cod_fornecedor+'">'+this.ds_fornecedor+'</option>');
				});
			$('#cod_fornecedor').selectpicker('refresh');

	  		}, 'json').always(function(data) {
  				//busca o ultimo código cadastrado
  				var sel = 1;
	  			$(data).each(function(){

	    			if(sel < this.cod_fornecedor){
						sel = this.cod_fornecedor;
	            	}
	  	
				});
  			 $('#cod_fornecedor').selectpicker('val', sel);	
		});
}

function cancelaUndmedida(){
	$('#btnAdicionarUndMedida').popover('hide');
	 $('#ds_undmedida_aux').val("");
}

function manutencaoDespesa(acao){
	if(acao == 1){
		limpaForm();
		$('#manutencaoDespesa').collapse('show');
		$('#tabelaDespesa').collapse('hide');
		$("#ds_despesa").focus();
		
	}else if(acao == 2){
		limpaForm();	
		$('#manutencaoDespesa').collapse('hide');
		$('#tabelaDespesa').collapse('show');

	}
	
}

function limpaForm(){
	$("#frm_despesa input").val("");
	$("#frm_despesa textarea").val("");	
	$("#frm_despesa").validate().resetForm();
	$(".form-group").removeClass("has-error");
	$("button.dropdown-toggle").css("border", "1px solid #DDD");
}

function editarDespesa(id){

		carregaDadosDespesaJson(id);
	}

	 function carregaDadosDespesaJson(id){
		 var base_urlArquivo = "<?= base_url('arquivo') ?>";
		 
 	    		$.post(base_url+'despesas/dadosDespesa', {
 	     			id: id
 	     		}, function (data){

 	     			$('#cod_despesa').val(data.cod_despesa);
 	     			$('#cod_despesaPai').val(data.cod_despesa);//aba Pagamento despesa
 	     			$('#ds_despesa').val(data.ds_depesa);
 	     			$('#num_nota').val(data.num_nota);
 	     			$('#cod_safra').selectpicker('val', data.cod_safra);
 	     			$('#cod_fornecedor').selectpicker('val', data.cod_fornecedor);
 	     			$('#cod_socio').selectpicker('val', data.cod_socio);
 	     			$('#cod_distribuicao').selectpicker('val', data.cod_distribuicao);
 	     			$("#dt_nota").val(data.dt_nota);
 	     			$('#cod_conta').selectpicker('val', data.cod_conta);
 	     			$('#ds_observacao').val(data.ds_observacao);
 	     			$('#vlr_total').val(data.vlr_total);

					//Pagamento Despesa
					///alert(data.cod_formapagamentoDespesa);
 	     			$('#cod_formapagamentoDespesa').selectpicker('val', data.cod_formapagamentoDespesa);

 	     			$('#cod_pagamentoDespesa').val(data.cod_pagamentodespesa);

 	     			 if(data.ds_tppagamentoDespesa == 'V'){ 	 	     			 
	 	     			 $('input[name="ds_tppagamentoV"]').iCheck('check');	
	 	     			 $('input[name="ds_tppagamentoP"]').iCheck('uncheck');

 	     				 $("#div_parcela").css("display", "none");
 	     				 $("#div_vecimentoDespesa").css("display", "block");
 	     				 
 	     			  }else  if(data.ds_tppagamentoDespesa == 'P'){ 	
 	     				$('input[name="ds_tppagamentoV"]').iCheck('uncheck');	
 	 	     			$('input[name="ds_tppagamentoP"]').iCheck('check');

	     				 $("#div_parcela").css("display", "block");
 	     				 $("#div_vecimentoDespesa").css("display", "none");
 	     			  }

 	     			$('#cod_parcelapagamentoDespesa').val(data.dt_vencimentoDespesa); 
 	     			$('#dt_vencimentoDespesa').val(data.dt_vencimentoDespesa);
 	     			$('#dt_pagamentoDespesa').val(data.dt_PagamentoDespesa);
 	     			$('#vlr_totalpagamentoDespesa').val(data.vlr_totalpagamentoDespesa);
 	     			$('#ds_observacaopagamentoDespesa').val(data.ds_observacaopagamentoDespesa);
 	     			$('#num_parcela').val(data.num_parcelas);

 	     			if(data.num_parcelas >0){	
 	     				$.ajax({
 	     	  			//url: '../application/views/arquivo.php',
 	     	  			url: base_urlArquivo,
 	     	  			data: 'qtdParcelas='+data.num_parcelas,
 	     	   		    type: "POST",
 	     	  			success: function(data) {
 	     	  	     	 $("#testDIV").css("display", "block");
 	     	     	
 	     	  			}
 	     	});

 	     	};
 	         		}, 'json').always(function() {
 						//após a inserção
 			 		$('#manutencaoDespesa').collapse('show');
 					$('#tabelaDespesa').collapse('hide');

 					$("#vlr_total").focus();
 					$("#ds_despesa").focus();
 					
 					});   			    		   
}
	     	     	function excluirDespesa(id){
	     	     		bootbox.confirm("Confirma a exclusão deste registro?", function(result) {
	     	     			 // Example.show("Confirm result: "+result);
	     	     			 if(result){
	     		    			 $.post(base_url+'despesas/remove', {
	     		     				id: id
	     		     			}).done(function() {
	     		     				//refresh na página
	     						    window.location.reload(true);
	     						  }).fail(function() {
	     		     			    bootbox.alert("Já existem dados vinculados a esta Despesa. Não é possível fazer a exclusão." );
	     		     			})
	     		     		}
	     	     		});
	     	        }


	     	  	 var base_url = "<?= base_url() ?>";

 	function carregaDadosPopUp(id){

 		$.post(base_url+'embalagens/dadosEmbalagem', {
 			id: id
 		}, function (data){

         	$("#titulo").append(data.ds_embalagem);
         	montaPopUp('Embalagem / Volume',data.ds_embalagem +" de "+data.num_volume+" "+data.ds_undmedida);
			montaPopUp('Devolução?',converte(data.ds_tipo));
			montaPopUp('Observações',data.ds_observacao);
			
			$('#popupDetalhe').modal('show');
		
     		}, 'json');

		function montaPopUp(valorLabel,valorCampo){
 			$("#conteudo > #conteudoForm").append('<div class="form-group" id="div-campos"><label class="col-sm-2 control-label" id="label-popup">'+valorLabel+'</label><label class="col-sm-8 control-label" id="valor-popup" style="text-align: left;font-weight: 400;">'+valorCampo+'</label></div>')
     	}

	

     	     	function converte(valor){
     	     			if(valor == "S"){
     	     				valor = 'Sim';
     	     			}else{
     	     				valor = 'Não';
     	     			}
     	     	return valor;
     	 		}
 	}

	    
</script>

<div class="modal fade" id="popupDetalhe" tabindex="-1" role="dialog"
	aria-labelledby="popupDetalheLabel">
	<div class="modal-content"
		style="height: auto; width: auto; margin: 150px auto; max-width: 70%">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<h4 class="modal-title" id="titulo"
				style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif"></h4>
		</div>
		<div class="modal-body" id="conteudo" style="font-size: 14px">
			<form class="form-horizontal"
				style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif"
				id="conteudoForm"></form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-success pull-left"
				data-dismiss="modal">Fechar</button>
		</div>
	</div>
</div>

<div class="box box-solid">
	<div class="box-default with-border">
		<h4 style="text-align: center">
			<b>Despesas</b>
		</h4>
	</div>

	<div class="box-body">
		<div id="manutencaoDespesa" class="col-md-12 collapse">
			<div class="col-md-12">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li><a href="#dadosDespesa" data-toggle="tab">Dados Despesa</a></li>
						<li><a href="#dadosPagamento" data-toggle="tab"
							onclick="mostraParcelas();">Dados Pagamento Despesa</a></li>
						<li class="pull-right"><a href="#" class="text-muted"><i
								class="fa fa-gear"></i></a></li>
					</ul>
				</div>
				<div class="tab-content">
					<div class="tab-pane active" id="dadosDespesa" name="dadosDespesa">
						<form class="form-horizontal" id="frm_despesa" name="frm_despesa"
							method="post" action="<?php echo base_url('despesas/save'); ?>">
							<!-- Campo de código -->
							<div class="form-group" style="display: none">
								<label class="col-sm-1 control-label">Código</label>
								<div class="col-sm-8">
									<input type="text" class="form-control-static" id="cod_despesa"
										name="cod_despesa" size="4" readonly="readonly">
								</div>
							</div>
							<div class="form-group" id="idfm">
								<label for="lbl_codsafra" class="col-sm-1 control-label"
									style="padding-left: 0%; width: 9%;">Safra</label>
								<div class="col-sm-2" style="padding-right: 0px; width: 21%;">
									<select id="cod_safra" name="cod_safra" required="required"
										data-live-search="true" data-size="2"
										class="selectpicker form-control">
							
								               <?php foreach ($safras as $safra) { ?>
								                <option selected="<?php ?>"
											value="<?php echo $safra['cod_safra']?>"><?php echo $safra['ds_safra']?> </option>
							                <?php }?>
							              </select>
								</div>
								<div class="col-xs-1" style="padding-left: 0px; width: 2.3%;">
									<a id="btnAdicionarFornecedor" class="btn btn-success"
										data-toggle="popover"><span class="glyphicon glyphicon-plus"></span></a>
								</div>

								<label for="lbl_dsdespesa" class="col-sm-1 control-label">Despesa</label>
								<div class="col-sm-3">
									<input type="text" class="form-control" id="ds_despesa"
										name="ds_despesa" required="required">
								</div>
								<label for="lbl_numnota" class="col-sm-1 control-label">Num.Nota
								</label>
								<div class="col-sm-3">
									<input type="text" class="form-control" id="num_nota"
										name="num_nota">
								</div>
							</div>
							<div class="form-group" id="idfm">
								<label for="lbl_codsocio" class="col-sm-1 control-label"
									style="padding-left: 0%; width: 9%;">Nome Nota</label>
								<div class="col-sm-2" style="padding-right: 0px; width: 21%;">
									<select id="cod_socio" name="cod_socio" required="required"
										data-live-search="true" class="selectpicker form-control">
							
								               <?php foreach ($socios as $socio) { ?>
								                <option selected="<?php ?>"
											value="<?php echo $socio['cod_socio']?>"><?php echo $socio['ds_socio']?> </option>
								                <?php }?>
								              </select>
								</div>
								<div class="col-xs-1" style="padding-left: 0px; width: 2%;">
									<a id="btnAdicionarSocio" class="btn btn-success"
										data-toggle="popover"><span class="glyphicon glyphicon-plus"></span></a>
								</div>

								<label class="col-sm-2 control-label"
									class="col-sm-1 control-label"
									style="padding-left: 0%; width: 9%;">Data Nota</label>
								<div class="col-sm-2" style="padding-right: 0px; width: 24%;">
									<div class="input-group">
										<input class="form-control pull-right active" id="dt_nota"
											name="dt_nota" type="text">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
									</div>
								</div>

								<label for="lbl_codgrupoconta" class="col-sm-1 control-label"
									style="padding-left: 3.2%; width: 9%;">Fornecedor</label>
								<div class="col-sm-2" style="padding-right: 0px; width: 21%;">
									<select id="cod_fornecedor" name="cod_fornecedor"
										required="required" data-live-search="true" data-size="2"
										class="selectpicker form-control">
							
								               <?php foreach ($fornecedores as $fornecedor) { ?>
								                <option selected="<?php ?>"
											value="<?php echo $fornecedor['cod_fornecedor']?>"><?php echo $fornecedor['ds_fornecedor']?> </option>
							                <?php }?>
							              </select>
								</div>
								<div class="col-xs-1" style="padding-left: 0px; width: 3.9%;">
									<a id="btnAdicionarFornecedor" class="btn btn-success"
										data-toggle="popover"><span class="glyphicon glyphicon-plus"></span></a>
								</div>
							</div>

							<div class="form-group" id="idfm">
								<label for="lbl_parceria" class="col-sm-1 control-label"
									style="padding-left: 2%; width: 9%;">Distribuição</label>
								<div class="col-sm-2" style="padding-right: 0px; width: 21%;">
									<select id="cod_distribuicao" name="cod_distribuicao"
										required="required" data-live-search="true" data-size="2"
										class="selectpicker form-control">
							
	               <?php foreach ($distribuicoes as $distribuicao) { ?>
	                <option selected="<?php ?>"
											value="<?php echo $distribuicao['cod_distribuicao']?>"><?php echo $distribuicao['ds_distribuicao']?> </option>
	                <?php }?>
	              </select>
								</div>

								<div class="col-xs-1" style="padding-left: 0px; width: 2%;">
									<a id="btnAdicionarSocio" class="btn btn-success"
										data-toggle="popover"><span class="glyphicon glyphicon-plus"></span></a>
								</div>
								<!-- 
									<label  class="col-sm-1 control-label" style="padding-left: 0%; width:9%;">Dt.Pagamento</label>
									<div class="col-sm-2"  style="padding-right: 0px; width: 24%;">
										<div class="input-group">
											<input class="form-control pull-right active"
												id="dt_pagamento" name="dt_pagamento" type="text"
												required="required">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
										</div>
									</div>
									 -->
								<label for="lbl_conta" class="col-sm-1 control-label"
									style="padding-left: 0%; width: 9%;">Conta</label>
								<div class="col-sm-2" style="padding-right: 0px; width: 21%;">
									<select id="cod_conta" name="cod_conta" required="required"
										data-live-search="true" class="selectpicker form-control">
							
								               <?php foreach ($contas as $conta) { ?>
								                <option selected="<?php ?>"
											value="<?php echo $conta['cod_conta']?>"><?php echo $conta['ds_conta']?> </option>
								                <?php }?>
								              </select>
								</div>
								<div class="col-xs-1" style="padding-left: 0px; width: 2%;">
									<a id="btnAdicionarSocio" class="btn btn-success"
										data-toggle="popover"><span class="glyphicon glyphicon-plus"></span></a>
								</div>

								<label for="lbl_numnota" class="col-sm-2 control-label"
									style="padding-left: 0%; width: 10%;">Valor Total</label>
								<div class="col-sm-2" style="padding-right: 0px; width: 24%;">
									<input type="text" class="form-control" id="vlr_total"
										name="vlr_total">
								</div>
							</div>

							<div class="form-group" id="idfm">
								<label for="lbl_observacao" class="col-sm-1 control-label"
									style="padding-left: 0%; width: 10.2%;">Observações</label>
								<div class="col-sm-8"
									style="padding-right: 0px; padding-left: 0px; width: 88%;">
									<textarea name="ds_observacao" id="ds_observacao"
										maxlength="2000" class="form-control" rows="4"></textarea>
								</div>
							</div>

							<div class="box-footer" align="center">
								<button type="submit" class="btn btn-success">Salvar</button>
								<button type="button" class="btn btn-danger"
									onclick="manutencaoDespesa(2)">Cancelar</button>
							</div>
						</form>
					</div>
					<div class="tab-pane" id="dadosPagamento" name="dadosPagamento">

						<form class="form-horizontal" id="frm_despesapagamento"
							name="frm_despesapagamento" method="post"
							onclick="ALert('oiii');"
							action="<?php echo base_url('despesas/pagamentoSave'); ?>">
							<div class="col-sm-2"
								style="padding-right: 0px; width: 24%; display: none">
								<input type="text" class="form-control" id="cod_despesaPai"
									name="cod_despesaPai">
							</div>
							<div class="col-sm-2"
								style="padding-right: 0px; width: 24%; display: none">
								<input type="text" class="form-control"
									id="cod_pagamentoDespesa" name="cod_pagamentoDespesa">
							</div>

							<div class="form-group">
								<label for="lbl_tp_crescimento" class="col-sm-2 control-label">Tipo
									Pagamento</label>
								<div class="col-sm-3">
									<div class="form-group">
										<div class="radio">
											<label> <input type="radio" class="minimal"
												name="ds_tppagamentoV" id="ds_tppagamentoDespesa" value="V">&nbsp;
												Vista
											</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <label> <input
												type="radio" class="minimal" name="ds_tppagamentoP"
												id="ds_tppagamentoDespesa" value="P"> &nbsp; Parcelado
											</label>
										</div>
									</div>
								</div>
							</div>

							<div id="div_parcela" name="div_parcela" class="form-group">
								<label for="lbl_numparcela" id="lbl_numparcela"
									name="lbl_numparcela" class="col-sm-2 control-label">Nº
									Parcelas</label>
								<div class="col-sm-2">
									<input type="text" class="form-control" id="num_parcela"
										name="num_parcela">
								</div>
							</div>

							<div class="form-group">

								<label for="lbl_tppagamento" class="col-sm-2 control-label">Forma
									Pagamento</label>
								<div class="col-sm-2" style="">
									<select id="cod_formapagamentoDespesa"
										name="cod_formapagamentoDespesa" data-live-search="true"
										class="selectpicker form-control">
								               <?php foreach ($formapagamentos as $formapagamento) { ?>
								                <option selected="<?php ?>"
											value="<?php echo $formapagamento['cod_formapagamento']?>"><?php echo $formapagamento['ds_formapagamento']?> </option>
								                <?php }?>
								              </select>
								</div>
							</div>

							<div id="div_vecimento" name="div_vencimento" class="form-group">
								<label id="lbl_dtvencimento" name="lbl_dtvencimento"
									class="col-sm-2 control-label">Data Vencimento</label>
								<div class="col-sm-2">
									<div class="input-group">
										<input class="form-control pull-right active"
											id="dt_vencimentoDespesa" name="dt_vencimentoDespesa"
											type="text">
										<div id="icon_vencimento" name="icon_vencimento"
											class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
									</div>
								</div>
							</div>

							<div id="div_PagamentoDespesa" name="div_PagamentoDespesa"
								class="form-group">
								<label id="lbl_dtpagamentodespesa" name="lbl_dtpagamentodespesa"
									class="col-sm-2 control-label">Data do Pagamento</label>
								<div class="col-sm-2">
									<div class="input-group">
										<input class="form-control pull-right active"
											id="dt_pagamentoDespesa" name="dt_pagamentoDespesa"
											type="text">
										<div id="icon_PagamentoDespesa" name="icon_PagamentoDespesa"
											class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label for="lbl_vlrpago" class="col-sm-2 control-label">Valor
									Pago</label>
								<div class="col-sm-2">
									<input type="text" class="form-control"
										id="vlr_totalpagamentoDespesa"
										name="vlr_totalpagamentoDespesa">
								</div>
							</div>

							<div class="form-group">
								<label for="lbl_observacao" class="col-sm-2 control-label">Observações</label>
								<div class="col-sm-8">
									<textarea name="ds_observacaopagamentoDespesa"
										id="ds_observacaopagamentoDespesa" maxlength="2000"
										class="form-control" rows="3"></textarea>
								</div>
							</div>

							<div id="testDIV"></div>

							<div class="box-footer" align="center">
								<button type="submit" id="btn_salvar" name="btn_salvar"
									class="btn btn-success">Salvar</button>
								<button type="button" class="btn btn-danger"
									onclick="manutencaoDespesa(2)">Cancelar</button>
							</div>

						</form>
					</div>
				</div>

				<!-- /.tab-pane -->
			</div>

			<!-- /.tab-pane -->
			<div class="tab-pane" id="transporte"></div>

			<!-- /.tab-content -->
		</div>

		<div id="tabelaDespesa" class="col-md-12 collapse in">
			<table class="table table-bordered table-striped" id="tblDespesa">
				<thead>
					<tr>
						<th class="tbl_col_ordenacao"></th>
						<th style="width: 1%">Código</th>
						<th>Despesa</th>
						<th>Fornecedor</th>
					</tr>
				</thead>
				<tbody>
				 <?php foreach ($despesas as $despesa) { ?>
					<tr>
						<td class="tbl_col_ordenacao"><a href="javascript:;"
							onclick="editarDespesa(<?=$despesa['cod_despesa']?>)"> <span
								class="fa fa-fw fa-edit"></span>
						</a></span> <a href="javascript:;"
							onclick="excluirDespesa(<?=$despesa['cod_despesa']?>)"> <span
								class="fa fa-fw fa-trash-o"></span>
						</a> </a></span>
						
						<td><?=$despesa['cod_despesa']?></td>
						<td><?=$despesa['ds_despesa']?></td>
						<td><?=$despesa['ds_fornecedor']?></td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>

	<script type="text/javascript">
		function mostraParcelas(){

			 var base_urlArquivo = "<?= base_url('arquivo') ?>";
				if(document.getElementById('num_parcela').value >0){
					$.ajax({
						url: base_urlArquivo,
						data: 'qtdParcelas='+$("#num_parcela").val(),
					    type: "POST",
						success: function(data) {
						$('#testDIV').html(data);
						}
			});		
		}
	}

 	  </script>