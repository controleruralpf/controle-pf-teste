<script type="text/javascript">

 $(document).ready(function() {

 	//quando fecha o modal ele exclui todos os campos
 	$('#popupDetalhe').on('hidden.bs.modal', function (e) {
 		$("#titulo").empty();
 		$("#conteudo > #conteudoForm").empty();
 	})

 	$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
 	  checkboxClass: 'icheckbox_square-green',
 	  radioClass: 'iradio_square-green'
 	});

	
	$('#btnAdicionarUndMedida').popover({
         trigger: 'manual',
         html: true,
         title: '<p align="center"><b>Undidade de Medida</b></p>',
         content: $('#div-popover').html() // Adiciona o conteúdo da div oculta para dentro do popover.
      }).click(function (e) {
         e.preventDefault();
         // Exibe o popover.
         $(this).popover('show');
      });
	

	$("#cod_embalagem").change(function(){
		//revalida somente o campo de seleção quando ele selecionar algun item.
		var validator = $("#frm_embalagem").validate();
		validator.element("#cod_embalagem");
	});
	
	$("#frm_embalagem").validate({
        ignore: ':not(select:hidden, input:visible, textarea:visible)',
        errorPlacement: function (error, element) {
            if ($(element).is('select')) {
                element.next().after(error); // Validação especial para os campos select
            } else {
                error.insertAfter(element);  //Validação normal para os outros campos
            }
        },
        highlight: function(element, errorClass, validClass) {
        		$(element).closest('.form-group').addClass(errorClass).removeClass(validClass);
		    	$(element).closest('.form-group').addClass('has-error');
		  },
		unhighlight: function(element, errorClass, validClass) {
			$(element).closest('.form-group').removeClass(errorClass).addClass(validClass);
			$(element).closest('.form-group').removeClass('has-error');
		  }
    });

	
	//função que adiciona plugins a tabela de Cultivar
	$('#tblEmbalagem').dataTable({
		"iDisplayLength":9,            
	    "oLanguage": {
	     "sProcessing": "Aguarde enquanto os dados são carregados ...",
	     /*"sLengthMenu": "Mostrar _ hghgghg MENU_ registros por pagina",*/
	     "sLengthMenu": "",
	     "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
	     "sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
	     "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
	     "sInfoFiltered": "",
	     "sSearch": "Procurar",
	     "oPaginate": {
	      "sFirst":    "Primeiro",
	      "sPrevious": "Anterior",
	      "sNext":     "Próximo",
	      "sLast":     "Último"
	    }
	  }                              
	});

	//cria um botão dentro da tabela de Cultivar
	$("#tblEmbalagem_length").append("<button id='btnCadastrar' class='btn btn-success' onclick='manutencaoEmbalagem(1)'>Nova Devolução</button>");

	$("#dt_devolucao").datepicker({language: 'pt-BR', format: 'dd/mm/yyyy'});
	
});

function insereUndmedida(){
	
	var ds_undmedida_aux = $('#ds_undmedida_aux').val();
	var parametros = "ds_undmedida="+ds_undmedida_aux;

	if(ds_undmedida_aux != ""){
		$.ajax({
			url: "embalagens/saveundmedida",
			type: "post",
			data: parametros,
			success: function(){
			$('#btnAdicionarUndMedida').popover('hide');
			$('#ds_undmedida_aux').val("");
		},
			error:function(){
			alert("Erro ao inserir uma nova Unidade de Medida");
			}
		}).always(function() {
			//após a inserção
			carregaDadosUndMedidaJson();
		})
	}else{
		$('#btnAdicionarUndMedida').popover('show');
	}
}

function carregaDadosUndMedidaJson(){

		$.post(base_url+'embalagens/listaUndMedida',{
			}, function (data){
			$('#cod_undmedida').children().remove().end();//.append('<option selected value="">Selecione um item</option>') ;
	    		$(data).each(function(){
	    			$("#cod_undmedida").append('<option value="'+this.cod_undmedida+'">'+this.ds_undmedida+'</option>');
				});
			$('#cod_undmedida').selectpicker('refresh');

	  		}, 'json').always(function(data) {
  				//busca o ultimo código cadastrado
  				var sel = 1;
	  			$(data).each(function(){

	    			if(sel < this.cod_undmedida){
						sel = this.cod_undmedida;
	            	}
	  	
				});
				//seleciona o último cadastrado

  			 $('#cod_undmedida').selectpicker('val', sel);
	
		});;

}

function cancelaUndmedida(){
	$('#btnAdicionarUndMedida').popover('hide');
	 $('#ds_undmedida_aux').val("");
}

function manutencaoEmbalagem(acao){
	if(acao == 1){
		limpaForm();
		$('#manutencaoEmbalagem').collapse('show');
		$('#tabelaEmbalagem').collapse('hide');
		 $("#ds_embalagem").focus();

	
			if ($('#cod_embalagem').val() == ""){
				$("#chk_tipo").attr("checked", true);
			}
		 
		
	}else if(acao == 2){
		limpaForm();	
		$('#manutencaoEmbalagem').collapse('hide');
		$('#tabelaEmbalagem').collapse('show');

	}
}

function limpaForm(){
	$("#frm_embalagem input").val("");
	$("#frm_embalagem textarea").val("");
	$('#cod_undmedida').selectpicker('val', "");
	$("#chk_devolucao").iCheck('uncheck');
	$("#frm_embalagem").validate().resetForm();
	$(".form-group").removeClass("has-error");
	$("button.dropdown-toggle").css("border", "1px solid #DDD");
}

function editarEmbalagem(id){
	
		carregaDadosEmbalagemJson(id);
	}


var base_url = "<?= base_url() ?>";

	 function carregaDadosEmbalagemJson(id){
		 
		 limpaForm();

 	     		$.post(base_url+'embalagens/dadosEmbalagem', {
 	     			id: id
 	     		}, function (data){
 	     			$('#cod_embalagem').val(data.cod_embalagem);
 	     			$('#ds_embalagem').val(data.ds_embalagem);
 	     			$('#cod_undmedida').val(data.cod_undmedida);

 	     		
 	     			if(data.sn_devolucao == 'S'){
 	 	     			//jQuery("#chk_tipo").attr('checked', 'checked');
 	     				$("#chk_devolucao").iCheck('check');
 	     			}

 	     			$('#num_volume').val(data.num_volume);
 	     			
 	     			$('#ds_observacao').val(data.ds_observacao);

 	     			//seleciona o valor	
 	     			 $('#cod_undmedida').selectpicker('val', data.cod_undmedida);
 	     			
 	         		}   , 'json');

         		
 	     		$('#manutencaoEmbalagem').collapse('show');
 	    		$('#tabelaEmbalagem').collapse('hide');

 	    		 $("#ds_embalagem").focus();	  
  	
	     	}

	     	     	function excluirEmbalagem(id){
	     	     		bootbox.confirm("Confirma a exclusão deste registro?", function(result) {
	     	     			 // Example.show("Confirm result: "+result);
	     	     			 if(result){
	     		    			 $.post(base_url+'embalagens/remove', {
	     		     				id: id
	     		     				//alert("ok")
	     		     			}).done(function() {
	     		     				//refresh na página
	     						    window.location.reload(true);
	     						  }).fail(function() {
	     		     			    bootbox.alert("Já existem dados vinculados a esta Embalagem. Não é possível fazer a exclusão." );
	     		     			})
	     		     		}
	     	     		});
	     	        }

 	function carregaDadosPopUp(id){

 		$.post(base_url+'embalagens/dadosEmbalagem', {
 			id: id
 		}, function (data){

         	$("#titulo").append(data.ds_embalagem);
         	montaPopUp('Embalagem / Volume',data.ds_embalagem +" de "+data.num_volume+" "+data.ds_undmedida);
			montaPopUp('Devolução?',converte(data.ds_tipo));
			montaPopUp('Observações',data.ds_observacao);
			
			$('#popupDetalhe').modal('show');
		
     		}, 'json');

		function montaPopUp(valorLabel,valorCampo){
 			$("#conteudo > #conteudoForm").append('<div class="form-group" id="div-campos"><label class="col-sm-2 control-label" id="label-popup">'+valorLabel+'</label><label class="col-sm-8 control-label" id="valor-popup" style="text-align: left;font-weight: 400;">'+valorCampo+'</label></div>')
     	}

     	     	function converte(valor){
     	     			if(valor == "S"){
     	     				valor = 'Sim';
     	     			}else{
     	     				valor = 'Não';
     	     			}
     	     	return valor;
     	 		}
 	}

	    
</script>

<div class="modal fade" id="popupDetalhe" tabindex="-1" role="dialog" aria-labelledby="popupDetalheLabel">
	<div class="modal-content" style="height: auto; width: auto; margin: 150px auto; max-width: 70%">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<h4 class="modal-title" id="titulo" style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif"></h4>
		</div>
		<div class="modal-body" id="conteudo" style="font-size: 14px">
			<form class="form-horizontal" style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif" id="conteudoForm">
			</form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-success pull-left"	data-dismiss="modal">Fechar</button>
		</div>
	</div>
</div>

<div class="box box-solid">
	<div class="box-default with-border">
		<h4 style="text-align: center">
			<b>Devolução Embalagens</b>
		</h4>
	</div>

	<div class="box-body">
		<div id="manutencaoEmbalagem" class="col-md-12 collapse">
			<form class="form-horizontal" id="frm_embalagem" name="frm_embalagem" method="post"  action="<?php echo base_url('embalagens/save'); ?>">
				
				
				<!-- Campo de código -->
				<div class="form-group" style="display: none">
					<label class="col-sm-2 control-label">Código</label>
					<div class="col-sm-10">
						<input type="text" class="form-control-static" id="cod_embalagem"
							name="cod_embalagem" size="4" readonly="readonly">
					</div>
				</div>
				
				<div class="form-group" style="margin-bottom: 7px;">
						<label class="col-sm-2 control-label">Data Devolução</label>
						<div class="col-sm-4">
							<div class="input-group">
								<input class="form-control pull-right active" id="dt_devolucao"
									name="dt_devolucao" type="text"   required="required">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
							</div>
						</div>
					</div>
				
				<!-- Campo Placa -->
				<div class="form-group">
						<label for="lbl_dsplaca" class="col-sm-2 control-label">Placa Veículo</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="ds_placa" name="ds_placa" required="required">
					</div>
				</div>

				<!-- Campo Motorista -->
				<div class="form-group">
						<label for="lbl_dsmorotista" class="col-sm-2 control-label">Motorista</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="ds_motorista" name="ds_motorista" required="required">
					</div>
				</div>
				
				<!-- NumeroNota -->
				<div class="form-group">
						<label for="lbl_dsnumnota" class="col-sm-2 control-label">Nº Nota</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="ds_numnota" name="ds_numnota" required="required">
					</div>
				</div>
				
				
				

				<div class="form-group">
					<label for="lbl_diasciclo" class="col-sm-2 control-label">Total Embalagens</label>
					<div class="col-sm-8">
						<input type="text" style="width: 80px;"  maxlength="5"  class="form-control" id="num_totalEmbalagem"
							name="num_totalEmbalagem" placeholder="">
					</div>
				</div>
				
				<div class="form-group">
					<label for="lbl_observacao" class="col-sm-2 control-label">Observações</label>
					<div class="col-sm-8">
						<textarea name="ds_observacao" id="ds_observacao" maxlength="2000" class="form-control" rows="3" ></textarea>
					</div>
				</div>


				<div class="box-footer" align="center">
					<button type="submit" class="btn btn-success">Salvar</button>
					<button type="button" class="btn btn-danger"
						onclick="manutencaoEmbalagem(2)">Cancelar</button>
				</div>
			</form>
		</div>

		<div id="tabelaEmbalagem" class="col-md-12 collapse in">
			<table class="table table-bordered table-striped" id="tblEmbalagem">
				<thead>
					<tr>
						<th class="tbl_col_ordenacao"></th>
						<th style="width: 1%">Código</th>
						<th>Embalagem / Volume</th>
						<th>Devolução</th>
					</tr>
				</thead>
				<tbody>
				 <?php foreach ($embalagens as $embalagem) { ?>
					<tr>
						<td class="tbl_col_ordenacao">
						<a href="javascript:;" onclick="editarEmbalagem(<?=$embalagem['cod_embalagem']?>)"> 
							<span class="fa fa-fw fa-edit"></span>
						</a></span>   
						
						<a	href="javascript:;"	onclick="excluirEmbalagem(<?=$embalagem['cod_embalagem']?>)">
														<span class="fa fa-fw fa-trash-o"></span>
												</a>
						</a></span><a href="javascript:;"
							onclick="carregaDadosPopUp(<?=$embalagem['cod_embalagem']?>)">
								<span class="fa fa-fw fa-reorder"></span></td>
						<td><?=$embalagem['cod_embalagem']?></td>
						<td><?=$embalagem['ds_embalagem'].' / ' .$embalagem['num_volume'].' '.$embalagem['ds_undmedida']?></td>
						<td><?=$embalagem['sn_devolucao'] == 'S' ? 'Sim' : 'Não'?></td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>

	</div>
