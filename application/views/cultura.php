
<script type="text/javascript">

$(document).ready(function() {

	$("#frm_cultura").validate({
	  	highlight: function(element, errorClass, validClass) {
	  		$(element).closest('.form-group').addClass(errorClass).removeClass(validClass);
	    	$(element).closest('.form-group').addClass('has-error');
		  },
		unhighlight: function(element, errorClass, validClass) {
			$(element).closest('.form-group').removeClass(errorClass).addClass(validClass);
			$(element).closest('.form-group').removeClass('has-error');
		  }
	});
	
	//função que adiciona plugins a tabela de Cultura
	$('#tblCultura').dataTable({ 
		"iDisplayLength":9,                             
	    "oLanguage": {
	     "sProcessing": "Aguarde enquanto os dados são carregados ...",
	     /*"sLengthMenu": "Mostrar _ hghgghg MENU_ registros por pagina",*/
	     "sLengthMenu": "",
	     "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
	     "sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
	     "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
	     "sInfoFiltered": "",
	     "sSearch": "Procurar",
	     "oPaginate": {
	      "sFirst":    "Primeiro",
	      "sPrevious": "Anterior",
	      "sNext":     "Próximo",
	      "sLast":     "Último"
	    }
	  }                            
	});

	//cria um botão dentro da tabela de cultura
	$("#tblCultura_length").append("<button id='btnCadastrar' class='btn btn-success' onclick='manutencaoCultura(1)'>Cadastrar</button>");
	  
});


function manutencaoCultura(acao){
	if(acao == 1){
		$("#frm_cultura input").val("")
		$('#manutencaoCultura').collapse('show');
		$('#tabelaCultura').collapse('hide');

		 $("#ds_cultura").focus();
		
	}else if(acao == 2){
		//reseta as validações do form, e tira a classe de erros do formulário
		$("#frm_cultura input").val("")
		$("#frm_cultura").validate().resetForm();
		$(".form-group").removeClass("has-error");

		$('#manutencaoCultura').collapse('hide');
		$('#tabelaCultura').collapse('show');

	}
}

var base_url = "<?= base_url() ?>";

 function carregaDadosCulturaJson(id){

     		$.post(base_url+'cultura/dadosCultura', {
     			id: id
     		}, function (data){
     			$('#cod_cultura').val(data.cod_cultura);
     			$('#ds_cultura').val(data.ds_cultura);
         		}, 'json');

     		$('#manutencaoCultura').collapse('show');
    		$('#tabelaCultura').collapse('hide');

    		 $("#ds_cultura").focus();

     		
     	}
    
     	function editarCultura(id){
     		carregaDadosCulturaJson(id);
     	}

     	
     	function excluirCultura(id){
         		     		bootbox.confirm("Confirma a exclusão deste registro?", function(result) {
	     			 // Example.show("Confirm result: "+result);
	     			 if(result){
		    			 $.post(base_url+'cultura/remove/', {
		     				id: id
		     				//alert("ok")
		     			}).done(function() {
		     				//refresh na página
						    window.location.reload(true);
						  }).fail(function() {
		     			    bootbox.alert("Já existem dados vinculados a esta Embalagem. Não é possível fazer a exclusão." );
		     			})
		     		}
	     		});
	        }
     	
  </script>

<div class="modal fade" id="popupDetalhe" tabindex="-1" role="dialog"
	aria-labelledby="popupDetalheLabel">
	<div class="modal-content"
		style="height: auto; width: auto; margin: 150px auto; max-width: 70%">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<h4 class="modal-title" id="titulo"
				style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif"></h4>
		</div>
		<div class="modal-body" id="conteudo" style="font-size: 14px">
			<form class="form-horizontal"
				style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif"
				id="conteudoForm"></form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-success pull-left"
				data-dismiss="modal">Fechar</button>
		</div>
	</div>
</div>

<div class="box box-solid">
	<div class="box-default with-border">
		<h4 style="text-align: center">
			<b>Culturas</b>
		</h4>
	</div>
	<div class="box-body">
		<div id="manutencaoCultura" class="col-md-12 collapse">
			<form class="form-horizontal" id="frm_cultura" name="frm_cultura"
				method="post" action="<?php echo base_url('cultura/save'); ?>">
				<!-- Campo de código -->
				<div class="form-group" style="display: none">
					<label class="col-sm-2 control-label">Código</label>
					<div class="col-sm-10">
						<input type="text" class="form-control-static" id="cod_cultura"
							name="cod_cultura" size="4" readonly="readonly">
					</div>
				</div>
				<!-- Campo de descrição -->
				<div class="form-group">
					<label for="lbl_cultura" class="col-sm-2 control-label">Descrição</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="ds_cultura"
							name="ds_cultura" required placeholder="">
					</div>
				</div>
				<div class="box-footer" align="center">
					<button type="submit" class="btn btn-success">Salvar</button>
					<button type="button" class="btn btn-danger"
						onclick="manutencaoCultura(2)">Cancelar</button>
				</div>
			</form>
		</div>
		<div id="tabelaCultura" class="col-md-12 collapse in">
			<table class="table table-bordered table-striped" id="tblCultura">
				<thead>
					<tr>
						<th class="tbl_col_ordenacao"></th>
						<th style="width: 1%">Código</th>
						<th>Descrição</th>
					</tr>
				</thead>
				<tbody>
				 <?php foreach ($culturas as $cultura) { ?>
					<tr>
						<td align=center class="tbl_col_ordenacao"><a href="javascript:;"
							onclick="editarCultura(<?=$cultura['cod_cultura']?>)"> <span
								class="fa fa-fw fa-edit"></span>
						</a></span> <a href="javascript:;"
							onclick="excluirCultura(<?=$cultura['cod_cultura']?>)"> <span
								class="fa fa-fw fa-trash-o"></span>
						</a></td>
						<td><?=$cultura['cod_cultura']?></td>
						<td><?=$cultura['ds_cultura']?></td>
					</tr>
					<?php }?>
				</tbody>
			</table>
		</div>
	</div>