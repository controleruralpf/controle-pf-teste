
<script type="text/javascript">

$(document).ready(function() {


	$("#ds_fonesocio").inputmask("(99) 9999-9999");
	
	$("#frm_socio").validate({
	  	highlight: function(element, errorClass, validClass) {
	  		$(element).closest('.form-group').addClass(errorClass).removeClass(validClass);
	    	$(element).closest('.form-group').addClass('has-error');
		  },
		unhighlight: function(element, errorClass, validClass) {
			$(element).closest('.form-group').removeClass(errorClass).addClass(validClass);
			$(element).closest('.form-group').removeClass('has-error');
		  }
	});
	
	//função que adiciona plugins a tabela de Fornecedor
	$('#tblSocio').dataTable({       
		"iDisplayLength":9,                       
	    "oLanguage": {
	     "sProcessing": "Aguarde enquanto os dados são carregados ...",
	     /*"sLengthMenu": "Mostrar _ hghgghg MENU_ registros por pagina",*/
	     "sLengthMenu": "",
	     "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
	     "sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
	     "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
	     "sInfoFiltered": "",
	     "sSearch": "Procurar",
	     "oPaginate": {
	      "sFirst":    "Primeiro",
	      "sPrevious": "Anterior",
	      "sNext":     "Próximo",
	      "sLast":     "Último"
	    }
	  }
	});

	//cria um botão dentro da tabela de fornecedor
	$("#tblSocio_length").append("<button id='btnCadastrar' class='btn btn-success' onclick='manutencaoSocio(1)'>Cadastrar</button>");

	//quando fecha o modal ele exclui todos os campos
	$('#popupDetalhe').on('hidden.bs.modal', function (e) {
		$("#titulo").empty();
		$("#conteudo > #conteudoForm").empty();
	});
	  
});


function manutencaoSocio(acao){
	if(acao == 1){
		limpaForm();
		$('#manutencaoSocio').collapse('show');
		$('#tabelaSocio').collapse('hide');

		 $("#ds_socio").focus();
		
	}else if(acao == 2){
		limpaForm();
		$('#manutencaoSocio').collapse('hide');
		$('#tabelaSocio').collapse('show');

	}
}

var base_url = "<?= base_url() ?>";

function carregaDadosSocioJson(id){

     		$.post(base_url+'socio/dadosSocio', {
     			id: id
     		}, function (data){
     			$('#cod_socio').val(data.cod_socio);
     			$('#ds_socio').val(data.ds_socio);
     			$('#ds_fonesocio').val(data.ds_fonesocio);
     			

         		}, 'json').always(function() {
					//após a inserção
		 		$('#manutencaoSocio').collapse('show');
				$('#tabelaSocio').collapse('hide');

				 $("#ds_socio").focus();
				});   		
}
    
     	function editarSocio(id){
     		carregaDadosSocioJson(id);
     	}
     	
     	function excluirSocio(id){
     		bootbox.confirm("Confirma a exclusão deste registro?", function(result) {
     			 // Example.show("Confirm result: "+result);
     			 if(result){
	    			 $.post(base_url+'socio/remove', {
	     				id: id
	     				//alert("ok")
	     			}).done(function() {
	     				//refresh na página
					    window.location.reload(true);
					  }).fail(function() {
	     			    bootbox.alert("Já existem dados vinculados a este Sócio. Não é possível fazer a exclusão." );
	     			})
	     		}
     		});
        }

     	function carregaDadosPopUp(id){

     		$.post(base_url+'fornecedor/dadosSocio', {
     			id: id
     		}, function (data){

             	$("#titulo").append(data.ds_fornecedor);
             	montaPopUp('Contato',data.ds_contato);
				montaPopUp('Telefone',data.ds_fone);
				montaPopUp('Telefone',data.ds_fone2);
				montaPopUp('Endereço',data.ds_endereco);
				montaPopUp('Observação',data.ds_observacao);
				
				$('#popupDetalhe').modal('show');
			
         		}, 'json');

			function montaPopUp(valorLabel,valorCampo){
     			$("#conteudo > #conteudoForm").append('<div class="form-group" id="div-campos"><label class="col-sm-2 control-label" id="label-popup">'+valorLabel+'</label><label class="col-sm-8 control-label" id="valor-popup" style="text-align: left;font-weight: 400;">'+valorCampo+'</label></div>')
         	}
     	}

     	function limpaForm(){
     		$("#frm_socio input").val("");
     		$("#frm_socio textarea").val("");
     		$("#frm_socio").validate().resetForm();
     		$(".form-group").removeClass("has-error");
     	}
     	
  </script>

<div class="modal fade" id="popupDetalhe" tabindex="-1" role="dialog" aria-labelledby="popupDetalheLabel">
	<div class="modal-content" style="height: auto; width: auto; margin: 150px auto; max-width: 70%">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<h4 class="modal-title" id="titulo" style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif"></h4>
		</div>
		<div class="modal-body" id="conteudo" style="font-size: 14px">
			<form class="form-horizontal" style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif" id="conteudoForm">
			</form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-success pull-left"	data-dismiss="modal">Fechar</button>
		</div>
	</div>
</div>

<div class="box box-solid">
	<div class="box-default with-border">
		<h4 style="text-align: center">
			<b>Sócios</b>
		</h4>
	</div>

	<div class="box-body">
		<div id="manutencaoSocio" class="col-md-12 collapse">
			<form class="form-horizontal" id="frm_socio"
				name="frm_socio" method="post"
				action="<?php echo base_url('socio/save'); ?>">

				<!-- Campo de código -->
				<div class="form-group" style="display: none">
					<label class="col-sm-2 control-label">Código</label>
					<div class="col-sm-10">
						<input type="text" class="form-control-static" id="cod_socio"
							name="cod_socio" size="4" readonly="readonly">
					</div>
				</div>

				<!-- Campo de descrição -->
				<div class="form-group">
					<label for="lbl_socio" class="col-sm-2 control-label">Sócio</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="ds_socio"
							name="ds_socio" required placeholder="">
					</div>
				</div>

				<!-- Campo de descrição -->
				<div class="form-group">
					<label for="lbl_fonesocio" class="col-sm-2 control-label">Telefone</label>
					<div class="col-sm-2">
						<input type="text" class="form-control" id="ds_fonesocio"
							name="ds_fonesocio" placeholder="">
					</div>
				</div>


				<div class="box-footer" align="center">
					<button type="submit" class="btn btn-success">Salvar</button>
					<button type="button" class="btn btn-danger"
						onclick="manutencaoSocio(2)">Cancelar</button>
				</div>
			</form>
		</div>

		<div id="tabelaSocio" class="col-md-12 collapse in">
			<table class="table table-bordered table-striped" id="tblSocio">
				<thead>
					<tr>
						<th class="tbl_col_ordenacao"></th>
						<th style="width: 1%">Código</th>
						<th>Sócio</th>
						<th style="width: 10%">Telefone</th>
						
					</tr>
				</thead>
				<tbody>
				 <?php foreach ($socio as $socio) { ?>
					<tr id="<?=$socio['cod_socio']?>">
						<td class="tbl_col_ordenacao"><a href="javascript:;"
							onclick="editarSocio(<?=$socio['cod_socio']?>)"> <span
								class="fa fa-fw fa-edit"></span>
						</a></span> <a
							href="javascript:;"
							onclick="excluirFornecedor(<?=$socio['cod_socio']?>)">
								<span class="fa fa-fw fa-trash-o"></span>
						</a></span></td>
						<td><?=$socio['cod_socio']?></td>
						<td><?=$socio['ds_socio']?></td>
						<td><?=$socio['ds_fonesocio']?></td>
						
					</tr>
					<?php }?>
				</tbody>
			</table>
		</div>

	</div>