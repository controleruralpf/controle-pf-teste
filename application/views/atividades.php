<script type="text/javascript">

	$(document).ready(function() {

	//$('#mnt_atividades').collapse();

		$(".dial").knob({
			'readOnly':true,
		});

		$('#areas').slimScroll({
			height: '570px',
			width: '335px',
			railVisible: true,
			alwaysVisible: true
		});

		$('#tblAreas').dataTable({ 
			"iDisplayLength":6,                             
			"oLanguage": {
				"sProcessing": "Aguarde enquanto os dados são carregados ...",
				/*"sLengthMenu": "Mostrar _ hghgghg MENU_ registros por pagina",*/
				"sLengthMenu": "",
				"sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
				"sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
				"sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
				"sInfoFiltered": "",
				"sSearch": "Buscar área",
				"oPaginate": {
					"sFirst":    "Primeiro",
					"sPrevious": "Anterior",
					"sNext":     "Próximo",
					"sLast":     "Último"
				}
			}                              
		});

$(".atividade").click(function(index){
	classAtividade($(this).index());
});

$(".areas").click(function(index){
	classArea($(this).index());
});

});
/*
function move(posicao) {
	if(posicao == 1){
		$('div.demo').animate( {scrollLeft: '+=120' }, 600);
	}else if(posicao == 2){
		$('div.demo').animate( {scrollLeft: '-=120' }, 600);
	}
}
*/
function classAtividade(index){
	$(".atividade").each(function() {
		if($(this).index() == index){
			$(this).addClass( "active");
			vr_area = $(this).find('.list-group-item-heading').text();
		}else{
			$(this).removeClass( "active");
		}
	});
}

function classArea(index){
	$(".areas").each(function() {
		if($(this).index() == index){
			$(this).addClass( "active");
			vr_area = $(this).find('.list-group-item-heading').text();
		}else{
			$(this).removeClass( "active");
		}
	});
}

var vr_manut = "";
var vr_area = "";

function manutencao(acao, obj){
	var codArea = $(obj).closest('ul').prop('id');

	//$('#mnt_atividades').('hidden.bs.collapse', function () {
	 //$('#mnt_atividades').on('hidden.bs.collapse',function(){
	  cancelar();
//	})
	

	if(acao != 'cancelar'){
		
		$.ajax({
	            url: "atividades/"+acao,
	            type: 'GET',
	            data: 'codArea='+codArea,
	            success: function(data){
	               $("#mnt_atividades").append(data);
	            }
	        }).always(function() {

	     //   setTimeout(function(){ 
				$('#mnt_atividades').collapse('show');
	       //  }, 2000);
				

			  }); 
	}

function cancelar(){
	$('#mnt_atividades').html("");

//	setTimeout(function(){ 
	$('#mnt_atividades').collapse('hide');
	
	//}, 1000);
	
}

}

var base_url = "<?= base_url() ?>";
</script>
<style>
	.atividadeAtiva {
		border: 2px solid #4D4545; <!--
		border-radius: 18px !important; -->
		border-color: #3C8DBC !important;
		background-color: #3C8DBC;
		color: #fff;
	}

	.btn-app {
		min-width: 120px;
	}

	<!--
	.col-md-2 {<!--
		width: 11%;
	-->
}

.btn-success {
	background-color: #02B060 !important;
	border-color: #008D4C;
}

-->
div.demo {
	margin: 5px;
	padding: 5px;
	position: relative;
	width: 100%;
	overflow: auto;
}

.p {
	width: 300%;
}

.hu {
	height: 55px;
	width: 100%;
}

.input-group-btn {
	padding-bottom: 2px;
}

.fa-caret-down {
	position: absolute;
	right: 10px;
}

.dropdown-menu {
	min-width: 100%;
	padding: 5px 0px;
	margin: 2px 0px 0px;
	font-size: 15px;
	box-shadow: none;
	border-color: #8F8787;
}

td{

	padding: 2px !important;
}

canvas{
	background-color: #fff;
	border-radius: 75px;
}
</style>


<div class="row">
	<div class="btn-group-vertical col-md-4" >
		<table id="tblAreas" class="table table-striped">
			<thead>
				<th>Selecione a área e a atividade que deseja cadastrar</th>
			</thead>
			<tbody>
				<?php foreach ($areas as $area) { ?>
				<tr>
					<td>
						<div>
							<div class="input-group-btn">
								<button aria-expanded="false" type="button"
								class="btn btn-primary dropdown-toggle hu" data-toggle="dropdown">
								<h4 class="list-group-item-heading"><?=$area['ds_area']?></h4>
								<span class="fa fa-caret-down"></span>
								<p class="list-group-item-text"><?=$area['num_hecplanta']?> Hectares de Plantio</p>
							</button>
							<ul class="dropdown-menu" id="<?=$area['cod_area']?>">
								<table>
									<tr>
										<td><a class="btn btn-app" onclick="manutencao('getPreparacaoSolo',$(this))"><i class="fa fa-fire"></i> Outras Atividades</a></td>
										<td><a class="btn btn-app" onclick="manutencao('getAdubacao',$(this))"><i class="fa fa-share-alt"></i> Adubação</a></td>
										<td><a class="btn btn-app" onclick="manutencao('getPlantio',$(this))"><i class="fa fa-leaf"></i> Dessecação</a></td>
										

									</tr>
									<tr>
										
										<td><a class="btn btn-app" onclick="manutencao('getPlantio',$(this))"><i class="fa fa-leaf"></i> Plantio</a></td>
											<td><a class="btn btn-app" onclick="manutencao('getDefensivos',$(this))"><i class="fa fa-bug"></i> Defensivos</a></td>
											<td><a class="btn btn-app" onclick="manutencao('getDefensivos',$(this))"><i class="fa fa-bug"></i> Receituario</a></td>
									</tr>
									<tr>
										<td><a class="btn btn-app" onclick="manutencao('getColheita',$(this))"><i class="fa fa-cogs"></i> Colheita</a></td>
										<td><a class="btn btn-app" onclick="manutencao('getChuva',$(this))"><i class="fa fa-umbrella"></i> Chuva</a></td>
									</tr>
								</table>
							</ul>
						</div>
					</div>
				</td>
			</tr>
			<?php }?>
		</tbody>
	</table>
</div>
<!--Fim tabela de áreas -->

<div class="col-md-8">
		<div class="panel-body">
			<div class="box box-solid">
				<div id="mnt_atividades" class="col-md-12 collapse">
				</div>
			</div>
		</div>
</div>

</div>
