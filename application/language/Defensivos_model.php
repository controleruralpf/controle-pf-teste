<?php
class defensivos_model extends CI_Model {
	public function get($id = false) {
		if ($id) {
			$this->db->where ( 'cod_insumo', $id );
		}

		
		$this->db->order_by ( 'cod_insumo', 'desc' );
		//$query = $this->db->get ( 'tbl_insumos' );

		$this->db->select ( 'tbl_insumos.cod_insumo,tbl_insumos.cod_categoria,tbl_categoria.ds_categoria,tbl_insumos.ds_descricao,tbl_insumos.ds_utilizacao,tbl_insumos.ds_observacao,tbl_insumos.ds_faixadefensivo,tbl_insumos.tp_defensivo,tbl_tipodefensivo.ds_tipodefensivo, tbl_insumos.cod_tipodefensivo,tbl_insumos.num_diasduracaodefensivo,tbl_insumos.ds_composicaoformula' );
		$this->db->from('tbl_insumos');
		$this->db->join('tbl_categoria', 'tbl_insumos.cod_categoria = tbl_categoria.cod_categoria','left');
		$this->db->join('tbl_tipodefensivo', 'tbl_insumos.cod_tipodefensivo = tbl_tipodefensivo.cod_tipodefensivo','left');
		$this->db->where ( 'tbl_categoria.cod_categoria', 6 );
		
		$query = $this->db->get();

		
		if ($id) {
			return $query->row_array ();
		}
		if ($query->result_array () != null) {
			
			return $query->result_array ();
		} else {
			
			return array ();
		}
	}
	
	function remove($id) {
		$this->db->where('cod_insumo', $id);
		return $this->db->delete('tbl_insumos');
	}
	
	public function create($data) {
		// $this->output->enable_profiler ( TRUE );
		$this->db->insert ( 'tbl_insumos', $data );
	}
	
	
	public function update($id, $data)
	{
		$this->db->where('cod_insumo', $id);
		$update = $this->db->update('tbl_insumos', $data);
		return $update;
	}
	
	public function getTipoDefensivos() {
		// $this->output->enable_profiler ( TRUE );
		$this->db->order_by ( 'ds_tipodefensivo', 'asc' );
		$query = $this->db->get ( 'tbl_tipodefensivo' );
		return $query->result_array ();
	}
	public function getMaxCodigo() {
		$this->db->select_max ( 'cod_insumo' );
		$query = $this->db->get ( 'tbl_insumos' );
		foreach ( $query->result () as $row ) {
			return  $row->cod_insumo;
		}
	}
}

?>