<?php
class areas_model extends CI_Model {
	

	public function get($id = false) {
		if ($id) {
			$this->db->where ( 'cod_area', $id );
		}

		$this->db->order_by ( 'ds_area', 'asc' );
		//$query = $this->db->get ( 'tbl_insumos' );

		$this->db->select ( 'tbl_areas.cod_area,tbl_areas.ds_area,tbl_areas.num_hectotal,tbl_areas.num_hecplanta,tbl_areas.ds_observacao,tbl_areas.ds_lugar,tbl_areas.sn_areaativa,tbl_areas.sn_areapropria' );
		$this->db->from('tbl_areas');
		$this->db->where ( 'tbl_areas.cod_empresa',  $this->session->userdata('codempresa') );
		
		$query = $this->db->get();

		if ($id) {
			return $query->row_array ();
		}
		if ($query->result_array () != null) {
			
			return $query->result_array ();
		} else {
			
			return array ();
		}
	}

	function remove($id) {
		$this->db->where ( 'cod_area', $id );
		$this->db->where ( 'cod_empresa', $this->session->userdata('codempresa'));
		return $this->db->delete ( 'tbl_areas' );
	}
	public function create($data) {
		$this->db->insert ( 'tbl_areas', $data );
	}
	public function update($id, $data) {
		$this->db->where ( 'cod_fornecedor', $id );
		$this->db->where ( 'cod_area', $this->session->userdata('codempresa'));
		$update = $this->db->update ( 'tbl_areas', $data );
		return $update;
	}
	
	public function getAreas() {
		// $this->output->enable_profiler ( TRUE );
		$this->db->order_by ( 'tbl_area.ds_area', 'asc' );
		$this->db->where ( 'cod_empresa', $this->session->userdata('codempresa'));
		$query = $this->db->get ( 'tbl_area' );
		return $query->result_array ();
	}

}
?>