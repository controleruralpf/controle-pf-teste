<?php
class produtos_model extends CI_Model {
	public function get($id = false) {
		if ($id) {
			$this->db->where ( 'cod_produto', $id );
		}

		$this->db->select ( 'tbl_produto.cod_produto,tbl_produto.ds_produto,tbl_produto.cod_categoriaproduto,tbl_categoriaproduto.ds_categoriaproduto,tbl_categoriaproduto.sn_insumo,tbl_produto.ds_observacao,tbl_produto.cod_empresa' );
		$this->db->from('tbl_produto');
		$this->db->join('tbl_categoriaproduto', 'tbl_categoriaproduto.cod_categoriaproduto = tbl_produto.cod_categoriaproduto','left');
		$this->db->where ( 'tbl_produto.cod_empresa', $this->session->userdata('codempresa'));
		
		$query = $this->db->get();

		
		if ($id) {
			return $query->row_array ();
		}
		if ($query->result_array () != null) {
			
			return $query->result_array ();
		} else {
			
			return array ();
		}
	}
	
	function remove($id) {
		$this->db->where('cod_produto', $id);
		return $this->db->delete('tbl_produto');
	}
	
	public function create($data) {
		// $this->output->enable_profiler ( TRUE );
		$this->db->insert ( 'tbl_produto', $data );
	}
	
	
	public function update($id, $data)
	{
		
		$this->db->where('cod_produto', $id);
		$this->db->where ( 'tbl_produto.cod_empresa', $this->session->userdata('codempresa'));
		$update = $this->db->update('tbl_produto', $data);
		return $update;
	}
	
	public function getCategoriaProdutos() {
		$this->db->order_by ( 'ds_categoriaproduto', 'asc' );
		$this->db->where ( 'tbl_categoriaproduto.cod_empresa', $this->session->userdata('codempresa'));
		$query = $this->db->get ( 'tbl_categoriaproduto' );
		
		return $query->result_array ();
	}
	
	public function getMaxCodigo() {
		$this->db->select_max ( 'cod_insumo' );
		$query = $this->db->get ( 'tbl_insumos' );
		foreach ( $query->result () as $row ) {
			return  $row->cod_insumo;
		}
	}
}

?>