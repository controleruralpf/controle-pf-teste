<?php
class adubos_model extends CI_Model {
	public function get($id = false) {
		if ($id) {
			$this->db->where ( 'cod_insumo', $id );
		}

		
		$this->db->order_by ( 'cod_insumo', 'desc' );
		//$query = $this->db->get ( 'tbl_insumos' );
		
		$this->db->select ( 'tbl_insumos.cod_insumo,tbl_insumos.cod_categoria,tbl_categoria.ds_categoria,tbl_insumos.ds_descricao,tbl_insumos.ds_observacao,tbl_insumos.ds_tipoadubo,tbl_insumos.ds_composicaoformula' );
		$this->db->from('tbl_insumos');
		$this->db->join('tbl_categoria', 'tbl_insumos.cod_categoria = tbl_categoria.cod_categoria','left');
		$this->db->where ( 'tbl_categoria.cod_categoria', 4 );
		$this->db->where ( 'tbl_insumos.cod_empresa',  $this->session->userdata('codempresa') );

		$query = $this->db->get();

		
		if ($id) {
			return $query->row_array ();
		}
		if ($query->result_array () != null) {
			
			return $query->result_array ();
		} else {
			
			return array ();
		}
	}
	
	function remove($id) {
		$this->db->where('cod_insumo', $id);
		$this->db->where ( 'cod_empresa',  $this->session->userdata('codempresa') );
		return $this->db->delete('tbl_insumos');
	}
	
	public function create($data) {
		// $this->output->enable_profiler ( TRUE );
		$this->db->insert ( 'tbl_insumos', $data );
	}
	
	
	public function update($id, $data)
	{
		$this->db->where('cod_insumo', $id);
		$this->db->where ( 'cod_empresa',  $this->session->userdata('codempresa') );
		$update = $this->db->update('tbl_insumos', $data);
		return $update;
	}
	
	public function getMaxCodigo() {
		$this->db->select_max ( 'cod_insumo' );
		$query = $this->db->get ( 'tbl_insumos' );
		foreach ( $query->result () as $row ) {
			return  $row->cod_insumo;
		}
	}
}

?>