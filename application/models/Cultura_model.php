<?php
class cultura_model extends CI_Model {
	public function get($id = false) {
		if ($id) {
			$this->db->where ( 'cod_cultura', $id );
		}
		
		$this->db->order_by ( 'cod_cultura', 'asc' );
		$this->db->where ( 'cod_empresa', $this->session->userdata('codempresa'));
		$this->db->select ( 'tbl_cultura.cod_cultura,tbl_cultura.ds_cultura' );
		$this->db->from ( 'tbl_cultura' );
		
		$query = $this->db->get ();
		
		if ($id) {
			return $query->row_array ();
		}
		
		if ($query->result_array () != null) {
			
			return $query->result_array ();
		} else {
			
			return array ();
		}
	}
	function remove($id) {
		$this->db->where ( 'cod_cultura', $id );
		$this->db->where ( 'cod_empresa', $this->session->userdata('codempresa'));
		
		return $this->db->delete ( 'tbl_cultura' );
	}
	public function create($data) {
		$this->db->insert ( 'tbl_cultura', $data );
	}
	public function update($id, $data) {
		$this->db->where ( 'cod_cultura', $id );
		$this->db->where ( 'cod_empresa', $this->session->userdata('codempresa'));
		$update = $this->db->update ( 'tbl_cultura', $data );
		return $update;
	}

	public function getCultura() {
		// $this->output->enable_profiler ( TRUE );
		$this->db->order_by ( 'ds_cultura', 'asc' );
		$this->db->where ( 'cod_empresa', $this->session->userdata('codempresa'));
		$query = $this->db->get ( 'tbl_cultura' );
		return $query->result_array ();
	}
}

?>