<?php
class contas_model extends CI_Model {
	public function get($id = false) {
		if ($id) {
			$this->db->where ( 'cod_conta', $id );
		}

		$this->db->select ( 'tbl_contas.cod_conta,tbl_contas.ds_conta,tbl_contas.ds_tipo,tbl_contas.cod_grupoconta,tbl_grupocontas.ds_grupoconta,tbl_contas.ds_observacao,tbl_contas.cod_empresa' );
		$this->db->from('tbl_contas');
		$this->db->join('tbl_grupocontas', 'tbl_contas.cod_grupoconta = tbl_grupocontas.cod_grupoconta','left');
		$this->db->where ( 'cod_empresa', $this->session->userdata('codempresa'));
		
		$query = $this->db->get();

		
		if ($id) {
			return $query->row_array ();
		}
		if ($query->result_array () != null) {
			
			return $query->result_array ();
		} else {
			
			return array ();
		}
	}
	
	function remove($id) {
		$this->db->where('cod_conta', $id);
		return $this->db->delete('tbl_contas');
	}
	
	public function create($data) {
		// $this->output->enable_profiler ( TRUE );
		$this->db->insert ( 'tbl_contas', $data );
	}
	
	
	public function update($id, $data)
	{
		$this->db->where('cod_conta', $id);
		$update = $this->db->update('tbl_contas', $data);
		return $update;
	}
	
	public function getGrupoContas() {
		$this->db->order_by ( 'ds_grupoconta', 'asc' );
		$query = $this->db->get ( 'tbl_grupocontas' );
		return $query->result_array ();
	}
	
	public function getMaxCodigo() {
		$this->db->select_max ( 'cod_insumo' );
		$query = $this->db->get ( 'tbl_insumos' );
		foreach ( $query->result () as $row ) {
			return  $row->cod_insumo;
		}
	}
}

?>