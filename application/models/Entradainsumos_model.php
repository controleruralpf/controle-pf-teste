<?php
class entradainsumos_model extends CI_Model {
	
	public function getInsumos($id = false) {
		
		if ($id) {
			$this->db->where ( 'cod_insumo', $id );
		}
	
		$this->db->order_by ( 'cod_insumo', 'desc' );
		// $query = $this->db->get ( 'tbl_defensivo' );
	
		$this->db->select ( 'tbl_insumos.cod_insumo,tbl_insumos.cod_categoria,tbl_categoria.ds_categoria,tbl_insumos.ds_descricao,tbl_insumos.ds_utilizacao,tbl_insumos.ds_observacao,tbl_insumos.ds_faixadefensivo,tbl_insumos.tp_defensivo,tbl_tipodefensivo.ds_tipodefensivo, tbl_insumos.cod_tipodefensivo,tbl_insumos.num_diasduracaodefensivo,tbl_insumos.ds_composicaoformula,tbl_insumos.ds_origemsemente,tbl_insumos.cod_cultivar,tbl_insumos.ds_tipoadubo,tbl_cultivar.ds_cultivar' );
		$this->db->from ( 'tbl_insumos' );
		$this->db->join('tbl_categoria', 'tbl_insumos.cod_categoria = tbl_categoria.cod_categoria','left');
		$this->db->join('tbl_tipodefensivo', 'tbl_insumos.cod_tipodefensivo = tbl_tipodefensivo.cod_tipodefensivo','left');
		$this->db->join('tbl_cultivar', 'tbl_insumos.cod_cultivar = tbl_cultivar.cod_cultivar AND tbl_tipodefensivo.cod_empresa = ' .$this->session->userdata('codempresa') ,'left');
		$this->db->where ( 'tbl_insumos.cod_empresa',  $this->session->userdata('codempresa') );
		//$this->db->where ( 'tbl_cultivar.cod_empresa',  $this->session->userdata('codempresa') );
		//$this->db->where ( 'tbl_tipodefensivo.cod_empresa',  $this->session->userdata('codempresa') );
		
		$query = $this->db->get ();

		if ($id)  {
			return $query->row_array ();
		}
	
		if ($query->result_array () != null) {
				
			return $query->result_array ();
		} else {
				
			return array ();
		}
	}
	
	
	public function getEntradaInsumosLista($id = false) {
	
		if ($id) {
			$this->db->where ( 'cod_entrada', $id );
		}
	
		$this->db->order_by ( 'cod_entrada', 'desc' );

		$this->db->select('tbl_entradainsumos.cod_entrada,tbl_entradainsumos.cod_insumo, DATE_FORMAT(tbl_entradainsumos.dt_entrada,"%d/%m/%Y") as dt_entrada ,tbl_entradainsumos.qtd_insumo,tbl_entradainsumos.vlr_unitario,tbl_entradainsumos.vlr_total,tbl_entradainsumos.ds_observacao,tbl_entradainsumos.cod_embalagem,tbl_entradainsumos.desc_num_nota,tbl_entradainsumos.cod_fornecedor,tbl_insumos.ds_descricao,tbl_fornecedores.ds_fornecedor,tbl_embalagem.ds_embalagem,tbl_embalagem.num_volume,tbl_undmedida.cod_undmedida,tbl_undmedida.ds_undmedida,tbl_categoria.cod_categoria,tbl_categoria.ds_categoria,(tbl_entradainsumos.vlr_unitario * tbl_entradainsumos.qtd_insumo) totalEntrada,tbl_entradainsumos.cod_area,tbl_areas.ds_area,tbl_entradainsumos.ds_placa,tbl_entradainsumos.ds_motorista' );
		$this->db->from ( 'tbl_entradainsumos' );
		$this->db->join('tbl_insumos', 'tbl_insumos.cod_insumo = tbl_entradainsumos.cod_insumo');
		$this->db->join('tbl_fornecedores', 'tbl_fornecedores.cod_fornecedor = tbl_entradainsumos.cod_fornecedor');
		$this->db->join('tbl_embalagem', 'tbl_embalagem.cod_embalagem = tbl_entradainsumos.cod_embalagem');
		$this->db->join('tbl_undmedida', 'tbl_undmedida.cod_undmedida = tbl_embalagem.cod_undmedida');
		$this->db->join('tbl_categoria', 'tbl_categoria.cod_categoria = tbl_insumos.cod_categoria');
		$this->db->join('tbl_areas', 'tbl_areas.cod_area = tbl_entradainsumos.cod_area','left');
		$this->db->where ( 'tbl_entradainsumos.cod_empresa',  $this->session->userdata('codempresa'),'left' );
		$this->db->where ( 'tbl_fornecedores.cod_empresa',  $this->session->userdata('codempresa'),'left' );
		$this->db->where ( 'tbl_embalagem.cod_empresa',  $this->session->userdata('codempresa'),'left' );
		$this->db->where ( 'tbl_undmedida.cod_empresa',  $this->session->userdata('codempresa'),'left' );
		//$this->db->where ( 'tbl_areas.cod_empresa',  $this->session->userdata('codempresa'),'left' );
	
		$query = $this->db->get ();
	
	
		if ($id) {
			return $query->row_array ();
		}
	
		if ($query->result_array () != null) {
	
			return $query->result_array ();
		} else {
	
			return array ();
		}
	}
	
	

	public function getCategoriaInsumos($id = false) {
		if ($id) {
			$this->db->where ( 'cod_categoria', $id );
		}
		
		$this->db->order_by ( 'ds_categoria', 'asc' );
		// $query = $this->db->get ( 'tbl_defensivo' );
		
		$this->db->select ( 'tbl_categoria.cod_categoria,tbl_categoria.ds_categoria' );
		$this->db->from ( 'tbl_categoria' );
		// $this->db->join('tbl_tipo_defensivo', 'tbl_defensivo.cod_tipo_defensivo = tbl_tipo_defensivo.cod_tipo_defensivo');
		
		$query = $this->db->get ();
		
		if ($id) {
			return $query->row_array ();
		}
		
		if ($query->result_array () != null) {
			
			return $query->result_array ();
		} else {
			
			return array ();
		}
	}
	
		
	public function getEntradaInsumos($id = false) {
	
		if ($id) {
			$this->db->where ( 'cod_insumo', $id );
		}
	
		$this->db->order_by('cod_insumo', 'desc' );
		// $query = $this->db->get ( 'tbl_defensivo' );
	
		$this->db->select ( 'tbl_insumos.cod_insumo,tbl_insumos.cod_categoria,tbl_categoria.ds_categoria,tbl_insumos.ds_descricao,tbl_insumos.ds_utilizacao,tbl_insumos.ds_observacao,tbl_insumos.ds_faixadefensivo,tbl_insumos.tp_defensivo,tbl_tipodefensivo.ds_tipodefensivo, tbl_insumos.cod_tipodefensivo,tbl_insumos.num_diasduracaodefensivo,tbl_insumos.ds_composicaoformula,tbl_insumos.ds_origemsemente,tbl_insumos.cod_cultivar,tbl_insumos.ds_tipoadubo,tbl_cultivar.ds_cultivar' );
		$this->db->from ( 'tbl_insumos' );
		$this->db->join('tbl_categoria', 'tbl_insumos.cod_categoria = tbl_categoria.cod_categoria','left');
		$this->db->join('tbl_tipodefensivo', 'tbl_insumos.cod_tipodefensivo = tbl_tipodefensivo.cod_tipodefensivo','left');
		$this->db->join('tbl_cultivar', 'tbl_insumos.cod_cultivar = tbl_cultivar.cod_cultivar AND tbl_cultivar.cod_empresa = '.$this->session->userdata('codempresa') ,'left');
		$this->db->where ( 'tbl_insumos.cod_empresa',  $this->session->userdata('codempresa') );
		$this->db->where ( 'tbl_tipodefensivo.cod_empresa',  $this->session->userdata('codempresa') );

		
		$query = $this->db->get ();
	
	
		if ($id) {
			return $query->row_array ();
		}
	
		if ($query->result_array () != null) {
	
			return $query->result_array ();
		} else {
	
			return array ();
		}
	}
	
	public function getTotalInsumos($id = false) {
		
 	if ($id) {

 			$this->db->where ( 'tbl_insumos.cod_insumo', $id );
 	}
		
 	$this->db->order_by('tbl_undmedida.ds_undmedida', 'desc' );
	$this->db->select(' tbl_entradainsumos.cod_insumo,tbl_insumos.ds_descricao,sum(tbl_entradainsumos.qtd_insumo * tbl_embalagem.num_volume) qtdTotalVolumeInsumo,tbl_undmedida.ds_undmedida ');
	$this->db->from ( 'tbl_entradainsumos');
	$this->db->join ( 'tbl_insumos','tbl_entradainsumos.cod_insumo = tbl_insumos.cod_insumo');
	$this->db->join ( 'tbl_embalagem', ' tbl_embalagem.cod_embalagem = tbl_entradainsumos.cod_embalagem ');
	$this->db->join ( 'tbl_undmedida',' tbl_undmedida.cod_undmedida = tbl_embalagem.cod_undmedida');
	$this->db->where ( 'tbl_entradainsumos.cod_empresa',  $this->session->userdata('codempresa') );
	$this->db->where ( 'tbl_insumos.cod_empresa',  $this->session->userdata('codempresa') );
	$this->db->where ( 'tbl_embalagem.cod_empresa',  $this->session->userdata('codempresa') );
	$this->db->where ( 'tbl_undmedida.cod_empresa',  $this->session->userdata('codempresa') );
	
	$this->db->group_by(" tbl_entradainsumos.cod_insumo,tbl_undmedida.cod_undmedida ");
	
	//$this->output->enable_profiler(TRUE);
	$query = $this->db->get ();
	
// 	if ($id) {
// 		return $query->row_array ();
// 	}
	
	if ($query->result_array () != null) {
			
		return $query->result_array ();
	} else {
			
		return array ();
	}
	
	}
	
	
	
	
	
	public function create($data) {
		$this->db->insert ( 'tbl_entradainsumos', $data );
	}
	
	public function update($id, $data) {
		$this->db->where ( 'cod_entrada', $id );
		$this->db->where ( 'cod_empresa',  $this->session->userdata('codempresa') );
		$update = $this->db->update ( 'tbl_entradainsumos', $data );
		return $update;
	}
	
	
	function remove($id) {
		$this->db->where ( 'cod_entrada', $id );
		$this->db->where ( 'cod_empresa',  $this->session->userdata('codempresa') );
		return $this->db->delete ( 'tbl_entradainsumos' );
	}
	
}

?>