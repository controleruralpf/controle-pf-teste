<?php
class atividades_model extends CI_Model {
	

	public function get($id = false) {
		if ($id) {
			$this->db->where ( 'cod_area', $id );
		}

	
		//$query = $this->db->get ( 'tbl_insumos' );

		$this->db->select ( 'tbl_areas.cod_area,tbl_areas.ds_area,tbl_areas.num_hectotal,tbl_areas.num_hecplanta,tbl_areas.ds_longitude,tbl_areas.ds_latitude,tbl_areas.ds_observacao,tbl_areas.ds_tiposolo,tbl_areas.ds_lugar,tbl_areas.sn_areaativa,tbl_areas.sn_areapropria' );
		$this->db->from('tbl_areas');
		$this->db->where ( 'tbl_areas.cod_empresa',  $this->session->userdata('codempresa') );
		$this->db->order_by ( 'ds_area', 'asc' );
		
		$query = $this->db->get();

		if ($id) {
			return $query->row_array ();
		}
		if ($query->result_array () != null) {
			
			return $query->result_array ();
		} else {
			
			return array ();
		}
	}
}
?>