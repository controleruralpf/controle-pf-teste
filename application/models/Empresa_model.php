<?php
class empresa_model extends CI_Model {

	public function getEmpresa() {
		// $this->output->enable_profiler ( TRUE );
		$this->db->order_by ( 'ds_nomefantasia', 'asc' );
		$query = $this->db->get ( 'tbl_empresa' );
		return $query->result_array ();
	}
	
}

?>