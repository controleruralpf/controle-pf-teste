<?php
class tipoDefensivo_model extends CI_Model {
	public function get($id = false) {
		if ($id) {
			$this->db->where ( 'cod_tipodefensivo', $id );
		}
		
		$this->db->order_by ( 'cod_tipodefensivo', 'asc' );
		// $query = $this->db->get ( 'tbl_defensivo' );
		
		$this->db->select ( 'tbl_tipodefensivo.cod_tipodefensivo,tbl_tipodefensivo.ds_tipodefensivo' );
		$this->db->from ( 'tbl_tipodefensivo' );
		// $this->db->join('tbl_tipo_defensivo', 'tbl_defensivo.cod_tipo_defensivo = tbl_tipo_defensivo.cod_tipo_defensivo');
		
		$query = $this->db->get ();
		
		if ($id) {
			return $query->row_array ();
		}
		
		if ($query->result_array () != null) {
			
			return $query->result_array ();
		} else {
			
			return array ();
		}
	}
	
}

?>