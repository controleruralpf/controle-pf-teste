<?php
class sementes_model extends CI_Model {
	
	public function getSementes($id = false) {
		
		if ($id) {
			$this->db->where ( 'cod_insumo', $id );
		}
	
		
		
		$this->db->order_by ( 'cod_insumo', 'desc' );
		// $query = $this->db->get ( 'tbl_defensivo' );
		
		$this->db->select ( 'tbl_insumos.cod_insumo,tbl_insumos.ds_descricao,tbl_insumos.ds_observacao,tbl_insumos.ds_origemsemente,tbl_insumos.cod_cultivar,tbl_cultivar.ds_cultivar,tbl_insumos.ds_lote,tbl_insumos.ds_germinacao,tbl_insumos.ds_peneira,tbl_insumos.ds_vigor' );
		$this->db->from ( 'tbl_insumos' );
		//$this->db->join('tbl_categoria', 'tbl_insumos.cod_categoria = tbl_categoria.cod_categoria','left');
		$this->db->join('tbl_cultivar', 'tbl_insumos.cod_cultivar = tbl_cultivar.cod_cultivar','left');
		//3 código da semente
		$this->db->where ( 'tbl_insumos.cod_categoria',3 );
		$this->db->where ( 'tbl_insumos.cod_empresa',$this->session->userdata('codempresa'));
		$this->db->where ( 'tbl_cultivar.cod_empresa',$this->session->userdata('codempresa'));
		
		$query = $this->db->get ();
	
		
		if ($id) {
			return $query->row_array ();
		}
	
		if ($query->result_array () != null) {
				
			return $query->result_array ();
		} else {
				
			return array ();
		}
	}
	
	
	public function get($id = false) {
		if ($id) {
			$this->db->where ( 'cod_cultivar', $id );
		}
	
		$this->db->order_by ( 'cod_cultivar', 'asc' );

		$this->db->select ( 'tbl_cultivar.cod_cultivar,tbl_cultivar.ds_cultivar,tbl_cultura.ds_cultura,tbl_cultivar.cod_cultura,tbl_cultivar.ds_tipo,tbl_cultivar.tp_crescimento,tbl_cultivar.num_diasciclo,tbl_cultivar.ds_anotacao' );
		$this->db->from ( 'tbl_cultivar' );
		$this->db->join('tbl_cultura', 'tbl_cultura.cod_cultura = tbl_cultivar.cod_cultura');
		$this->db->where ( 'tbl_cultivar.cod_empresa',$this->session->userdata('codempresa'));
		$this->db->where ( 'tbl_cultura.cod_empresa',$this->session->userdata('codempresa'));

		
		$query = $this->db->get ();
	
		if ($id) {
			return $query->row_array ();
		}
	
		if ($query->result_array () != null) {
				
			return $query->result_array ();
		} else {
				
			return array ();
		}
	}
	
	public function getCategoriaSementes($id = false) {
		/*if ($id) {
			$this->db->where ( 'cod_categoria', $id );
		}
		
		$this->db->order_by ( 'ds_categoria', 'asc' );
		// $query = $this->db->get ( 'tbl_defensivo' );
		
		$this->db->select ( 'tbl_categoria.cod_categoria,tbl_categoria.ds_categoria' );
		$this->db->from ( 'tbl_categoria' );
		// $this->db->join('tbl_tipo_defensivo', 'tbl_defensivo.cod_tipo_defensivo = tbl_tipo_defensivo.cod_tipo_defensivo');
		
		$query = $this->db->get ();
		
		if ($id) {
			return $query->row_array ();
		}
		
		if ($query->result_array () != null) {
			
			return $query->result_array ();
		} else {
			
			return array ();
		}*/
		return 3;
	}
	
	function remove($id) {
		$this->db->where ( 'cod_insumo', $id );
		$this->db->where ( 'cod_empresa',$this->session->userdata('codempresa'));
		return $this->db->delete ( 'tbl_insumos' );
	}
	public function create($data) {
		$this->db->insert ( 'tbl_insumos', $data );
	}
	public function update($id, $data) {
		$this->db->where ( 'cod_insumo', $id );
		$this->db->where ( 'cod_empresa',$this->session->userdata('codempresa'));
		$update = $this->db->update ( 'tbl_insumos', $data );
		return $update;
	}

	public function getCultivares() {
		// $this->output->enable_profiler ( TRUE );
		$this->db->order_by ( 'ds_cultivar', 'asc' );
		$this->db->where ( 'cod_empresa',$this->session->userdata('codempresa'));
		$query = $this->db->get ( 'tbl_cultivar' );
		return $query->result_array ();
	}
}

?>