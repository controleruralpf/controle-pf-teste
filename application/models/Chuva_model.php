<?php
class chuva_model extends  CI_Model {
	
	
	public function get($id = false) {
		if ($id) {
			$this->db->where ( 'cod_chuva', $id );
		}
		
		$this->db->order_by ( 'cod_chuva', 'asc' );
		
		$this->db->select ('tbl_chuvas.cod_chuva,tbl_chuvas.ds_descricao,tbl_chuvas.sn_todasarea, DATE_FORMAT(tbl_chuvas.dt_ini,"%d/%m/%Y") as dt_ini,DATE_FORMAT(tbl_chuvas.dt_fim,"%d/%m/%Y") as dt_fim,tbl_chuvas.ds_observacao' );
		$this->db->from ( 'tbl_chuvas' );
	   	$query = $this->db->get ();
		
		if ($id) {
			return $query->row_array ();
		}
		
		if ($query->result_array () != null) {
			
			return $query->result_array ();
		} else {
			
			return array ();
		}
	}
	
	function remove($id) {
		$this->db->where ( 'cod_chuva', $id );
		return $this->db->delete ( 'tbl_chuvas' );
	}
	public function create($data) {
		$this->db->insert ( 'tbl_chuvas', $data );
	}
	public function update($id, $data) {
		$this->db->where ( 'cod_chuva', $id );
		$update = $this->db->update ( 'tbl_chuvas', $data );
		return $update;
	}
	
	//Consulta que vem da tela de entradaInsumo
	public function getChuvas() {
		$this->db->order_by ( 'cod_chuva', 'asc' );																			
	
		$this->db->select ('tbl_chuvas.cod_chuva,tbl_chuvas.ds_descricao,tbl_chuvas.sn_todasarea, DATE_FORMAT(tbl_chuvas.dt_ini,"%d/%m/%Y") as dt_ini,DATE_FORMAT(tbl_chuvas.dt_fim,"%d/%m/%Y") as dt_fim ,tbl_chuvas.ds_observacao' );
		$this->db->from ( 'tbl_chuvas' );
	
		$query = $this->db->get ();
	
		if ($query->result_array () != null) {
	
			return $query->result_array ();
		} else {
	
			return array ();
		}
	}
	
	
	public function getChuvaArea($codchuva = false,$codarea=false) {
		
		
		if ($codchuva) {
			$this->db->where ( 'tbl_chuvaareas.cod_chuva', $codchuva );
		}
		
		if ($codarea) {
			
			$this->db->where ( 'tbl_chuvaareas.cod_area', $codarea );
		}
	
		$this->db->order_by ( 'cod_chuva', 'asc' );
	
		$this->db->select ('tbl_chuvaareas.cod_area,tbl_chuvaareas.cod_chuva,tbl_chuvaareas.cod_chuvaarea,tbl_chuvaareas.num_mmchuvatotal' );
		$this->db->from ( 'tbl_chuvaareas' );
		$this->db->from ( 'tbl_areas','tbl_areas.cod_area = tbl_chuvaareas.cod_area','left' );
		$query = $this->db->get ();

		
		if ($query->result_array () != null) {
						
			return $query->result_array ();
		} else {
				
			return array ();
		}
	}
	
	public function createChuvaArea($data) {
		$this->db->insert ( 'tbl_chuvaareas', $data );
	}
	
	public function updateChuvaArea($codchuva,$codarea, $data) {
		$this->db->where ( 'cod_chuva', $codchuva );
		$this->db->where ( 'cod_area', $codarea );
		$update = $this->db->update ( 'tbl_chuvaareas', $data );
		return $update;
	}
	
	
}

?>