<?php
class cultivar_model extends CI_Model {
	public function get($id = false) {
		if ($id) {
			$this->db->where ( 'cod_cultivar', $id );
		}
		
		$this->db->order_by ( 'cod_cultivar', 'asc' );
	
		$this->db->select ( 'tbl_cultivar.cod_cultivar,tbl_cultivar.ds_cultivar,tbl_cultura.ds_cultura,tbl_cultivar.cod_cultura,tbl_cultivar.ds_tipo,tbl_cultivar.tp_crescimento,tbl_cultivar.num_diasciclo,tbl_cultivar.ds_anotacao' );
		$this->db->from ( 'tbl_cultivar' );
	 	$this->db->join('tbl_cultura', 'tbl_cultura.cod_cultura = tbl_cultivar.cod_cultura');
	 	$this->db->where ( 'tbl_cultivar.cod_empresa', $this->session->userdata('codempresa'));
	 	
	   	$query = $this->db->get ();
		
		if ($id) {
			return $query->row_array ();
		}
		
		if ($query->result_array () != null) {
			
			return $query->result_array ();
		} else {
			
			return array ();
		}
	}
	function remove($id) {
		$this->db->where ( 'cod_cultivar', $id );
		$this->db->where ( 'cod_empresa', $this->session->userdata('codempresa'));
		return $this->db->delete ( 'tbl_cultivar' );
	}
	public function create($data) {
		$this->db->insert ( 'tbl_cultivar', $data );
	}
	public function update($id, $data) {
		$this->db->where ( 'cod_cultivar', $id );
		$this->db->where ( 'cod_empresa', $this->session->userdata('codempresa'));
		$update = $this->db->update ( 'tbl_cultivar', $data );
		return $update;
	}
	
}

?>