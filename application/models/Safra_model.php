<?php
class safra_model extends CI_Model {

	public function get($id = false) {
		if ($id) {
			$this->db->where ( 'cod_safra', $id );
		}
	
		$this->db->order_by ( 'cod_safra', 'asc' );
		// $query = $this->db->get ( 'tbl_defensivo' );
	
		$this->db->select ( 'tbl_safra.cod_safra,tbl_safra.ds_safra,tbl_safra.dt_inicio,tbl_safra.dt_fim' );
		$this->db->from ( 'tbl_safra' );
		$this->db->where ( 'tbl_safra.cod_safra', $this->session->userdata('codempresa'));
		
		$query = $this->db->get ();
	
		if ($id) {
			return $query->row_array ();
		}
	
		if ($query->result_array () != null) {
				
			return $query->result_array ();
		} else {
				
			return array ();
		}
	}
	function remove($id) {
		$this->db->where ( 'cod_safra', $id );
		$this->db->where ( 'cod_safra', $this->session->userdata('codempresa'));
		return $this->db->delete ( 'tbl_safra' );
	}
	public function create($data) {
		$this->db->insert ( 'tbl_safra', $data );
	}
	public function update($id, $data) {
		$this->db->where ( 'cod_safra', $id );
		$this->db->where ( 'cod_safra', $this->session->userdata('codempresa'));
		$update = $this->db->update ( 'tbl_safra', $data );
		return $update;
	}
	
	public function getSafras() {
		// $this->output->enable_profiler ( TRUE );
		$this->db->order_by ( 'tbl_safra.cod_safra,tbl_safra.ds_safra,tbl_safra.dt_inicio', 'asc' );
		$this->db->where ( 'cod_safra', $this->session->userdata('codempresa'));
		$query = $this->db->get ( 'tbl_safra' );
		return $query->result_array ();
	}
	
	/*
	public function getFornecedores() {
		$this->db->order_by ( 'ds_fornecedor', 'asc' );
	
		$this->db->select ('tbl_fornecedores.cod_fornecedor,tbl_fornecedores.ds_fornecedor,tbl_fornecedores.ds_fone,tbl_fornecedores.ds_fone2,tbl_fornecedores.ds_endereco,tbl_fornecedores.ds_observacao' );
		$this->db->from ( 'tbl_fornecedores' );
		//$this->db->join ( 'tbl_undmedida', 'tbl_undmedida.cod_undmedida = tbl_embalagem.cod_undmedida' );
		
		$query = $this->db->get ();
		
		if ($query->result_array () != null) {
			
			return $query->result_array ();
		} else {
			
			return array ();
		}
	}*/
}

?>