<?php
class socio_model extends CI_Model {

	public function get($id = false) {
		if ($id) {
			$this->db->where ( 'cod_socio', $id );
		}
	
		$this->db->order_by ( 'cod_socio', 'asc' );
		// $query = $this->db->get ( 'tbl_defensivo' );
	
		$this->db->select ( 'tbl_socio.cod_socio,tbl_socio.ds_socio,tbl_socio.ds_fonesocio' );
		$this->db->from ( 'tbl_socio' );
		$this->db->where ( 'tbl_socio.cod_empresa', $this->session->userdata('codempresa'));
		
		$query = $this->db->get ();
	
		if ($id) {
			return $query->row_array ();
		}
	
		if ($query->result_array () != null) {
				
			return $query->result_array ();
		} else {
				
			return array ();
		}
	}
	function remove($id) {
		$this->db->where ( 'cod_socio', $id );
		$this->db->where ( 'cod_empresa', $this->session->userdata('codempresa'));
		return $this->db->delete ( 'tbl_socio' );
	}
	public function create($data) {
		$this->db->insert ( 'tbl_socio', $data );
	}
	public function update($id, $data) {
		$this->db->where ( 'cod_socio', $id );
		$this->db->where ( 'cod_empresa', $this->session->userdata('codempresa'));
		$update = $this->db->update ( 'tbl_socio', $data );
		return $update;
	}
	
	public function getSocios() {
		// $this->output->enable_profiler ( TRUE );
		$this->db->order_by ( 'tbl_socio.cod_socio,tbl_socio.ds_socio,tbl_socio.ds_fonesocio,tbl_socio.cod_empresa', 'asc' );
		$this->db->where ( 'cod_empresa', $this->session->userdata('codempresa'));
		$query = $this->db->get ( 'tbl_socio' );
		return $query->result_array ();
	}
	
	/*
	public function getFornecedores() {
		$this->db->order_by ( 'ds_fornecedor', 'asc' );
	
		$this->db->select ('tbl_fornecedores.cod_fornecedor,tbl_fornecedores.ds_fornecedor,tbl_fornecedores.ds_fone,tbl_fornecedores.ds_fone2,tbl_fornecedores.ds_endereco,tbl_fornecedores.ds_observacao' );
		$this->db->from ( 'tbl_fornecedores' );
		//$this->db->join ( 'tbl_undmedida', 'tbl_undmedida.cod_undmedida = tbl_embalagem.cod_undmedida' );
		
		$query = $this->db->get ();
		
		if ($query->result_array () != null) {
			
			return $query->result_array ();
		} else {
			
			return array ();
		}
	}*/
}

?>