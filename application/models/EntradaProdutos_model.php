<?php
class entradaprodutos_model extends CI_Model {
	public function get($id = false) {
		if ($id) {
			$this->db->where ( 'cod_entradaproduto', $id );
		}
		$this->db->order_by ( 'cod_entradaproduto', 'asc' );
		
	
		$this->db->select ( 'tbl_cultivar.cod_cultivar,tbl_cultivar.ds_cultivar,tbl_cultura.ds_cultura,tbl_cultivar.cod_cultura,tbl_cultivar.ds_tipo,tbl_cultivar.tp_crescimento,tbl_cultivar.num_diasciclo,tbl_cultivar.ds_anotacao' );
		$this->db->from ( 'tbl_entradaprodutos' );
	 	$this->db->join('tbl_cultura', 'tbl_cultura.cod_cultura = tbl_cultivar.cod_cultura');
	 	
	 	$this->db->where ( 'tbl_cultivar.cod_empresa', $this->session->userdata('codempresa'));
	 	
	   	$query = $this->db->get ();
		
		if ($id) {
			return $query->row_array ();
		}
		
		if ($query->result_array () != null) {
			
			return $query->result_array ();
		} else {
			
			return array ();
		}
	}
	
	/************************************************************************************/
	/*
	public function createEntradaProdutos() {
		$insert = $this->db->insert ( 'tbl_entradaprodutos');
		if($insert)
			return $this->db->insert_id();
			else
				return false;
	}
	*/
	
	/*
	function remove($id) {
		$this->db->where ( 'cod_cultivar', $id );
		$this->db->where ( 'cod_empresa', $this->session->userdata('codempresa'));
		return $this->db->delete ( 'tbl_cultivar' );
	}
	*/
			
	public function createNota_model($data) {
		 $insert = $this->db->insert ( 'tbl_entradaprodutos', $data );
		 $resultado = $this->db->insert_id();
         return $resultado;
	}
	
	public function insereItemNota_model($data) {
		 $insert = $this->db->insert ( 'tbl_itensentrada', $data );
		 $resultado = $this->db->insert_id();
         return $resultado;
	}
	
	public function updateDadosNota_model($id, $data) {
		$this->db->where ( 'cod_entradaproduto', $id );
		$this->db->where ( 'cod_empresa', $this->session->userdata('codempresa'));
		$update = $this->db->update ( 'tbl_entradaprodutos', $data );
		return $update;
	}
	
	public function updateItemNota_model($cod_itementradaproduto,$cod_entradaproduto, $data) {
		$this->db->where ( 'cod_entradaproduto', $cod_entradaproduto );
		$this->db->where ( 'cod_itementradaproduto', $cod_itementradaproduto );
		$this->db->where ( 'cod_empresa', $this->session->userdata('codempresa'));
		$update = $this->db->update ( 'tbl_itensentrada', $data );
		return $update;
	}
	
	public function getItensNota_model($cod_itementradaproduto) {
		
		if($cod_itementradaproduto != ""){
			$this->db->where ( 'cod_itementradaproduto', $cod_itementradaproduto );
		}
		$this->db->select ( 'tbl_itensentrada.cod_itementradaproduto,tbl_itensentrada.cod_itementradaproduto,tbl_itensentrada.cod_produto,tbl_itensentrada.qtd_insumo,tbl_itensentrada.cod_embalagem,tbl_itensentrada.vlr_unitario' );
		$this->db->where ( 'cod_empresa', $this->session->userdata('codempresa'));
		$this->db->from ( 'tbl_itensentrada' );
		$query = $this->db->get ();
		return $query->result_array ();
	}
	
	public function getProdutos_model() {

		$this->db->select ( 'tbl_produto.cod_produto,tbl_produto.ds_produto' );
		$this->db->from ( 'tbl_produto' );
		$query = $this->db->get ();
		return $query->result_array ();
	}
	
}

?>