<?php
class despesas_model extends CI_Model {
	public function get($id = false) {
		if ($id) {
			$this->db->where ( 'tbl_despesa.cod_despesa', $id );
		}

		$this->db->select ( 'tbl_despesa.cod_despesa,tbl_despesa.num_nota,tbl_despesa.ds_despesa,tbl_despesa.cod_safra, tbl_despesa.cod_distribuicao, tbl_despesa.cod_socio,tbl_despesa.cod_fornecedor,tbl_fornecedores.ds_fornecedor,tbl_despesa.ds_observacao,DATE_FORMAT(tbl_despesa.dt_pagamento,"%d/%m/%Y") as dt_pagamento ,DATE_FORMAT(tbl_despesa.dt_nota,"%d/%m/%Y") as dt_nota,DATE_FORMAT(tbl_despesa.dt_uso,"%d/%m/%Y") as dt_uso, tbl_despesa.vlr_total,tbl_despesa.cod_empresa,tbl_despesa.cod_conta,tbl_pagamentodespesa.cod_pagamentodespesa,DATE_FORMAT(tbl_pagamentodespesa.dt_pagamento,"%d/%m/%Y") as dt_PagamentoDespesa,DATE_FORMAT(tbl_pagamentodespesa.dt_vencimento,"%d/%m/%Y") as dt_vencimentoDespesa,tbl_pagamentodespesa.ds_observacao as ds_observacaopagamentodespesa,  tbl_pagamentodespesa.ds_tppagamento as ds_tppagamentoDespesa,tbl_pagamentodespesa.cod_formapagamento as cod_formapagamentoDespesa,tbl_pagamentodespesa.vlr_pago as vlr_totalpagamentodespesa,tbl_pagamentodespesa.num_parcelas as num_parcelas ' );
		$this->db->from('tbl_despesa');
		$this->db->join('tbl_fornecedores', 'tbl_fornecedores.cod_fornecedor = tbl_despesa.cod_fornecedor','left');
		$this->db->join('tbl_pagamentodespesa', 'tbl_pagamentodespesa.cod_despesa = tbl_despesa.cod_despesa','left');
		$this->db->where ( 'tbl_despesa.cod_empresa', $this->session->userdata('codempresa'));

		$query = $this->db->get();

		
		if ($id) {
			return $query->row_array ();
		}
		if ($query->result_array () != null) {
			
			return $query->result_array ();
		} else {
			
			return array ();
		}
	}
	
	function remove($id) {
	
		$this->db->where('cod_despesa', $id);
		return $this->db->delete('tbl_despesa');
	}
	
	public function create($data) {
		// $this->output->enable_profiler ( TRUE );
		$this->db->insert ( 'tbl_despesa', $data );
	}
	
	public function createPagamento($data) {
		// $this->output->enable_profiler ( TRUE );
		$this->db->insert ( 'tbl_pagamentodespesa', $data );
	}
	
	public function createParcelasPagamentoDespesa($data) {
		// $this->output->enable_profiler ( TRUE );
		$this->db->insert ( 'tbl_parcelaspagamentodespesa', $data );
	}
	
	public function updatePagamentoDespesa($id, $data)
	{
		$this->db->where('cod_pagamentodespesa', $id);
		$update = $this->db->update('tbl_pagamentodespesa', $data);
		return $update;
	}
	
	public function updateParcelasPagamentoDespesa($cod_parcelapagamentodespesa,$num_parcela,$cod_despesa,$cod_pagamentodespesa, $data) {
		
		$this->db->where('cod_parcelapagamentodespesa',$cod_parcelapagamentodespesa);
		$this->db->where('num_parcela', $num_parcela);
		$this->db->where('cod_despesa', $cod_despesa);
		$this->db->where('cod_pagamentodespesa', $cod_pagamentodespesa);
		$update = $this->db->update('tbl_parcelaspagamentodespesa', $data);
		return $update;
	}
	
	public function update($id, $data)
	{
		$this->db->where('cod_despesa', $id);
		$update = $this->db->update('tbl_despesa', $data);
		return $update;
	}
	
	
	public function getFornecedores() {
		$this->db->order_by ( 'ds_fornecedor', 'asc' );
		$query = $this->db->get ( 'tbl_fornecedores' );
		return $query->result_array ();
	}
	
	public function getSocios() {
		$this->db->order_by ( 'ds_socio', 'asc' );
		$query = $this->db->get ( 'tbl_socio' );
		return $query->result_array ();
	}
	
	public function getDistribuicao() {
		$this->db->order_by ( 'ds_distribuicao', 'asc' );
		$query = $this->db->get ( 'tbl_distribuicao' );
		return $query->result_array ();
	}
	
	public function getSafra() {
		$this->db->order_by ( 'ds_safra', 'asc' );
		$query = $this->db->get ( 'tbl_safra' );
		return $query->result_array ();
	}
	
	public function getContas() {
		$this->db->order_by ( 'ds_Conta', 'asc' );
		$query = $this->db->get ( 'tbl_contas' );
		return $query->result_array ();
	}
	
	public function getFormaPagamentos() {
		$this->db->order_by ( 'ds_formapagamento', 'asc' );
		$query = $this->db->get ( 'tbl_formapagamento' );
		return $query->result_array ();
	}
	
	public function getMaxCodigo() {
		$this->db->select_max ( 'cod_insumo' );
		$query = $this->db->get ( 'tbl_insumos' );
		foreach ( $query->result () as $row ) {
			return  $row->cod_insumo;
		}
	}
	
	public function getParcelaPagamentoDespesa($id) {
	
		$this->db->select ( 'tbl_parcelaspagamentodespesa.cod_parcelapagamentodespesa,tbl_parcelaspagamentodespesa.cod_despesa,tbl_parcelaspagamentodespesa.num_parcela,tbl_parcelaspagamentodespesa.cod_pagamentodespesa,DATE_FORMAT(tbl_parcelaspagamentodespesa.dt_vencimento,"%d/%m/%Y") as dtVencimentoParcelaDespesa,tbl_parcelaspagamentodespesa.vlr_parcela as vlr_parcelaDespesa,DATE_FORMAT(tbl_parcelaspagamentodespesa.dt_pagamento,"%d/%m/%Y") as dt_pagamentoParcelaDespesa,tbl_parcelaspagamentodespesa.cod_empresa,tbl_parcelaspagamentodespesa.vlr_pago as vlrPagoParcelaDespesa,tbl_parcelaspagamentodespesa.ds_observacao as ds_observacaoParcelaDespesa,tbl_parcelaspagamentodespesa.cod_formapagamento as cod_formapagamentoParcelaDespesa' );
		$this->db->from('tbl_parcelaspagamentodespesa');
		$this->db->where ( 'tbl_parcelaspagamentodespesa.cod_despesa', $id );
		$this->db->where ( 'tbl_parcelaspagamentodespesa.cod_empresa', $this->session->userdata('codempresa'));
	
		$query = $this->db->get();
	
		if ($query->result_array () != null) {

			return $query->result_array ();
		} else {
				
			return array ();
		}
	}
	
	function removePagamentoParcelaDespesa ( $codparcelapagamentodespesa,$coddespesa,$numparcela,$codpagamentodespesa) {
	

		
		$this->db->where('cod_parcelapagamentodespesa', $codparcelapagamentodespesa);
		$this->db->where('cod_despesa', $coddespesa);
		$this->db->where('num_parcela', $numparcela);
		$this->db->where('cod_pagamentodespesa', $codpagamentodespesa);
		return $this->db->delete('tbl_parcelaspagamentodespesa');
	}
	
	
}

?>