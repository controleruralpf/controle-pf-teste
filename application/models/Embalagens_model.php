<?php
class embalagens_model extends  CI_Model {
	
	
	public function get($id = false) {
		if ($id) {
			$this->db->where ( 'cod_embalagem', $id );
		}
		
		$this->db->order_by ( 'cod_embalagem', 'asc' );
		
		$this->db->select ('tbl_embalagem.cod_embalagem,tbl_embalagem.ds_embalagem,tbl_embalagem.num_volume,tbl_embalagem.cod_undmedida,tbl_embalagem.cod_undmedida,tbl_embalagem.ds_observacao,tbl_embalagem.sn_devolucao,tbl_embalagem.sn_lavavel,tbl_embalagem.ds_tipo,tbl_undmedida.ds_undmedida' );
		$this->db->from ( 'tbl_embalagem' );
		$this->db->join ( 'tbl_undmedida', 'tbl_undmedida.cod_undmedida = tbl_embalagem.cod_undmedida' );
		$this->db->where ( 'tbl_embalagem.cod_empresa', $this->session->userdata('codempresa') );
		
	   	$query = $this->db->get ();
		
		if ($id) {
			return $query->row_array ();
		}
		
		if ($query->result_array () != null) {
			
			return $query->result_array ();
		} else {
			
			return array ();
		}
	}
	
	public function getUndMedida($id = false) {
		if ($id) {
			$this->db->where ( 'cod_undmedida', $id );
		}
	
		$this->db->order_by ( 'ds_undmedida', 'asc' );
	
		$this->db->select ('tbl_undmedida.cod_undmedida,tbl_undmedida.ds_undmedida' );
		$this->db->from ( 'tbl_undmedida' );
		$this->db->where ( 'tbl_undmedida.cod_empresa', $this->session->userdata('codempresa') );
		 
		$query = $this->db->get ();
	
		if ($id) {
			return $query->row_array ();
		}
	
		if ($query->result_array () != null) {
				
			return $query->result_array ();
		} else {
				
			return array ();
		}
	}
	
	function remove($id) {
		$this->db->where ( 'cod_embalagem', $id );
		$this->db->where ( 'cod_empresa', $this->session->userdata('codempresa') );
		
		return $this->db->delete ( 'tbl_embalagem' );
	}
	public function create($data) {
		$this->db->insert ( 'tbl_embalagem', $data );
	}
	public function update($id, $data) {
		$this->db->where ( 'cod_embalagem', $id );
		$this->db->where ( 'cod_empresa', $this->session->userdata('codempresa') );		
		$update = $this->db->update ( 'tbl_embalagem', $data );
		return $update;
	}
	
	//Consulta que vem da tela de entradaInsumo
	public function getEmbalagens() {
		$this->db->order_by ( 'ds_undMedida', 'asc' );
	
		$this->db->select ('tbl_embalagem.cod_embalagem,tbl_embalagem.ds_embalagem,tbl_embalagem.num_volume,tbl_embalagem.cod_undmedida,tbl_embalagem.cod_undmedida,tbl_embalagem.ds_observacao,tbl_embalagem.sn_devolucao,tbl_embalagem.sn_lavavel,tbl_embalagem.ds_tipo,tbl_undmedida.ds_undmedida' );
		$this->db->from ( 'tbl_embalagem' );
		$this->db->join ( 'tbl_undmedida', 'tbl_undmedida.cod_undmedida = tbl_embalagem.cod_undmedida' );
		$this->db->where ( 'tbl_embalagem.cod_empresa', $this->session->userdata('codempresa') );
		$this->db->where ( 'tbl_undmedida.cod_empresa',$this->session->userdata('codempresa'));
		
		$query = $this->db->get ();
	
		if ($query->result_array () != null) {
	
			return $query->result_array ();
		} else {
	
			return array ();
		}
	}
	
	public function createundmedida($data) {
		$this->db->insert ( 'tbl_undmedida', $data );
	}
	
	
	public function getUndMedidaJson() {
		$this->db->order_by ( 'cod_undmedida', 'asc' );
		$this->db->where ( 'cod_empresa', $this->session->userdata('codempresa') );
		$query = $this->db->get ( 'tbl_undmedida' );
		
		return $query->result_array ();
	}
	
}

?>