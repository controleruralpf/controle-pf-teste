<?php
class login_model extends CI_Model {
	private $salt = 'r4nd0m';
	public $USER_LEVEL_ADMIN = 1;
	public $USER_LEVEL_PM = 2;
	public $USER_LEVEL_DEV = 3;
	
	public function validate($login, $senha, $empresa) {
		
		//$this->output->enable_profiler ( TRUE );
		
		
		
		$this->db->select ( 'usu.cod_usuario,usu.ds_nomeusuario,usu.ds_login,usu.ds_email,usu.ds_endereco,emp.ds_razaosocial,emp.ds_nomefantasia,emp.cod_empresa' );
		$this->db->from ( 'tbl_usuario usu' );
		$this->db->join ( 'tbl_usuario_empresa usu_emp', 'usu_emp.cod_usuario = usu.cod_usuario' );
		$this->db->join ( 'tbl_empresa emp', 'emp.cod_empresa = usu_emp.cod_empresa' );
		$this->db->where ( 'usu.ds_login', $login );
		$this->db->where ( 'usu.ds_senha', $senha );
		if($empresa != null OR $empresa != '' ){
			$this->db->where ( 'usu_emp.cod_empresa', $empresa );
		}
		$this->db->where ( 'usu.ds_status', 'A' );
		
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			if ($query->num_rows () == 1) {
				return $query->row_array ();
			} else {
				
				return $query->result_array ();
			}
		} else {
			return array ();
		}
	}
	
	public function get($id = false) {
		;
		if ($id)
			$this->db->where ( 'id', $id );
		$this->db->order_by ( 'email', 'asc' );
		$get = $this->db->get ( 'user' );
		if ($id)
			return $get->row_array ();
		if ($get->num_rows > 0)
			return $get->result_array ();
		return array ();
	}
	public function create($data) {
		$data ['password'] = sha1 ( $data ['password'] . $this->salt );
		return $this->db->insert ( 'user', $data );
	}
	
	
	function autenticado() {
		$logado = $this->session->userdata('logado');
	
		if (!isset($logado) || $logado != true) {
			echo 'Voce nao tem permissao para entrar nessa pagina.';
			die();
		}
	}
	
	function getListaEmpresa() {

		$this->load->model ( 'empresa_model' );
		
		$empresa = $this->empresa_model->getEmpresa();
		
		
	    //echo json_encode ( $empresa );
	    return $empresa;
	}
	
	public function createLog($data) {
		$this->db->insert ( 'tbl_logusuario', $data );
	}
	

	/*
	 * public function validate($email, $password)
	 * {
	 * $this->output->enable_profiler(TRUE);
	 * $this->db->where('email', $email)->where('password', sha1($password.$this->salt));
	 *
	 * $get = $this->db->get('user');
	 *
	 *
	 * if($get->num_rows > 0) return $get->row_array();
	 * return array();
	 * }
	 */
}

?>