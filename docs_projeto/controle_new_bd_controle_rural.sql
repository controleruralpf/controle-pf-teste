-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 11-Abr-2016 às 17:27
-- Versão do servidor: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `controle_new_bd_controle_rural`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `maps`
--

CREATE TABLE `maps` (
  `id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `center_lat` double DEFAULT NULL,
  `center_lng` double DEFAULT NULL,
  `zoom` int(11) DEFAULT NULL,
  `typeid` varchar(50) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `map_objects`
--

CREATE TABLE `map_objects` (
  `id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `coords` text,
  `marker_icon` varchar(255) NOT NULL,
  `object_id` int(11) NOT NULL,
  `map_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `objects`
--

CREATE TABLE `objects` (
  `id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_aduboplantio`
--

CREATE TABLE `tbl_aduboplantio` (
  `cod_areaplantio` int(11) NOT NULL,
  `cod_safra` int(11) NOT NULL,
  `cod_plantio` int(11) NOT NULL,
  `cod_insumo` int(11) NOT NULL,
  `num_qtdVolumeHectare` decimal(8,2) DEFAULT NULL COMMENT 'QUantidade usada por hect',
  `cod_undmedida` int(11) NOT NULL,
  `num_hecaplicado` decimal(8,2) DEFAULT NULL COMMENT 'rcar toda a area\r\n)',
  `ds_observacao` varchar(400) DEFAULT NULL,
  `cod_empresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_areaplantio`
--

CREATE TABLE `tbl_areaplantio` (
  `cod_areaplantio` int(11) NOT NULL,
  `cod_safra` int(11) NOT NULL,
  `cod_plantio` int(11) NOT NULL,
  `cod_area` int(11) NOT NULL,
  `dt_inicio` date DEFAULT NULL,
  `dt_fim` date DEFAULT NULL,
  `num_graometro` decimal(8,2) DEFAULT NULL COMMENT 'Quantidade de sementes por metro',
  `cod_sementetratada` int(11) NOT NULL,
  `num_hecplantado` decimal(8,2) DEFAULT NULL COMMENT 'Criar check para colocar se toda a area foi plantada',
  `ds_condicaosolo` varchar(200) DEFAULT NULL COMMENT 'Informa como estava o solo',
  `ds_clima` varchar(400) DEFAULT NULL COMMENT 'Informa como estava o clima durante o plantio',
  `ds_observacao` varchar(2000) DEFAULT NULL,
  `num_espacamento` decimal(8,2) DEFAULT NULL,
  `sn_adubo` char(1) DEFAULT NULL COMMENT 'Informa se foi usado adubo na linha durante o plantio',
  `tp_producao` char(1) DEFAULT NULL COMMENT 'Informa se é cobertura ou colheita ou outros',
  `cod_empresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_areas`
--

CREATE TABLE `tbl_areas` (
  `cod_area` int(11) NOT NULL,
  `ds_area` varchar(40) NOT NULL,
  `num_hectotal` decimal(8,2) DEFAULT NULL COMMENT 'Quantidade total de hectares da area',
  `num_hecplanta` decimal(8,2) DEFAULT NULL COMMENT 'Quantidade de hectares da area q são aptos a plantar',
  `ds_longitude` varchar(2000) DEFAULT NULL,
  `ds_latitude` varchar(2000) DEFAULT NULL,
  `ds_observacao` varchar(200) DEFAULT NULL,
  `ds_tiposolo` varchar(2000) DEFAULT NULL COMMENT '...',
  `ds_lugar` varchar(80) DEFAULT NULL COMMENT 'Lugar onde esta a lavoura',
  `sn_areaativa` char(1) DEFAULT NULL,
  `sn_areapropria` char(1) NOT NULL,
  `cod_empresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_areas`
--

INSERT INTO `tbl_areas` (`cod_area`, `ds_area`, `num_hectotal`, `num_hecplanta`, `ds_longitude`, `ds_latitude`, `ds_observacao`, `ds_tiposolo`, `ds_lugar`, `sn_areaativa`, `sn_areapropria`, `cod_empresa`) VALUES
(4, 'DANTE', '230.00', '228.00', NULL, NULL, '', NULL, ' 5 km da sede', 'S', 'P', 1),
(5, 'Bortolucci', '220.00', '180.00', NULL, NULL, '', NULL, 'Arvoredo', 'S', 'P', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_categoriaproduto`
--

CREATE TABLE `tbl_categoriaproduto` (
  `cod_categoriaproduto` int(11) NOT NULL,
  `ds_categoriaproduto` varchar(40) NOT NULL,
  `sn_insumo` varchar(1) DEFAULT NULL COMMENT 'S->SIM N-> NÃO',
  `cod_empresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_categoriaproduto`
--

INSERT INTO `tbl_categoriaproduto` (`cod_categoriaproduto`, `ds_categoriaproduto`, `sn_insumo`, `cod_empresa`) VALUES
(1, 'Geral', 'S', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_chuvaareas`
--

CREATE TABLE `tbl_chuvaareas` (
  `cod_chuvaarea` int(11) NOT NULL,
  `cod_chuva` int(11) NOT NULL,
  `cod_area` int(11) NOT NULL,
  `dt_chuvainicial` date DEFAULT NULL,
  `dt_chuvafinal` date DEFAULT NULL,
  `num_mmchuvatotal` decimal(3,0) DEFAULT NULL COMMENT 'Armazena o total daquela chuva na area',
  `ck_diatodo` varchar(1) NOT NULL DEFAULT 'S',
  `ck_turnomanha` varchar(1) NOT NULL DEFAULT 'S',
  `num_mmchuvamanha` decimal(3,0) DEFAULT NULL,
  `chk_dsturnotarde` varchar(1) NOT NULL DEFAULT 'S',
  `num_mmchuvatarde` decimal(3,0) DEFAULT NULL,
  `ck_dsturnonoite` varchar(1) NOT NULL DEFAULT 'S',
  `num_mmchuvanoite` decimal(3,0) DEFAULT NULL,
  `ds_observacao` varchar(400) DEFAULT NULL,
  `cod_empresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_chuvas`
--

CREATE TABLE `tbl_chuvas` (
  `cod_chuva` int(11) NOT NULL,
  `sn_todasarea` varchar(1) DEFAULT NULL COMMENT 'Informa se choveu em todas as areas',
  `ds_observacao` varchar(400) DEFAULT NULL,
  `ds_descricao` varchar(40) DEFAULT NULL,
  `dt_ini` date NOT NULL,
  `dt_fim` date DEFAULT NULL,
  `cod_empresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_contas`
--

CREATE TABLE `tbl_contas` (
  `cod_conta` int(11) NOT NULL,
  `ds_conta` varchar(50) NOT NULL,
  `ds_tipo` varchar(1) DEFAULT NULL COMMENT 'D -> despesa R-> Receita',
  `cod_grupoconta` int(11) NOT NULL,
  `ds_observacao` varchar(400) DEFAULT NULL,
  `cod_empresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_cultivar`
--

CREATE TABLE `tbl_cultivar` (
  `cod_cultivar` int(11) NOT NULL,
  `ds_cultivar` varchar(80) NOT NULL,
  `cod_cultura` int(11) NOT NULL,
  `ds_tipo` char(1) DEFAULT NULL COMMENT 'tipo se é transgenica ou convencional',
  `tp_crescimento` char(1) DEFAULT NULL COMMENT 'Tipo de é determinado,indeterminado,semiindeterminado',
  `tp_ciclo` char(1) DEFAULT NULL COMMENT 'precoce,tardio,superprecoce,médio',
  `tp_reslagarta` char(1) DEFAULT NULL COMMENT 'se é ou não resistente a lagartas',
  `num_diasciclo` int(11) DEFAULT NULL,
  `ds_anotacao` varchar(2000) DEFAULT NULL,
  `cod_empresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_cultura`
--

CREATE TABLE `tbl_cultura` (
  `cod_cultura` int(11) NOT NULL,
  `ds_cultura` varchar(80) NOT NULL,
  `cod_empresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_cultura`
--

INSERT INTO `tbl_cultura` (`cod_cultura`, `ds_cultura`, `cod_empresa`) VALUES
(1, 'Soja', 1),
(2, 'Milho', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_defensivosemente`
--

CREATE TABLE `tbl_defensivosemente` (
  `cod_defensivosemente` int(11) NOT NULL,
  `cod_sementetratada` int(11) NOT NULL,
  `num_dosagem` decimal(8,2) DEFAULT NULL COMMENT 'dosagem por kg de semente',
  `cod_empresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_despesa`
--

CREATE TABLE `tbl_despesa` (
  `cod_despesa` int(11) NOT NULL,
  `cod_fornecedor` int(11) NOT NULL,
  `cod_socio` int(11) DEFAULT NULL,
  `ds_despesa` varchar(50) NOT NULL,
  `ds_observacao` varchar(2000) NOT NULL,
  `cod_empresa` int(11) NOT NULL,
  `dt_nota` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_devolucaoembalagem`
--

CREATE TABLE `tbl_devolucaoembalagem` (
  `cod_insumo` int(11) NOT NULL,
  `cod_devolucaoembalagem` int(11) NOT NULL,
  `dt_devolucao` date NOT NULL,
  `ds_placa` varchar(10) DEFAULT NULL,
  `qtd_totalembalagem` double DEFAULT NULL,
  `ds_motorista` varchar(40) DEFAULT NULL,
  `ds_numnota` varchar(50) DEFAULT NULL,
  `ds_observacao` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_distribuicao`
--

CREATE TABLE `tbl_distribuicao` (
  `cod_distribuicao` int(11) NOT NULL,
  `ds_distribuicao` varchar(50) NOT NULL,
  `ds_observacao` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_embalagem`
--

CREATE TABLE `tbl_embalagem` (
  `cod_embalagem` int(11) NOT NULL,
  `ds_embalagem` varchar(80) NOT NULL,
  `cod_undmedida` int(11) NOT NULL,
  `num_volume` decimal(8,0) DEFAULT NULL,
  `ds_observacao` varchar(2000) DEFAULT NULL,
  `sn_devolucao` varchar(1) NOT NULL,
  `cod_empresa` int(11) NOT NULL,
  `sn_lavavel` varchar(1) DEFAULT NULL COMMENT 'Define se a embalagem é ou não lavavel',
  `ds_tipo` varchar(1) DEFAULT NULL COMMENT 'Informa o tipo da embalagem se a mesma é rígida ou felixivel'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_embalagem`
--

INSERT INTO `tbl_embalagem` (`cod_embalagem`, `ds_embalagem`, `cod_undmedida`, `num_volume`, `ds_observacao`, `sn_devolucao`, `cod_empresa`, `sn_lavavel`, `ds_tipo`) VALUES
(1, 'Galão', 4, '20', '', 'S', 1, 'S', 'R'),
(2, 'Saco', 2, '50', '', 'N', 1, 'N', 'F'),
(3, 'Saco', 2, '25', '', 'N', 1, 'N', 'F'),
(4, 'Peça', 5, '0', '', 'N', 1, 'N', 'F');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_empresa`
--

CREATE TABLE `tbl_empresa` (
  `cod_empresa` int(11) NOT NULL,
  `ds_razaosocial` varchar(60) NOT NULL,
  `ds_fone` varchar(20) NOT NULL,
  `ds_nomefantasia` varchar(60) NOT NULL,
  `ds_cnpj` varchar(20) DEFAULT NULL,
  `ds_endereco` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_empresa`
--

INSERT INTO `tbl_empresa` (`cod_empresa`, `ds_razaosocial`, `ds_fone`, `ds_nomefantasia`, `ds_cnpj`, `ds_endereco`) VALUES
(1, '654654654546', '(54) 9123-3114', 'Fazenda Scherer', '7897987989789', 'Passo Fundo'),
(2, '878798798798', '(54) 9123-5698', 'Fazenda Tavares', '21321321321', 'Pontão');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_entradaprodutos`
--

CREATE TABLE `tbl_entradaprodutos` (
  `cod_entradaproduto` int(11) NOT NULL,
  `ds_numeroNota` varchar(100) DEFAULT NULL,
  `vlr_totalnota` decimal(8,2) DEFAULT NULL,
  `dt_entrada` date NOT NULL,
  `dt_nota` date DEFAULT NULL,
  `cod_fornecedor` int(11) NOT NULL,
  `cod_empresa` int(11) NOT NULL,
  `cod_safra` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela que armazena as entradas de produtos. exemplo cada nota fiscal será uma entrada de produto.';

--
-- Extraindo dados da tabela `tbl_entradaprodutos`
--

INSERT INTO `tbl_entradaprodutos` (`cod_entradaproduto`, `ds_numeroNota`, `vlr_totalnota`, `dt_entrada`, `dt_nota`, `cod_fornecedor`, `cod_empresa`, `cod_safra`) VALUES
(20, 'asdasd', '0.00', '0000-00-00', '0000-00-00', 4, 1, 1),
(21, 'asdasd', '0.00', '0000-00-00', '0000-00-00', 2, 1, 1),
(22, 'xasxas', '0.00', '0000-00-00', '0000-00-00', 1, 1, 1),
(23, '2342', '0.00', '0000-00-00', '0000-00-00', 3, 1, 1),
(24, 'dwdw', '0.00', '0000-00-00', '0000-00-00', 2, 1, 1),
(25, 'dwdw', '0.00', '0000-00-00', '0000-00-00', 1, 1, 1),
(26, '654165165', '0.00', '0000-00-00', '0000-00-00', 1, 1, 1),
(27, '3213213', '0.00', '0000-00-00', '0000-00-00', 1, 1, 1),
(28, '2342', '0.00', '0000-00-00', '0000-00-00', 1, 1, 1),
(29, 'asdas', '0.00', '0000-00-00', '0000-00-00', 1, 1, 1),
(30, '3213213', '3000.00', '0000-00-00', '0000-00-00', 3, 1, 1),
(32, '654165165', '0.00', '0000-00-00', '0000-00-00', 2, 1, 1),
(33, 'asdas', '0.00', '0000-00-00', '0000-00-00', 1, 1, 1),
(34, 'asdasd', '300.00', '0000-00-00', '0000-00-00', 2, 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_fornecedores`
--

CREATE TABLE `tbl_fornecedores` (
  `cod_fornecedor` int(11) NOT NULL,
  `ds_fornecedor` varchar(50) NOT NULL,
  `ds_fone` varchar(15) DEFAULT NULL,
  `ds_fone2` varchar(15) DEFAULT NULL,
  `ds_endereco` varchar(80) NOT NULL,
  `ds_observacao` varchar(2000) NOT NULL,
  `ds_contato` varchar(200) DEFAULT NULL,
  `cod_empresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_fornecedores`
--

INSERT INTO `tbl_fornecedores` (`cod_fornecedor`, `ds_fornecedor`, `ds_fone`, `ds_fone2`, `ds_endereco`, `ds_observacao`, `ds_contato`, `cod_empresa`) VALUES
(1, 'Cotrijal', '(54) 6546-5465', '(65) 4654-6546', 'Não-Me-Toque', '', 'Dagoberto', 1),
(2, 'Cotriba', '(54) 6546-5465', '(65) 4654-6545', 'Ibirubá ', '', 'Anderson', 1),
(3, 'Revenda Kuhn do Brasil', '(45) 6465-4654', '(65) 4654-6546', 'Passo Fundo', '', 'Paulo', 1),
(4, 'Lavoro', '(56) 4654-6546', '(65) 4654-6546', 'Passo Fundo', '', 'André', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_grupocontas`
--

CREATE TABLE `tbl_grupocontas` (
  `cod_grupoconta` int(11) NOT NULL,
  `ds_grupoconta` varchar(30) NOT NULL,
  `ds_observacao` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_itensentrada`
--

CREATE TABLE `tbl_itensentrada` (
  `cod_entrada` int(11) NOT NULL,
  `qtd_insumo` decimal(8,2) NOT NULL,
  `vlr_unitario` decimal(8,2) DEFAULT NULL,
  `cod_entradaproduto` int(11) NOT NULL,
  `cod_empresa` int(11) NOT NULL,
  `cod_produto` int(11) NOT NULL,
  `cod_embalagem` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela que armazena os itens de entrada do produto';

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_logusuario`
--

CREATE TABLE `tbl_logusuario` (
  `cod_logusuario` int(11) NOT NULL,
  `cod_usuario` varchar(50) NOT NULL,
  `ds_usuario` varchar(50) NOT NULL,
  `ip` varchar(16) NOT NULL DEFAULT '0',
  `dthr_login` datetime NOT NULL,
  `dthr_logout` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_pagamentoentrada`
--

CREATE TABLE `tbl_pagamentoentrada` (
  `cod_pagamentoentrada` int(11) NOT NULL,
  `vlr_pago` decimal(8,2) NOT NULL,
  `ds_pago` varchar(1) DEFAULT NULL COMMENT 'S se já foi pago e N se ainda não foi pago',
  `dt_pagamento` date DEFAULT NULL,
  `cod_entradaproduto` int(11) NOT NULL,
  `cod_socio` int(11) NOT NULL,
  `cod_empresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_plantio`
--

CREATE TABLE `tbl_plantio` (
  `cod_plantio` int(11) NOT NULL,
  `cod_safra` int(11) NOT NULL,
  `dt_inicio` date NOT NULL,
  `dt_fim` date DEFAULT NULL,
  `ds_observacao` varchar(2000) DEFAULT NULL,
  `cod_empresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_produto`
--

CREATE TABLE `tbl_produto` (
  `cod_produto` int(11) NOT NULL,
  `ds_produto` varchar(80) NOT NULL,
  `ds_lote` varchar(5) DEFAULT NULL,
  `ds_utilizacao` varchar(2000) DEFAULT NULL COMMENT 'modo de usar o produto',
  `ds_observacao` varchar(1000) DEFAULT NULL,
  `ds_origem` varchar(80) DEFAULT NULL COMMENT 'informar de onde veio a semente',
  `cod_categoriaproduto` int(11) NOT NULL,
  `def_ds_faixadefensivo` varchar(20) DEFAULT NULL COMMENT 'do veneno',
  `def_tp_defensivo` varchar(1) DEFAULT NULL COMMENT 'Informa se é um defensivo do tipo Insetcida ou fungicida',
  `def_ds_composicaoformula` varchar(2000) DEFAULT NULL COMMENT 'inserir a formula do adubo/fertilizante',
  `def_num_diasduracaodefensivo` int(11) DEFAULT NULL COMMENT 'Numeros de dias efeito defensivo',
  `cod_tipodefensivo` int(11) DEFAULT NULL,
  `sem_ds_germinacao` varchar(5) DEFAULT NULL,
  `sem_ds_vigor` varchar(5) DEFAULT NULL,
  `sem_ds_vigor_1` varchar(5) DEFAULT NULL,
  `cod_empresa` int(11) NOT NULL,
  `cod_grupoconta` int(11) DEFAULT NULL,
  `cod_cultivar` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_produto`
--

INSERT INTO `tbl_produto` (`cod_produto`, `ds_produto`, `ds_lote`, `ds_utilizacao`, `ds_observacao`, `ds_origem`, `cod_categoriaproduto`, `def_ds_faixadefensivo`, `def_tp_defensivo`, `def_ds_composicaoformula`, `def_num_diasduracaodefensivo`, `cod_tipodefensivo`, `sem_ds_germinacao`, `sem_ds_vigor`, `sem_ds_vigor_1`, `cod_empresa`, `cod_grupoconta`, `cod_cultivar`) VALUES
(1, 'Prego 12x8', NULL, NULL, '', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
(2, 'Parafuso', NULL, NULL, '', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
(3, 'Mangueira para Jardin', NULL, NULL, '', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_safra`
--

CREATE TABLE `tbl_safra` (
  `cod_safra` int(11) NOT NULL,
  `ds_safra` varchar(40) NOT NULL,
  `dt_inicio` date DEFAULT NULL,
  `dt_fim` date DEFAULT NULL,
  `cod_empresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_safra`
--

INSERT INTO `tbl_safra` (`cod_safra`, `ds_safra`, `dt_inicio`, `dt_fim`, `cod_empresa`) VALUES
(1, '2015/2016', '2015-01-01', '2016-01-01', 1),
(2, '2016/2017', '2016-01-01', '2017-01-01', 1),
(3, '2016/2017', '2016-01-01', '2016-01-01', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_sementetratada`
--

CREATE TABLE `tbl_sementetratada` (
  `cod_sementetratada` int(11) NOT NULL,
  `cod_insumo` int(11) NOT NULL,
  `cod_safra` int(11) NOT NULL,
  `cod_embalagem` int(11) NOT NULL,
  `ds_observacao` varchar(400) DEFAULT NULL,
  `dt_tratamento` date NOT NULL,
  `qtd_tratada` decimal(8,2) NOT NULL,
  `cod_empresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_socio`
--

CREATE TABLE `tbl_socio` (
  `cod_socio` int(11) NOT NULL,
  `ds_socio` varchar(40) NOT NULL,
  `ds_cpf` varchar(14) NOT NULL,
  `ds_fonesocio` varchar(18) DEFAULT NULL,
  `percentual` double NOT NULL,
  `cod_empresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_socio`
--

INSERT INTO `tbl_socio` (`cod_socio`, `ds_socio`, `ds_cpf`, `ds_fonesocio`, `percentual`, `cod_empresa`) VALUES
(1, 'Felipe Scherer', '654654654', '(54) 9123-3114', 50, 1),
(2, 'Marcelo', '6546546', '(54) 9123-4565', 50, 1),
(3, 'Antonio Carlos', '6546565', '(54) 9123-4565', 30, 2),
(4, 'Valmir Tavares', '6546546', '(54) 9123-5855', 70, 2),
(5, 'Valmir Tavares', '6546546', '(54) 9123-5855', 5, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_tipodefensivo`
--

CREATE TABLE `tbl_tipodefensivo` (
  `cod_tipodefensivo` int(11) NOT NULL,
  `ds_tipodefensivo` varchar(40) NOT NULL,
  `cod_empresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_tipoembalagem`
--

CREATE TABLE `tbl_tipoembalagem` (
  `cod_tipoembalagem` int(11) NOT NULL,
  `ds_tipoembalagem` varchar(40) NOT NULL,
  `cod_empresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_undmedida`
--

CREATE TABLE `tbl_undmedida` (
  `cod_undmedida` int(11) NOT NULL,
  `ds_undmedida` varchar(25) NOT NULL,
  `cod_empresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_undmedida`
--

INSERT INTO `tbl_undmedida` (`cod_undmedida`, `ds_undmedida`, `cod_empresa`) VALUES
(1, 'Peso', 1),
(2, 'KG', 1),
(3, 'Granel', 1),
(4, 'Litros', 1),
(5, '', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_usuario`
--

CREATE TABLE `tbl_usuario` (
  `cod_usuario` int(11) NOT NULL,
  `ds_nomeusuario` varchar(50) NOT NULL,
  `ds_login` varchar(20) NOT NULL,
  `ds_email` varchar(60) NOT NULL,
  `ds_telefone` varchar(20) DEFAULT NULL,
  `ds_endereco` varchar(60) DEFAULT NULL,
  `ds_senha` varchar(20) NOT NULL,
  `ds_status` varchar(1) NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_usuario`
--

INSERT INTO `tbl_usuario` (`cod_usuario`, `ds_nomeusuario`, `ds_login`, `ds_email`, `ds_telefone`, `ds_endereco`, `ds_senha`, `ds_status`) VALUES
(1, 'Felipe Scherer', 'felipe', 'felipescherer.ads@gmail.com', '(54) 9123 - 3114', 'Passo Fundo', '123', 'A'),
(2, 'Marcelo Tavares', 'marcelo', 'marceloanzolin.mv@gmail.com', '(54) 9123 - 4654', 'Pontão', '123', 'A');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_usuario_empresa`
--

CREATE TABLE `tbl_usuario_empresa` (
  `cod_usuario` int(11) NOT NULL,
  `cod_empresa` int(11) NOT NULL,
  `ds_observacao` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_usuario_empresa`
--

INSERT INTO `tbl_usuario_empresa` (`cod_usuario`, `cod_empresa`, `ds_observacao`) VALUES
(1, 1, 'Acesso para o Felipe'),
(2, 2, 'Acesso para o Marcelo');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `maps`
--
ALTER TABLE `maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `map_objects`
--
ALTER TABLE `map_objects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `objects`
--
ALTER TABLE `objects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_aduboplantio`
--
ALTER TABLE `tbl_aduboplantio`
  ADD PRIMARY KEY (`cod_areaplantio`,`cod_safra`,`cod_plantio`),
  ADD KEY `tbl_undmedida_tbl_aduboplantio_fk` (`cod_undmedida`) USING BTREE,
  ADD KEY `tbl_insumos_tbl_aduboplantio_fk` (`cod_insumo`) USING BTREE,
  ADD KEY `tbl_aduboplantio_tbl_empresa_fk` (`cod_empresa`) USING BTREE;

--
-- Indexes for table `tbl_areaplantio`
--
ALTER TABLE `tbl_areaplantio`
  ADD PRIMARY KEY (`cod_areaplantio`,`cod_safra`,`cod_plantio`),
  ADD KEY `tbl_plantio_tbl_areaplantio_fk` (`cod_plantio`,`cod_safra`) USING BTREE,
  ADD KEY `tbl_areas_tbl_areaplantio_fk` (`cod_area`) USING BTREE,
  ADD KEY `tbl_sementetratada_tbl_areaplantio_fk` (`cod_sementetratada`) USING BTREE,
  ADD KEY `tbl_areaplantio_tbl_empresa_fk` (`cod_empresa`) USING BTREE;

--
-- Indexes for table `tbl_areas`
--
ALTER TABLE `tbl_areas`
  ADD PRIMARY KEY (`cod_area`),
  ADD KEY `tbl_areas_tbl_empresa_fk` (`cod_empresa`) USING BTREE;

--
-- Indexes for table `tbl_categoriaproduto`
--
ALTER TABLE `tbl_categoriaproduto`
  ADD PRIMARY KEY (`cod_categoriaproduto`),
  ADD KEY `tbl_empresa_tbl_categoriaproduto_fk` (`cod_empresa`);

--
-- Indexes for table `tbl_chuvaareas`
--
ALTER TABLE `tbl_chuvaareas`
  ADD PRIMARY KEY (`cod_chuvaarea`),
  ADD KEY `tbl_chuvas_tbl_chuvaareas_fk` (`cod_chuva`) USING BTREE,
  ADD KEY `tbl_areas_tbl_chuvaareas_fk` (`cod_area`) USING BTREE,
  ADD KEY `tbl_chuvaareas_tbl_empresa_fk` (`cod_empresa`) USING BTREE;

--
-- Indexes for table `tbl_chuvas`
--
ALTER TABLE `tbl_chuvas`
  ADD PRIMARY KEY (`cod_chuva`),
  ADD KEY `tbl_chuvas_tbl_empresa_fk` (`cod_empresa`) USING BTREE;

--
-- Indexes for table `tbl_contas`
--
ALTER TABLE `tbl_contas`
  ADD PRIMARY KEY (`cod_conta`);

--
-- Indexes for table `tbl_cultivar`
--
ALTER TABLE `tbl_cultivar`
  ADD PRIMARY KEY (`cod_cultivar`),
  ADD KEY `tbl_cultura_tbl_variedade_fk` (`cod_cultura`) USING BTREE,
  ADD KEY `tbl_cultivar_tbl_empresa_fk` (`cod_empresa`) USING BTREE;

--
-- Indexes for table `tbl_cultura`
--
ALTER TABLE `tbl_cultura`
  ADD PRIMARY KEY (`cod_cultura`),
  ADD KEY `tbl_cultura_tbl_empresa_fk` (`cod_empresa`) USING BTREE;

--
-- Indexes for table `tbl_defensivosemente`
--
ALTER TABLE `tbl_defensivosemente`
  ADD PRIMARY KEY (`cod_defensivosemente`,`cod_sementetratada`),
  ADD KEY `tbl_sementetratada_tbl_defensivosemente_fk` (`cod_sementetratada`) USING BTREE,
  ADD KEY `tbl_defensivosementetbl_empresa_fk` (`cod_empresa`) USING BTREE;

--
-- Indexes for table `tbl_despesa`
--
ALTER TABLE `tbl_despesa`
  ADD PRIMARY KEY (`cod_despesa`);

--
-- Indexes for table `tbl_devolucaoembalagem`
--
ALTER TABLE `tbl_devolucaoembalagem`
  ADD PRIMARY KEY (`cod_insumo`);

--
-- Indexes for table `tbl_distribuicao`
--
ALTER TABLE `tbl_distribuicao`
  ADD PRIMARY KEY (`cod_distribuicao`);

--
-- Indexes for table `tbl_embalagem`
--
ALTER TABLE `tbl_embalagem`
  ADD PRIMARY KEY (`cod_embalagem`),
  ADD KEY `tbl_undmedida_tbl_embalagem_fk` (`cod_undmedida`) USING BTREE,
  ADD KEY `tbl_embalagem_tbl_empresa_fk` (`cod_empresa`) USING BTREE;

--
-- Indexes for table `tbl_empresa`
--
ALTER TABLE `tbl_empresa`
  ADD PRIMARY KEY (`cod_empresa`);

--
-- Indexes for table `tbl_entradaprodutos`
--
ALTER TABLE `tbl_entradaprodutos`
  ADD PRIMARY KEY (`cod_entradaproduto`),
  ADD KEY `tbl_empresa_tbl_entradaprodutos_fk` (`cod_empresa`),
  ADD KEY `tbl_safra_tbl_entradaprodutos_fk` (`cod_safra`),
  ADD KEY `tbl_fornecedores_tbl_entradaprodutos_fk` (`cod_fornecedor`);

--
-- Indexes for table `tbl_fornecedores`
--
ALTER TABLE `tbl_fornecedores`
  ADD PRIMARY KEY (`cod_fornecedor`),
  ADD KEY `tbl_fornecedores_tbl_empresa_fk` (`cod_empresa`) USING BTREE;

--
-- Indexes for table `tbl_grupocontas`
--
ALTER TABLE `tbl_grupocontas`
  ADD PRIMARY KEY (`cod_grupoconta`);

--
-- Indexes for table `tbl_itensentrada`
--
ALTER TABLE `tbl_itensentrada`
  ADD PRIMARY KEY (`cod_entrada`),
  ADD KEY `tbl_empresa_tbl_itensentrada_fk` (`cod_empresa`),
  ADD KEY `tbl_entradaprodutos_tbl_itensentrada_fk` (`cod_entradaproduto`),
  ADD KEY `tbl_embalagem_tbl_itensentrada_fk` (`cod_embalagem`),
  ADD KEY `tbl_produto_tbl_itensentrada_fk` (`cod_produto`);

--
-- Indexes for table `tbl_logusuario`
--
ALTER TABLE `tbl_logusuario`
  ADD PRIMARY KEY (`cod_logusuario`);

--
-- Indexes for table `tbl_pagamentoentrada`
--
ALTER TABLE `tbl_pagamentoentrada`
  ADD PRIMARY KEY (`cod_pagamentoentrada`),
  ADD KEY `tbl_empresa_tbl_pagamentoentrada_fk` (`cod_empresa`),
  ADD KEY `tbl_socio_tbl_pagamentoentrada_fk` (`cod_socio`),
  ADD KEY `tbl_entradaprodutos_tbl_pagamentoentrada_fk` (`cod_entradaproduto`);

--
-- Indexes for table `tbl_plantio`
--
ALTER TABLE `tbl_plantio`
  ADD PRIMARY KEY (`cod_plantio`,`cod_safra`),
  ADD KEY `tbl_safra_tbl_plantio_fk` (`cod_safra`) USING BTREE,
  ADD KEY `tbl_plantio_tbl_empresa_fk` (`cod_empresa`) USING BTREE;

--
-- Indexes for table `tbl_produto`
--
ALTER TABLE `tbl_produto`
  ADD PRIMARY KEY (`cod_produto`),
  ADD KEY `tbl_grupocontas_tbl_produto_fk` (`cod_grupoconta`),
  ADD KEY `tbl_empresa_tbl_produto_fk` (`cod_empresa`),
  ADD KEY `tbl_tipodefensivo_tbl_produto_fk` (`cod_tipodefensivo`),
  ADD KEY `tbl_cultivar_tbl_produto_fk` (`cod_cultivar`),
  ADD KEY `tbl_categoriaproduto_tbl_produto_fk` (`cod_categoriaproduto`);

--
-- Indexes for table `tbl_safra`
--
ALTER TABLE `tbl_safra`
  ADD PRIMARY KEY (`cod_safra`),
  ADD KEY `tbl_safra_tbl_empresa_fk` (`cod_empresa`) USING BTREE;

--
-- Indexes for table `tbl_sementetratada`
--
ALTER TABLE `tbl_sementetratada`
  ADD PRIMARY KEY (`cod_sementetratada`),
  ADD KEY `tbl_safra_tbl_sementetratada_fk` (`cod_safra`) USING BTREE,
  ADD KEY `tbl_sementetratada_tbl_empresa_fk` (`cod_empresa`) USING BTREE;

--
-- Indexes for table `tbl_socio`
--
ALTER TABLE `tbl_socio`
  ADD PRIMARY KEY (`cod_socio`),
  ADD KEY `tbl_empresa_tbl_socio_fk` (`cod_empresa`);

--
-- Indexes for table `tbl_tipodefensivo`
--
ALTER TABLE `tbl_tipodefensivo`
  ADD PRIMARY KEY (`cod_tipodefensivo`),
  ADD KEY `tbl_tipodefensivo_tbl_empresa_fk` (`cod_empresa`) USING BTREE;

--
-- Indexes for table `tbl_tipoembalagem`
--
ALTER TABLE `tbl_tipoembalagem`
  ADD PRIMARY KEY (`cod_tipoembalagem`),
  ADD KEY `tbl_tipoembalagem_tbl_empresa_fk` (`cod_empresa`) USING BTREE;

--
-- Indexes for table `tbl_undmedida`
--
ALTER TABLE `tbl_undmedida`
  ADD PRIMARY KEY (`cod_undmedida`),
  ADD KEY `tbl_undmedida_tbl_empresa_fk` (`cod_empresa`) USING BTREE;

--
-- Indexes for table `tbl_usuario`
--
ALTER TABLE `tbl_usuario`
  ADD PRIMARY KEY (`cod_usuario`);

--
-- Indexes for table `tbl_usuario_empresa`
--
ALTER TABLE `tbl_usuario_empresa`
  ADD PRIMARY KEY (`cod_usuario`,`cod_empresa`),
  ADD KEY `tbl_empresa_tbl_usuario_empresa_fk` (`cod_empresa`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `maps`
--
ALTER TABLE `maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `map_objects`
--
ALTER TABLE `map_objects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `objects`
--
ALTER TABLE `objects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_areas`
--
ALTER TABLE `tbl_areas`
  MODIFY `cod_area` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_categoriaproduto`
--
ALTER TABLE `tbl_categoriaproduto`
  MODIFY `cod_categoriaproduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_chuvaareas`
--
ALTER TABLE `tbl_chuvaareas`
  MODIFY `cod_chuvaarea` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_chuvas`
--
ALTER TABLE `tbl_chuvas`
  MODIFY `cod_chuva` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_contas`
--
ALTER TABLE `tbl_contas`
  MODIFY `cod_conta` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_cultivar`
--
ALTER TABLE `tbl_cultivar`
  MODIFY `cod_cultivar` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_cultura`
--
ALTER TABLE `tbl_cultura`
  MODIFY `cod_cultura` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_defensivosemente`
--
ALTER TABLE `tbl_defensivosemente`
  MODIFY `cod_defensivosemente` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_despesa`
--
ALTER TABLE `tbl_despesa`
  MODIFY `cod_despesa` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_devolucaoembalagem`
--
ALTER TABLE `tbl_devolucaoembalagem`
  MODIFY `cod_insumo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_distribuicao`
--
ALTER TABLE `tbl_distribuicao`
  MODIFY `cod_distribuicao` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_embalagem`
--
ALTER TABLE `tbl_embalagem`
  MODIFY `cod_embalagem` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_empresa`
--
ALTER TABLE `tbl_empresa`
  MODIFY `cod_empresa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_entradaprodutos`
--
ALTER TABLE `tbl_entradaprodutos`
  MODIFY `cod_entradaproduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `tbl_fornecedores`
--
ALTER TABLE `tbl_fornecedores`
  MODIFY `cod_fornecedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_grupocontas`
--
ALTER TABLE `tbl_grupocontas`
  MODIFY `cod_grupoconta` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_itensentrada`
--
ALTER TABLE `tbl_itensentrada`
  MODIFY `cod_entrada` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_logusuario`
--
ALTER TABLE `tbl_logusuario`
  MODIFY `cod_logusuario` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_pagamentoentrada`
--
ALTER TABLE `tbl_pagamentoentrada`
  MODIFY `cod_pagamentoentrada` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_plantio`
--
ALTER TABLE `tbl_plantio`
  MODIFY `cod_plantio` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_produto`
--
ALTER TABLE `tbl_produto`
  MODIFY `cod_produto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_sementetratada`
--
ALTER TABLE `tbl_sementetratada`
  MODIFY `cod_sementetratada` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_socio`
--
ALTER TABLE `tbl_socio`
  MODIFY `cod_socio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_tipodefensivo`
--
ALTER TABLE `tbl_tipodefensivo`
  MODIFY `cod_tipodefensivo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_tipoembalagem`
--
ALTER TABLE `tbl_tipoembalagem`
  MODIFY `cod_tipoembalagem` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_undmedida`
--
ALTER TABLE `tbl_undmedida`
  MODIFY `cod_undmedida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_usuario`
--
ALTER TABLE `tbl_usuario`
  MODIFY `cod_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `tbl_aduboplantio`
--
ALTER TABLE `tbl_aduboplantio`
  ADD CONSTRAINT `tbl_aduboplantio_tbl_empresa_fk` FOREIGN KEY (`cod_empresa`) REFERENCES `tbl_empresa` (`cod_empresa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_areaplantio_tbl_aduboplantio_fk` FOREIGN KEY (`cod_areaplantio`,`cod_safra`,`cod_plantio`) REFERENCES `tbl_areaplantio` (`cod_areaplantio`, `cod_safra`, `cod_plantio`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_undmedida_tbl_aduboplantio_fk` FOREIGN KEY (`cod_undmedida`) REFERENCES `tbl_undmedida` (`cod_undmedida`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tbl_areaplantio`
--
ALTER TABLE `tbl_areaplantio`
  ADD CONSTRAINT `tbl_areaplantio_tbl_empresa_fk` FOREIGN KEY (`cod_empresa`) REFERENCES `tbl_empresa` (`cod_empresa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_areas_tbl_areaplantio_fk` FOREIGN KEY (`cod_area`) REFERENCES `tbl_areas` (`cod_area`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_plantio_tbl_areaplantio_fk` FOREIGN KEY (`cod_plantio`,`cod_safra`) REFERENCES `tbl_plantio` (`cod_plantio`, `cod_safra`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_sementetratada_tbl_areaplantio_fk` FOREIGN KEY (`cod_sementetratada`) REFERENCES `tbl_sementetratada` (`cod_sementetratada`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tbl_areas`
--
ALTER TABLE `tbl_areas`
  ADD CONSTRAINT `tbl_areas_tbl_empresa_fk` FOREIGN KEY (`cod_empresa`) REFERENCES `tbl_empresa` (`cod_empresa`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tbl_categoriaproduto`
--
ALTER TABLE `tbl_categoriaproduto`
  ADD CONSTRAINT `tbl_empresa_tbl_categoriaproduto_fk` FOREIGN KEY (`cod_empresa`) REFERENCES `tbl_empresa` (`cod_empresa`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tbl_chuvaareas`
--
ALTER TABLE `tbl_chuvaareas`
  ADD CONSTRAINT `tbl_areas_tbl_chuvaareas_fk` FOREIGN KEY (`cod_area`) REFERENCES `tbl_areas` (`cod_area`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_chuvaareas_tbl_empresa_fk` FOREIGN KEY (`cod_empresa`) REFERENCES `tbl_empresa` (`cod_empresa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_chuvas_tbl_chuvaareas_fk` FOREIGN KEY (`cod_chuva`) REFERENCES `tbl_chuvas` (`cod_chuva`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tbl_cultivar`
--
ALTER TABLE `tbl_cultivar`
  ADD CONSTRAINT `tbl_cultivar_tbl_empresa_fk` FOREIGN KEY (`cod_empresa`) REFERENCES `tbl_empresa` (`cod_empresa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_cultura_tbl_variedade_fk` FOREIGN KEY (`cod_cultura`) REFERENCES `tbl_cultura` (`cod_cultura`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tbl_cultura`
--
ALTER TABLE `tbl_cultura`
  ADD CONSTRAINT `tbl_cultura_tbl_empresa_fk` FOREIGN KEY (`cod_empresa`) REFERENCES `tbl_empresa` (`cod_empresa`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tbl_defensivosemente`
--
ALTER TABLE `tbl_defensivosemente`
  ADD CONSTRAINT `tbl_defensivosementetbl_empresa_fk` FOREIGN KEY (`cod_empresa`) REFERENCES `tbl_empresa` (`cod_empresa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_sementetratada_tbl_defensivosemente_fk` FOREIGN KEY (`cod_sementetratada`) REFERENCES `tbl_sementetratada` (`cod_sementetratada`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tbl_embalagem`
--
ALTER TABLE `tbl_embalagem`
  ADD CONSTRAINT `tbl_embalagem_tbl_empresa_fk` FOREIGN KEY (`cod_empresa`) REFERENCES `tbl_empresa` (`cod_empresa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_undmedida_tbl_embalagem_fk` FOREIGN KEY (`cod_undmedida`) REFERENCES `tbl_undmedida` (`cod_undmedida`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tbl_entradaprodutos`
--
ALTER TABLE `tbl_entradaprodutos`
  ADD CONSTRAINT `tbl_empresa_tbl_entradaprodutos_fk` FOREIGN KEY (`cod_empresa`) REFERENCES `tbl_empresa` (`cod_empresa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_fornecedores_tbl_entradaprodutos_fk` FOREIGN KEY (`cod_fornecedor`) REFERENCES `tbl_fornecedores` (`cod_fornecedor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_safra_tbl_entradaprodutos_fk` FOREIGN KEY (`cod_safra`) REFERENCES `tbl_safra` (`cod_safra`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tbl_fornecedores`
--
ALTER TABLE `tbl_fornecedores`
  ADD CONSTRAINT `tbl_fornecedores_tbl_empresa_fk` FOREIGN KEY (`cod_empresa`) REFERENCES `tbl_empresa` (`cod_empresa`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tbl_itensentrada`
--
ALTER TABLE `tbl_itensentrada`
  ADD CONSTRAINT `tbl_embalagem_tbl_itensentrada_fk` FOREIGN KEY (`cod_embalagem`) REFERENCES `tbl_embalagem` (`cod_embalagem`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_empresa_tbl_itensentrada_fk` FOREIGN KEY (`cod_empresa`) REFERENCES `tbl_empresa` (`cod_empresa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_entradaprodutos_tbl_itensentrada_fk` FOREIGN KEY (`cod_entradaproduto`) REFERENCES `tbl_entradaprodutos` (`cod_entradaproduto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_produto_tbl_itensentrada_fk` FOREIGN KEY (`cod_produto`) REFERENCES `tbl_produto` (`cod_produto`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tbl_pagamentoentrada`
--
ALTER TABLE `tbl_pagamentoentrada`
  ADD CONSTRAINT `tbl_empresa_tbl_pagamentoentrada_fk` FOREIGN KEY (`cod_empresa`) REFERENCES `tbl_empresa` (`cod_empresa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_entradaprodutos_tbl_pagamentoentrada_fk` FOREIGN KEY (`cod_entradaproduto`) REFERENCES `tbl_entradaprodutos` (`cod_entradaproduto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_socio_tbl_pagamentoentrada_fk` FOREIGN KEY (`cod_socio`) REFERENCES `tbl_socio` (`cod_socio`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tbl_plantio`
--
ALTER TABLE `tbl_plantio`
  ADD CONSTRAINT `tbl_plantio_tbl_empresa_fk` FOREIGN KEY (`cod_empresa`) REFERENCES `tbl_empresa` (`cod_empresa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_safra_tbl_plantio_fk` FOREIGN KEY (`cod_safra`) REFERENCES `tbl_safra` (`cod_safra`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tbl_produto`
--
ALTER TABLE `tbl_produto`
  ADD CONSTRAINT `tbl_categoriaproduto_tbl_produto_fk` FOREIGN KEY (`cod_categoriaproduto`) REFERENCES `tbl_categoriaproduto` (`cod_categoriaproduto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_cultivar_tbl_produto_fk` FOREIGN KEY (`cod_cultivar`) REFERENCES `tbl_cultivar` (`cod_cultivar`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_empresa_tbl_produto_fk` FOREIGN KEY (`cod_empresa`) REFERENCES `tbl_empresa` (`cod_empresa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_grupocontas_tbl_produto_fk` FOREIGN KEY (`cod_grupoconta`) REFERENCES `tbl_grupocontas` (`cod_grupoconta`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_tipodefensivo_tbl_produto_fk` FOREIGN KEY (`cod_tipodefensivo`) REFERENCES `tbl_tipodefensivo` (`cod_tipodefensivo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tbl_safra`
--
ALTER TABLE `tbl_safra`
  ADD CONSTRAINT `tbl_safra_tbl_empresa_fk` FOREIGN KEY (`cod_empresa`) REFERENCES `tbl_empresa` (`cod_empresa`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tbl_sementetratada`
--
ALTER TABLE `tbl_sementetratada`
  ADD CONSTRAINT `tbl_safra_tbl_sementetratada_fk` FOREIGN KEY (`cod_safra`) REFERENCES `tbl_safra` (`cod_safra`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_sementetratada_tbl_empresa_fk` FOREIGN KEY (`cod_empresa`) REFERENCES `tbl_empresa` (`cod_empresa`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tbl_socio`
--
ALTER TABLE `tbl_socio`
  ADD CONSTRAINT `tbl_empresa_tbl_socio_fk` FOREIGN KEY (`cod_empresa`) REFERENCES `tbl_empresa` (`cod_empresa`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tbl_tipodefensivo`
--
ALTER TABLE `tbl_tipodefensivo`
  ADD CONSTRAINT `tbl_tipodefensivo_tbl_empresa_fk` FOREIGN KEY (`cod_empresa`) REFERENCES `tbl_empresa` (`cod_empresa`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tbl_tipoembalagem`
--
ALTER TABLE `tbl_tipoembalagem`
  ADD CONSTRAINT `tbl_tipoembalagem_tbl_empresa_fk` FOREIGN KEY (`cod_empresa`) REFERENCES `tbl_empresa` (`cod_empresa`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tbl_undmedida`
--
ALTER TABLE `tbl_undmedida`
  ADD CONSTRAINT `tbl_undmedida_tbl_empresa_fk` FOREIGN KEY (`cod_empresa`) REFERENCES `tbl_empresa` (`cod_empresa`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tbl_usuario_empresa`
--
ALTER TABLE `tbl_usuario_empresa`
  ADD CONSTRAINT `tbl_empresa_tbl_usuario_empresa_fk` FOREIGN KEY (`cod_empresa`) REFERENCES `tbl_empresa` (`cod_empresa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_usuario_tbl_usuario_empresa_fk` FOREIGN KEY (`cod_usuario`) REFERENCES `tbl_usuario` (`cod_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
